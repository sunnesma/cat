﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Data;
using Microsoft.Office.Interop.Word;
using System.Xml.Xsl;
using System.Xml;
using MTSDKDN;

namespace CentralAccessMacros
{
    class ProTools
    {
        public static void RunSuite()
        {
            List<SuiteFunction> functions = new List<SuiteFunction>
            {
                General.RemoveAllImageAltText,
                ProTools.ClearFormattingInTablesProTools,
                ProTools.RemoveEmptyParagraphsProTools,
                WordDocPCAudio.ConvertOutlineLevelToHeading,
                ProTools.RemoveHyperlinksProTools,
                ProTools.ImageBreakLink,
                ProTools.RemoveAllPageNumbers,
                ProTools.FixEllipsesProTools,
                ProTools.FixBulletsProTools,
                ProTools.OldConvertOMMLToMathType,
            };

            General.RunSuite(functions, "ProTools");
        }

        [SuiteDescription("Clear formatting in tables", optional1: true, optional2: true, optional3: true, optional4: false)]
        public static void ClearFormattingInTablesProTools(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            int currentTable = 0;
            foreach (Word.Table table in doc.Tables)
            {
                if (args.IsCanceled())
                    break;

                currentTable += 1;
                args.UpdatePercentage(currentTable, doc.Tables.Count);

                table.Select();
                Globals.ThisAddIn.Application.Selection.ClearFormatting();
            }

            // Set selection back to beginning
            Word.Range myRange = doc.Content;
            myRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            myRange.Select();
        }

        [SuiteDescription("Remove empty paragraphs", optional1: true, optional2: true, optional3: true, optional4: false)]
        public static void RemoveEmptyParagraphsProTools(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, @"^13{2,}", @"^p", useWildcards: true);
        }

        [SuiteDescription("Break Link to Images", optional1: true, optional2: true, optional3: true, optional4: false)]
        public static void ImageBreakLink(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            int i = 0;
            int count = range.InlineShapes.Count;
            foreach (Word.InlineShape s in range.InlineShapes)
            {

                s.LinkFormat.BreakLink();

            }
            i += 1;
        }

        /// <summary>
        /// Removes all hyperlinks in the document.
        /// </summary>
        [SuiteDescription("Remove hyperlinks", optional1: true, optional2: true, optional3: true, optional4: false)]
        public static void RemoveHyperlinksProTools(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            while (doc.Hyperlinks.Count > 0)
            {
                doc.Hyperlinks[1].Delete();
            }
        }

        [SuiteDescription("Remove all page numbers", optional1: true)]
        public static void RemoveAllPageNumbers(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: Word.WdBuiltinStyle.wdStylePageNumber))
            {
                //r.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                r.Delete();
            }
        }

        [SuiteDescription("Fix Ellipses", optional1: true)]
        public static void FixEllipsesProTools(SuiteFunctionUpdateArgs args)
        {
            General.FixEllipses(args);

        }

        [SuiteDescription("Fix Bullets", optional1: true)]
        public static void FixBulletsProTools(SuiteFunctionUpdateArgs args)
        {
            WordDocPCAudio.FixBullets(new SuiteFunctionUpdateArgs());

        }

        [SuiteDescription("[Old] Convert all OMML to MathType", optional1: true)]
        public static void OldConvertOMMLToMathType(SuiteFunctionUpdateArgs args)
        {
            if (Math.IsMathTypeInstalled)
            {
                Globals.ThisAddIn.Application.Options.ReplaceSelection = true;

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                try
                {
                    //MathTypeSDK mathType = MathTypeSDK.Instance;

                    string dynamicDll = DynamicDLL.getDLL();
                    string MATHTYPE_DLL_DEFAULT = @"C:\Program Files (x86)\MathType\System\MT6.dll";
                    string MATHTYPE_DLL_32 = @"C:\Program Files (x86)\MathType\System\32\MT6.dll";
                    string MATHTYPE_DLL_64 = @"C:\Program Files (x86)\MathType\System\64\MT6.dll";

                    if (dynamicDll.Equals(MATHTYPE_DLL_DEFAULT, StringComparison.Ordinal))
                    {
                        MathTypeSDK_DEFAULT mathType = MathTypeSDK_DEFAULT.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();
                        //mathType.MTXFormSetTranslatorMgn((ushort)MTXFormSetTranslator.mtxfmTRANSL_INC_MTDEFAULT,
                        //    "MathML2 (namespace attr).tdl");

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        int numMaths = range.OMaths.Count;
                        int currentMath = 0;
                        foreach (Word.OMath math in range.OMaths)
                        {
                            if (args.IsCanceled())
                                break;

                            // Show progress of conversion
                            currentMath += 1;
                            args.UpdatePercentage(currentMath, numMaths);

                            XmlReader xml = null;
                            while (xml == null)
                            {
                                try
                                {
                                    xml = XmlReader.Create(new StringReader(math.Range.WordOpenXML));
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            StringWriter mathmlStringWriter = new StringWriter();
                            XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                            // Do actual XSL transformation
                            ommlToMathML.Transform(xml, mathmlWriter);
                            mathmlStringWriter.Close();
                            string mathml = mathmlStringWriter.ToString();

                            Math.MathTypeData mathmlData = new Math.MathTypeData(mathml);

                            // Convert MathML to MathType and place on clipboard
                            MTAPI_DIMS dims = new MTAPI_DIMS();
                            int destLength = 10000;
                            StringBuilder stringDest = new StringBuilder(destLength);

                            int result = mathType.MTXFormEqnMgn(
                                MTXFormEqn.mtxfmLOCAL,
                                MTXFormEqn.mtxfmMTEF,
                                mathmlData.MtefData,
                                mathmlData.MtefDataLength,
                                MTXFormEqn.mtxfmCLIPBOARD,
                                MTXFormEqn.mtxfmTEXT,
                                stringDest,
                                destLength,
                                "",
                                ref dims);

                            if (result != MathTypeReturnValue.mtOK)
                            {
                                Debug.Print("MathType Error: {0}", result);
                            }

                            // Paste it over the math (might need to attempt this several times)
                            bool gotPasted = false;
                            while (!gotPasted)
                            {
                                try
                                {
                                    System.Threading.Thread.Sleep(100);
                                    Word.Range myRange = math.Range;
                                    myRange.Paste();
                                    gotPasted = true;
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            System.Threading.Thread.Sleep(50);
                        }

                        mathType.MTAPIDisconnectMgn();
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                    {
                        MathTypeSDK_32 mathType = MathTypeSDK_32.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();
                        //mathType.MTXFormSetTranslatorMgn((ushort)MTXFormSetTranslator.mtxfmTRANSL_INC_MTDEFAULT,
                        //    "MathML2 (namespace attr).tdl");

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        int numMaths = range.OMaths.Count;
                        int currentMath = 0;
                        foreach (Word.OMath math in range.OMaths)
                        {
                            if (args.IsCanceled())
                                break;

                            // Show progress of conversion
                            currentMath += 1;
                            args.UpdatePercentage(currentMath, numMaths);

                            XmlReader xml = null;
                            while (xml == null)
                            {
                                try
                                {
                                    xml = XmlReader.Create(new StringReader(math.Range.WordOpenXML));
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            StringWriter mathmlStringWriter = new StringWriter();
                            XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                            // Do actual XSL transformation
                            ommlToMathML.Transform(xml, mathmlWriter);
                            mathmlStringWriter.Close();
                            string mathml = mathmlStringWriter.ToString();

                            Math.MathTypeData mathmlData = new Math.MathTypeData(mathml);

                            // Convert MathML to MathType and place on clipboard
                            MTAPI_DIMS dims = new MTAPI_DIMS();
                            int destLength = 10000;
                            StringBuilder stringDest = new StringBuilder(destLength);

                            int result = mathType.MTXFormEqnMgn(
                                MTXFormEqn.mtxfmLOCAL,
                                MTXFormEqn.mtxfmMTEF,
                                mathmlData.MtefData,
                                mathmlData.MtefDataLength,
                                MTXFormEqn.mtxfmCLIPBOARD,
                                MTXFormEqn.mtxfmTEXT,
                                stringDest,
                                destLength,
                                "",
                                ref dims);

                            if (result != MathTypeReturnValue.mtOK)
                            {
                                Debug.Print("MathType Error: {0}", result);
                            }

                            // Paste it over the math (might need to attempt this several times)
                            bool gotPasted = false;
                            while (!gotPasted)
                            {
                                try
                                {
                                    System.Threading.Thread.Sleep(100);
                                    Word.Range myRange = math.Range;
                                    myRange.Paste();
                                    gotPasted = true;
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            System.Threading.Thread.Sleep(50);
                        }

                        mathType.MTAPIDisconnectMgn();
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                    {
                        MathTypeSDK_64 mathType = MathTypeSDK_64.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();
                        //mathType.MTXFormSetTranslatorMgn((ushort)MTXFormSetTranslator.mtxfmTRANSL_INC_MTDEFAULT,
                        //    "MathML2 (namespace attr).tdl");

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        int numMaths = range.OMaths.Count;
                        int currentMath = 0;
                        foreach (Word.OMath math in range.OMaths)
                        {
                            if (args.IsCanceled())
                                break;

                            // Show progress of conversion
                            currentMath += 1;
                            args.UpdatePercentage(currentMath, numMaths);

                            XmlReader xml = null;
                            while (xml == null)
                            {
                                try
                                {
                                    xml = XmlReader.Create(new StringReader(math.Range.WordOpenXML));
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            StringWriter mathmlStringWriter = new StringWriter();
                            XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                            // Do actual XSL transformation
                            ommlToMathML.Transform(xml, mathmlWriter);
                            mathmlStringWriter.Close();
                            string mathml = mathmlStringWriter.ToString();

                            Math.MathTypeData mathmlData = new Math.MathTypeData(mathml);

                            // Convert MathML to MathType and place on clipboard
                            MTAPI_DIMS dims = new MTAPI_DIMS();
                            int destLength = 10000;
                            StringBuilder stringDest = new StringBuilder(destLength);

                            int result = mathType.MTXFormEqnMgn(
                                MTXFormEqn.mtxfmLOCAL,
                                MTXFormEqn.mtxfmMTEF,
                                mathmlData.MtefData,
                                mathmlData.MtefDataLength,
                                MTXFormEqn.mtxfmCLIPBOARD,
                                MTXFormEqn.mtxfmTEXT,
                                stringDest,
                                destLength,
                                "",
                                ref dims);

                            if (result != MathTypeReturnValue.mtOK)
                            {
                                Debug.Print("MathType Error: {0}", result);
                            }

                            // Paste it over the math (might need to attempt this several times)
                            bool gotPasted = false;
                            while (!gotPasted)
                            {
                                try
                                {
                                    System.Threading.Thread.Sleep(100);
                                    Word.Range myRange = math.Range;
                                    myRange.Paste();
                                    gotPasted = true;
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            System.Threading.Thread.Sleep(50);
                        }

                        mathType.MTAPIDisconnectMgn();
                    }
                    else
                    {
                        MathTypeSDK_DEFAULT mathType = MathTypeSDK_DEFAULT.Instance;

                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        int numMaths = range.OMaths.Count;
                        int currentMath = 0;
                        foreach (Word.OMath math in range.OMaths)
                        {
                            if (args.IsCanceled())
                                break;

                            // Show progress of conversion
                            currentMath += 1;
                            args.UpdatePercentage(currentMath, numMaths);

                            XmlReader xml = null;
                            while (xml == null)
                            {
                                try
                                {
                                    xml = XmlReader.Create(new StringReader(math.Range.WordOpenXML));
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            StringWriter mathmlStringWriter = new StringWriter();
                            XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                            // Do actual XSL transformation
                            ommlToMathML.Transform(xml, mathmlWriter);
                            mathmlStringWriter.Close();
                            string mathml = mathmlStringWriter.ToString();

                            Math.MathTypeData mathmlData = new Math.MathTypeData(mathml);

                            // Convert MathML to MathType and place on clipboard
                            MTAPI_DIMS dims = new MTAPI_DIMS();
                            int destLength = 10000;
                            StringBuilder stringDest = new StringBuilder(destLength);

                            int result = mathType.MTXFormEqnMgn(
                                MTXFormEqn.mtxfmLOCAL,
                                MTXFormEqn.mtxfmMTEF,
                                mathmlData.MtefData,
                                mathmlData.MtefDataLength,
                                MTXFormEqn.mtxfmCLIPBOARD,
                                MTXFormEqn.mtxfmTEXT,
                                stringDest,
                                destLength,
                                "",
                                ref dims);

                            if (result != MathTypeReturnValue.mtOK)
                            {
                                Debug.Print("MathType Error: {0}", result);
                            }

                            // Paste it over the math (might need to attempt this several times)
                            bool gotPasted = false;
                            while (!gotPasted)
                            {
                                try
                                {
                                    System.Threading.Thread.Sleep(100);
                                    Word.Range myRange = math.Range;
                                    myRange.Paste();
                                    gotPasted = true;
                                }
                                catch (COMException ex)
                                {
                                    Debug.Print("Application busy. Trying again...");
                                }
                            }

                            System.Threading.Thread.Sleep(50);
                        }

                        mathType.MTAPIDisconnectMgn();
                    }


                }
                catch
                {
                    Debug.Print("MathType sdk is not initialized.Please check user administrator settings");
                }
            }
            else
            {
                MessageBox.Show("1.Check that MathType 6.7 or newer is installed" + "\n" + "2.Ensure you have executable permission(ability to alter files) in the MathType program folder(C:\\Program Files(x86)\\MathType\\System\\MT6.dll).Check with your administrator for more information." + "\n\n" + "If MathType is installed ,then you can run the process manually: Word Ribbon > MathType(tab) >Convert Equations", "Unable to convert OMML to MathType");
                Debug.Print("MathType is not installed!");
            }
        }
    }
}
