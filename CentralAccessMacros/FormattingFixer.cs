﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    public static class FormattingFixerCommands
    {
        public static readonly RoutedUICommand FindNext = new RoutedUICommand("Find Next", "FindNext", typeof(FormattingFixerCommands));
        public static readonly RoutedUICommand Replace = new RoutedUICommand("Replace", "Replace", typeof(FormattingFixerCommands));
        public static readonly RoutedUICommand ReplaceAll = new RoutedUICommand("Replace All", "ReplaceAll", typeof(FormattingFixerCommands));
    }

    class FormattingFixer
    {

        public static UserControl ShowFormattingFixer()
        {
            FormattingFixerForm f = new FormattingFixerForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Formatting Fixer", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
            //return Globals.ThisAddIn.ShowForm(form);
        }

        [CheckerDescription("Correct formatting")]
        public static CheckerResult CheckGoodFormatting()
        {
            Word.Range rng = Globals.ThisAddIn.GetActiveDocument().Content;
            if (Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).Any())
                return CheckerResult.Failed;
            else
                return CheckerResult.Passed;
        }

        /// <summary>
        /// Checks for bad formatting after the current selection. Returns true
        /// if there is.
        /// </summary>
        /// <returns></returns>
        public static bool CheckAfterSelection()
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            rng.SetRange(rng.End, Globals.ThisAddIn.GetActiveDocument().Content.End);
            return Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).Any();
        }

        /// <summary>
        /// Checks for bad formatting before the current selection. Returns true
        /// if there is.
        /// </summary>
        /// <returns></returns>
        public static bool CheckBeforeSelection()
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            rng.SetRange(1, rng.Start);
            return Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).Any();
        }

        private static string BadSpacingRegex
        {
            get
            {
                return "[ ^t]{2,}";
            }
        }
    }
}
