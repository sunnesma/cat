﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Data;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{

    public enum VBABoolean
    {
        True = -1,
        False = 0,
        wdUndefined = Word.WdConstants.wdUndefined
    }    

    #region Suite Functions

    /// <summary>
    /// Use this for wrapping the function.
    /// </summary>
    public delegate void SuiteFunction(SuiteFunctionUpdateArgs args);

    public delegate void SuiteProgressUpdater(int progress);
    public delegate bool SuiteRequestCancel();

    /// <summary>
    /// Given to each suite function so that it can update its progress, if
    /// it so chooses. It also provides a flag that the function should check
    /// if user wants to cancel.
    /// </summary>
    public class SuiteFunctionUpdateArgs
    {
        private SuiteProgressUpdater _updateProgress;
        public SuiteProgressUpdater UpdateProgress
        {
            get
            {
                if (_updateProgress != null)
                    return _updateProgress;
                else
                    return (progress) => { }; // Give an empty function
            }
            set
            {
                _updateProgress = value;
            }
        }

        private SuiteRequestCancel _isCanceled;
        public SuiteRequestCancel IsCanceled
        {
            get
            {
                if (_isCanceled != null)
                    return _isCanceled;
                else
                    return () => { return false; }; // Always return false
            }
            set
            {
                _isCanceled = value;
            }
        }

        public System.Windows.Controls.UserControl OrigControl { get; set; }

        #region Update Helpers

        /// <summary>
        /// Returns a percentage (0-100) of the total content that the range
        /// falls under. 
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public void UpdateRangePercentage(Word.Range range, Word.Range totalRange)
        {
            UpdateProgress((int)((double)range.End / (double)totalRange.End * 100.0));
        }

        /// <summary>
        /// Updates the percentage using the basic fraction of num over total.
        /// </summary>
        /// <param name="num"></param>
        /// <param name="total"></param>
        public void UpdatePercentage(decimal num, decimal total)
        {
            UpdateProgress((int)((double)num / (double)total * 100.0));
        }

        #endregion
    }

    /// <summary>
    /// Use this when one needs to do something before and/or after running
    /// the function. Whatever uses this must run "func" somewhere in its
    /// code path.
    /// </summary>
    /// <param name="func"></param>
    public delegate void SuiteRunner(SuiteFunction func);

    public class SuiteDescription : System.Attribute
    {
        private string description;
        public string Description
        {
            get
            {
                return description;
            }
        }

        private bool optional1;
        private bool optional2;
        private bool optional3;
        private bool optional4;
        
        public bool Optional1
        {
            get
            {
               return optional1;
            }
        }
        public bool Optional2
        {
            get
            {
                return optional2;
            }
        }
        public bool Optional3
        {
            get
            {
                return optional3;
            }
        }
        public bool Optional4
        {
            get
            {
                return optional4;
            }
        }
        public SuiteDescription(string description, bool optional1 = false, bool optional2 = false, bool optional3 = false, bool optional4 = false)
        {
            this.description = description;
            this.optional1 = optional1;
            this.optional2 = optional2;
            this.optional3 = optional3;
            this.optional4 = optional4;
        }
    }

    [ValueConversion(typeof(SuiteFunction), typeof(DelegateCommand))]
    public class SuiteFunctionToCommandConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SuiteFunction d = (SuiteFunction)value;
            return new DelegateCommand(() => d(null));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    #endregion

    #region Checker Functions

    public enum CheckerResult { Passed, Failed, Empty, NotInspected };

    public delegate CheckerResult CheckerFunction();
    public delegate Form CheckerFixerFunctionForm();
    public delegate UserControl CheckerFixerFunctionUserControl();

    public class CheckerDescription : System.Attribute
    {
        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
        }

        private List<Resource> _resources;
        public List<Resource> Resources
        {
            get
            {
                return _resources;
            }
        }

        public class Resource
        {
            public Resource(string text, string url)
            {
                Text = text;
                URL = url;
            }

            public string Text { get; set; }

            public string URL { get; set; }
        }

        public CheckerDescription(string description, string[] resourceTexts = null, string[] resourceURLs = null)
        {
            _description = description;
            _resources = new List<Resource>();

            // Compile the links and texts into resource links
            if (resourceTexts != null && resourceURLs != null)
            {
                if (resourceTexts.Length != resourceURLs.Length)
                    throw new ArgumentException("Resource texts and resource URL arrays must have equal length", "resourceTexts or resourceURLs");

                for (int i = 0; i < resourceTexts.Length; i++)
                {
                    _resources.Add(new Resource(resourceTexts[i], resourceURLs[i]));
                }
            }
            else
            {
                if (resourceTexts != null ^ resourceURLs != null)
                    throw new ArgumentNullException("resourceTexts or resourceURLs", "One of the arrays are null while the other has values");
            }
        }
    }

    public class CheckerItem
    {
        public CheckerFunction Checker { get; set; }
        public CheckerFixerFunctionForm FixerForm { get; set; }
        public CheckerFixerFunctionUserControl FixerUserControl { get; set; }
        private bool boolRefreshResult = false;
        private CheckerResult currentResult = CheckerResult.NotInspected;
        private string currentResultText = "";
        private string strDescriptionPassed = "No description found";
        private string strDescriptionFailed = "No description found";
        private string strDescriptionEmpty = "No description found";
        private string strDescriptionNotInspected = "No description found";
        private string strCheckOnceButtonName = "Check Once";
        private string strRunFunctionButtonName = "Run Function";
        private bool isActionsAvailable = true;
                
        private Dictionary<CheckerResult, string> dictResultText = new Dictionary<CheckerResult, string>{
            { CheckerResult.Passed, "Passed" },
            { CheckerResult.Failed, "Needs Review" },
            { CheckerResult.Empty, "No Instances" },
            { CheckerResult.NotInspected, "Not Inspected" }
        };

        public bool IsActionsAvailable
        {
            get { return isActionsAvailable; }
            set { isActionsAvailable = value; }
        }
        public CheckerResult CurrentResult
        {
            get { return currentResult; }
            set { currentResult = value; }
        }
        public bool RefreshResult
        {
            get { return boolRefreshResult; }
            set { boolRefreshResult = value; }
        }
        public string DescriptionPassed
        {
            get { return strDescriptionPassed; }
            set { strDescriptionPassed = value; }
        }
        public string DescriptionFailed
        {
            get { return strDescriptionFailed; }
            set { strDescriptionFailed = value; }
        }
        public string DescriptionEmpty
        {
            get { return strDescriptionEmpty; }
            set { strDescriptionEmpty = value; }
        }
        public string DescriptionNotInspected
        {
            get { return strDescriptionNotInspected; }
            set { strDescriptionNotInspected = value; }
        }
        public string CheckOnceButtonName
        {
            get { return strCheckOnceButtonName; }
            set { strCheckOnceButtonName = value; }
        }
        public string RunFunctionButtonName
        {
            get { return strRunFunctionButtonName; }
            set { strRunFunctionButtonName = value; }
        }
        public string Label
        {
            get
            {
                // Get the description of the function from the attribute
                IList<object> attributes = Checker.Method.GetCustomAttributes(typeof(CheckerDescription), false);
                string myDescription = "";
                if (attributes.Count > 0)
                {
                    CheckerDescription d = (CheckerDescription)attributes[0];
                    myDescription = d.Description;
                }

                return myDescription;
            }
        }
        public CheckerResult Result
        {
            get
            {
                if (boolRefreshResult)
                {
                    boolRefreshResult = false;
                    currentResult = Checker();
                    return currentResult;
                }
                else
                    return currentResult;
            }
        }
        public string ResultText
        {
            get
            {
                return currentResultText = " - " + (dictResultText.ContainsKey(currentResult) ? dictResultText[currentResult] : "N/A");
            }
        }
    }

    #endregion

    /// <summary>
    /// Set of functions that are generally applicable to any kind of media
    /// production.
    /// </summary>
    class General
    {

        public static bool CollectUserInputData(string title, out int ret)
        {
            Form dc = new Form();
            dc.Text = title;

            dc.HelpButton = dc.MinimizeBox = dc.MaximizeBox = false;
            dc.ShowIcon = dc.ShowInTaskbar = false;
            dc.TopMost = true;

            dc.Height = 100;
            dc.Width = 300;
            dc.MinimumSize = new Size(dc.Width, dc.Height);

            int margin = 5;
            Size size = dc.ClientSize;

            TextBox tb = new TextBox();
            tb.TextAlign = HorizontalAlignment.Right;
            tb.Height = 20;
            tb.Width = size.Width - 2 * margin;
            tb.Location = new Point(margin, margin);
            tb.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            dc.Controls.Add(tb);

            Button ok = new Button();
            ok.Text = "Ok";
            ok.Click += new EventHandler(ok_Click);
            ok.Height = 23;
            ok.Width = 75;
            ok.Location = new Point(size.Width / 2 - ok.Width / 2, size.Height / 2);
            ok.Anchor = AnchorStyles.Bottom;
            dc.Controls.Add(ok);
            dc.AcceptButton = ok;

            dc.ShowDialog();

            return int.TryParse(tb.Text, out ret);
        }

        private static void ok_Click(object sender, EventArgs e)
        {
            Form form = (sender as Control).Parent as Form;
            form.DialogResult = DialogResult.OK;
            form.Close();
        }

        /// <summary>
        /// Runs the functions given in the list. It will provide a user
        /// interface for it, allowing the user to itemize the tasks.
        /// </summary>
        /// <param name="functions"></param>
        public static void RunSuite(List<SuiteFunction> functions, string suiteName)
        {

            //// Ask user to save changes
            //DialogResult result = MessageBox.Show("Want to save?\n\nSaving allows you to revert your changes later.", "Save?", MessageBoxButtons.YesNo);
            //if (result == DialogResult.Yes)
            //{
            //    Globals.ThisAddIn.GetActiveDocument().Save();
            //}

            // Ask user for which functions to run
            if (suiteName.Equals("Prepare Document"))
            {
            SuiteSetupForm myForm = new SuiteSetupForm();
            myForm.SuiteName = suiteName;
            myForm.LoadFunctions(functions);
            Globals.ThisAddIn.ShowForm(myForm);   
            }
            else if (suiteName.Equals("Duxbury Ready"))
            {
                BrailleSuiteSetupForm myForm = new BrailleSuiteSetupForm();
                myForm.LoadFunctions(functions);
                myForm.SuiteName = suiteName;
                Globals.ThisAddIn.ShowForm(myForm);
            }
            else if (suiteName.Equals("DAISY"))
            {
                DaisySuiteSetupForm myForm = new DaisySuiteSetupForm();
                myForm.LoadFunctions(functions);
                myForm.SuiteName = suiteName;
                Globals.ThisAddIn.ShowForm(myForm);
            }

            else if (suiteName.Equals("Audio"))
            {
                AudioSuiteSetupForm myForm = new AudioSuiteSetupForm();
                myForm.LoadFunctions(functions);
                myForm.SuiteName = suiteName;
                Globals.ThisAddIn.ShowForm(myForm);
            }

            else if (suiteName.Equals("ProTools"))
            {
                ProToolsSuiteSetupForm myForm = new ProToolsSuiteSetupForm();
                myForm.LoadFunctions(functions);
                myForm.SuiteName = suiteName;
                Globals.ThisAddIn.ShowForm(myForm);
            }
        }

        /// <summary>
        /// Runs a checker that checks for each of the items and reports on
        /// their results. It will also allow the user to run the fixer for
        /// that item if it doesn't pass.
        /// </summary>
        /// <param name="checkFunctions"></param>
        public static void RunChecker(List<CheckerItem> checkFunctions)
        {
            CheckerForm f = new CheckerForm();
            f.CheckerItems = checkFunctions;
            Globals.ThisAddIn.ShowTaskPane(f, "Checker", customWidth: 410, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
//            Globals.ThisAddIn.ShowForm(f);
        }

        /// <summary>
        /// Converts all occurrences of "(a)" to "a."
        /// </summary>
        [SuiteDescription("Convert (a) to a.", optional1: true, optional2: true, optional3: true, optional4: true)]
        public static void ConvertParenLetterToLetterDot(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, @"\([a-zA-Z]{1}\)", useWildcards: true))
            {
                // Do the text transformation
                string myText = r.Text.Trim()[1] + ".";
                r.Text = myText;
            }
        }

        /// <summary>
        /// Converts all occurrences of "(1)" to "1."
        /// </summary>
        [SuiteDescription("Convert (1) to 1.", optional1: true, optional2: true, optional3: true, optional4: true)]
        public static void ConvertParenNumberToNumberDot(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, @"\([0-9]{1,}\)", useWildcards: true))
            {
                // Do the text transformation
                string origText = r.Text.Trim();
                string myText = origText.Substring(1, origText.Length - 2) + ".";
                r.Text = myText;
            }
        }

        /// <summary>
        /// Repairs the paragraph in the range.
        /// </summary>
        /// <param name="range"></param>
        public static void RepairParagraph(Word.Range range)
        {
            Word.Range myRange = Globals.ThisAddIn.Application.Selection.Range;

            if (myRange.Text.Last() == '\u000D')
                myRange.MoveEnd(Word.WdUnits.wdCharacter, -1);

            // Replace paragraphs with spaces
            Globals.ThisAddIn.ReplaceAll(myRange, "^p", " ");

            // Replace redundant spacing with single space
            Globals.ThisAddIn.ReplaceAll(myRange, "[^t ]{2,}", " ", useWildcards: true);

            // Replace dash-space from hyphenating words with nothing
            Globals.ThisAddIn.ReplaceAll(myRange, "- ", "");

            // Replace dash-paragraph mark from hyphenating words with nothing
            Globals.ThisAddIn.ReplaceAll(myRange, "-^013", "", useWildcards: true);
        }

        /// <summary>
        /// Formats the range to the paragraph style given. It will only do the
        /// first paragraph.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="style"></param>
        public static void FormatFirstParagraphStyle(Word.Range range, object style)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            range.Paragraphs.First.set_Style(doc.Styles[style]);
            range.Paragraphs.First.Range.Select();
        }

        public static void FormatParagraphStyle(Word.Range range, object style)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            range.set_Style(doc.Styles[style]);
        }

        /// <summary>
        /// Formats the range to the character style given.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="style"></param>
        public static void FormatCharacterStyle(Word.Range range, Word.Style style)
        {
            range.set_Style(style);
        }

        /// <summary>
        /// Formats the range using the character style, applying it as if it
        /// was a paragraph style. It will only do the first paragraph.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="style"></param>
        public static void FormatCharacterStyleLikeParagraph(Word.Range range, object style, bool selectRange = true)
        {
            // Set the style of the whole range as a page number
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Get the whole first paragraph only
            Word.Range myRange = range.Paragraphs.First.Range;
            myRange.set_Style(doc.Styles[style]);
            
            
            

            if (selectRange)
                myRange.Select();
        }

        /// <summary>
        /// As name states, sets the Page Number style so that it looks like
        /// like the Page Number style for DAISY. They are much easier to
        /// spot this way.
        /// </summary>
        [SuiteDescription("Set page number style like DAISY")]
        public static void SetPageStyleToLookLikeDAISY(SuiteFunctionUpdateArgs args)
        {
            Word.Style pageStyle = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            pageStyle.Font.ColorIndex = Word.WdColorIndex.wdRed;
            pageStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdYellow;
            pageStyle.Font.Size = 14.0f;
            pageStyle.Font.Name = "Consolas";
            pageStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);

            // Create the border
            Word.Border myBorder = pageStyle.Font.Borders[Word.WdBorderType.wdBorderTop];
            myBorder.ColorIndex = Word.WdColorIndex.wdBlack;
            myBorder.LineStyle = Word.WdLineStyle.wdLineStyleSingle;
            myBorder.LineWidth = Word.WdLineWidth.wdLineWidth050pt;
        }

        /// <summary>
        /// Sets the default formatting for a document.
        /// </summary>
        [SuiteDescription("Set default formatting")]
        public static void SetDefaultFormatting(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Set basic paragraph style
            Word.Style normalStyle = doc.Styles[Word.WdBuiltinStyle.wdStyleNormal];

            normalStyle.Font.Name = "Times New Roman";
            normalStyle.Font.Size = 12.0f;
            normalStyle.ParagraphFormat.SpaceAfterAuto = (int)VBABoolean.True;
            normalStyle.ParagraphFormat.SpaceBeforeAuto = (int)VBABoolean.True;
            normalStyle.ParagraphFormat.WidowControl = (int)VBABoolean.False;
            normalStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);

            // Set basic character style
            Word.Style characterStyle = doc.Styles[Word.WdBuiltinStyle.wdStyleDefaultParagraphFont];
            characterStyle.Font.Name = "Times New Roman";
            characterStyle.Font.Size = 12.0f;

            // Set the style of the headings while I am at it
            Word.Style headingStyle = doc.Styles[Word.WdBuiltinStyle.wdStyleHeading1];
            headingStyle.Font.Size = 22.0f;
            headingStyle = doc.Styles[Word.WdBuiltinStyle.wdStyleHeading2];
            headingStyle.Font.Size = 20.0f;
            headingStyle = doc.Styles[Word.WdBuiltinStyle.wdStyleHeading3];
            headingStyle.Font.Size = 16.0f;

            // Set the margins to 1"
            doc.PageSetup.LeftMargin = Globals.ThisAddIn.Application.InchesToPoints(1.0f);
            doc.PageSetup.RightMargin = Globals.ThisAddIn.Application.InchesToPoints(1.0f);
            doc.PageSetup.TopMargin = Globals.ThisAddIn.Application.InchesToPoints(1.0f);
            doc.PageSetup.BottomMargin = Globals.ThisAddIn.Application.InchesToPoints(1.0f);
        }

        /// <summary>
        /// Removes existing formatting in document.
        /// </summary>
        [SuiteDescription("Clear non-default formatting")]
        public static void ClearNonDefaultFormatting(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Clear any and all character formatting applied after style
            doc.Content.Font.Reset();


            // Style the whole document with default character style
            Word.Range range = doc.Content;
            Globals.ThisAddIn.ReplaceAll(range, "", "",
                findStyle: doc.Styles[Word.WdBuiltinStyle.wdStyleDefaultParagraphFont],
                replaceStyle: doc.Styles[Word.WdBuiltinStyle.wdStyleDefaultParagraphFont]);

            // Align everything left
            doc.Content.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

            // Remove background colors of paragraphs
            doc.Content.ParagraphFormat.Shading.BackgroundPatternColor = Word.WdColor.wdColorAutomatic;
        }
        [SuiteDescription("Clean up apostrophes, quotes, and dashes")]
        public static void CleanUpApostrophesQuotesDashes(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Fix the weird apostrophes and quotes that come up
            Globals.ThisAddIn.ReplaceAll(doc.Content, "'", "'");
            Globals.ThisAddIn.ReplaceAll(doc.Content, "\"", "\"");
            Globals.ThisAddIn.ReplaceAll(doc.Content, "\u2010", "-");
        }

        /// <summary>
        /// Removes all hyperlinks in the document.
        /// </summary>
        [SuiteDescription("Remove hyperlinks")]
        public static void RemoveHyperlinks(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            while (doc.Hyperlinks.Count > 0)
            {
                doc.Hyperlinks[1].Delete();
            }
        }

        /// <summary>
        /// Left justifies the entire document.
        /// </summary>
        [SuiteDescription("Left justify all content")]
        public static void LeftJustifyAll(SuiteFunctionUpdateArgs args)
        {
            Word.Range r = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Paragraph p in r.Paragraphs)
            {
                p.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
            }
        }

        [SuiteDescription("Remove empty paragraphs")]
        public static void RemoveEmptyParagraphs(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, @"^13{2,}", @"^p", useWildcards: true);
        }

        [SuiteDescription("Fix common OCR errors")]
        public static void FixCommonOCRErrors(SuiteFunctionUpdateArgs args)
        {
            Dictionary<string, string> ocrErrors = new Dictionary<string, string>
            {
                {"<leam>", "learn"}, // surrounded words in carets so that it treats them as single words
                {"<pattem>", "pattern"},
                {"<1s>", "Is"},
                {"^13n ", "^p"} // OCR replaces bullet points with "n" sometimes
            };

            int currentItem = 0;
            foreach (string k in ocrErrors.Keys)
            {
                if (args.IsCanceled())
                    break;

                currentItem += 1;
                args.UpdatePercentage(currentItem, ocrErrors.Keys.Count);

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Globals.ThisAddIn.ReplaceAll(range, k, ocrErrors[k], useWildcards: true);
            }

            // Also fix when the end double quote rams up next to a word or
            // marker awkwardly
            Word.Range myRange = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(myRange, @"(”)([! \?\!\)\]\}]{1})", @"\1 \2", useWildcards: true);            
        }

        [SuiteDescription("Trim whitespace around paragraphs")]
        public static void TrimWhitespaceFromParagraphs(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, @"^13([ ^t]{1,})", "^p", useWildcards: true);
            Globals.ThisAddIn.ReplaceAll(range, @"([ ^t]{1,})^13", "^p", useWildcards: true);
        }

        [SuiteDescription("Convert tables to text")]
        public static void ConvertTablesToText(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            foreach (Word.Table table in doc.Tables)
            {
                table.Select();

                DialogResult result = MessageBox.Show("Want to convert this table?", "Convert Table?", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    Globals.ThisAddIn.Application.Selection.Rows.ConvertToText(Word.WdTableFieldSeparator.wdSeparateByParagraphs);
                }
                else if (result == DialogResult.Cancel)
                {
                    break;
                }
            }

            MessageBox.Show("Search complete!");
        }

        [SuiteDescription("Clear formatting in tables")]
        public static void ClearFormattingInTables(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            int currentTable = 0;
            foreach (Word.Table table in doc.Tables)
            {
                if (args.IsCanceled())
                    break;

                currentTable += 1;
                args.UpdatePercentage(currentTable, doc.Tables.Count);

                table.Select();
                Globals.ThisAddIn.Application.Selection.ClearFormatting();
            }

            // Set selection back to beginning
            Word.Range myRange = doc.Content;
            myRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            myRange.Select();
        }

        // <summary>
        // Replaces the auto-numbering in a document into straight plain-text.
        // The auto-numbering refers to numbers in an ordered list, not page
        // numbers.
        // </summary>
        [SuiteDescription("Replace auto-numbered lists with plain text", optional1: true, optional2: true, optional3: true, optional4: true)]
        public static void ReplaceAutoNumbering(SuiteFunctionUpdateArgs args)
        {
           // <summary>
            // Former solution
            //Globals.ThisAddIn.GetActiveDocument().Content.ListFormat.ConvertNumbersToText();

            // Revised solution 6/9/2017
            // This solution skips bullets
            // Template source https://stackoverflow.com/questions/27687034/how-to-read-list-item-of-same-list-group
            //</summary>

            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Paragraph firstItem in doc.Content.ListParagraphs.OfType<Word.Paragraph>().Reverse())
            {
                if (firstItem.Range.ListFormat.List != null)
                {
                    foreach (Word.Paragraph item in firstItem.Range.ListFormat.List.ListParagraphs)
                    {
                        if (firstItem.Range.ListFormat.ListType != Word.WdListType.wdListBullet)
                        {
                            item.Range.ListFormat.ConvertNumbersToText();
                        }
                    }
                }
            }
        }

        [SuiteDescription("Remove all alt text")]
        public static void RemoveAllAltText(SuiteFunctionUpdateArgs args)
        {
            DialogResult result = MessageBox.Show("This will REMOVE all alt text. Make a copy before running this function.\n\nDo you still want to run it?", "Remove Alt Text", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                foreach (Word.InlineShape shape in range.InlineShapes)
                {
                    shape.AlternativeText = "";
                }

                MessageBox.Show("Done!");
            }
        }

        [SuiteDescription("Convert all DAISY page numbers to regular", optional1: true, optional2: true, optional3: true, optional4: true)]
        public static void ConvertDAISYNumbersToPageNumbers(SuiteFunctionUpdateArgs args)
        {
            try
            {
                // Check that the DAISY style exists first
                Word.Style daisyStyle = Globals.ThisAddIn.GetActiveDocument().Styles["Page Number (DAISY)"];
                Word.Style regularStyle = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];

                daisyStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                regularStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);

                General.SetPageStyleToLookLikeDAISY(args);

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Globals.ThisAddIn.ReplaceAll(range, "", "", findStyle: daisyStyle, replaceStyle: regularStyle);

                //DialogResult result = MessageBox.Show("This will convert all DAISY page numbers to regular page numbers.\n\nDo you still want to run it?", "Convert DAISY Page Numbers To Regular Numbers", MessageBoxButtons.YesNo);

                //if (result == DialogResult.Yes)
                //{
                //    Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                //    Globals.ThisAddIn.FindReplaceAll(range, "", "", findStyle: daisyStyle, replaceStyle: regularStyle);

                //    MessageBox.Show("Done!");
                //}
            }
            catch (COMException ex)
            {
                //MessageBox.Show("No DAISY page numbers.");
                Debug.Print("No DAISY page numbers...");
            }
        }

        /// <summary>
        /// Removes the Page Number style from currently styled page numbers.
        /// This function does NOT remove the text. 
        /// It either retains some other stlye previously applied on the same text, or resets it to Default Paragraph Font.
        /// </summary>
        public static void RemovePageNumberKeepText()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            object wdStylePageNumber = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: wdStylePageNumber).ToList())
            {
                r.Select();
                Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
            }
        }

        /// <summary>
        /// Selects a possible broken paragraph pair. It will find the next
        /// occurence from the current selection. It will not automatically
        /// fix them in any way.
        /// </summary>
        /// <returns></returns>
        public static bool SelectBrokenParagraphPair()
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            range.SetRange(range.Start, Globals.ThisAddIn.GetActiveDocument().Content.End);

            bool gotOne = false;
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, @"([!(:.\!\?)?]){1}^13", useWildcards: true))
            {

                // Set the range as the paragraphs before and after this term
                int start = r.Paragraphs[1].Range.Start;
                r.SetRange(start, r.End);
                r.MoveEnd(Word.WdUnits.wdParagraph, 1);                

                // Check that the paragraphs in the range are not any of these
                // styles
                List<Word.WdBuiltinStyle> myStyles = new List<Word.WdBuiltinStyle> {
                    Word.WdBuiltinStyle.wdStyleHeading1,
                    Word.WdBuiltinStyle.wdStyleHeading2,
                    Word.WdBuiltinStyle.wdStyleHeading3,
                    Word.WdBuiltinStyle.wdStyleHeading4,
                    Word.WdBuiltinStyle.wdStyleHeading5,
                    Word.WdBuiltinStyle.wdStyleHeading6,
                    Word.WdBuiltinStyle.wdStyleHeading7,
                    Word.WdBuiltinStyle.wdStyleHeading8,
                    Word.WdBuiltinStyle.wdStylePageNumber
                };

                bool isInStyle = false;
                foreach (Word.WdBuiltinStyle s in myStyles)
                {
                    foreach (Word.Paragraph p in r.Paragraphs)
                    {
                        Word.Style myStyle = (Word.Style)p.Range.get_Style();
                        Word.Style otherStyle = Globals.ThisAddIn.GetActiveDocument().Styles[s];

                        if (myStyle.NameLocal.Equals(otherStyle.NameLocal))
                        {
                            isInStyle = true;
                            break;
                        }
                    }

                    if (isInStyle)
                        break;
                }

                if (!isInStyle)
                {
                    r.Select();
                    gotOne = true;
                    break;
                }
            }

            return gotOne;
        }

        [CheckerDescription("Ran spell check")]
        public static CheckerResult CheckSpellCheckWasRan()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            if (doc.SpellingErrors.Count == 0)
                return CheckerResult.Passed;
            else
                return CheckerResult.Failed;
        }

        public static Form RunSpellCheck()
        {
            //Commented the following piece of code which programmatically calls the Spell Checker for MS Word. 
            //It used to randomly quit on its own, and didn't allow random access on the document while the spell checker window was on.
            #region old code
            //Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            //doc.CheckSpelling();
            #endregion

            //New approach summons the Spell Checker of MS Word by using the keyboard shortcut: F7. 
            //In the process, it minimizes the checker window and maximizes the Word document, so that SendKeys function sends the key combination to the right application.
            
            //CheckerForm.ActiveForm.WindowState = FormWindowState.Minimized;
            //Globals.ThisAddIn.Application.WindowState = Word.WdWindowState.wdWindowStateMaximize;
            //Globals.ThisAddIn.Application.Activate();
            Globals.ThisAddIn.Application.ActiveDocument.ActiveWindow.SetFocus();
            SendKeys.Send("{F7}");
            return null;
        }

        [CheckerDescription("Images have alt text")]
        public static CheckerResult CheckImagesHaveAltText()
        {
            return ImageAltTextEditorControl.CheckImagesHaveAltText();
        }

        public static Form ShowImageAltTextEditor()
        {
            ImageAltTextEditor form = new ImageAltTextEditor();
            return Globals.ThisAddIn.ShowForm(form);
        }
        public static Form TitleAuthorConfirmBox()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            dynamic docproperties = doc.BuiltInDocumentProperties;
            string author = docproperties["Author"].Value;
            string title = docproperties["Title"].Value;
            string promptText = "Please verify the document properties:";
            string formtitle = "Finalize Document";
            Form form = new Form();
            Label label = new Label();
            Label label1 = new Label();
            Label label2 = new Label();
            TextBox titleTextBox1 = new TextBox();
            TextBox authorTextBox2 = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = formtitle;
            label.Text = promptText;
            label1.Text = "Document Title : ";
            label2.Text = "Author[s] : ";
            titleTextBox1.Text = title;
            authorTextBox2.Text = author;
            buttonOk.Text = "Confirm";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
            buttonOk.Click += (sender, args) =>
            {
                Microsoft.Office.Core.DocumentProperties properties = (Microsoft.Office.Core.DocumentProperties)Globals.ThisAddIn.GetActiveDocument().BuiltInDocumentProperties;
                properties["Author"].Value = authorTextBox2.Text.Trim();
                properties["Title"].Value = titleTextBox1.Text.Trim();
            };

            label.SetBounds(9, 20, 372, 13);
            label1.SetBounds(9, 45, 100, 13);
            titleTextBox1.SetBounds(109, 45, 372, 20);
            label2.SetBounds(9, 70, 100, 13);
            authorTextBox2.SetBounds(109, 70, 372, 20);
            buttonOk.SetBounds(270, 100, 75, 23);
            buttonCancel.SetBounds(350, 100, 75, 23);

            label.AutoSize = true;
            label1.Anchor = AnchorStyles.Left;
            label2.Anchor = AnchorStyles.Left;
            titleTextBox1.Anchor = titleTextBox1.Anchor | AnchorStyles.Right;
            authorTextBox2.Anchor = authorTextBox2.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(496, 150);
            form.Controls.AddRange(new Control[] { label,label1,titleTextBox1,label2, authorTextBox2, buttonOk, buttonCancel });
            form.ClientSize = new Size(System.Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;


            DialogResult dialogResult = form.ShowDialog();
            title = titleTextBox1.Text;
            author = authorTextBox2.Text;
            //Globals.ThisAddIn.ShowTaskPane(form, "Finalize Document", customWidth: 370, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
        }

        [CheckerDescription("Title and Author")]
        public static CheckerResult CheckFinalizeDocument()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            dynamic docproperties = doc.BuiltInDocumentProperties;
            string author = docproperties["Author"].Value;
            string title = docproperties["Title"].Value;

            if (author == null || title == null || author.Trim() == "" || title.Trim() == "")
                return CheckerResult.Failed;
            else
                return CheckerResult.Passed;
        }

        [CheckerDescription("Word Accessibility Checker")]
        public static CheckerResult InbuiltWordAccessibilityChecker()
        {
            return CheckerResult.Failed;
        }

        //public static void FinalizeDocument()
        //{
        //    Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
        //    dynamic docproperties = doc.BuiltInDocumentProperties;
        //    string author = docproperties["Author"].Value;
        //    string title = docproperties["Title"].Value;
        //    Microsoft.Office.Core.DocumentProperties properties = (Microsoft.Office.Core.DocumentProperties)Globals.ThisAddIn.GetActiveDocument().BuiltInDocumentProperties;

        //    if (TitleAuthorConfirmBox("Finalize Document", "Please verify the document properties:", ref author, ref title) == DialogResult.OK)
        //    {
        //        properties["Author"].Value = author;
        //        properties["Title"].Value = title;
        //        try
        //        {
        //            doc.Final = true;
        //            MessageBox.Show("Finalized document!");
        //        }
        //        catch (COMException ex)
        //        {
        //            // Do nothing. User didn't want to finalize it.
        //            Debug.Print("Finalization of document canceled.");
        //        }
        //    }
                        
           
        //}

        public static void ShowFindReplaceDialog()
        {
            Word.Dialog dlg = Globals.ThisAddIn.Application.Dialogs[Word.WdWordDialog.wdDialogEditReplace];
            dlg.Show();
        }

        public static void ClearFormattingOfSelection()
        {
            Globals.ThisAddIn.Application.Selection.ClearFormatting();
        }

        public static void CreateBullets(Word.Range range)
        {
            Word.ListGallery bulletGallery = Globals.ThisAddIn.Application.ListGalleries[Word.WdListGalleryType.wdBulletGallery];
            range.ListFormat.ApplyListTemplateWithLevel(bulletGallery.ListTemplates[1],
                true,
                Word.WdListApplyTo.wdListApplyToSelection,
                Word.WdDefaultListBehavior.wdWord10ListBehavior);
        }

        public static void FixSquareBullets(Word.Range range)
        {
            Word.ListGallery bulletGallery = Globals.ThisAddIn.Application.ListGalleries[Word.WdListGalleryType.wdBulletGallery];
            range.ListFormat.ApplyListTemplateWithLevel(bulletGallery.ListTemplates[1],
                false,
                //Word.WdListApplyTo.wdListApplyToSelection,
                Word.WdDefaultListBehavior.wdWord10ListBehavior);
        }

        public static void CreateNumberList(Word.Range range)
        {
            Word.ListGallery numberGallery = Globals.ThisAddIn.Application.ListGalleries[Word.WdListGalleryType.wdNumberGallery];
            range.ListFormat.ApplyListTemplateWithLevel(numberGallery.ListTemplates[1],
                false,
                Word.WdListApplyTo.wdListApplyToSelection,
                Word.WdDefaultListBehavior.wdWord10ListBehavior);
        }

        public static void CreateRomanList(Word.Range range)
        {
            Word.ListGallery romanGallery = Globals.ThisAddIn.Application.ListGalleries[Word.WdListGalleryType.wdOutlineNumberGallery];
            romanGallery.ListTemplates[1].ListLevels[1].NumberStyle = Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman;
            romanGallery.ListTemplates[1].ListLevels[1].NumberFormat = "%1.";
            romanGallery.ListTemplates[1].ListLevels[1].NumberPosition = Globals.ThisAddIn.Application.InchesToPoints(0.25f);
            romanGallery.ListTemplates[1].ListLevels[1].TextPosition = Globals.ThisAddIn.Application.InchesToPoints(0.7f);
            range.ListFormat.ApplyListTemplateWithLevel(romanGallery.ListTemplates[1],
                false,
                Word.WdListApplyTo.wdListApplyToSelection,
                Word.WdDefaultListBehavior.wdWord10ListBehavior);
        }

        [SuiteDescription("Save copy of document")]
        public static void SaveAsNewCopy(SuiteFunctionUpdateArgs args)
        {
            DialogResult result = MessageBox.Show("Would you like to save a copy?", "Save Copy?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                args.OrigControl.Dispatcher.Invoke(new Action(General.DoSaveAs));
            }
        }

        private static void DoSaveAs()
        {
            // Have user save document to different place
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Get original path (or make one up if not saved)
            string originalPath = doc.Path;
            if (originalPath.Length == 0)
            {
                // Make up something on the desktop
                originalPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = System.IO.Path.GetFileNameWithoutExtension(doc.Name);
            dialog.DefaultExt = "docx";
            dialog.Filter = "Word documents (.docx)|*.docx";
            dialog.InitialDirectory = originalPath;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                doc.SaveAs2(dialog.FileName);
            }
        }

        [SuiteDescription("Refresh document")]
        public static void RepaginateDocument(SuiteFunctionUpdateArgs args)
        {
            Globals.ThisAddIn.GetActiveDocument().Repaginate();

        }
        /// <summary>
        /// Converts all occurrences of images to word default
        /// </summary>
        [SuiteDescription("Convert to Compatible Mode", optional1: true, optional2: true, optional3: false, optional4: true)]
        public static void ConvertImagesToDefaultFormat(SuiteFunctionUpdateArgs args)
        {
           Globals.ThisAddIn.GetActiveDocument().SetCompatibilityMode(14);               
        }

        /// <summary>
        /// Finds specified range around a paragraph mark, and then applies specific changes in that range.
        /// Intent is to eliminate unnecessary line breaks within paragraphs.
        /// </summary>
        [SuiteDescription("Auto-repair paragraphs", optional1: true, optional2: false, optional3: true, optional4: true)]
        public static void AutoRepairParagraph(SuiteFunctionUpdateArgs args)
        {
            /// Find a lower-case letter, paragraph mark, lower-case letter. Replace paragraph mark with space.
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
             foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"[a-z]^013[a-z]", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {
                if (r.Text.Last() == '\u000D')
                r.MoveEnd(Word.WdUnits.wdCharacter, -1);

                // Replace paragraphs with spaces
                Globals.ThisAddIn.ReplaceAll(r, "^p", " ");

                // Replace redundant spacing with single space
                Globals.ThisAddIn.ReplaceAll(r, "[^t ]{2,}", " ", useWildcards: true);

                //// Replace dash-space from hyphenating words with nothing
                //Globals.ThisAddIn.ReplaceAll(r, "- ", "");

                //// Replace dash-paragraph mark from hyphenating words with nothing
                //Globals.ThisAddIn.ReplaceAll(r, "-^013", "", useWildcards: true);

            }
             /// Find a lower-case letter, comma, paragraph mark, lower-case letter. Replace paragraph mark with space. 
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"[a-z],^013[a-z]", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
             {
                 if (r.Text.Last() == '\u000D')
                     r.MoveEnd(Word.WdUnits.wdCharacter, -1);

                 // Replace paragraphs with spaces
                 Globals.ThisAddIn.ReplaceAll(r, ",^p", ", ");

                 // Replace redundant spacing with single space
                 Globals.ThisAddIn.ReplaceAll(r, "[^t ]{2,}", " ", useWildcards: true);

             }
            /// Find a lower-case letter, hyphen, paragraph mark, lower-case letter. Replace hyphen and paragraph mark with space. 
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"[a-z]-^013[a-z]", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
             {
                 if (r.Text.Last() == '\u000D')
                     r.MoveEnd(Word.WdUnits.wdCharacter, -1);

                 // Replace paragraphs with spaces
                 Globals.ThisAddIn.ReplaceAll(r, "-^p", "");

                 // Replace redundant spacing with single space
                 Globals.ThisAddIn.ReplaceAll(r, "[^t ]{2,}", " ", useWildcards: true);

             }

        }
            [SuiteDescription("Remove All Image Alt Text", optional1: true, optional2: true, optional3: true, optional4: true)]
            public static void RemoveAllImageAltText(SuiteFunctionUpdateArgs args)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                int i = 0;
                int count = range.InlineShapes.Count;
                foreach (Word.InlineShape s in range.InlineShapes)
                {
                    if (s.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                    {
                        string altText = "";
                        if (s.AlternativeText != null)
                            altText = s.AlternativeText;

                        if (altText.Length > 0)
                        {
                            s.AlternativeText = "";
                            //altText = null;
                        }
                    }
                }

            }

            [SuiteDescription("Remove extra space between ellipses dots (period)")]
            public static void FixEllipses(SuiteFunctionUpdateArgs args)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Globals.ThisAddIn.ReplaceAll(range, @".. .", "...", useWildcards: true);
                Globals.ThisAddIn.ReplaceAll(range, @". ..", "...", useWildcards: true);
                Globals.ThisAddIn.ReplaceAll(range, @". . .", "...", useWildcards: true);
            }
    }
}
    

