﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Tools = Microsoft.Office.Tools;

namespace CentralAccessMacros
{
    class ComputerCode
    {

        public static Word.Style ParagraphStyle
        {
            get
            {
                string styleName = "Code Block";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraphOnly);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleNormal);

                newStyle.ParagraphFormat.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray05;
                newStyle.ParagraphFormat.KeepWithNext = (int)VBABoolean.False;
                newStyle.ParagraphFormat.LineSpacing = Globals.ThisAddIn.Application.LinesToPoints(1.0f);
                newStyle.ParagraphFormat.SpaceAfterAuto = (int)VBABoolean.False;
                newStyle.ParagraphFormat.SpaceAfter = 0;
                newStyle.ParagraphFormat.SpaceBeforeAuto = (int)VBABoolean.False;
                newStyle.ParagraphFormat.SpaceBefore = 0;

                newStyle.Font.Name = "Consolas";

                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);

                List<Word.WdBorderType> borders = new List<Word.WdBorderType>
                {
                    Word.WdBorderType.wdBorderLeft,
                    Word.WdBorderType.wdBorderRight,
                    Word.WdBorderType.wdBorderTop,
                    Word.WdBorderType.wdBorderBottom
                };

                foreach (Word.WdBorderType b in borders)
                {
                    newStyle.Borders[b].LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    newStyle.Borders[b].LineWidth = Word.WdLineWidth.wdLineWidth050pt;
                    newStyle.Borders[b].Color = Word.WdColor.wdColorBlack;
                }

                newStyle.Borders.DistanceFromTop = 5;
                newStyle.Borders.DistanceFromBottom = 5;
                newStyle.Borders.DistanceFromLeft = 5;
                newStyle.Borders.DistanceFromRight = 5;
                newStyle.Borders.Shadow = true;

                return newStyle;
            }
        }

        public static Word.Style InlineStyle
        {
            get
            {
                string styleName = "Code Inline";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);

                newStyle.Font.Name = "Consolas";
                // Taking out bold because it shows through in Braille output                
                //newStyle.Font.Bold = (int)VBABoolean.True;
                newStyle.Font.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray05;

                return newStyle;
            }
        }

    }
}
