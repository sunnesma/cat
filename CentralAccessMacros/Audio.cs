﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Data;
using Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    class Audio
    {
        public static void RunSuite()
        {
            List<SuiteFunction> functions = new List<SuiteFunction>
            {
                General.SaveAsNewCopy,
                Audio.AddPageToPageNumbers,
                Audio.ExtractAltTextFromImages,
                DAISY.RemoveImages,
                Audio.ConvertTablesToAudioFriendlyText,
                Audio.AddAudioFootnoteNoteReferenceIndicators,
                WordDocPCAudio.FixBullets,
                Audio.DeclareListItems,
                General.ReplaceAutoNumbering,
                Audio.TypeBulletLists,
                Audio.TableIndicatorandSummary,
                Audio.TableAudio,
                Audio.AnnounceRow,
                
                
            };

            General.RunSuite(functions, "Audio");
        }

        [SuiteDescription("Extract alt text from images", optional1: true)]
        public static void ExtractAltTextFromImages(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            int i = 0;
            int count = range.InlineShapes.Count;
            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (args.IsCanceled())
                    break;

                if (s.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    string altText = "";
                    if (s.AlternativeText != null)
                        altText = s.AlternativeText;

                    if (altText.Length > 0)
                    {
                        Word.Range r = s.Range;
                        r.Move(Word.WdUnits.wdParagraph, -1);
                        r.InsertParagraphBefore();
                        TranscribersNote.InsertTranscribersNote(r, altText: altText, haveCloseTag: true);
                    }
                }

                i += 1;
                args.UpdatePercentage(i, count);
            }
        }


        [SuiteDescription("Insert Page before page numbers", optional1: true)]
        public static void AddPageToPageNumbers(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: Word.WdBuiltinStyle.wdStylePageNumber))
            {
                r.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                r.Text = "Page ";
            }
        }

        [SuiteDescription("Add [Footnote ] around each footnote", optional1: false, optional2: false)]
        public static void AddAudioFootnoteNoteReferenceIndicators(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            object characterunit = Word.WdUnits.wdCharacter;
            object FootnoteReference = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.FootnoteReference];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: FootnoteReference).ToList())
            {
                if (r.Characters.Count > 0)
                {
                    char[] r_Array = r.Text.ToCharArray();
                    Char lastChar = r_Array[r_Array.Length - 1];
                    if (lastChar.Equals((char)0x2029) || lastChar.Equals('\r') || lastChar.Equals('\n'))
                    {
                        r.MoveEnd(ref characterunit, -1);
                    }
                    char[] r_reverse = (char[])r.Text.ToCharArray().Clone();
                    Array.Reverse(r_reverse);
                    foreach (char character_i in r_Array)
                    {
                        if (Char.IsWhiteSpace(character_i))
                        {
                            r.MoveStart(ref characterunit, 1);
                        }
                        else
                        {
                            break;
                        }
                    }
                    foreach (char character_j in r_reverse)
                    {
                        if (Char.IsWhiteSpace(character_j))
                        {
                            r.MoveEnd(ref characterunit, -1);
                        }
                        else
                        {
                            break;
                        }
                    }
                    //The fresh range after processing for paragraph end and starting and trailing whitespaces
                    char[] r_characterarray = r.Text.ToCharArray();
                    if (Char.IsNumber(r_characterarray[0]))
                    {
                        r.InsertBefore(" [Footnote ");
                        r.InsertAfter(".] ");
                    }
                    else if (Char.IsLetter(r_characterarray[0]))
                    {
                        r.InsertBefore(" [Footnote ");
                        r.InsertAfter(".] ");
                    }
                    else
                    {
                        r.InsertBefore(" [Footnote ");
                        r.InsertAfter(".] ");
                    }

                   
                    r.set_Style(MainRibbon.FootnoteReference);
                }
            }
        }

        [SuiteDescription("Convert tables to text", optional1: true)]
        public static void ConvertTablesToAudioFriendlyText(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            foreach (Word.Table table in doc.Tables)
            {
                table.Select();

                DialogResult result = MessageBox.Show("Want to convert this table?", "Convert Table?", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {

                    //Globals.ThisAddIn.Application.Selection.Rows.ConvertToText(Word.WdTableFieldSeparator.wdSeparateByDefaultListSeparator);
                    Globals.ThisAddIn.Application.Selection.Rows.ConvertToText(Word.WdTableFieldSeparator.wdSeparateByParagraphs);

                }
                else if (result == DialogResult.Cancel)
                {
                    break;
                }
            }

            MessageBox.Show("Search complete!");
        }

        // <summary>
        // Replaces the auto-numbering in a document into straight plain-text.
        // The auto-numbering refers to numbers in an ordered list, not page
        // numbers.
        // </summary>
        [SuiteDescription("[experimental] Declare number of list items before each list", optional1: false)]
        public static void DeclareListItems(SuiteFunctionUpdateArgs args)
        {
            // <summary>
            // Former solution
            //Globals.ThisAddIn.GetActiveDocument().Content.ListFormat.ConvertNumbersToText();



            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            //Begin code taken and modified from https://stackoverflow.com/questions/13311310/insert-text-before-a-list-using-word-interop
            const string endtext = "[End List.]";
            Word.Style myStyle = Globals.ThisAddIn.GetActiveDocument().Styles["Normal"];
            foreach (List list in doc.Lists)
            {
                // Range of the current list
                Word.Range listRange = list.Range;

                int listcount = listRange.ListParagraphs.Count;
                string starttext = "[List of " + listcount + " items.]";

                // Range of character before the list
                var prevRange = listRange.Previous(WdUnits.wdCharacter);


                // If null, the list might be located in the very beginning of the doc
                if (prevRange == null)
                {
                    // Insert new paragraph before
                    listRange.InsertParagraphBefore();
                    listRange.SetRange(0, 0);
                    listRange.Select();
                    Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
                    // Insert the text before
                    listRange.InsertBefore(starttext);

                }
                else
                {


                    if (prevRange.Text.Any())
                    {
                        // Dont't append the list text to any lines that might already be just before the list
                        // Instead, make sure the text gets its own line
                        listRange.SetRange(listRange.Start, listRange.Start);
                        listRange.Select();
                        Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
                        prevRange.InsertBefore("\r\n" + starttext);

                    }
                    else
                    {
                        // Insert the list text
                        prevRange.InsertBefore(starttext);

                    }
                }
            }
            foreach (List list in doc.Lists)
            {
                // Range of the current list
                var listRange = list.Range;
                //Range of character after the list
                var followingRange = listRange.Next(WdUnits.wdCharacter);
                if (followingRange == null)
                {
                    // Insert new paragraph below
                    //listRange.InsertParagraphAfter();
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    //selection.MoveEnd(ref characterunit, ref new Integer(-1));
                    selection.InsertAfter("\r\n" + endtext);
                }
                else
                {
                    if (followingRange.Text.Any())
                    {
                        listRange.InsertParagraphAfter();
                        char[] rangeCharArray = selection.Text.ToCharArray();
                        char charLast = rangeCharArray[rangeCharArray.Length - 1];
                        followingRange.InsertBefore(endtext + "\r\n");
                    }
                    else
                    {
                        listRange.InsertParagraphAfter();
                        char[] rangeCharArray = selection.Text.ToCharArray();
                        char charLast = rangeCharArray[rangeCharArray.Length - 1];
                        followingRange.InsertBefore(endtext);
                    }
                
                }
            }

            }
        
            // End taken code

            [SuiteDescription("Add [BULLET.] to each bullet list item")]
        public static void TypeBulletLists(SuiteFunctionUpdateArgs args)
        {
            // <summary>
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            foreach (Word.Paragraph firstItem in doc.Content.ListParagraphs.OfType<Word.Paragraph>().Reverse())
            {

                if (firstItem.Range.ListFormat.List != null)
                {
                    foreach (Word.Paragraph item in firstItem.Range.ListFormat.List.ListParagraphs)
                    {
                        if (firstItem.Range.ListFormat.ListType != Word.WdListType.wdListBullet)
                        {
                            item.Range.ListFormat.ConvertNumbersToText();
                        }
                        else
                        {
                            item.Range.InsertBefore("[BULLET.] ");
                            item.Range.ListFormat.ConvertNumbersToText();
                        }
                    }
                }
            }
        }

        // Summary
        // Adds table row and column summary before table, and end table indicator after. Note, the code
        // used in this example uses sendkeys, which virtually press keyboard buttons. Although, this is
        // not an elegant solution, there seems to be no way to insert things before and after a table as of 
        // 10/31/2017. Good luck. -MPS
        [SuiteDescription("Add table indicators and summary.", optional1: true, optional2: true, optional3: true)]
        public static void TableIndicatorandSummary(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;


            foreach (Table tb in range.Tables)
            {
                int RowCount = tb.Rows.Count;
                int ColumnCount = tb.Columns.Count;
                String str = "";


                tb.Range.Select();
                Globals.ThisAddIn.Application.Activate();
                SendKeys.SendWait("^+{ENTER}");
                str = "[Table with " + RowCount + " rows, and " + ColumnCount + " columns.]";
                tb.Range.Previous().InsertBefore(str);
                //Globals.ThisAddIn.Application.Selection.ClearParagraphAllFormatting();



                tb.Select();
                Globals.ThisAddIn.Application.Activate();
                SendKeys.SendWait("{DOWN}");
                Globals.ThisAddIn.Application.Selection.TypeText("[End Table]");
                SendKeys.SendWait("{ENTER}");
                SendKeys.SendWait("{UP}");
                Globals.ThisAddIn.Application.Selection.ClearParagraphAllFormatting();
                Globals.ThisAddIn.Application.Selection.Expand(WdUnits.wdParagraph);
                Globals.ThisAddIn.Application.Selection.ClearCharacterAllFormatting();

            }


        }

        [SuiteDescription("Repeat Header in each table row", optional1: true, optional2: true, optional3: true)]
        public static void TableAudio(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            //int i = 0;
            //int count = range.InlineShapes.Count;
            //int count = range.Tables.;
            foreach (Table tb in range.Tables)
            {
                int RowCount = tb.Rows.Count;
                int ColumnCount = tb.Columns.Count;
                for (int j = 2; j <= RowCount; ++j)
                {
                    for (int i = 1; i <= ColumnCount; ++i)
                    {
                        var cell = tb.Cell(j, i);
                        var cellh = tb.Cell(1, i);
                        var cellhtext = cellh.Range.Text;
                        var text = cell.Range.Text;
                        //object charUnit = Word.WdUnits.wdCharacter;
                        //object move = -1;  // move left 1
                        //cell.Range.InsertBefore(text);
                        //MessageBox.Show(text.ToString() + " " + cellhtext.ToString());
                        cell.Range.InsertBefore(cellhtext);

                    }


                    //var singlecell = tb.Cell(j, 1);
                    //singlecell = tb.Cell(j, 1);
                    //MessageBox.Show(text.ToString());
                    //singlecell.Range.InsertBefore(text);
                }

                String str = "";
                tb.Range.Select();
                Globals.ThisAddIn.Application.Activate();
                SendKeys.SendWait("^+{ENTER}");
                str = "[First row values are repeated for each following row.]";
                tb.Range.Previous().InsertBefore(str);
                //Globals.ThisAddIn.Application.Selection.ClearParagraphAllFormatting();
            }


        }
        
        [SuiteDescription("Number table rows", optional1: true, optional2: true, optional3: true)]
        public static void AnnounceRow(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            //int i = 0;
            //int count = range.InlineShapes.Count;
            //int count = range.Tables.;
            foreach (Table tb in range.Tables)
            {
                tb.Columns.Add(tb.Columns[1]);
                int RowCount = tb.Rows.Count;
                int ColumnCount = tb.Columns.Count;
                for (int j = 1; j <= RowCount; ++j)
                {


                    for (int i = 1; i <= 1; ++i)
                    {
                    var cell = tb.Cell(j, 1);
                    var cellr = "Row " + j;
                    //    var text = cell.Range.Text;

                    cell.Range.InsertBefore(cellr);

                    }

                }


            }


        }



    }
}
