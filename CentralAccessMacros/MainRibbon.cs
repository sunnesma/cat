﻿using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;
using System.Threading;
using System.Globalization;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new MainRibbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace CentralAccessMacros
{
    [ComVisible(true)]
    public class MainRibbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;

        private bool ShowEditDocument { get; set; }
        private bool ShowBrailleEdit { get; set; }
        private bool ShowTools { get; set; }
        private bool ShowPanes { get; set; }
        private bool ShowFormatReady { get; set; }
        private bool ShowAdvancedEdit { get; set; }
        
        //Additional variables required for the functioning of the checkbox for use of Title Case in Heading format.
        private static bool CapitalizeEachWord = true;
        private static bool pressedState = true;
        
        public MainRibbon()
        {
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("CentralAccessMacros.MainRibbon.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        
        // To enable the popup on first load of version (note this is currently not in use):
        //  1. Uncomment "FirstLoadPopUpMessage();" "ResetFirstPopUp();"
        //  2. Start project (run it)
        //  3. Close the Word Document.
        //  4. Re-comment "ResetFirstPopUp();" AND Build solution (DO NOT RUN/START): BUILD > BUILD SOLUTION
        //  5. Publish solution as release

        // *** WARNING. If you do not re-comment "ResetFirstPopUp();" and BUILD solution, then the popup
        //  will appear each start up! ***
        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;

            ShowEditDocument = false;
            ShowBrailleEdit = false;
            ShowTools = false;
            ShowPanes = false;
            //FirstLoadPopUpMessage();
            //ResetFirstPopUp();
        }

        public void FirstLoadPopUpMessage()
        {
            if ((bool)Properties.Settings.Default.FirstRun == true)
            {
                Properties.Settings.Default.FirstRun = false;
                Properties.Settings.Default.Save();
                MessageBox.Show("First Time");
            }
        }

        public void ResetFirstPopUp()
        {
            Properties.Settings.Default.FirstRun = true;
            Properties.Settings.Default.Save();
            MessageBox.Show("The reset of first pop up was successful. Please re-comment ResetFirstPopUp(); AND Build solution (DO NOT RUN/START): BUILD > BUILD SOLUTION");
        }

        public void ShowEditDocumentActionPane(IRibbonControl control)
        {
            EditDocumentActionPane pane = new EditDocumentActionPane();
            Globals.ThisAddIn.ShowTaskPane(pane, "Edit",
                startPosition: MsoCTPDockPosition.msoCTPDockPositionFloating,
                customWidth: 60,
                customHeight: 700,
                posX: 100,
                posY: 140,
                restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoChange);
        }

        #region Category Show/Hide

        public bool IsEditDocumentVisible(IRibbonControl control)
        {
            return ShowEditDocument;
        }
        
        public void ToggleEditDocumentVisible(IRibbonControl control, bool pressed)
        {
            ShowEditDocument = pressed;
            this.ribbon.Invalidate();
        }

        public bool IsBrailleVisible(IRibbonControl control)
        {
            return ShowBrailleEdit;
        }

        public void ToggleBrailleVisible(IRibbonControl control, bool pressed)
        {
            ShowBrailleEdit = pressed;
            this.ribbon.Invalidate();
        }

        public bool IsToolsVisible(IRibbonControl control)
        {
            return ShowTools;
        }

        public void ToggleToolsVisible(IRibbonControl control, bool pressed)
        {
            ShowTools = pressed;
            this.ribbon.Invalidate();
        }

        public bool IsPanesVisible(IRibbonControl control)
        {
            return ShowPanes;
        }

        public void TogglePanesVisible(IRibbonControl control, bool pressed)
        {
            ShowPanes = pressed;
            this.ribbon.Invalidate();
        }

        public bool IsFormatReadyVisible(IRibbonControl control)
        {
            return ShowFormatReady;
        }

        public void ToggleFormatReadyVisible(IRibbonControl control, bool pressed)
        {
            ShowFormatReady = pressed;
            this.ribbon.Invalidate();
        }
        public bool IsAdvancedEditVisible(IRibbonControl control)
        {
            return ShowAdvancedEdit;
        }

        public void ToggleAdvancedEditVisible(IRibbonControl control, bool pressed)
        {
            ShowAdvancedEdit = pressed;
            this.ribbon.Invalidate();
        }
        #endregion

        #region Fixers

        public void RunImageAltTextEditor(IRibbonControl control)
        {
            General.ShowImageAltTextEditor();
        }

        public void RunPageCounter(IRibbonControl control)
        {
            PageNumbers.ShowPageCounter();
        }
        public void RunFootnoteCounter(IRibbonControl control)
        {
            FootnoteNumbers.ShowFootnoteCounter();
        }
        public void RunTranscribersNoteCounter(IRibbonControl control)
        {
            TranscribersNote.ShowTranscriberNoteEditor();
        }

        public void RunFormattingFixer(IRibbonControl control)
        {
            FormattingFixer.ShowFormattingFixer();
        }

        public void RunCharacterAccuracyChecker(IRibbonControl control)
        {
            CharacterAccuracyChecker.ShowCharacterAccuracyChecker();
        }

        public void RunTextBoxFinder(IRibbonControl control)
        {
            TextBoxFinder.ShowTextboxFinder();
        }

        public void RunChartConverter(IRibbonControl control)
        {
            ChartConverter.ShowChartConverter();
        }

        #endregion

        #region General

        public void ToggleNavigationPane(IRibbonControl control)
        {
            bool before = Globals.ThisAddIn.Application.ActiveWindow.DocumentMap;
            Globals.ThisAddIn.Application.ActiveWindow.DocumentMap = !before;
        }

        public void RunConvertDAISYNumbersToRegular(IRibbonControl control)
        {
            General.ConvertDAISYNumbersToPageNumbers(new SuiteFunctionUpdateArgs());
        }

        public void RunGeneralChecker(IRibbonControl control)
        {
            WordDocPCAudio.RunChecker();
        }

        public void RunLargePrintFunctions(IRibbonControl control)
        {
            LargePrint.ShowForm();
        }

        public void RunHelp(IRibbonControl control)
        {
            Process.Start(Properties.Resources.HelpURL);
        }

        public void RunFeedback(IRibbonControl control)
        {
            Process.Start(Properties.Resources.FeedbackURL);
        }

        public void RunReleaseNotes(IRibbonControl control)
        {
            Process.Start(Properties.Resources.ReleaseNotesURL);
        }

        public void AboutCAT(IRibbonControl control)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }
        public void RunFindReplace(IRibbonControl control)
        {
            General.ShowFindReplaceDialog();
        }

        public void RunCreateBullets(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            General.CreateBullets(range);
        }

        public void RunCreateNumberList(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            General.CreateNumberList(range);
        }

        public void RunCreateRomanList(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            General.CreateRomanList(range);
        }

        public void ClearFormatting(IRibbonControl control)
        {
            General.ClearFormattingOfSelection();
        }

        public void ToggleStylesPane(IRibbonControl control)
        {
            Word.TaskPane p = Globals.ThisAddIn.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneFormatting];
            p.Visible = !p.Visible;
        }

        public void RunGuideMe(IRibbonControl control)
        {
            GuideMe.ShowActionPane();
        }

        public void RunDAISYSuite(IRibbonControl control)
        {
            DAISY.RunSuite();
        }

        public void RunAudioSuite(IRibbonControl control)
        {
            Audio.RunSuite();
        }

        public void RunProToolsSuite(IRibbonControl control)
        {
            ProTools.RunSuite();
        }

        public void ToggleFormatMarkers(IRibbonControl control)
        {
            bool isPressed = Globals.ThisAddIn.Application.ActiveWindow.View.ShowAll;
            Globals.ThisAddIn.Application.ActiveWindow.View.ShowAll = !isPressed;
            this.ribbon.InvalidateControl("ShowMarks");
        }

        public string FormatMarkers_getLabel(IRibbonControl control)
        {
            if (Globals.ThisAddIn.Application.ActiveWindow.View.ShowAll)
                return "Hide Markers";
            else
                return "Show Markers";
        }

        #endregion

        #region Word Doc/PC Audio

        public void RunWordDocSuite(IRibbonControl control)
        {
            WordDocPCAudio.RunSuite();
        }

        public void ConvertAllParenLetterToLetterDot(IRibbonControl control)
        {
            General.ConvertParenLetterToLetterDot(new SuiteFunctionUpdateArgs());
        }

        public void ConvertAllParenNumberToNumberDot(IRibbonControl control)
        {
            General.ConvertParenNumberToNumberDot(new SuiteFunctionUpdateArgs());
        }

        public void RunRepairParagraph(IRibbonControl control)
        {
            General.RepairParagraph(Globals.ThisAddIn.Application.Selection.Range);
        }

        public void AddEquationCommentary(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(EquationCommentaryStyle);
        }
        public void MarkSpanish(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Spanish);
        }
        public void MarkFrench(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(French);
        }

        public void MarkGerman(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(German);
        }
        public void MarkIPA(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(IPA);
        }
        public void MarkItalian(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Italian);
        }
        public void MarkLatin(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Latin);
        }
        public void oneThreeIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("1-3"));
        }
        public void oneFiveIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("1-5"));
        }
        public void oneSevenIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("1-7"));
        }
        public void oneNineIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("1-9"));
        }
        public void threeFiveIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("3-5"));
        }
        public void threeSevenIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("3-7"));
        }
        public void threeNineIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("3-9"));
        }
        public void fiveSevenIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("5-7"));
        }
        public void fiveNineIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("5-9"));
        }
        public void sevenNineIndentation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(ApplyIndentationStyle("7-9"));
        }
        public void Poetry1styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry1Style);
        }
        public void Poetry2styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry2Style);
        }
        public void Poetry3styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry3Style);
        }
        public void Poetry4styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry4Style);
        }
        public void Poetry5styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry5Style);
        }
        public void Poetry6styling(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.set_Style(Poetry6Style);
        }
        
        // Changes: Date 11/28/2017, Anuj Bhatia.
        // Puts the Begin Box marker before the selected text or the current selected paragraph. 
        // Works for both Single and Double Boxes.
        public void BeginBoxstyling(IRibbonControl control)
        {
            #region Field Declarations
                Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
                string strBeginBoxText="";
                object BoxBeginStyle = null;
            #endregion

            // Determine from the control.Tag property if the call was made for a Single Box 
            // or a Double Box, and set corresponding variables accordingly. 
            switch (control.Tag.ToString())
            {
                case "BeginBox":
                    strBeginBoxText = "[Begin Box]";
                    BoxBeginStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxBegin];
                    break;

                case "DoubleBoxBegin":
                    strBeginBoxText = "[Double Box Line]";
                    BoxBeginStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxDouble];
                    break;
            }

            AddAndStyleTextBeforeSelection(selection.Start, selection.End, BoxBeginStyle, strBeginBoxText);

        }

        // Changes: Date 11/28/2017, Anuj Bhatia.
        // Puts the End Box marker after the selected text or the current selected paragraph. 
        // Works for both Single and Double Boxes.
        public void EndBoxstyling(IRibbonControl control)
        {
            #region Field Declarations
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            object BoxEndStyle = null;
            string strEndBoxText = "";
            #endregion

            // Determine from the control.Tag property if the call was made for a Single Box 
            // or a Double Box, and set corresponding variables accordingly. 
            switch (control.Tag.ToString())
            {
                case "EndBox":
                    strEndBoxText = "[End Box]";
                    BoxEndStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxEnd];
                    break;

                case "DoubleBoxEnd":
                    strEndBoxText = "[Double Box Line]";
                    BoxEndStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxDouble];
                    break;
            }

            AddAndStyleTextAfterSelection(selection.Start, selection.End, BoxEndStyle, strEndBoxText);
        }

        // Changes: Date 11/17/2017, Anuj Bhatia.
        // Puts the Begin Box and End Box markers around the selected text. 
        // Works for both Single and Double Boxes.
        public void MarkSelectionBox(IRibbonControl control)
        {
            #region Field Declarations
                Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
                string strBeginBoxText = "";
                string strEndBoxText = "";
                object BoxBeginStyle = null;
                object BoxEndStyle = null;
            #endregion

            // The following code can be used to stop this function and do nothing if the user has not selected any text in the document.
            // Uncomment the line "return" to enable this functionality.
            if (selection.Start == selection.End)
            {
                //    return;
            }

            // Determine from the control.Tag property if the call was made for a Single Box 
            // or a Double Box, and set corresponding variables accordingly. 
            switch (control.Tag.ToString())
            {
                case "Box":
                    strBeginBoxText = "[Begin Box]";
                    strEndBoxText = "[End Box]";
                    BoxBeginStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxBegin];
                    BoxEndStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxEnd];
                    break;

                case "BoxDouble":
                    strBeginBoxText = strEndBoxText = "[Double Box Line]";
                    BoxBeginStyle = BoxEndStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxDouble];
                    break;
            }

            AddAndStyleTextBeforeSelection(selection.Start, selection.End, BoxBeginStyle, strBeginBoxText);
                    
            AddAndStyleTextAfterSelection(selection.Start, selection.End, BoxEndStyle, strEndBoxText);
        }

        // General utility function to add and style text before the selected text in the document.
        public void AddAndStyleTextBeforeSelection(int textRangeStart, int textRangeEnd, object textStyle, string textToInsert = "")
        {
            #region Field Declarations
                Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            #endregion

            selection.SetRange(textRangeStart, textRangeEnd);
            selection.Select();

            //If nothing is selected in the document, we find the nearest paragraph mark.
            if (textRangeStart == textRangeEnd)
            {
                textRangeStart = 0;
                foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(selection, "\r", forwardDirection: false, useWildcards: false, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
                {
                    textRangeStart = r.Start;
                    break;
                }
            }
            else
            {
                //Selecting the actual text range from the selection. 
                DynamicTextSelection(textRangeStart, textRangeEnd);

                textRangeStart = Globals.ThisAddIn.Application.Selection.Start;
                textRangeEnd = Globals.ThisAddIn.Application.Selection.End;
            }

            #region Insert Text Before Selection
            selection.SetRange(textRangeStart, textRangeStart);
            //Insert page break before the highlighted area. Helps especially in the case where a table is involved.
            selection.InsertBreak();
            selection.InsertBefore("\n" + textToInsert + "\n");

            //Clearing the default formatting of the inserted text
            selection.SetRange(selection.Start, selection.Start + textToInsert.Length);
            selection.Select();
            Globals.ThisAddIn.Application.Selection.ClearFormatting();
            if (selection.CharacterStyle() == "Page Number")
                Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();

            //Deleting the page break
            selection.SetRange(selection.Start - 2, selection.Start);
            selection.Delete();

            //Setting the range to start of the selected text. This helps in finding and setting the styles for the inserted text
            selection.SetRange(textRangeStart, textRangeStart);

            //Setting style for inserted text
            Globals.ThisAddIn.FindWildCard(selection, textToInsert, useWildcards: false, wrapSearch: Word.WdFindWrap.wdFindStop).ToList().First().set_Style(textStyle);
            #endregion

            //Remove empty paragraphs inserted on the way
            RemoveEmptyParagraphsInSelection(textRangeStart, textRangeEnd, 50);

            //Bring the cursor back to where the user had selected the text
            selection.SetRange(textRangeStart + textToInsert.Length, textRangeStart + textToInsert.Length);
            selection.Select();
        }

        // General utility function to add and style text after the selected text in the document.
        public void AddAndStyleTextAfterSelection(int textRangeStart, int textRangeEnd, object textStyle, string textToInsert = "")
        {

            #region Field Declarations
                Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            #endregion

            selection.SetRange(textRangeStart, textRangeEnd);
            selection.Select();

            //If nothing is selected in the document, we find the nearest paragraph mark.
            if (textRangeStart == textRangeEnd)
            {
                foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(selection, "\r", useWildcards: false, forwardDirection: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
                {
                    textRangeEnd = r.Start;
                    break;
                }
            }
            else
            {
                //Selecting the actual text range from the selection. 
                DynamicTextSelection(textRangeStart, textRangeEnd);
                textRangeStart = Globals.ThisAddIn.Application.Selection.Start;
                textRangeEnd = Globals.ThisAddIn.Application.Selection.End;
            }

            //Inserting a new page break after the selection.
            selection.SetRange(textRangeEnd, textRangeEnd);
            selection.InsertBreak();

            //Deleting the page break
            selection.SetRange(textRangeEnd, textRangeEnd + 2);
            selection.Select();
            selection.Delete();

            //Clearing the formatting of the new paragraph, created by the page break.
            selection.SetRange(textRangeEnd, textRangeEnd + 1);
            selection.Select();
            Globals.ThisAddIn.Application.Selection.ClearFormatting();
            if (selection.CharacterStyle() == "Page Number")
                Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();

            #region Insert Text After Selection
            selection.SetRange(textRangeEnd, textRangeEnd);
            selection.Select();
            selection.InsertAfter("\n" + textToInsert + "\n");
            DynamicTextSelection(selection.Start, selection.End);

            //Clearing the formatting of the inserted text
            Globals.ThisAddIn.Application.Selection.ClearFormatting();
            if (selection.CharacterStyle() == "Page Number")
                Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();

            //Setting the range to start of the selected text. This helps in finding and setting the styles for the inserted text.
            selection.SetRange(textRangeStart, textRangeStart);

            //Setting style for inserted text
            Globals.ThisAddIn.FindWildCard(selection, textToInsert, useWildcards: false, wrapSearch: Word.WdFindWrap.wdFindStop).ToList().First().set_Style(textStyle);
            #endregion

            //Remove empty paragraphs inserted on the way
            RemoveEmptyParagraphsInSelection(textRangeStart, textRangeEnd, 50);

            //Bring the cursor back to where the user had selected the text
            selection.SetRange(textRangeStart, textRangeStart);
            selection.Select();
        }

        //Loop implemented for dynamic selection of text, which excludes paragraph marks, tab spaces and emptylines in the current selected range.
        public void DynamicTextSelection(int textRangeStart, int textRangeEnd)
        {
            //Swapping the ranges if start range is greater than end range.
            if (textRangeStart > textRangeEnd)
            {
                int temp = textRangeStart;
                textRangeStart = textRangeEnd;
                textRangeEnd = temp;
            }

            Word.Range tempSelection = Globals.ThisAddIn.Application.Selection.Range;
            string strExclude = "\r\n\t ";

            while (textRangeStart != textRangeEnd) 
            {
                tempSelection.SetRange(textRangeStart, textRangeStart + 1);

                if (tempSelection.Text == null || strExclude.Contains(tempSelection.Text.ToString()))
                    textRangeStart++;
                else
                {
                    tempSelection.SetRange(textRangeEnd - 1, textRangeEnd);

                    if (tempSelection.Text == null || strExclude.Contains(tempSelection.Text.ToString()))
                        textRangeEnd--;
                    else
                        break;
                }
            } 
            tempSelection.SetRange(textRangeStart, textRangeEnd);
            tempSelection.Select();
        }

        //Removes empty paragraphs in the range specified by startRange and endRange variables. 
        //offset variable is used to select extra characters before and after the range, if needed.
        public void RemoveEmptyParagraphsInSelection(int startRange, int endRange, int offset = 0)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.SetRange((startRange - offset < 0) ? 0 : (startRange - offset), endRange + offset);
            selection.Select();
            foreach (Word.Paragraph p in selection.Paragraphs)
                if (p.Range.Text.ToString() == "\r")
                {
                    Word.Range tempSelection = Globals.ThisAddIn.Application.Selection.Range;
                    tempSelection.SetRange(p.Range.Start, p.Range.End - 1);
                    tempSelection.Delete();
                } 
        }
     
        public void MarkFootnoteReference(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            string rangeText = selection.Text;
            object characterunit = Word.WdUnits.wdCharacter;
            if (rangeText == null)
                return;
            char[] rangeCharArray = selection.Text.ToCharArray();
            char charLast = rangeCharArray[rangeCharArray.Length - 1];
            if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
            {
                selection.InsertAfter(" ");
                selection.MoveEnd(ref characterunit, -2);
            }
            selection.set_Style(FootnoteReference);
            
        }

        // Check for possible FootNote references in the whole document.
        // Use regex to find possible scenarios, like (at max) 3 digit number, (at max) 4 asterisks,
        // text followed by a (at max) 3 digit number, and some regular footnote markers found in documents.
        public void FootnoteHunter(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            bool checkConditions = false;
           
            //Regex to find a 1/2/3 digit number.
            System.Text.RegularExpressions.Regex rgxNumber = new System.Text.RegularExpressions.Regex("\\b[0-9]{1,3}\\b");
            
            //Regex to find 1-4 asterisks
            System.Text.RegularExpressions.Regex rgxAsterisk = new System.Text.RegularExpressions.Regex("[*]{1,4}");
            
            //Regex to find a text followed by a 1/2/3 digit number
            System.Text.RegularExpressions.Regex rgxNumberAfterText = new System.Text.RegularExpressions.Regex("\\b[a-zA-Z]*[0-9]{1,3}\\b");
            
            //Common footnote markers. Add any new marker here at the end of the string, without any spaces or punctuation.
            string otherFootNoteMarkers = "†‡§¿¡";
            string falseFlags = "\r\n\t...";

            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
           
            foreach (Word.Range rangeWord in doc.Words)
            {

                Microsoft.Office.Interop.Word.Style style = rangeWord.get_Style();
                if (!CheckStylePerCharacter(rangeWord.Start, rangeWord.End, "Page Number", "FootnoteReference"))
                {
                    checkConditions = (rgxNumber.IsMatch(rangeWord.Text.Trim())
                                || rgxNumberAfterText.IsMatch(rangeWord.Text.Trim())
                                || rgxAsterisk.IsMatch(rangeWord.Text.Trim())
                                || otherFootNoteMarkers.Contains(rangeWord.Text.Trim().Replace(".", "")))
                                && !falseFlags.Contains(rangeWord.Text.Trim());

                    if (checkConditions)
                    {
                        rangeWord.Select();
                        int rangeStart = rangeWord.Start;
                        int rangeEnd = rangeWord.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);

                        // Checking if the word contains a full stop punctuation. MS Word considers full stop as a part of the word.
                        // If it exists, remove the full stop from the selection, so that its not marked as a footnote later.
                        if (rangeWord.Text.Trim().Contains("."))
                        {
                            if (Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeStart + 1).Text.Contains("."))
                                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(++rangeStart, rangeEnd);
                            else if (Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd - 1, rangeEnd).Text.Contains("."))
                                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd - 1);
                        }

                        // Special if case written for text followed by numbers, because we need to take out the number part from the whole word.
                        if (!rgxNumber.IsMatch(rangeWord.Text.Trim()) && rgxNumberAfterText.IsMatch(rangeWord.Text.Trim()))
                        {
                            int myInt = 0;
                            // A loop written to find and select only the number part in the word. This will later be marked as a footnote.
                            do
                                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(++rangeStart, rangeEnd);
                            while (!int.TryParse(selection.Text, out myInt));
                            selection.Select();
                        }

                        DialogResult result = MessageBox.Show(selection.Text.Trim(), "Style as footnote reference?", MessageBoxButtons.YesNoCancel);
                        if (result == DialogResult.Yes)
                            selection.set_Style(MainRibbon.FootnoteReference);
                        else if (result != DialogResult.No)
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// New version of Footnote hunter, which shows all the footnote suspects and already styled footnotes in different lists.
        /// Allows random access of footnotes in the document.
        /// </summary>
        /// <param name="control"></param>
        public void FootnoteHunterV2(IRibbonControl control) 
        {
            FootNoteHunter.ShowFootNoteHunter();
        }
        /// <summary>
        /// Checks the style of every character in a word with the style names provided. 
        /// Can be used for multiple styles at once.
        /// </summary>
        public static bool CheckStylePerCharacter(int startRange, int endRange, params string[] styleName)
        {
            Word.Range selection;
            Microsoft.Office.Interop.Word.Style style;
            while (startRange != endRange)
            {
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(startRange, ++startRange);
                style=selection.get_Style();
                foreach (string styleToCheck in styleName)
                    if (style.NameLocal == styleToCheck)
                        return true;
            }
            return false;
        }

        public void Skip1Line(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*sk1*]]");
            selection.set_Style(DBTCode);
        }
        /// <summary>
        /// Inserts DBT Codes for tables
        /// </summary>
        public void ColumarTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbs;r:0:0:b:n:c*]]");
            selection.set_Style(DBTCode);
        }
        public void LinearTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbs;r:0:0:b:n:p*]]");
            selection.set_Style(DBTCode);
        }
        public void ListedTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbs;r:0:0:b:n:l*]]");
            selection.set_Style(DBTCode);
        }
        public void StairStepTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbs;r:0:0:b:n:s*]]");
            selection.set_Style(DBTCode);
        }
        public void UnrelatedColumnsTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbs;u*]]");
            selection.set_Style(DBTCode);
        }
        public void EndTableTableCode(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.InsertBefore("[[*htbe*]][[*sk1*]]");
            selection.set_Style(DBTCode);
        }
        public void MarkLineNumber(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.set_Style(LineNumber);
        }
        public void MarkExactTranslation(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //selection.InsertParagraphBefore();
            selection.set_Style(Braille.ExactTranslation);
        }

        /// <summary>
        /// Gets the style for the EquationCommentary in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style EquationCommentaryStyle
        {
            get
            {
                string styleName = "EquationCommentary";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.Font.Color = Word.WdColor.wdColorGreen;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }

        /// <summary>
        /// Gets the style for the Spanish in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Spanish
        {
            get
            {
                string styleName = "Spanish";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.LanguageID = Word.WdLanguageID.wdSpanish;
                newStyle.Font.Color = Word.WdColor.wdColorRed;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the French in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style French
        {
            get
            {
                string styleName = "French";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.LanguageID = Word.WdLanguageID.wdFrench;
                newStyle.Font.Color = Word.WdColor.wdColorPlum;
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the German in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style German
        {
            get
            {
                string styleName = "German";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.LanguageID = Word.WdLanguageID.wdGerman;
                newStyle.Font.Color = Word.WdColor.wdColorGreen;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Italian in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Italian
        {
            get
            {
                string styleName = "Italian";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.LanguageID = Word.WdLanguageID.wdItalian;
                newStyle.Font.Color = Word.WdColor.wdColorAqua;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Latin in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Latin
        {
            get
            {
                string styleName = "Latin";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.LanguageID = Word.WdLanguageID.wdLatin;
                newStyle.Font.Color = Word.WdColor.wdColorOrange;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for IPA in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style IPA
        {
            get
            {
                string styleName = "IPA";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                newStyle.Font.Color = Word.WdColor.wdColorBrown;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the BrailleBase in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style BrailleBase
        {
            get
            {
                string styleName = "Braille Base";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle("Normal");
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                    Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                    //newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);
                    newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                    newStyle.Font.Name = "Times New Roman";
                    newStyle.Font.Size = 14;
                    newStyle.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                    newStyle.ParagraphFormat.SpaceAfter = 3;
                    newStyle.set_NextParagraphStyle("Normal");
                    //newStyle.Visibility= false; 
                    return newStyle;
                }
            }
        }

        public static Word.Style ApplyIndentationStyle(String styleName)
        {
            int leftIndent, firstLineIndent;
            string[] styleNameArray;
            string[] separators = { "-" };
            styleNameArray= styleName.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            leftIndent = 18 * int.Parse(styleNameArray[1]) / 2;
            firstLineIndent = -18 * (int.Parse(styleNameArray[1]) - int.Parse(styleNameArray[0])) / 2;
            
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = leftIndent;
                newStyle.ParagraphFormat.FirstLineIndent = firstLineIndent;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 214, 227, 188));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
        }

        /// <summary>
        /// Gets the style for the 1-3 indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        
        public static Word.Style Poetry1Style
        {
            get
            {
                string styleName = "Poetry1";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 0;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0,229,223,236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Poetry 2 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Poetry2Style
        {
            get
            {
                string styleName = "Poetry2";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 18;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Poetry 3 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Poetry3Style
        {
            get
            {
                string styleName = "Poetry3";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 36;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Poetry 4 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Poetry4Style
        {
            get
            {
                string styleName = "Poetry4";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 54;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Poetry 5 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Poetry5Style
        {
            get
            {
                string styleName = "Poetry5";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 72;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Poetry 6 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style Poetry6Style
        {
            get
            {
                string styleName = "Poetry6";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.set_BaseStyle(mybaseStyle);
                newStyle.ParagraphFormat.LeftIndent = 90;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the BoxBegin style in the
        /// Advance Edit part. This appears in regular text. See the Braille version for SimBraille.
        /// Creates a new one if it doesn't exist in the document.
        /// </summary>
        public static Word.Style BoxBegin
        {
            get
            {
                string styleName = "BoxBegin";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                //newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 215, 228, 248));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                newStyle.set_BaseStyle(mybaseStyle);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the BoxEnd style in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style BoxEnd
        {
            get
            {
                string styleName = "BoxEnd";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                //newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 215, 228, 248));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                newStyle.set_BaseStyle(mybaseStyle);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the BoxDouble style in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style BoxDouble
        {
            get
            {
                string styleName = "BoxDouble";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                //newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 215, 228, 248));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                newStyle.set_BaseStyle(mybaseStyle);
                return newStyle;
            }
        }
        public static Word.Style FootnoteReference
        {
            get
            {
                string styleName = "FootnoteReference";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there

                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Color = Word.WdColor.wdColorPink;
                //newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 204, 0, 255));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                return newStyle;
            }
        }
        public static Word.Style FootnoteReferenceNumber
        {
            get
            {
                string styleName = "FootnoteReferenceNumber";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = BrailleBase;
                }
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                //newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Color = Word.WdColor.wdColorPink;
                //newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 204, 0, 255));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the DBT Code style in the
        /// Advance Edit part. This appears in regular text. See the Braille version for SimBraille.
        /// Creates a new one if it doesn't exist in the document.
        /// </summary>
        public static Word.Style DBTCode
        {
            get
            {
                string styleName = "DBT Code";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.Font.Color = Word.WdColor.wdColorPlum;
                
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the Line Number style.
        /// Creates a new one if it doesn't exist in the document.
        /// </summary>
        public static Word.Style LineNumber
        {
            get
            {
                string styleName = "LineNums";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there

                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.Font.Color = Word.WdColor.wdColorTeal;

                return newStyle;
            }
        }

        /// <summary>
        /// Gets the style for the Exact Translation style.
        /// Creates a new one if it doesn't exist in the document.
        /// </summary>
        /// 
        /// <Modifications 11/2/2017>: Added code to handle the problem of headings near a table. 
        /// Previously, the heading used to unexpectedly go inside the first cell of the table.
        /// Handled this by adding a new line between the heading and the table and then deleting it later.
        /// Similar changes done for all other FormantHeadings.
        /// </Modifications 11/2/2017>

        public void FormatHeading1(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading1);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if(selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select(); 
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    }
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading1);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
            
        }
        public static void RunConvertTitleTextHelper(Word.Range range)
        {
            string textSelection = range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                // Dialog box with exclamation icon.
                MessageBox.Show("Title Case cannot be applied, because the selection has an equation or special object.", "Important Note", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (range.Text!= null)
                {
                    range.Case = Word.WdCharacterCase.wdLowerCase;
                    range.Case = Word.WdCharacterCase.wdTitleWord;
                    object pStyle = range.get_Style();
                    range.Text = FindAndReplace(range.Text.ToString());
                    range.set_Style(ref pStyle);
                }
            }
        }
        public void RunConvertTitleText(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            string textSelection = range.WordOpenXML.ToString();
            range.Font.ColorIndex = Word.WdColorIndex.wdDarkYellow;
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                // Dialog box with exclamation icon.
               // MessageBox.Show("Title Case cannot be applied, because the selection has an equation or special object.", "Important Note", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                foreach (Word.Range r in Globals.ThisAddIn.Find(range, @"[a-zA-Z]*^013",useWildcards:true,wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
                {
                    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                    TextInfo textInfo = cultureInfo.TextInfo;
                    if (r.Text != null)
                    {
                        r.Case = Word.WdCharacterCase.wdLowerCase;
                        r.Case = Word.WdCharacterCase.wdTitleWord;
                        object pStyle = r.get_Style();
                        r.Text = FindAndReplace(r.Text.ToString());
                        r.set_Style(ref pStyle);
                    }

                }
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if(range.Text != null){
                    range.Case = Word.WdCharacterCase.wdLowerCase;
                    range.Case = Word.WdCharacterCase.wdTitleWord;
                    object pStyle = range.get_Style();
                    range.Text= FindAndReplace(range.Text.ToString());
                    range.set_Style(ref pStyle);
                }
            }
        }
        public static string FindAndReplace(String text)
        {
            string[] exceptionWordList = new string[] { "A ", "An ", "And", "At ", "But ", "By ", "For ", "In ","Nor ", "Of ", "On ","Or ","So ", "The ", "To ", "With ", "Yet " };
            string text_modified = text;
            foreach(string eword in exceptionWordList){
                StringBuilder subStringPositions_SB= new StringBuilder();
                subStringPositions_SB = FindWordOccurences(text_modified, eword);
                string subStringPositions = subStringPositions_SB.ToString();
                //Exception Case: In case new heading is starting with colon and a whitespace
                int position_colon_whitespace = text_modified.IndexOf(": ");
                int position_tobechecked = -1;
                if (position_colon_whitespace != -1) { position_tobechecked = position_colon_whitespace + 2; }
                
                if (subStringPositions.StartsWith("0"))
                {
                    subStringPositions = subStringPositions.Remove(0, 1);
                }
                
                if(subStringPositions.Contains(position_tobechecked.ToString()))
                {
                    int startposition = subStringPositions.IndexOf(position_tobechecked.ToString());
                    subStringPositions = subStringPositions.Remove(startposition, 2);
                }

                string[] position_cr ;
                string[] separators = { "," };
                position_cr = subStringPositions.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                foreach(string position in position_cr)
                {
                    if (!string.IsNullOrEmpty(text_modified))
                    {
                        char[] a = text_modified.ToCharArray();
                      a[Int32.Parse(position)] = char.ToLower(a[Int32.Parse(position)]);
                      text_modified = new string(a);
                    }
                }
            }
            return text_modified;
        }

        public static StringBuilder FindWordOccurences(String text,String eword)
        {
             
             StringBuilder subStringPositions = new StringBuilder();
             int position = text.IndexOf(eword);
             if(position == -1)
                {
                    Console.WriteLine("Your sub-string was not found in your string.\n");
                    return subStringPositions;
                }
             subStringPositions.Append(Convert.ToString(position) + ",");

             while (position < text.Length)
            {
                // Search restart from the character after the previous found substring
                int position_start = position;
                position = text.IndexOf(eword, position + 1);
                if (position_start.Equals(position)|| position == -1)
                {
                    break;                
                }
                else
                {
                    subStringPositions.Append(Convert.ToString(position) + ","); 
                }
                
            }

            return subStringPositions;
        }
        public void FormatHeading2(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading2);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if (selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select();
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    } 
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading2);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
        }

        public void FormatHeading3(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading3);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if (selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select();
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    }
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading3);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
        }

        public void FormatHeading4(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading4);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if (selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select();
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    } 
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading4);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
        }

        public void FormatHeading5(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading5);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if (selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select();
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    }
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading5);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
        }

        public void FormatHeading6(IRibbonControl control)
        {
            int rangeStart = 0, rangeEnd = 0;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            //Code change for title case
            string textSelection = selection.Paragraphs.First.Range.WordOpenXML.ToString();
            if (textSelection.Contains("m:oMath") || textSelection.Contains("w:object"))
            {
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading6);
            }
            else
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                if (selection.Paragraphs.First.Range.Text != null)
                {
                    if (selection.Text == null)
                    {
                        rangeStart = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.Start;
                        rangeEnd = Globals.ThisAddIn.Application.Selection.Paragraphs[1].Range.End;
                        selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeStart, rangeEnd);
                        selection.Select();
                    }
                    rangeEnd = selection.End;
                    char[] rangeCharArray = selection.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        selection.MoveEnd(Word.WdUnits.wdCharacter, -1);
                    }
                    selection.InsertAfter("\n");
                    if (CapitalizeEachWord)
                    {
                        selection.Paragraphs.First.Range.Text = FindAndReplace(HandleRomanNumerals(selection.Paragraphs.First.Range.Text.ToString()));
                    }
                    object pStyle = selection.Paragraphs.First.Range.get_Style();
                    selection.Paragraphs.First.Range.set_Style(ref pStyle);
                }
                General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleHeading6);
                selection = Globals.ThisAddIn.Application.ActiveDocument.Range(rangeEnd, rangeEnd + 1);
                selection.Delete();
            }
        }

        public string HandleRomanNumerals(string text)
        {
            System.Text.RegularExpressions.Regex rgxRomanNumerals = new System.Text.RegularExpressions.Regex("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            string[] delimiter = { " " };
            string[] words = text.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            for(int i = 0; i<words.Length; i++)
            {
                string tempWord = words[i];
                string tempPunctuation = "";
                char lastcharacter;
        again:  if (tempWord.Length > 0)
                {
                    lastcharacter = tempWord.ElementAt(tempWord.Length - 1);
                    if (!Char.IsLetter(lastcharacter))
                    {
                        tempPunctuation = lastcharacter + tempPunctuation;
                        tempWord = tempWord.Substring(0, tempWord.Length - 1);
                        goto again;
                    }
                }

                if (!rgxRomanNumerals.IsMatch(tempWord.Trim()))
                {
                    tempWord = tempWord.ToLower();
                    char[] a = tempWord.ToCharArray();
                    a[0] = char.ToUpper(a[0]);
                    words[i] = new string(a) + tempPunctuation;
                }
                else
                    words[i] = tempWord + tempPunctuation;
            }
            return String.Join(" ", words);
        }

        public bool CapitalizeEachWord_getPressed(IRibbonControl control)
        {
            return pressedState;
        }

        public void CapitalizeEachWord_onAction(IRibbonControl control, bool pressed)
        {
           CapitalizeEachWord = pressed;
           pressedState = pressed;
        }

        public void FormatTitle(IRibbonControl control)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            General.FormatFirstParagraphStyle(selection, Word.WdBuiltinStyle.wdStyleTitle);
        }

        public void FormatPageNumber(IRibbonControl control)
        {
            General.SetPageStyleToLookLikeDAISY(new SuiteFunctionUpdateArgs());
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            General.FormatCharacterStyleLikeParagraph(selection, Word.WdBuiltinStyle.wdStylePageNumber);
        }

        public void RunParagraphFixerControl(IRibbonControl control)
        {
            ParagraphFixerForm form = new ParagraphFixerForm();
            Globals.ThisAddIn.ShowForm(form);
        }

        #endregion

        #region Math

        public void RunRemoveEquationHighlights(IRibbonControl control)
        {
            Math.RemoveEquationHighlights(new SuiteFunctionUpdateArgs());
        }

        public void RunHighlightEquations(IRibbonControl control)
        {
            Math.HighlightEquations(new SuiteFunctionUpdateArgs());
        }

        public static void RunHighlightSingleEquation()
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            Math.HighlightSingleEquation(range,new SuiteFunctionUpdateArgs());
        }
        public void RunHighlightSingleEquationNonStatic(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            Math.HighlightSingleEquation(range, new SuiteFunctionUpdateArgs());
        }
        public void RunNumbersToMathType(IRibbonControl control)
        {
            Math.ConvertAllNumbersToOMML(new SuiteFunctionUpdateArgs());
            Math.ConvertOMMLToMathType(new SuiteFunctionUpdateArgs());
        }

        public void RunConvertTablesToText(IRibbonControl control)
        {
            General.ConvertTablesToText(new SuiteFunctionUpdateArgs());
        }

        public void RunClearFormattingInTables(IRibbonControl control)
        {
            General.ClearFormattingInTables(new SuiteFunctionUpdateArgs());
        }

        public void InsertOMMLEquation(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            Math.InsertOMMLEquation(range);
        }

        public void InsertMathTypeEquation(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            Math.InsertMathTypeEquation(range);
        }

        public void RunTextToMath(IRibbonControl control)
        {
            Math.ShowTextToMath();
        }

        public bool IsMathTypeInstalled(IRibbonControl control)
        {
            return Math.IsMathTypeInstalled;
        }

        #endregion

        #region Braille

        public void RunBrailleSuite(IRibbonControl control)
        {
            Braille.RunSuite();
        }

        public void RunPageBreakBeforePageNumber(IRibbonControl control)
        {
            Braille.AddPageBreakBeforePageNumbers(new SuiteFunctionUpdateArgs());
        }

        public void RunAddAmpersandsToPageNumbers(IRibbonControl control)
        {
            Braille.AddAmpersandsToPageNumbers(new SuiteFunctionUpdateArgs());
        }

        public void RunRemoveAllAltText(IRibbonControl control)
        {
            General.RemoveAllAltText(new SuiteFunctionUpdateArgs());
        }

        public void RunRemoveRedundantPunctuation(IRibbonControl control)
        {
            Braille.RemoveRedundantPunctuation(new SuiteFunctionUpdateArgs());
        }

        public void RunReplaceImagesWithAltText(IRibbonControl control)
        {
            Braille.ReplaceImagesWithAltText(new SuiteFunctionUpdateArgs());
        }

        public void AddTranscribersNote(IRibbonControl control)
        {
            TranscribersNote.ShowTranscribersNoteDialog();
        }

        public void RunStyleTactileDescription(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            TranscribersNote.StyleAsTactileDescription(range);
        }

        public void RunStyleTactilePageNumber(IRibbonControl control)
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            TranscribersNote.StyleAsTactilePageNumber(range);
        }

        
        #endregion

        #region Computer Code

        public void FormatComputerCodeParagraph(IRibbonControl control)
        {
            Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
            General.FormatParagraphStyle(r, ComputerCode.ParagraphStyle);
        }

        public void FormatComputerCodeInline(IRibbonControl control)
        {
            Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
            General.FormatCharacterStyle(r, ComputerCode.InlineStyle);
        }

        #endregion

        //#region Text Box

        //public void FormatTextBoxParagraph(IRibbonControl control)
        //{
        //    Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
        //    General.FormatParagraphStyle(r, TextBox.ParagraphStyle);
        //}

        //public void FormatTextBoxInline(IRibbonControl control)
        //{
        //    Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
        //    General.FormatCharacterStyle(r, TextBox.InlineStyle);
        //}

        //#endregion

        //#region

        //public void FormatTextBoxParagraph(IRibbonControl control)
        //{
        //    Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
        //    General.FormatParagraphStyle(r, TextBox.ParagraphStyle);
        //}

        //public void FormatTextBoxInline(IRibbonControl control)
        //{
        //    Word.Range r = Globals.ThisAddIn.Application.Selection.Range;
        //    General.FormatCharacterStyle(r, TextBox.InlineStyle);
        //}

        //#endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        public Bitmap GetImage(IRibbonControl control)
        {
            ResourceManager rm = Properties.Resources.ResourceManager;
            return (Bitmap)rm.GetObject(control.Tag);
        }

        public Bitmap GetShowMarkersImage(IRibbonControl control)
        {
            ResourceManager rm = Properties.Resources.ResourceManager;
            if (Globals.ThisAddIn.Application.ActiveWindow.View.ShowAll)
                return (Bitmap)rm.GetObject("ShowMarkersHidden");
            else
                return (Bitmap)rm.GetObject("ShowMarkers");
        }

        #endregion

       
    }
        #endregion
}