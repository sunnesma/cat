﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Core = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    class DAISY
    {
        public static void RunSuite()
        {
            List<SuiteFunction> functions = new List<SuiteFunction>
            {
                General.SaveAsNewCopy,
                DAISY.ConvertPageNumbersToDAISY,
                DAISY.ExtractAltTextFromImages,
                DAISY.RemoveImages,
            };

            General.RunSuite(functions, "DAISY");
        }

        [SuiteDescription("Convert page numbers to DAISY")]
        public static void ConvertPageNumbersToDAISY(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            Word.Style currentStyle = doc.Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            Word.Style daisyStyle = DAISY.PageNumberStyle;

            Globals.ThisAddIn.ReplaceAll(doc.Content, "", "", findStyle: currentStyle, replaceStyle: daisyStyle);
        }

        public static Word.Style PageNumberStyle
        {
            get
            {
                string styleName = "Page Number (DAISY)";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                    Debug.Print("Creating DAISY page number style...");
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStylePageNumber);

                return newStyle;
            }
        }

        [SuiteDescription("Extract alt text from images")]
        public static void ExtractAltTextFromImages(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            int i = 0;
            int count = range.InlineShapes.Count;
            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (args.IsCanceled())
                    break;

                if (s.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    string altText = "";
                    if (s.AlternativeText != null)
                        altText = s.AlternativeText;

                    if (altText.Length > 0)
                    {
                        Word.Range r = s.Range;
                        r.Move(Word.WdUnits.wdParagraph, -1);
                        r.InsertParagraphBefore();
                        TranscribersNote.InsertTranscribersNote(r, altText: altText, haveCloseTag: true);
                    }
                }

                i += 1;
                args.UpdatePercentage(i, count);
            }
        }

        [SuiteDescription("Remove images", optional1: true, optional2: true, optional3: true)]
        public static void RemoveImages(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            // For keeping track of progress
            int i = 0;
            int count = range.InlineShapes.Count;

            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (args.IsCanceled())
                    break;

                if (s.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    s.Delete();
                }

                i += 1;
                args.UpdatePercentage(i, count);
            }
        }
    }
}
