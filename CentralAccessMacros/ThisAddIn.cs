﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Microsoft.Office.Tools;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CentralAccessMacros
{
    public partial class ThisAddIn
    {

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            MyForms = new Dictionary<string, Form>();
            MyTaskPanes = new Dictionary<TaskPaneKey, CustomTaskPane>();

            this.Application.DocumentOpen += Application_DocumentOpen;
            this.Application.DocumentBeforeClose += Application_DocumentBeforeClose;
        }

        void Application_DocumentOpen(Word.Document Doc)
        {
            // Load GuideMe checked data from file, if any-Commented as Check box functionality is not available 
            //GuideMe.LoadCheckedInformation();
        }

        void Application_DocumentBeforeClose(Word.Document Doc, ref bool Cancel)
        {
            // Save GuideMe data for that document-Commented as Check box functionality is not available 
           // GuideMe.SaveCheckedInformation();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new MainRibbon();
        }

        public Word.Document GetActiveDocument()
        {
            return this.Application.ActiveDocument;
        }

        #region Search Functions
        /// <summary>
        /// Finds the search terms in the document using same interface as
        /// Microsofts.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="findTerms"></param>
        /// <param name="forwardDirection"></param>
        /// <param name="useWildcards"></param>
        /// <param name="style"></param>
        /// <returns>Ranges that correspond to search results.</returns>
        public IEnumerable<Word.Range> FindWildCard(Word.Range range, string findTerms,
            bool forwardDirection = true,
            bool useWildcards = true,
            bool matchCase = false,
            bool wholeWords = false,
            Word.WdFindWrap wrapSearch = Word.WdFindWrap.wdFindStop,
            object style = null)
        {
            Word.Range myRange = Globals.ThisAddIn.CopyRange(range);

            Word.Find f = myRange.Find;
            f.ClearFormatting();
            f.Text = findTerms;
            f.Forward = forwardDirection;
            f.MatchWildcards = useWildcards;
            f.MatchCase = matchCase;
            f.MatchWholeWord = wholeWords;
            f.Wrap = wrapSearch;
            if (style != null)
                f.set_Style(style);

            int lastStart = -1;
            int lastEnd = 1;
            
                f.Execute(ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing);

                while (f.Found)
                {
                    if ((myRange.Start != lastStart) || (myRange.End != lastEnd))
                    {
                        lastStart = myRange.Start;
                        lastEnd = myRange.End;
                        yield return Globals.ThisAddIn.CopyRange(myRange);
                        try
                        {
                        f.Execute(ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing);
                        }
                        catch (Exception e)
                        {
                            Debug.Print("Exception" + e);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
           
        }
        /// <summary>
        /// Finds the search terms in the document using same interface as
        /// Microsofts.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="findTerms"></param>
        /// <param name="forwardDirection"></param>
        /// <param name="useWildcards"></param>
        /// <param name="style"></param>
        /// <returns>Ranges that correspond to search results.</returns>
        public IEnumerable<Word.Range> Find(Word.Range range, string findTerms,
            bool forwardDirection=true,
            bool useWildcards=false,
            bool matchCase=false,
            bool wholeWords=false,
            Word.WdFindWrap wrapSearch=Word.WdFindWrap.wdFindStop,
            object style=null)
        {
            Word.Range myRange = CopyRange(range);

            Word.Find f = myRange.Find;
            f.ClearFormatting();
            f.Text = findTerms;
            f.Forward = forwardDirection;
            f.MatchWildcards = useWildcards;
            f.MatchCase = matchCase;
            f.MatchWholeWord = wholeWords;
            f.Wrap = wrapSearch;
            if (style != null)
                f.set_Style(style);

            int lastStart = -1;
            int lastEnd = 1;

            f.Execute(ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing);

            while (f.Found)
            {
                if ((myRange.Start != lastStart) || (myRange.End != lastEnd))
                {
                    lastStart = myRange.Start;
                    lastEnd = myRange.End;
                    yield return CopyRange(myRange);

                    f.Execute(ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing);
                }
                else
                {
                    break;
                }
            }
        }
        
        /// <summary>
        ///Finds and replaces a single occurrence. If it was not successful, it
        ///will return null, otherwise the range of the replaced text.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="findText"></param>
        /// <param name="replaceText"></param>
        /// <param name="useWildcards"></param>
        /// <param name="matchCase"></param>
        /// <param name="fontName"></param>
        /// <param name="findStyle"></param>
        /// <param name="replaceStyle"></param>
        public Word.Range Replace(Word.Range range, string findText,
            string replaceText,
            bool useWildcards = false,
            bool matchCase = false,
            bool wholeWords = false,
            Word.WdFindWrap wrapSearch = Word.WdFindWrap.wdFindStop,
            string fontName = "",
            object findStyle = null,
            object replaceStyle = null)
        {
            Word.Range myRange = CopyRange(range);

            // Set find
            Word.Find f = myRange.Find;
            f.ClearFormatting();
            f.Text = findText;
            f.MatchWildcards = useWildcards;
            f.MatchCase = matchCase;
            f.MatchWholeWord = wholeWords;
            f.Wrap = wrapSearch;
            if (findStyle != null)
                f.set_Style(findStyle);

            if (fontName.Length > 0)
            {
                f.Font.Name = fontName;
            }

            // Set replace
            f.Replacement.ClearFormatting();
            f.Replacement.Text = replaceText;
            if (replaceStyle != null)
                f.Replacement.set_Style(replaceStyle);

            object replace = Word.WdReplace.wdReplaceOne;
            bool success = f.Execute(ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref replace,
                ref missing, ref missing, ref missing, ref missing);

            if (success)
            {
                Debug.Print("Replaced successfully!");
                return myRange;
            }
            else
            {
                Debug.Print("Replace did not work...");
                return null;
            }
        }

        /// <summary>
        /// Finds and replaces all search terms using same interface as
        /// Microsoft's.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="findText"></param>
        /// <param name="replaceText"></param>
        /// <param name="selectRange"></param>
        /// <param name="useWildcards"></param>
        /// <param name="matchCase"></param>
        /// <param name="fontName"></param>
        /// <param name="findStyle"></param>
        /// <param name="replaceStyle"></param>
        /// <param name="func">A function that transforms the replaced range.</param>
        public void ReplaceAll(Word.Range range, string findText,
            string replaceText,
            bool selectRange = false,
            bool useWildcards = false,
            bool matchCase = false,
            string fontName = "",
            object findStyle = null,
            object replaceStyle = null)
        {
            Word.Range myRange = CopyRange(range);

            // Set the settings for the search/replace functions
            Word.Find f = myRange.Find;

            f.ClearFormatting();
            f.Text = findText;
            f.MatchWildcards = useWildcards;
            f.MatchCase = matchCase;
            if (findStyle != null)
                f.set_Style(findStyle);

            if (fontName.Length > 0)
            {
                f.Font.Name = fontName;
            }

            f.Replacement.ClearFormatting();
            f.Replacement.Text = replaceText;
            if (replaceStyle != null)
                f.Replacement.set_Style(replaceStyle);

            object replaceAll = Word.WdReplace.wdReplaceAll;
            f.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref replaceAll, ref missing, ref missing, ref missing, ref missing);

            // Select the range to update UI if requested
            if (selectRange)
            {
                myRange = myRange.Paragraphs.First.Range;
                myRange.Select();
            }
        }

        #endregion

        #region Form Management
        
        /// <summary>
        /// Hold the forms shown and managed by this add-in. It helps to ensure
        /// that there is only 1 of any type of form in the application.
        /// </summary>
        private Dictionary<string, Form> MyForms;

        /// <summary>
        /// Function that shows a form. It enforces singleton status, so if the
        /// form already exists, it will be brought to the front instead of
        /// creating a duplicate window.
        /// 
        /// Returns the Form object actually being shown.
        /// </summary>
        /// <param name="f"></param>
        public Form ShowForm(Form f)
        {
            string key = f.GetType().Name;
            if (MyForms.ContainsKey(key))
            {
                Form myForm = MyForms[key];

                // Bring it up if minimized
                if (myForm.WindowState == FormWindowState.Minimized)
                    myForm.WindowState = FormWindowState.Normal;

                // Bring it to the front
                myForm.BringToFront();
                PositionForm(myForm);
                return myForm;
            }
            else
            {
                if (!f.IsDisposed)
                {
                    f.FormClosed += (sender, e) => MyFormCloseEvent(sender, e, key);
                    MyForms.Add(key, f);
                    f.Show();
                    PositionForm(f);
                    return f;
                }
                else
                {
                    MyForms.Remove(key);
                    return null;
                }
            }
        }

        /// <summary>
        /// Re-shows the form if the form of that type currently exists.
        /// Otherwise, it does nothing.
        /// </summary>
        /// <param name="type"></param>
        public void ReshowForm(Type type)
        {
            string key = type.Name;
            if (MyForms.ContainsKey(key))
            {
                Form myForm = MyForms[key];

                if (myForm.InvokeRequired)
                {
                    myForm.Invoke(new ShowFormByTypeDelegate(ReshowForm), new object[] { type });
                }
                else
                {
                    // Bring it up if minimized
                    if (myForm.WindowState == FormWindowState.Minimized)
                        myForm.WindowState = FormWindowState.Normal;

                    // Bring it to the front
                    myForm.BringToFront();
                    PositionForm(myForm);
                }
            }
        }
        private delegate void ShowFormByTypeDelegate(Type type);

        /// <summary>
        /// Positions the form at the correct place on load-up so that it
        /// doesn't obscure content.
        /// </summary>
        /// <param name="f"></param>
        private void PositionForm(Form f)
        {
            float pointsPerPixel = f.CreateGraphics().DpiX / 72.0f;

            // Get data regarding my active window
            int left = (int)((float)this.Application.ActiveWindow.Left * pointsPerPixel);
            int top = (int)((float)this.Application.ActiveWindow.Top * pointsPerPixel);
            int width = (int)((float)this.Application.ActiveWindow.Width * pointsPerPixel);

            f.Location = new System.Drawing.Point(
                left + width - f.Width - 30,
                top + 140);
        }

        private void MyFormCloseEvent(object sender, FormClosedEventArgs e, string key)
        {
            MyForms.Remove(key);
        }

        #endregion

        #region Task Pane Management

        public static Dictionary<TaskPaneKey, CustomTaskPane> MyTaskPanes { get; set; }

        public class TaskPaneKey
        {
            public TaskPaneKey(UserControl control, Word.Window window)
            {
                ControlKey = control.GetType().FullName;
                WindowKey = window;
            }

            public override bool Equals(object obj)
            {
                TaskPaneKey key = obj as TaskPaneKey;
                return (ControlKey == key.ControlKey) && WindowKey.Equals(key.WindowKey);
            }

            public override int GetHashCode()
            {
                return ControlKey.GetHashCode() ^ WindowKey.GetHashCode();
            }

            public string ControlKey { get; set; }

            public Word.Window WindowKey { get; set; }
        }

        /// <summary>
        /// Shows a task pane. It will enforce singletons.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="name"></param>
        /// <param name="startPosition"></param>
        /// <param name="customWidth"></param>
        /// <param name="customHeight"></param>
        public void ShowTaskPane(UserControl control, string name,
            Office.MsoCTPDockPosition startPosition = Office.MsoCTPDockPosition.msoCTPDockPositionRight,
            int customWidth = -1, int customHeight = -1,
            int posX = -1, int posY = -1,
            Office.MsoCTPDockPositionRestrict restrictPosition = Office.MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNone)
        {
            TaskPaneKey key = new TaskPaneKey(control, this.Application.ActiveWindow);

            if (MyTaskPanes.ContainsKey(key))
            {
                CustomTaskPane p = MyTaskPanes[key];
                p.Visible = true;
                return;
            }

            CustomTaskPane myPane = this.CustomTaskPanes.Add(control, name);
            MyTaskPanes.Add(key, myPane);

            myPane.DockPosition = startPosition;
            myPane.DockPositionRestrict = restrictPosition;

            if (customWidth > 0)
                myPane.Width = customWidth;

            if (customHeight > 0)
                myPane.Height = customHeight;

            //if (myPane.DockPosition == Office.MsoCTPDockPosition.msoCTPDockPositionFloating)
            //{
            float pointsPerPixel = control.CreateGraphics().DpiX / 72.0f;

            // Get data regarding my active window
            int left = (int)((float)this.Application.ActiveWindow.Left * pointsPerPixel);
            int top = (int)((float)this.Application.ActiveWindow.Top * pointsPerPixel);
            int width = (int)((float)this.Application.ActiveWindow.Width * pointsPerPixel);

            if (posX >= 0 && posY >= 0)
            {
                // Make the custom position relative to the current window
                SetCustomPanePositionWhenFloating(myPane, left + posX, top + posY, myPane.Height, myPane.Width);
            }
            else
            {
                // Set it to the top-right corner
                SetCustomPanePositionWhenFloating(myPane,
                    left + width - myPane.Width - 30,
                    top + 140, myPane.Height, myPane.Width);
            }
            //}

            myPane.Visible = true;
            myPane.VisibleChanged += (sender, e) => ActionPane_VisibleChanged(sender, e, key);
        }

        private void ActionPane_VisibleChanged(object sender, EventArgs e, TaskPaneKey key)
        {
            CustomTaskPane p = sender as CustomTaskPane;
            if (!p.Visible)
            {
                this.CustomTaskPanes.Remove(p);
                MyTaskPanes.Remove(key);
            }
        }

        /// <summary>
        /// Set a custom panes position in the undocked state.
        /// </summary>
        /// <param name="customTaskPane">The custom task pane.</param>
        /// <param name="x">The new X position.</param>
        /// <param name="y">The new Y position.</param>
        private void SetCustomPanePositionWhenFloating(CustomTaskPane customTaskPane, int x, int y, int height = 100, int width = 100)
        {
            var oldDockPosition = customTaskPane.DockPosition;
            var oldVisibleState = customTaskPane.Visible;

            customTaskPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionFloating;
            customTaskPane.Visible = true; //The task pane must be visible to set its position

            var window = FindWindowW("MsoCommandBar", customTaskPane.Title); //MLHIDE
            if (window == null) return;

            MoveWindow(window, x, y, customTaskPane.Width, customTaskPane.Height, true);
            customTaskPane.Height = height;
            customTaskPane.Width = width;
            customTaskPane.Visible = oldVisibleState;
            customTaskPane.DockPosition = oldDockPosition;
        }

        [DllImport("user32.dll", EntryPoint = "FindWindowW")]
        public static extern System.IntPtr FindWindowW([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string lpClassName, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "MoveWindow")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool MoveWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, int X, int Y, int nWidth, int nHeight, [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)] bool bRepaint);


        #endregion

        #region Misc

        /// <summary>
        /// Sets the current selection to the beginning.
        /// </summary>
        public void SetSelectionToBeginning()
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            rng.SetRange(1, 1);
            rng.Select();
        }

        /// <summary>
        /// Keeps the value between a specific range. If the value breaches the
        /// maximum or minimum, it will be set as whatever the maximum or
        /// minimum was.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <returns></returns>
        public T Clamp<T>(T value, T max, T min) where T : System.IComparable<T>
        {
            T result = value;
            if (value.CompareTo(max) > 0)
                result = max;
            if (value.CompareTo(min) < 0)
                result = min;
            return result;
        }

        public static BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                return bitmapImage;
            }
        }

        /// <summary>
        /// Returns a copy of the range so that modifying the new one does not
        /// modify the old one.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public Word.Range CopyRange(Word.Range r)
        {
            Word.Range myRange = Globals.ThisAddIn.GetActiveDocument().Content;
            myRange.SetRange(r.Start, r.End);
            return myRange;
        }

        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
