﻿using MTSDKDN;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Contains all of the functions relevant for Math production.
    /// </summary>
    class Math
    {

        #region Suite Functions

        [SuiteDescription("Remove spaces in math equations", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void RemoveSpacesInMath(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Math.FindReplaceAllInMath(range, "[ ]", "", useWildcards: true);
        }

        [SuiteDescription("Linearize all OMML equations",optional1:true,optional2:false,optional3:true)]
        public static void LinearizeOMMLEquations(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            range.OMaths.Linearize();
        }

        [SuiteDescription("Unlinearize all OMML equations", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void UnlinearizeOMMLEquations(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.OMaths collection = range.OMaths;
            int numMaths = range.OMaths.Count;

            //Surprisingly, collection starts at index 1 and not 0. 
            for (int i = 1; i <= numMaths; i++)
            {
                args.UpdatePercentage(i, numMaths);
                collection[i].Range.Select();
                collection[i].Range.OMaths.BuildUp();
            }

        }
        [SuiteDescription("Highlight single equations")]
        public static void HighlightSingleEquation(Word.Range range,SuiteFunctionUpdateArgs args)
        {
            HighlightMathType(range, args);
            HighlightOMML(range, args);
        }

        [SuiteDescription("Highlight all equations",optional1:true,optional2:false,optional3:true)]
        public static void HighlightEquations(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            HighlightMathType(range, args);
            HighlightOMML(range, args);
        }

        public static void HighlightOMML(Word.Range range, SuiteFunctionUpdateArgs args)
        {
            System.Drawing.Color ommlColor = System.Drawing.Color.FromArgb(1, 94, 130);

            Word.WdBorderType[] borders = new Word.WdBorderType[]
            {
                Word.WdBorderType.wdBorderLeft,
                Word.WdBorderType.wdBorderRight,
                Word.WdBorderType.wdBorderTop,
                Word.WdBorderType.wdBorderBottom
            };

            // OMML Equations
            int currentMath = 0;
            int numMaths = range.OMaths.Count;
            foreach (Word.OMath math in range.OMaths)
            {
                if (args.IsCanceled())
                    break;

                currentMath += 1;
                args.UpdatePercentage(currentMath, numMaths);

                foreach (Word.WdBorderType t in borders)
                {
                    Word.Border b = math.Range.Borders[t];
                    b.LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    b.LineWidth = Word.WdLineWidth.wdLineWidth225pt;
                    b.Color = (Word.WdColor)System.Drawing.ColorTranslator.ToOle(ommlColor);
                }
            }
        }

        public static void HighlightMathType(Word.Range range, SuiteFunctionUpdateArgs args)
        {
            //System.Drawing.Color mathTypeColor = System.Drawing.Color.FromArgb(95, 0, 112);
            System.Drawing.Color mathTypeColor = System.Drawing.Color.FromArgb(160, 48, 112);

            Word.WdBorderType[] borders = new Word.WdBorderType[]
            {
                Word.WdBorderType.wdBorderLeft,
                Word.WdBorderType.wdBorderRight,
                Word.WdBorderType.wdBorderTop,
                Word.WdBorderType.wdBorderBottom
            };

            // MathType Equations
            int currentMath = 0;
            int numMaths = range.InlineShapes.Count;
            foreach (Word.InlineShape equation in range.InlineShapes)
            {
                if (args.IsCanceled())
                    break;

                currentMath += 1;
                args.UpdatePercentage(currentMath, numMaths);

                if (equation.Type == Word.WdInlineShapeType.wdInlineShapeEmbeddedOLEObject)
                {
                    foreach (Word.WdBorderType t in borders)
                    {
                        try
                        {
                            Word.Border b = equation.Borders[t];
                            b.LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                            b.LineWidth = Word.WdLineWidth.wdLineWidth225pt;
                            b.Color = (Word.WdColor)System.Drawing.ColorTranslator.ToOle(mathTypeColor);
                        }
                        catch (COMException ex)
                        {
                            // Sometimes, it just fails
                            Debug.Print("ERROR: Couldn't finish highlighting MathType equation.");
                        }
                    }
                    equation.Borders.Shadow = false;
                }
            }
        }

        [SuiteDescription("Remove equation highlights")]
        public static void RemoveEquationHighlights(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.InlineShape equation in range.InlineShapes)
            {
                if (equation.Type == Word.WdInlineShapeType.wdInlineShapeEmbeddedOLEObject)
                {
                    equation.Borders.Enable = (int)VBABoolean.False;
                }
            }

            foreach (Word.OMath equation in range.OMaths)
            {
                equation.Range.Borders.Enable = (int)VBABoolean.False;
            }
        }
        [SuiteDescription("Replace () with 0s", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void ReplaceEmptyParensWithZero(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Math.FindReplaceAllInMath(range, "()", "0");
        }

        [SuiteDescription("Correct Unicode in math", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void FixUnicodeCharacters(SuiteFunctionUpdateArgs args)
        {
            Dictionary<string, string> replaceMap = new Dictionary<string, string>
            {
                // All codes are in decimal
                {"^u8211", " -- "},
                {"^u8212", " -- "},
                {"^u173", ""},
                {"^u224", "a"},
                {"^u233", "e"},
                {"^u8194", " "},
                {"^u8195", " "},
                {"^u8216", "'"},
                {"^u8217", "'"},
                {"^u8220", "\"\""},
                {"^u8221", "\"\""},
                {"^u8230", "..."},
                {"^u8226", " "},
                {"^u172", ""},
                {"^u45", ""},
                {"^u9633", ""}
            };

            // Replace all occurrences of each item in the dictionary
            int currentItem = 0;
            foreach (string k in replaceMap.Keys)
            {
                if (args.IsCanceled())
                    break;

                currentItem += 1;
                args.UpdatePercentage(currentItem, replaceMap.Keys.Count);

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Math.FindReplaceAllInMath(range, k, replaceMap[k], matchCase: true);
            }

        }

        [SuiteDescription("Replace dot with times", optional1: true,optional2: true,optional3:true)]
        public static void ReplaceDotWithTimes(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, "^u8901", "\u00d7", matchCase: true);
        }

        [SuiteDescription("Convert numbers to OMML", optional1: true, optional2: true, optional3: true, optional4: false)]
        public static void ConvertAllNumbersToOMML(SuiteFunctionUpdateArgs args)
        {
            Math.RemoveCommasInNumbers();

            string[] symbolSearches = 
            {
                @"[0-9]{1,}.[0-9]{1,}",
                @".[0-9]{1,}",
                @"[0-9]{1,}",
            };

            List<Word.Range> myRanges = new List<Word.Range>();
            int currentSearchNum = 0;
            double ratio = 1.0 / (double)symbolSearches.Length;

            foreach (string search in symbolSearches)
            {
                currentSearchNum += 1;
                if (args.IsCanceled())
                    break;

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                foreach (Word.Range r in Globals.ThisAddIn.Find(range, search, useWildcards: true))
                {
                    // Calculate and update my percentage
                    double percent = (ratio * ((double)currentSearchNum / symbolSearches.Length)) + (ratio * 0.5 * ((double)r.End / range.End));
                    args.UpdateProgress((int)(percent * 100));

                    // Only do change if the number isn't styled as a page
                    // number
                    Word.Style myStyle = (Word.Style) r.CharacterStyle;
                    if (myStyle.NameLocal != Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber].NameLocal)
                    {
                        myRanges.Add(r);
                    }
                }

                // Convert all ranges I found into OMML
                foreach (Word.Range r in myRanges)
                {
                    // Calculate and update my percentage
                    double percent = (ratio * ((double)currentSearchNum / symbolSearches.Length)) + (ratio * (0.5 * ((double)r.End / range.End) + 0.5));
                    args.UpdateProgress((int)(percent * 100));
                    r.OMaths.Add(r);
                }
                myRanges.Clear();
            }
        }

        public static void RemoveCommasInNumbers()
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range myRange = null;
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, @"<[0-9]{1,3}(,[0-9]{3}){1,}>", useWildcards: true))
            {
                if (myRange == null)
                {
                    // Make copy of another range before I modify it
                    myRange = Globals.ThisAddIn.GetActiveDocument().Content;
                    myRange.SetRange(r.Start, r.End);
                }

                // If the search found was a continuation, then add it to my
                // range. Otherwise, remove the commas in it and restart
                if (myRange.End >= r.Start)
                {
                    myRange.End = r.End;
                }
                else
                {
                    Globals.ThisAddIn.ReplaceAll(myRange, ",", "");
                    myRange = null;
                }
            }

            // Process the last lingering range
            if (myRange != null)
            {
                Globals.ThisAddIn.ReplaceAll(myRange, ",", "");
            }
        }

        /// <summary>
        /// Returns the default font for math in OMML.
        /// </summary>
        /// <returns></returns>
        public static string GetMathFont()
        {
            return "Cambria Math";
        }

        /// <summary>
        /// Styles all math in the default math font. This makes it easier to
        /// perform fast search-and-replace operations for math later.
        /// </summary>
        [SuiteDescription("Style math in default math font")]
        public static void StyleMathInDefaultFont(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.OMath math in range.OMaths)
            {
                math.Range.Font.Name = Math.GetMathFont();
            }
        }

        #endregion

        /// <summary>
        /// Finds and replaces text inside of OMML math equations. To work, the
        /// font in the OMML must match the font in Math.GetMathFont(). Use
        /// Math.StyleMathInDefaultFont() to set the equations to that font.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="findTerms"></param>
        /// <param name="replaceTerms"></param>
        /// <param name="useWildcards"></param>
        public static void FindReplaceAllInMath(Word.Range range, string findTerms, string replaceTerms, bool useWildcards = false, bool matchCase = false)
        {
            if (range.OMaths.Count > 0)
            {
                Globals.ThisAddIn.ReplaceAll(range, findTerms, replaceTerms, useWildcards: useWildcards, matchCase: matchCase, fontName: Math.GetMathFont());
            }
        }

        public static void InsertOMMLEquation(Word.Range range)
        {
            Word.Range mathRange = Globals.ThisAddIn.GetActiveDocument().Content.OMaths.Add(range);
            HighlightOMML(mathRange, new SuiteFunctionUpdateArgs());
        }

        public static void InsertMathTypeEquation(Word.Range range)
        {
            if (Math.IsMathTypeInstalled)
            {
                Word.InlineShape myEquation = range.InlineShapes.AddOLEObject("Equation.DSMT4", "", false, false);
                HighlightMathType(myEquation.Range, new SuiteFunctionUpdateArgs());
            }
        }

        public static Form ShowTextToMath()
        {
            MathReplaceForm form = new MathReplaceForm();
            return Globals.ThisAddIn.ShowForm(form);
        }

        #region MathType Functions

        private struct ConvertEquationsInfo
        {
            public bool showStats;
            public long equationTypes;
            public bool selectionOnly;
            public bool promptUser;
            public string translatorFileName;
            public long translatorOptions;
            public long count;
        }

        //Public Const mt_CVT_FIND_MTEE       As Long = 1 'find MathType and EE equations
        //Public Const mt_CVT_FIND_WORDEQ     As Long = 2 'find Word EQ fields
        //Public Const mt_CVT_FIND_TRANSLATOR As Long = 4 'find translated (text) equations
        //Public Const mt_CVT_TRAN_NAME As Long = 1 'include translator name in translation
        //Public Const mt_CVT_TRAN_MTEF As Long = 2 'include MathType data in translation
        private static long MT_CVT_FIND_MTEE = 1;
        private static long MT_CVT_FIND_WORDEQ = 2;
        private static long MT_CVT_FIND_TRANSLATOR = 4;
        private static long MT_CVT_TRAN_NAME = 1;
        private static long MT_CVT_TRAN_MTEF = 2;

        /// <summary>
        /// Returns true if MathType is installed.
        /// </summary>
        public static bool IsMathTypeInstalled
        {
            get
            {
                try
                {
                   // MathTypeSDK mathtype = MathTypeSDK.Instance;
                    string dynamicDll = DynamicDLL.getDLL();
                    string MATHTYPE_DLL_DEFAULT = @"C:\Program Files (x86)\MathType\System\MT6.dll";
                    string MATHTYPE_DLL_32 = @"C:\Program Files (x86)\MathType\System\32\MT6.dll";
                    string MATHTYPE_DLL_64 = @"C:\Program Files (x86)\MathType\System\64\MT6.dll";
                    if (dynamicDll.Equals(MATHTYPE_DLL_DEFAULT,StringComparison.Ordinal)){
                        MathTypeSDK_DEFAULT mathtype = MathTypeSDK_DEFAULT.Instance;
                        return mathtype.Installed;
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                    {
                        MathTypeSDK_32 mathtype = MathTypeSDK_32.Instance;
                        return mathtype.Installed;
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                    {
                        MathTypeSDK_64 mathtype = MathTypeSDK_64.Instance;
                        return mathtype.Installed;
                    }
                    else
                    {
                        MathTypeSDK_DEFAULT mathtype = MathTypeSDK_DEFAULT.Instance;
                        return mathtype.Installed;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Converts all OMML equations into MathType. This will not run if
        /// MathType is not installed
        /// </summary>
        [SuiteDescription("Convert all OMML to MathType",optional1:true,optional2:false,optional3:true)]
        public static void ConvertOMMLToMathType(SuiteFunctionUpdateArgs args)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;

            //Kill MathType process
            foreach (Process proc in Process.GetProcessesByName("MathType"))
                proc.Kill();

            if (Math.IsMathTypeInstalled)
            {
                Globals.ThisAddIn.Application.Options.ReplaceSelection = true;

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                try
                {
                    //MathTypeSDK mathType = MathTypeSDK.Instance;

                    string dynamicDll = DynamicDLL.getDLL();
                    string MATHTYPE_DLL_32 = @"C:\Program Files (x86)\MathType\System\32\MT6.dll";
                    string MATHTYPE_DLL_64 = @"C:\Program Files (x86)\MathType\System\64\MT6.dll";

                    MathTypeSDK_DEFAULT mathType_default = null;
                    MathTypeSDK_32 mathType_32 = null;
                    MathTypeSDK_64 mathType_64 = null;

                    if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                    {
                        mathType_32 = MathTypeSDK_32.Instance;
                        mathType_32.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType_32.MTXFormResetMgn();
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                    {
                        mathType_64 = MathTypeSDK_64.Instance;
                        mathType_64.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType_64.MTXFormResetMgn();
                    }
                    else
                    {
                        mathType_default = MathTypeSDK_DEFAULT.Instance;
                        mathType_default.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType_default.MTXFormResetMgn();
                    }

                    XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                    ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                    int numMaths = range.OMaths.Count;
                    int currentMath = 0;
                    int tryAgainOnErrorCount = 0;

                    foreach (Word.OMath math in range.OMaths)
                    {
                        tryAgainOnErrorCount = 0;
                        if (args.IsCanceled())
                            break;

                        //Selecting the current equation for visual progress to the user.
                        selection.SetRange(math.Range.Start, math.Range.Start);
                        selection.Select();

                        currentMath++;

                        //Kill MathType process and reset connection with MathType after every 5 equations are converted. 
                        if (currentMath % 6 == 0)
                        {
                            foreach (Process proc in Process.GetProcessesByName("MathType"))
                            {
                                //Close the MathType connection.
                                if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                                    mathType_32.MTAPIDisconnectMgn();
                                else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                                    mathType_64.MTAPIDisconnectMgn();
                                else
                                    mathType_default.MTAPIDisconnectMgn();

                                //Kill the MathType process.
                                proc.Kill();

                                //Wait for garbage collection.
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                GC.WaitForPendingFinalizers();

                                //Re-initiate MathType connection
                                if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                                {
                                    mathType_32 = MathTypeSDK_32.Instance;
                                    mathType_32.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                    mathType_32.MTXFormResetMgn();
                                }
                                else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                                {
                                    mathType_64 = MathTypeSDK_64.Instance;
                                    mathType_64.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                    mathType_64.MTXFormResetMgn();
                                }
                                else
                                {
                                    mathType_default = MathTypeSDK_DEFAULT.Instance;
                                    mathType_default.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                    mathType_default.MTXFormResetMgn();
                                }

                                ommlToMathML = new XslCompiledTransform();
                                ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));
                            }
                        }

                        // Show progress of conversion
                        args.UpdatePercentage(currentMath, numMaths);

                        XmlReader xml = null;
                        while (xml == null)
                        {
                            try
                            {
                                xml = XmlReader.Create(new StringReader(math.Range.WordOpenXML));
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }

                        StringWriter mathmlStringWriter = new StringWriter();
                        XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                        // Do actual XSL transformation
                        ommlToMathML.Transform(xml, mathmlWriter);
                        mathmlStringWriter.Close();
                        string mathml = mathmlStringWriter.ToString();

                        Math.MathTypeData mathmlData = new Math.MathTypeData(mathml);

                        // Convert MathML to MathType and place on clipboard
                        MTAPI_DIMS dims = new MTAPI_DIMS();
                        int destLength = 10000;
                        StringBuilder stringDest = new StringBuilder(destLength);

                        int result;

                    again: if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                        {
                            result = mathType_32.MTXFormEqnMgn(MTXFormEqn.mtxfmLOCAL, MTXFormEqn.mtxfmMTEF, mathmlData.MtefData,
                            mathmlData.MtefDataLength, MTXFormEqn.mtxfmCLIPBOARD, MTXFormEqn.mtxfmTEXT, stringDest, destLength,
                            "", ref dims);

                        }
                        else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                        {
                            result = mathType_64.MTXFormEqnMgn(MTXFormEqn.mtxfmLOCAL, MTXFormEqn.mtxfmMTEF, mathmlData.MtefData,
                            mathmlData.MtefDataLength, MTXFormEqn.mtxfmCLIPBOARD, MTXFormEqn.mtxfmTEXT, stringDest, destLength,
                            "", ref dims);
                        }
                        else
                        {
                            result = mathType_default.MTXFormEqnMgn(MTXFormEqn.mtxfmLOCAL, MTXFormEqn.mtxfmMTEF, mathmlData.MtefData,
                            mathmlData.MtefDataLength, MTXFormEqn.mtxfmCLIPBOARD, MTXFormEqn.mtxfmTEXT, stringDest, destLength,
                            "", ref dims);
                        }

                        if (result != MathTypeReturnValue.mtOK)
                        {
                            Debug.Print("MathType Error: {0}", result);
                            if (tryAgainOnErrorCount++ < 2)
                            {
                                foreach (Process proc in Process.GetProcessesByName("MathType"))
                                {
                                    //Close the MathType connection.
                                    if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                                        mathType_32.MTAPIDisconnectMgn();
                                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                                        mathType_64.MTAPIDisconnectMgn();
                                    else
                                        mathType_default.MTAPIDisconnectMgn();

                                    //Kill the MathType process.
                                    proc.Kill();

                                    //Wait for garbage collection.
                                    GC.WaitForPendingFinalizers();
                                    GC.Collect();
                                    GC.WaitForPendingFinalizers();

                                    //Re-initiate MathType connection
                                    if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                                    {
                                        mathType_32 = MathTypeSDK_32.Instance;
                                        mathType_32.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                        mathType_32.MTXFormResetMgn();
                                    }
                                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                                    {
                                        mathType_64 = MathTypeSDK_64.Instance;
                                        mathType_64.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                        mathType_64.MTXFormResetMgn();
                                    }
                                    else
                                    {
                                        mathType_default = MathTypeSDK_DEFAULT.Instance;
                                        mathType_default.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                                        mathType_default.MTXFormResetMgn();
                                    }

                                    ommlToMathML = new XslCompiledTransform();
                                    ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));
                                }
                                goto again;
                            }
                            else
                            {
                                MessageBox.Show("There was an error while converting OMML equations to MathType. " +
                                    "Please go to Ribbon-> MathType -> Convert Eqations to manually convert the equations." +
                                    "\n\nThe suite will skip converting equations to MathType.", caption: "CAT", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Warning);
                                break;
                            }
                        }

                        // Paste it over the math (might need to attempt this several times)
                        bool gotPasted = false;
                        while (!gotPasted)
                        {
                            try
                            {
                                //System.Threading.Thread.Sleep(100);
                                Word.Range myRange = math.Range;
                                myRange.Paste();
                                gotPasted = true;
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }
                        System.Threading.Thread.Sleep(50);
                    }

                    //Close the MathType connection.
                    if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                        mathType_32.MTAPIDisconnectMgn();
                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                        mathType_64.MTAPIDisconnectMgn();
                    else
                        mathType_default.MTAPIDisconnectMgn();
                }

                catch
                {
                    Debug.Print("MathType sdk is not initialized.Please check user administrator settings");
                }
            }
            else
            {
                MessageBox.Show("1.Check that MathType 6.7 or newer is installed" + "\n" + "2.Ensure you have executable permission(ability to alter files) in the MathType program folder(C:\\Program Files(x86)\\MathType\\System\\MT6.dll).Check with your administrator for more information." + "\n\n" + "If MathType is installed ,then you can run the process manually: Word Ribbon > MathType(tab) >Convert Equations", "Unable to convert OMML to MathType");
                Debug.Print("MathType is not installed!");
            }
        }


        public static void ConvertRangeToMath(Word.Range range)
        {
            Globals.ThisAddIn.Application.Options.ReplaceSelection = true;

            Word.Range omath = range.OMaths.Add(range.Duplicate);
            omath.OMaths.BuildUp();

            // Sometimes, the range of the OMML object is lost after the buildup function is executed. 
            // Hence, resetting the range of that object, by finding the omath object in the same paragraph.
            foreach (Word.OMath math in range.Paragraphs[1].Range.OMaths)
            {
                range = math.Range;

                if (Math.IsMathTypeInstalled)
                {
                    string dynamicDll = DynamicDLL.getDLL();
                    string MATHTYPE_DLL_DEFAULT = @"C:\Program Files (x86)\MathType\System\MT6.dll";
                    string MATHTYPE_DLL_32 = @"C:\Program Files (x86)\MathType\System\32\MT6.dll";
                    string MATHTYPE_DLL_64 = @"C:\Program Files (x86)\MathType\System\64\MT6.dll";

                    if (dynamicDll.Equals(MATHTYPE_DLL_DEFAULT, StringComparison.Ordinal))
                    {
                        MathTypeSDK_DEFAULT mathType = MathTypeSDK_DEFAULT.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        XmlReader xml = null;
                        while (xml == null)
                        {
                            try
                            {
                                xml = XmlReader.Create(new StringReader(range.WordOpenXML));
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }

                        StringWriter mathmlStringWriter = new StringWriter();
                        XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                        // Do actual XSL transformation
                        ommlToMathML.Transform(xml, mathmlWriter);
                        mathmlStringWriter.Close();
                        string mathml = mathmlStringWriter.ToString();

                        MathTypeData mathmlData = new MathTypeData(mathml);

                        // Convert MathML to MathType and place on clipboard
                        MTAPI_DIMS dims = new MTAPI_DIMS();
                        int destLength = 10000;
                        StringBuilder stringDest = new StringBuilder(destLength);

                        int result = mathType.MTXFormEqnMgn(
                            MTXFormEqn.mtxfmLOCAL,
                            MTXFormEqn.mtxfmMTEF,
                            mathmlData.MtefData,
                            mathmlData.MtefDataLength,
                            MTXFormEqn.mtxfmCLIPBOARD,
                            MTXFormEqn.mtxfmTEXT,
                            stringDest,
                            destLength,
                            "",
                            ref dims);

                        if (result != MathTypeReturnValue.mtOK)
                        {
                            Debug.Print("MathType Error: {0}", result);
                        }

                        // Paste it over the math (might need to attempt this several times)
                        bool gotPasted = false;
                        while (!gotPasted)
                        {
                            try
                            {
                                range.Paste();
                                gotPasted = true;
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                    {
                        MathTypeSDK_32 mathType = MathTypeSDK_32.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        XmlReader xml = null;
                        while (xml == null)
                        {
                            try
                            {
                                xml = XmlReader.Create(new StringReader(range.WordOpenXML));
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }

                        StringWriter mathmlStringWriter = new StringWriter();
                        XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                        // Do actual XSL transformation
                        ommlToMathML.Transform(xml, mathmlWriter);
                        mathmlStringWriter.Close();
                        string mathml = mathmlStringWriter.ToString();

                        MathTypeData mathmlData = new MathTypeData(mathml);

                        // Convert MathML to MathType and place on clipboard
                        MTAPI_DIMS dims = new MTAPI_DIMS();
                        int destLength = 10000;
                        StringBuilder stringDest = new StringBuilder(destLength);

                        int result = mathType.MTXFormEqnMgn(
                            MTXFormEqn.mtxfmLOCAL,
                            MTXFormEqn.mtxfmMTEF,
                            mathmlData.MtefData,
                            mathmlData.MtefDataLength,
                            MTXFormEqn.mtxfmCLIPBOARD,
                            MTXFormEqn.mtxfmTEXT,
                            stringDest,
                            destLength,
                            "",
                            ref dims);

                        if (result != MathTypeReturnValue.mtOK)
                        {
                            Debug.Print("MathType Error: {0}", result);
                        }

                        // Paste it over the math (might need to attempt this several times)
                        bool gotPasted = false;
                        while (!gotPasted)
                        {
                            try
                            {
                                range.Paste();
                                gotPasted = true;
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                    {
                        MathTypeSDK_64 mathType = MathTypeSDK_64.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        XmlReader xml = null;
                        while (xml == null)
                        {
                            try
                            {
                                xml = XmlReader.Create(new StringReader(range.WordOpenXML));
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }

                        StringWriter mathmlStringWriter = new StringWriter();
                        XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                        // Do actual XSL transformation
                        ommlToMathML.Transform(xml, mathmlWriter);
                        mathmlStringWriter.Close();
                        string mathml = mathmlStringWriter.ToString();

                        MathTypeData mathmlData = new MathTypeData(mathml);

                        // Convert MathML to MathType and place on clipboard
                        MTAPI_DIMS dims = new MTAPI_DIMS();
                        int destLength = 10000;
                        StringBuilder stringDest = new StringBuilder(destLength);

                        int result = mathType.MTXFormEqnMgn(
                            MTXFormEqn.mtxfmLOCAL,
                            MTXFormEqn.mtxfmMTEF,
                            mathmlData.MtefData,
                            mathmlData.MtefDataLength,
                            MTXFormEqn.mtxfmCLIPBOARD,
                            MTXFormEqn.mtxfmTEXT,
                            stringDest,
                            destLength,
                            "",
                            ref dims);

                        if (result != MathTypeReturnValue.mtOK)
                        {
                            Debug.Print("MathType Error: {0}", result);
                        }

                        // Paste it over the math (might need to attempt this several times)
                        bool gotPasted = false;
                        while (!gotPasted)
                        {
                            try
                            {
                                range.Paste();
                                gotPasted = true;
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }
                    }
                    else
                    {
                        MathTypeSDK_DEFAULT mathType = MathTypeSDK_DEFAULT.Instance;
                        mathType.MTAPIConnectMgn(MTApiStartValues.mtinitLAUNCH_NOW, -1);
                        mathType.MTXFormResetMgn();

                        // Transform the OMML inside to MathML
                        XslCompiledTransform ommlToMathML = new XslCompiledTransform();
                        ommlToMathML.Load(XmlReader.Create(new StringReader(Properties.Resources.OMML2MML)));

                        XmlReader xml = null;
                        while (xml == null)
                        {
                            try
                            {
                                xml = XmlReader.Create(new StringReader(range.WordOpenXML));
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }

                        StringWriter mathmlStringWriter = new StringWriter();
                        XmlWriter mathmlWriter = XmlWriter.Create(mathmlStringWriter);

                        // Do actual XSL transformation
                        ommlToMathML.Transform(xml, mathmlWriter);
                        mathmlStringWriter.Close();
                        string mathml = mathmlStringWriter.ToString();

                        MathTypeData mathmlData = new MathTypeData(mathml);

                        // Convert MathML to MathType and place on clipboard
                        MTAPI_DIMS dims = new MTAPI_DIMS();
                        int destLength = 10000;
                        StringBuilder stringDest = new StringBuilder(destLength);

                        int result = mathType.MTXFormEqnMgn(
                            MTXFormEqn.mtxfmLOCAL,
                            MTXFormEqn.mtxfmMTEF,
                            mathmlData.MtefData,
                            mathmlData.MtefDataLength,
                            MTXFormEqn.mtxfmCLIPBOARD,
                            MTXFormEqn.mtxfmTEXT,
                            stringDest,
                            destLength,
                            "",
                            ref dims);

                        if (result != MathTypeReturnValue.mtOK)
                        {
                            Debug.Print("MathType Error: {0}", result);
                        }

                        // Paste it over the math (might need to attempt this several times)
                        bool gotPasted = false;
                        while (!gotPasted)
                        {
                            try
                            {
                                range.Paste();
                                gotPasted = true;
                            }
                            catch (COMException ex)
                            {
                                Debug.Print("Application busy. Trying again...");
                            }
                        }
                    }

                }
            }
            omath.MoveStart(Word.WdUnits.wdParagraph, -1);
            omath.MoveEnd(Word.WdUnits.wdParagraph, 1);

            Math.HighlightOMML(omath, new SuiteFunctionUpdateArgs());
            Math.HighlightMathType(omath, new SuiteFunctionUpdateArgs());
        }

        public class MathTypeData
        {

            public MathTypeData(string mathmlXML)
            {
                string dynamicDll = DynamicDLL.getDLL();
                    string MATHTYPE_DLL_DEFAULT = @"C:\Program Files (x86)\MathType\System\MT6.dll";
                    string MATHTYPE_DLL_32 = @"C:\Program Files (x86)\MathType\System\32\MT6.dll";
                    string MATHTYPE_DLL_64 = @"C:\Program Files (x86)\MathType\System\64\MT6.dll";

                    if (dynamicDll.Equals(MATHTYPE_DLL_DEFAULT, StringComparison.Ordinal))
                    {
                        System.Runtime.InteropServices.ComTypes.IDataObject dataObject = MathTypeSDK_DEFAULT.getIDataObject();

                        if (dataObject == null)
                        {
                            Debug.Print("Could not create data object!");
                        }

                        FORMATETC formatEtc = new FORMATETC();
                        STGMEDIUM stgMedium = new STGMEDIUM();

                        try
                        {
                            // Setup the formatting information to use for the conversion.
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathML").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_HGLOBAL;

                            // Setup the MathML content to convert
                            stgMedium.unionmember = Marshal.StringToHGlobalAuto(mathmlXML);
                            stgMedium.tymed = TYMED.TYMED_HGLOBAL;
                            stgMedium.pUnkForRelease = 0;

                            // Perform the conversion
                            dataObject.SetData(ref formatEtc, ref stgMedium, false);

                            // Set the format for the output
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathType EF").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_ISTORAGE;

                            // Create a blank data structure to hold the converted result.
                            stgMedium = new STGMEDIUM();
                            stgMedium.tymed = TYMED.TYMED_NULL;
                            stgMedium.pUnkForRelease = 0;

                            // Get the conversion result in MTEF format
                            dataObject.GetData(ref formatEtc, out stgMedium);
                        }
                        catch (COMException e)
                        {
                            Debug.Print("MathML conversion to MathType threw an exception: " + Environment.NewLine + e.ToString());
                        }

                        // The pointer now becomes a Handle reference.
                        HandleRef handleRef = new HandleRef(null, stgMedium.unionmember);

                        try
                        {
                            // Lock in the handle to get the pointer to the data
                            IntPtr ptrToHandle = MathTypeSDK_DEFAULT.GlobalLock(handleRef);

                            // Get the size of the memory block
                            MtefDataLength = MathTypeSDK_DEFAULT.GlobalSize(handleRef);

                            // New an array of bytes and Marshal the data across.
                            MtefData = new byte[MtefDataLength];
                            Marshal.Copy(ptrToHandle, MtefData, 0, MtefDataLength);
                            MtefString = System.Text.ASCIIEncoding.ASCII.GetString(MtefData);
                        }
                        catch (Exception e)
                        {
                            Debug.Print("Generation of image from MathType failed: " + Environment.NewLine + e.ToString());
                        }
                        finally
                        {
                            MathTypeSDK_DEFAULT.GlobalUnlock(handleRef);
                        }
                    }
                    else if (dynamicDll.Equals(MATHTYPE_DLL_32, StringComparison.Ordinal))
                    {
                        System.Runtime.InteropServices.ComTypes.IDataObject dataObject = MathTypeSDK_32.getIDataObject();

                        if (dataObject == null)
                        {
                            Debug.Print("Could not create data object!");
                        }

                        FORMATETC formatEtc = new FORMATETC();
                        STGMEDIUM stgMedium = new STGMEDIUM();

                        try
                        {
                            // Setup the formatting information to use for the conversion.
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathML").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_HGLOBAL;

                            // Setup the MathML content to convert
                            stgMedium.unionmember = Marshal.StringToHGlobalAuto(mathmlXML);
                            stgMedium.tymed = TYMED.TYMED_HGLOBAL;
                            stgMedium.pUnkForRelease = 0;

                            // Perform the conversion
                            dataObject.SetData(ref formatEtc, ref stgMedium, false);

                            // Set the format for the output
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathType EF").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_ISTORAGE;

                            // Create a blank data structure to hold the converted result.
                            stgMedium = new STGMEDIUM();
                            stgMedium.tymed = TYMED.TYMED_NULL;
                            stgMedium.pUnkForRelease = 0;

                            // Get the conversion result in MTEF format
                            dataObject.GetData(ref formatEtc, out stgMedium);
                        }
                        catch (COMException e)
                        {
                            Debug.Print("MathML conversion to MathType threw an exception: " + Environment.NewLine + e.ToString());
                        }

                        // The pointer now becomes a Handle reference.
                        HandleRef handleRef = new HandleRef(null, stgMedium.unionmember);

                        try
                        {
                            // Lock in the handle to get the pointer to the data
                            IntPtr ptrToHandle = MathTypeSDK_32.GlobalLock(handleRef);

                            // Get the size of the memory block
                            MtefDataLength = MathTypeSDK_32.GlobalSize(handleRef);

                            // New an array of bytes and Marshal the data across.
                            MtefData = new byte[MtefDataLength];
                            Marshal.Copy(ptrToHandle, MtefData, 0, MtefDataLength);
                            MtefString = System.Text.ASCIIEncoding.ASCII.GetString(MtefData);
                        }
                        catch (Exception e)
                        {
                            Debug.Print("Generation of image from MathType failed: " + Environment.NewLine + e.ToString());
                        }
                        finally
                        {
                            MathTypeSDK_32.GlobalUnlock(handleRef);
                        }
                    }
                    else if(dynamicDll.Equals(MATHTYPE_DLL_64, StringComparison.Ordinal))
                    {
                        System.Runtime.InteropServices.ComTypes.IDataObject dataObject = MathTypeSDK_64.getIDataObject();

                        if (dataObject == null)
                        {
                            Debug.Print("Could not create data object!");
                        }

                        FORMATETC formatEtc = new FORMATETC();
                        STGMEDIUM stgMedium = new STGMEDIUM();

                        try
                        {
                            // Setup the formatting information to use for the conversion.
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathML").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_HGLOBAL;

                            // Setup the MathML content to convert
                            stgMedium.unionmember = Marshal.StringToHGlobalAuto(mathmlXML);
                            stgMedium.tymed = TYMED.TYMED_HGLOBAL;
                            stgMedium.pUnkForRelease = 0;

                            // Perform the conversion
                            dataObject.SetData(ref formatEtc, ref stgMedium, false);

                            // Set the format for the output
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathType EF").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_ISTORAGE;

                            // Create a blank data structure to hold the converted result.
                            stgMedium = new STGMEDIUM();
                            stgMedium.tymed = TYMED.TYMED_NULL;
                            stgMedium.pUnkForRelease = 0;

                            // Get the conversion result in MTEF format
                            dataObject.GetData(ref formatEtc, out stgMedium);
                        }
                        catch (COMException e)
                        {
                            Debug.Print("MathML conversion to MathType threw an exception: " + Environment.NewLine + e.ToString());
                        }

                        // The pointer now becomes a Handle reference.
                        HandleRef handleRef = new HandleRef(null, stgMedium.unionmember);

                        try
                        {
                            // Lock in the handle to get the pointer to the data
                            IntPtr ptrToHandle = MathTypeSDK_64.GlobalLock(handleRef);

                            // Get the size of the memory block
                            MtefDataLength = MathTypeSDK_64.GlobalSize(handleRef);

                            // New an array of bytes and Marshal the data across.
                            MtefData = new byte[MtefDataLength];
                            Marshal.Copy(ptrToHandle, MtefData, 0, MtefDataLength);
                            MtefString = System.Text.ASCIIEncoding.ASCII.GetString(MtefData);
                        }
                        catch (Exception e)
                        {
                            Debug.Print("Generation of image from MathType failed: " + Environment.NewLine + e.ToString());
                        }
                        finally
                        {
                            MathTypeSDK_64.GlobalUnlock(handleRef);
                        }
                    }
                    else
                    {
                        System.Runtime.InteropServices.ComTypes.IDataObject dataObject = MathTypeSDK_DEFAULT.getIDataObject();

                        if (dataObject == null)
                        {
                            Debug.Print("Could not create data object!");
                        }

                        FORMATETC formatEtc = new FORMATETC();
                        STGMEDIUM stgMedium = new STGMEDIUM();

                        try
                        {
                            // Setup the formatting information to use for the conversion.
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathML").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_HGLOBAL;

                            // Setup the MathML content to convert
                            stgMedium.unionmember = Marshal.StringToHGlobalAuto(mathmlXML);
                            stgMedium.tymed = TYMED.TYMED_HGLOBAL;
                            stgMedium.pUnkForRelease = 0;

                            // Perform the conversion
                            dataObject.SetData(ref formatEtc, ref stgMedium, false);

                            // Set the format for the output
                            formatEtc.cfFormat = (Int16)DataFormats.GetFormat("MathType EF").Id;
                            formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
                            formatEtc.lindex = -1;
                            formatEtc.ptd = (IntPtr)0;
                            formatEtc.tymed = TYMED.TYMED_ISTORAGE;

                            // Create a blank data structure to hold the converted result.
                            stgMedium = new STGMEDIUM();
                            stgMedium.tymed = TYMED.TYMED_NULL;
                            stgMedium.pUnkForRelease = 0;

                            // Get the conversion result in MTEF format
                            dataObject.GetData(ref formatEtc, out stgMedium);
                        }
                        catch (COMException e)
                        {
                            Debug.Print("MathML conversion to MathType threw an exception: " + Environment.NewLine + e.ToString());
                        }

                        // The pointer now becomes a Handle reference.
                        HandleRef handleRef = new HandleRef(null, stgMedium.unionmember);

                        try
                        {
                            // Lock in the handle to get the pointer to the data
                            IntPtr ptrToHandle = MathTypeSDK_DEFAULT.GlobalLock(handleRef);

                            // Get the size of the memory block
                            MtefDataLength = MathTypeSDK_DEFAULT.GlobalSize(handleRef);

                            // New an array of bytes and Marshal the data across.
                            MtefData = new byte[MtefDataLength];
                            Marshal.Copy(ptrToHandle, MtefData, 0, MtefDataLength);
                            MtefString = System.Text.ASCIIEncoding.ASCII.GetString(MtefData);
                        }
                        catch (Exception e)
                        {
                            Debug.Print("Generation of image from MathType failed: " + Environment.NewLine + e.ToString());
                        }
                        finally
                        {
                            MathTypeSDK_DEFAULT.GlobalUnlock(handleRef);
                        }
                    }
                
                
            }

            public int MtefDataLength { get; set; }

            public byte[] MtefData { get; set; }

            public string MtefString { get; set; }
        }

        #endregion
        /// <summary>
        /// Delete dash space between two lowercase letters.
        /// </summary>
        [SuiteDescription("Delete dash space between two lowercase letters", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void DeleteDashSpace(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            ///Find lowercase-letter, hyphen, space, lower-case letter.
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"[a-z]- [a-z]", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {

                // Replace dash space with paragraph
                Globals.ThisAddIn.ReplaceAll(r, "- ", "");

            }
        }
        /// <summary>
        /// Delete space dash between two lowercase letters.
        /// </summary>
        [SuiteDescription("Delete space dash between two lowercase letters", optional1: true, optional2: false, optional3: true, optional4: false)]
        public static void DeleteSpaceDash(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            ///Find lowercase-letter, space, hyphen, lower-case letter.
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"[a-z] -[a-z]", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {

                // Replace space dash with paragraph
                Globals.ThisAddIn.ReplaceAll(r, " -", "");

            }
        }
    }
}
