﻿namespace CentralAccessMacros
{
    partial class AboutBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.picCATLogo = new System.Windows.Forms.PictureBox();
            this.lblAboutCATHeading = new System.Windows.Forms.Label();
            this.txtContributors = new System.Windows.Forms.RichTextBox();
            this.lnkAlternateFormatTools = new System.Windows.Forms.LinkLabel();
            this.btnLicence = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCATLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.picCATLogo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAboutCATHeading, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtContributors, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lnkAlternateFormatTools, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnLicence, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnClose, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(439, 307);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // picCATLogo
            // 
            this.picCATLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.SetColumnSpan(this.picCATLogo, 2);
            this.picCATLogo.Image = global::CentralAccessMacros.Properties.Resources.CAT_Logo;
            this.picCATLogo.Location = new System.Drawing.Point(43, 3);
            this.picCATLogo.Name = "picCATLogo";
            this.picCATLogo.Size = new System.Drawing.Size(352, 81);
            this.picCATLogo.TabIndex = 0;
            this.picCATLogo.TabStop = false;
            // 
            // lblAboutCATHeading
            // 
            this.lblAboutCATHeading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAboutCATHeading.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblAboutCATHeading, 2);
            this.lblAboutCATHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAboutCATHeading.Location = new System.Drawing.Point(152, 87);
            this.lblAboutCATHeading.Name = "lblAboutCATHeading";
            this.lblAboutCATHeading.Size = new System.Drawing.Size(135, 20);
            this.lblAboutCATHeading.TabIndex = 1;
            this.lblAboutCATHeading.Text = "About CAT Tool";
            this.lblAboutCATHeading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtContributors
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtContributors, 2);
            this.txtContributors.Location = new System.Drawing.Point(3, 110);
            this.txtContributors.Name = "txtContributors";
            this.txtContributors.ReadOnly = true;
            this.txtContributors.Size = new System.Drawing.Size(433, 140);
            this.txtContributors.TabIndex = 3;
            this.txtContributors.Text = "";
            // 
            // lnkAlternateFormatTools
            // 
            this.lnkAlternateFormatTools.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lnkAlternateFormatTools.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lnkAlternateFormatTools, 2);
            this.lnkAlternateFormatTools.Location = new System.Drawing.Point(144, 257);
            this.lnkAlternateFormatTools.Name = "lnkAlternateFormatTools";
            this.lnkAlternateFormatTools.Size = new System.Drawing.Size(150, 13);
            this.lnkAlternateFormatTools.TabIndex = 4;
            this.lnkAlternateFormatTools.TabStop = true;
            this.lnkAlternateFormatTools.Text = "http://www.altformattools.com";
            this.lnkAlternateFormatTools.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAlternateFormatTools_LinkClicked);
            // 
            // btnLicence
            // 
            this.btnLicence.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLicence.Location = new System.Drawing.Point(72, 279);
            this.btnLicence.Name = "btnLicence";
            this.btnLicence.Size = new System.Drawing.Size(75, 23);
            this.btnLicence.TabIndex = 5;
            this.btnLicence.Text = "Licence";
            this.btnLicence.UseVisualStyleBackColor = true;
            this.btnLicence.Click += new System.EventHandler(this.btnLicence_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(291, 279);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // AboutBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 331);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AboutBox1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCATLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox picCATLogo;
        private System.Windows.Forms.Label lblAboutCATHeading;
        private System.Windows.Forms.RichTextBox txtContributors;
        private System.Windows.Forms.LinkLabel lnkAlternateFormatTools;
        private System.Windows.Forms.Button btnLicence;
        private System.Windows.Forms.Button btnClose;


    }
}
