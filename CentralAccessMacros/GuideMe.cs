﻿using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    public class GuideMe
    {

        public static string XML_NAMESPACE = "http://www.cwu.edu/central-access/guide-me";

        public static void ShowActionPane()
        {
            GuideMeActionPane pane = new GuideMeActionPane();
            Globals.ThisAddIn.ShowTaskPane(pane, "Guide Me", customWidth: 400, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);

        }

        /// <summary>
        /// Returns the formats for the Guide Me.
        /// </summary>
        public static List<Format> FormatGuideFormats
        {
            get
            {
                if (_formatGuideFormats != null)
                    return _formatGuideFormats;

                _formatGuideFormats = new List<Format>();

                //if (GuideMe.ValidateData())
                //{
                XDocument xml = XDocument.Load(new StringReader(Properties.Resources.GuideMeData_FormatGuide));
                _formatGuideFormats = LoadFormats(xml.Descendants("Format"));
                //}

                return _formatGuideFormats;
            }
        }
        private static List<Format> _formatGuideFormats;

        /// <summary>
        /// Returns the formats for the Guide Me for Getting Content Ready for
        /// Word
        /// </summary>
        public static List<Format> ContentReadyFormats
        {
            get
            {
                if (_contentReadyFormats != null)
                    return _contentReadyFormats;

                _contentReadyFormats = new List<Format>();

                //if (GuideMe.ValidateData())
                //{
                XDocument xml = XDocument.Load(new StringReader(Properties.Resources.GuideMeData_ContentFreadyForCAR));
                _contentReadyFormats = LoadFormats(xml.Descendants("Format"));
                //}

                return _contentReadyFormats;
            }
        }
        private static List<Format> _contentReadyFormats;


        /// <summary>
        /// Returns the formats for the Guide Me for Getting Content Ready for
        /// Word
        /// </summary>
        public static List<Format> ExportContentFormats
        {
            get
            {
                if (_exportContentFormats != null)
                    return _exportContentFormats;

                _exportContentFormats = new List<Format>();

                //if (GuideMe.ValidateData())
                //{
                XDocument xml = XDocument.Load(new StringReader(Properties.Resources.GuideMeData_ExportContent));
                _exportContentFormats = LoadFormats(xml.Descendants("Format"));
                //}

                return _exportContentFormats;
            }
        }
        private static List<Format> _exportContentFormats;

        #region Format Functions

        /// <summary>
        /// Loads the formats.
        /// </summary>
        /// <param name="formats"></param>
        /// <returns></returns>
        private static List<Format> LoadFormats(IEnumerable<XElement> formats)
        {
            return formats.Select(f => new Format
            {
                Label = f.Attribute("Label").Value,
                Description = f.Element("Description").Value,
                Steps = LoadSteps(f.Elements("Step"))
            }).ToList();
        }

        /// <summary>
        /// Loads the steps
        /// </summary>
        /// <param name="steps"></param>
        /// <returns></returns>
        private static List<Step> LoadSteps(IEnumerable<XElement> steps)
        {
            return steps.Select(s => new Step
            {
                Label = s.Attribute("Label").Value,
                Description = s.Element("Description").Value,
                IsChecked = LoadChecked(s),
                Links = LoadLinks(s.Elements("Link")),
                Substeps = LoadSteps(s.Elements("Step"))
            }).ToList();
        }

        private static bool LoadChecked(XElement step)
        {
            bool isChecked = false;
            if (step.Attribute("IsChecked") != null)
            {
                isChecked = XmlConvert.ToBoolean(step.Attribute("IsChecked").Value);
            }
            return isChecked;
        }

        /// <summary>
        /// Loads the links
        /// </summary>
        /// <param name="links"></param>
        /// <returns></returns>
        private static List<Link> LoadLinks(IEnumerable<XElement> links)
        {
            return links.Select(l => new Link
            {
                Text = l.Attribute("Text").Value,
                URL = l.Attribute("URL").Value
            }).ToList();
        }

        private static bool ValidateData()
        {
            XMLValidator validator = new XMLValidator(Properties.Resources.GuideMeData_FormatGuide, Properties.Resources.GuideMeSchema);
            return validator.IsValid;
        }

        #endregion

        #region Checked Information

        /// <summary>
        /// Property that gets/sets the XML for the custom XML part that stores
        /// the Guide Me checked data.
        /// </summary> 
        private static string CheckedXMLData
        {
            get
            {
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
                CustomXMLParts collection = doc.CustomXMLParts.SelectByNamespace(GuideMe.XML_NAMESPACE);

                if (collection.Count > 0)
                {
                    Debug.Print("Checked XML: {0}", collection[1].XML);
                    return collection[1].XML;
                }

                return "";
            }

            set
            {
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
                CustomXMLParts collection = doc.CustomXMLParts.SelectByNamespace(GuideMe.XML_NAMESPACE);

                if (collection.Count > 0)
                {
                  foreach (CustomXMLPart part in collection){
                    //CustomXMLPart part = collection[1];
                    part.Delete();
                  }
                }
               Debug.Print("Current CustomXML count!", collection.Count);  
                doc.CustomXMLParts.Add(value);
            }
        }
        private static string ReadDocumentProperty(string propertyName)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)doc.CustomDocumentProperties;

            foreach (Microsoft.Office.Core.DocumentProperty prop in properties)
            {
                if (prop.Name == propertyName)
                {
                    return prop.Value.ToString();
                }
            }
            return null;
        }

        /// <summary>
        /// Loads the checked information from the document into the list.
        /// </summary>
        /// <param name="loadedFormats"></param>
        public static void LoadCheckedInformation()
        {            
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Microsoft.Office.Core.DocumentProperties builtInProperties= Globals.ThisAddIn.GetActiveDocument().BuiltInDocumentProperties;

            //Addition done for storing custom properties in the document -start
            //String fullDocName = Globals.ThisAddIn.GetActiveDocument().fullDocName;
                     
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)doc.CustomDocumentProperties;
            string propertyNameID = "GuideMeID";
            if (ReadDocumentProperty(propertyNameID) != null)
            {
                string contentcontrol_id = ReadDocumentProperty(propertyNameID);

                string propertyName = "GuideMe" + contentcontrol_id;
                if (ReadDocumentProperty(propertyName) != null)
                {
                    //properties[propertyName].Delete();
                    CustomXMLParts collection = doc.CustomXMLParts.SelectByNamespace(GuideMe.XML_NAMESPACE + "/" + propertyName);

                    if (collection.Count > 0)
                    {
                       string myXML = collection[1].XML;

                        //string myXML = ReadDocumentProperty(propertyName);
                        //Addition done for storing custom properties in the document - end
                        Debug.Print("Loading GuideMe checked information! {0}", myXML);                        
                        XElement root = XElement.Parse(myXML);
                        XNamespace ns = GuideMe.XML_NAMESPACE + "/" + propertyName;
                        var paths = from check in root.Descendants(ns + "Check")
                                    select LoadCheckedPath(check);

                        foreach (Queue<string> path in paths)
                        {
                            // Check through formats first
                            string formatLabel = path.Dequeue();

                            foreach (Format f in GuideMe.ExportContentFormats)
                            {
                                if (f.Label == formatLabel)
                                {
                                    foreach (Step s in f.Steps)
                                    {
                                        if (LoadCheckedIntoStep(s, path))
                                            break;
                                    }
                                    break;
                                }
                            }
                            foreach (Format f in GuideMe.FormatGuideFormats)
                            {
                                if (f.Label == formatLabel)
                                {
                                    foreach (Step s in f.Steps)
                                    {
                                        if (LoadCheckedIntoStep(s, path))
                                            break;
                                    }
                                    break;
                                }
                            }
                            foreach (Format f in GuideMe.ContentReadyFormats)
                            {
                                if (f.Label == formatLabel)
                                {
                                    foreach (Step s in f.Steps)
                                    {
                                        if (LoadCheckedIntoStep(s, path))
                                            break;
                                    }
                                    break;
                                }
                            }
                        }

                    }
                    else
                    {
                        Debug.Print("Could not find my custom XML...");
                    }
                }
            }
        }

        private static bool LoadCheckedIntoStep(Step s, Queue<string> path)
        {
            if (s.Label == path.Peek())
            {
                path.Dequeue();
                if (path.Count == 0)
                {
                    s.IsChecked = true;
                }
                else
                {
                    if (path.Count > 0)
                    {
                        foreach (Step substep in s.Substeps)
                        {
                            bool result = LoadCheckedIntoStep(substep, path);
                            if (result)
                                return result;
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private static Queue<string> LoadCheckedPath(XElement check)
        {
            Debug.Print("Trying to split string: {0}", check.Value);
            string[] parts = check.Value.Split('.');

            for (int i = 0; i < parts.Length; i++)
            {
                byte[] raw = Convert.FromBase64String(parts[i]);
                parts[i] = Encoding.Unicode.GetString(raw);
            }

            foreach (var item in parts)
                Debug.Print("My Path: {0}", item);
            
            return new Queue<string>(parts);
        }

        /// <summary>
        /// Saves the checked information from the list to the document. It
        /// saves it in the following format:
        /// <CheckedItems>
        ///     <Check Path="<Base64Name>.<Base64Name>..." />
        /// </CheckedItems>
        /// </summary>
        /// <param name="loadedFormats"></param>
        public static void SaveCheckedInformation()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            StringWriter myString = new StringWriter();
            //Addition done for storing custom properties in the document -start
            //String fullDocName = Globals.ThisAddIn.GetActiveDocument().FullName;
            //Debug.WriteLine("fullDocName: ", fullDocName);
            Guid obj;
            string contentcontrol_id ;
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)doc.CustomDocumentProperties;
             if (ReadDocumentProperty("GuideMeID") == null)
            {
                 obj = Guid.NewGuid();
                 contentcontrol_id = obj.ToString();
                 properties.Add("GuideMeID", false,
                  Microsoft.Office.Core.MsoDocProperties.msoPropertyTypeString,
                  contentcontrol_id.ToString());
            }
             else
             {
                 contentcontrol_id = ReadDocumentProperty("GuideMeID");
             }
                
                     
            string propertyName = "GuideMe" + contentcontrol_id;
            if (ReadDocumentProperty(propertyName) != null)
            {
                properties[propertyName].Delete();
            }



            //Addition done for storing custom properties in the document - end
            using (XmlWriter writer = XmlWriter.Create(myString))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("CheckedItems", GuideMe.XML_NAMESPACE + "/" + propertyName);

                Stack<string> pathStack = new Stack<string>();

                foreach (Format f in ExportContentFormats)
                {
                    pathStack.Push(f.Label);
                    foreach (Step s in f.Steps)
                    {
                        WriteStepChecked(s, pathStack, writer);
                    }
                    pathStack.Pop();
                }
                foreach (Format f in FormatGuideFormats)
                {
                    pathStack.Push(f.Label);
                    foreach (Step s in f.Steps)
                    {
                        WriteStepChecked(s, pathStack, writer);
                    }
                    pathStack.Pop();
                }
                foreach (Format f in ContentReadyFormats)
                {
                    pathStack.Push(f.Label);
                    foreach (Step s in f.Steps)
                    {
                        WriteStepChecked(s, pathStack, writer);
                    }
                    pathStack.Pop();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            myString.Close();

            Debug.WriteLine("Trying to save XML: {0}", myString.ToString());
            CustomXMLParts collection = doc.CustomXMLParts.SelectByNamespace(GuideMe.XML_NAMESPACE + "/" + propertyName);

            if (collection.Count > 0)
            {
                foreach (CustomXMLPart part in collection)
                {
                  part.Delete();
                }
            }
            Debug.Print("Current CustomXML count!", collection.Count);  
            GuideMe.CheckedXMLData = myString.ToString();
           //Added for creating a custom property -Start
            properties.Add(propertyName, false,
               Microsoft.Office.Core.MsoDocProperties.msoPropertyTypeString,
               myString.ToString());
          
            //Added for creating a custom property -End
        }

        private static void WriteStepChecked(Step s, Stack<string> pathStack, XmlWriter writer)
        {
            pathStack.Push(s.Label);
            if (s.IsChecked)
            {
                // Create path to this element
                string path = "";
                foreach (string p in pathStack.Reverse())
                {
                    path += "." + Convert.ToBase64String(Encoding.Unicode.GetBytes(p));
                }
                path = path.Substring(1, path.Length - 1);
                Debug.WriteLine("Generated Path: {0}", path);

                // Save it
                writer.WriteElementString("Check", path);
            }

            foreach (Step substep in s.Substeps)
            {
                WriteStepChecked(substep, pathStack, writer);
            }

            pathStack.Pop();
        }

        #endregion

        #region XML Save Functions

        private static string FormatsToXML()
        {
            StringWriter myString = new StringWriter();

            using (XmlWriter writer = XmlWriter.Create(myString))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("GuideMe");

                foreach (Format f in ExportContentFormats)
                {
                    writer.WriteStartElement("Format");
                    writer.WriteAttributeString("Label", f.Label);

                    foreach (Step s in f.Steps)
                    {
                        WriteStep(s, writer);
                    }

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            myString.Close();
            Debug.WriteLine("myString.ToString() :", myString.ToString());
            return myString.ToString();
        }

        private static void WriteStep(Step s, XmlWriter writer)
        {
            writer.WriteStartElement("Step");
            writer.WriteAttributeString("Label", s.Label);
            writer.WriteAttributeString("IsChecked", XmlConvert.ToString(s.IsChecked));

            foreach (Link l in s.Links)
            {
                writer.WriteStartElement("Link");
                writer.WriteAttributeString("Text", l.Text);
                writer.WriteAttributeString("URL", l.URL);
                writer.WriteEndElement();
            }

            foreach (Step substep in s.Substeps)
            {
                WriteStep(substep, writer);
            }

            writer.WriteEndElement();
        }

        #endregion

        #region Data Classes

        public class Format
        {
            public string Label { get; set; }
            public string Description { get; set; }
            public bool HasSteps
            {
                get
                {
                    if (Steps != null)
                    {
                        if (Steps.Count > 0)
                            return true;
                    }
                    return false;
                }
            }
            public List<Step> Steps { get; set; }
        }

        public class Step : INotifyPropertyChanged
        {
            public string Label { get; set; }
            public string Description { get; set; }

            private bool _isChecked = false;
            public bool IsChecked
            {
                get
                {
                    return _isChecked;
                }
                set
                {
                    _isChecked = value;
                    if (this.PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
                    }
                }
            }

            public List<Link> Links { get; set; }
            public bool HasLinks
            {
                get
                {
                    if (Links != null)
                    {
                        if (Links.Count > 0)
                            return true;
                    }
                    return false;
                }
            }
            public List<Step> Substeps { get; set; }
            public bool HasSteps
            {
                get
                {
                    if (Substeps != null)
                    {
                        if (Substeps.Count > 0)
                            return true;
                    }
                    return false;
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        public class Link
        {
            public string Text { get; set; }
            public string URL { get; set; }
        }

        #endregion

    }
}