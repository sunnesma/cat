﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for FootnoteHunterControl.xaml
    /// </summary>
    public partial class FootnoteHunterControl : UserControl
    {
        List<FootNoteHunter> lstFootNoteSuspects;
        List<FootNoteHunter> lstFootNoteSuspectsFiltered;
        List<FootNoteHunter> lstFootNotes;
        List<FootNoteHunter> lstFootNotesFiltered;
        bool isSuspectListFiltered = false;
        bool isFootNoteListFiltered = false;
        bool isSortByRange = true;
        bool isSuspectSuggestionEnabled = true;
        int intDigitLimit = 2;
        FootNoteHunter objRecentlyAppliedStyle = null;
        FootNoteHunter objRecentlyClearedStyle = null;

        public FootnoteHunterControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshFootNoteSuspects();
            RefreshFootNotes();
            if (lstFootNotes != null && lstFootNotes.Count != 0)
                FindAndHighlightNextSuspect(lstFootNotes[lstFootNotes.Count - 1].strText, lstFootNotes[lstFootNotes.Count - 1].rngLocation.Start);
        }

        /// <summary>
        /// Finds all the Footnote Suspects and then refreshes the Suspect listbox.
        /// </summary>
        private void RefreshFootNoteSuspects()
        {
            lstFootNoteSuspects = FootNoteHunter.GetAllFootNoteSuspects(intDigitLimit);

            if(isSortByRange)
                lstFootNoteSuspects.Sort(new FootNoteSortByRangeAscending());
            else
                lstFootNoteSuspects.Sort(new FootNoteSortByTextAscending());

            RefreshListBoxes();
        }

        /// <summary>
        /// Finds all the Footnotes and then refreshes the Footnote listbox.
        /// </summary>
        private void RefreshFootNotes()
        {
            lstFootNotes = FootNoteHunter.GetAllFootNotes();

            if (isSortByRange)
                lstFootNotes.Sort(new FootNoteSortByRangeAscending());
            else
                lstFootNotes.Sort(new FootNoteSortByTextAscending());

            RefreshListBoxes();
        }

        /// <summary>
        /// Refreshes all the listboxes by resetting the data context to the appropriate list. 
        /// This is required to update the listbox UI, when any changes are made to the internal lists.
        /// </summary>
        private void RefreshListBoxes()
        {
            this.lstBoxFootNoteSuspects.ItemsSource = "";
            this.lstBoxFootNoteSuspects.ItemsSource = isSuspectListFiltered ? lstFootNoteSuspectsFiltered : lstFootNoteSuspects;

            this.lstBoxFootNotes.ItemsSource = "";
            this.lstBoxFootNotes.ItemsSource = isFootNoteListFiltered ? lstFootNotesFiltered : lstFootNotes;
        }

        /// <summary>
        /// Sorts all the internal lists with a specific sort order, decided by isSortByRange. 
        /// This is required when any changes are made, like add or remove.
        /// </summary>
        private void SortAllLists()
        {
            if (isSortByRange)
            {
                lstFootNoteSuspects.Sort(new FootNoteSortByRangeAscending());
                if (isSuspectListFiltered)
                    lstFootNoteSuspectsFiltered.Sort(new FootNoteSortByTextAscending());
            }
            else
            {
                lstFootNoteSuspects.Sort(new FootNoteSortByTextAscending());
                if (isSuspectListFiltered)
                    lstFootNoteSuspectsFiltered.Sort(new FootNoteSortByTextAscending());
            }
            lstFootNotes.Sort(new FootNoteSortByRangeAscending());
            if (isFootNoteListFiltered)
                lstFootNotesFiltered.Sort(new FootNoteSortByRangeAscending());
        }

        /// <summary>
        /// Shows the actual location of the highligted item in the Suspect List box, inside the document. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBoxFootNoteSuspects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IList items = e.AddedItems;

            if (items.Count > 0)
            {
                FootNoteHunter item = (FootNoteHunter)items[0];
                item.rngLocation.Select();
                txtSuspectFilter.Text = item.strText;
            }
        }

        /// <summary>
        /// Shows the actual location of the highligted item in the Footnote List box, inside the document. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBoxFootNotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IList items = e.AddedItems;

            if (items.Count > 0)
            {
                FootNoteHunter item = (FootNoteHunter)items[0];
                item.rngLocation.Select();
                txtFootNoteFilter.Text = item.strText;
            }
        }

        /// <summary>
        /// Gets all the items selected in the Suspect list box and applies the Footnote reference style.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApplyStyle_Click(object sender, RoutedEventArgs e)
        {
            int currentIndex = 0;
            string currentValue = "";
            int currentLocation = 0;
            IList lstSelectedItems = lstBoxFootNoteSuspects.SelectedItems;
            if (lstSelectedItems.Count > 0)
            {
                FootNoteHunter item = null;
                for (int i = 0; i < lstSelectedItems.Count; i++)
                {
                    item = objRecentlyAppliedStyle = (FootNoteHunter)lstSelectedItems[i];
                    currentIndex = lstFootNoteSuspects.FindIndex(a => a.rngLocation == item.rngLocation);
                    currentValue = item.strText;
                    currentLocation = item.rngLocation.Start;
                    item.rngLocation.set_Style(MainRibbon.FootnoteReference);

                    lstFootNoteSuspects.Remove(item);
                    if (isSuspectListFiltered)
                        lstFootNoteSuspectsFiltered.Remove(item);

                    lstFootNotes.Add(item);
                    if (isFootNoteListFiltered)
                        lstFootNotesFiltered.Add(item);
                }
                
                #region Color next possible suspects
                if (isSuspectSuggestionEnabled)
                    FindAndHighlightNextSuspect(currentValue, currentLocation);
                #endregion
                
                SortAllLists();
                RefreshListBoxes();

                #region Make item visible in Foot note list
                if (item != null)
                {
                    lstBoxFootNotes.ScrollIntoView(item);
                    lstBoxFootNotes.SelectedItem = item;
                }
                #endregion

                #region Select next item in Suspect list
                List<FootNoteHunter> objCurrentList = isSuspectListFiltered ? lstFootNoteSuspectsFiltered : lstFootNoteSuspects;

                if (currentIndex >= objCurrentList.Count)
                    currentIndex = objCurrentList.Count - 1;

                if (currentIndex != -1)
                {
                    lstBoxFootNoteSuspects.ScrollIntoView(objCurrentList.ElementAt(currentIndex));
                    lstBoxFootNoteSuspects.SelectedItem = objCurrentList.ElementAt(currentIndex);
                    ((ListBoxItem)lstBoxFootNoteSuspects.ItemContainerGenerator.ContainerFromIndex(lstBoxFootNoteSuspects.SelectedIndex)).Focus();
                }
                #endregion
            }
        }

        /// <summary>
        /// Gets all the items selected in the Footnote list box and clears the style.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearStyle_Click(object sender, RoutedEventArgs e)
        {
            int currentIndex = 0;
            IList lstSelectedItems = lstBoxFootNotes.SelectedItems;
            if (lstSelectedItems.Count > 0)
            {
                FootNoteHunter item = null;
                for (int i = 0; i < lstSelectedItems.Count; i++)
                {
                    item = objRecentlyClearedStyle = (FootNoteHunter)lstSelectedItems[i];
                    currentIndex = lstFootNotes.FindIndex(a => a.rngLocation == item.rngLocation);
                    item.rngLocation.Select();
                    Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();

                    lstFootNoteSuspects.Add(item);
                    if (isSuspectListFiltered)
                        lstFootNoteSuspectsFiltered.Add(item);

                    lstFootNotes.Remove(item);
                    if (isFootNoteListFiltered)
                        lstFootNotesFiltered.Remove(item);
                }

                //Find and highlight next suspect, which is calculated as "(last element in Footnote List) + 1".
                lstFootNoteSuspects.ForEach(a => a.strColor = "Transparent");
                if (isSuspectSuggestionEnabled)
                    if (lstFootNotes.Count != 0)
                        FindAndHighlightNextSuspect(lstFootNotes[lstFootNotes.Count - 1].strText, lstFootNotes[lstFootNotes.Count - 1].rngLocation.Start);

                SortAllLists();
                RefreshListBoxes();

                #region Make item visible in Suspect list
                if (item != null)
                {
                    lstBoxFootNoteSuspects.ScrollIntoView(item);
                    lstBoxFootNoteSuspects.SelectedItem = item;
                }
                #endregion

                #region Select next item in Foot note list

                List<FootNoteHunter> objCurrentList = isFootNoteListFiltered ? lstFootNotesFiltered: lstFootNotes;
                
                if (currentIndex >= objCurrentList.Count)
                    currentIndex = objCurrentList.Count - 1;

                if (currentIndex != -1)
                {
                    lstBoxFootNotes.ScrollIntoView(objCurrentList.ElementAt(currentIndex));
                    lstBoxFootNotes.SelectedItem = objCurrentList.ElementAt(currentIndex);
                    ((ListBoxItem)lstBoxFootNotes.ItemContainerGenerator.ContainerFromIndex(lstBoxFootNotes.SelectedIndex)).Focus();
                }
                #endregion
            }
        }

        /// <summary>
        /// Code to find next possible suspect. Logic: (last element in Footnote List) + 1.
        /// </summary>
        /// <param name="currentValue"></param>
        /// <param name="currentLocation"></param>
        private void FindAndHighlightNextSuspect(string currentValue, int currentLocation)
        {
            List<FootNoteHunter> objCurrentList = isSuspectListFiltered ? lstFootNoteSuspectsFiltered : lstFootNoteSuspects;

            if (System.Text.RegularExpressions.Regex.IsMatch(currentValue, @"^\d+$"))
            {
                lstFootNoteSuspects.ForEach(a => a.strColor = "Transparent");
                objCurrentList.Where(a => int.Parse(a.strText) == int.Parse(currentValue) + 1 && a.rngLocation.Start > currentLocation).ToList().ForEach(a => a.strColor = "Yellow");
            }
        }

        /// <summary>
        /// Code to filter the contents in the Suspect Listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSuspectFilter_Click(object sender, RoutedEventArgs e)
        {
            FootNoteHunter itemToBeFocused = null;
            int currentIndex = 0;
            if (isSuspectListFiltered)
            {
                if (objRecentlyAppliedStyle == null)
                    itemToBeFocused = (FootNoteHunter)lstBoxFootNoteSuspects.SelectedItem;
                else
                {
                    List<FootNoteHunter> lstCopy = lstFootNoteSuspects.ToList();
                    lstCopy.Add(objRecentlyAppliedStyle);
                    lstCopy.Sort(new FootNoteSortByRangeAscending());
                    currentIndex = lstCopy.FindIndex(a => a.rngLocation.Start == objRecentlyAppliedStyle.rngLocation.Start);
                    
                    if(currentIndex < lstCopy.Count - 1)
                        itemToBeFocused = (FootNoteHunter)lstCopy.ElementAt(currentIndex + 1);
                    else if (currentIndex > 0)
                        itemToBeFocused = (FootNoteHunter)lstCopy.ElementAt(currentIndex - 1);
                } 
                btnSuspectFilter.Content = "Filter";
                txtSuspectFilter.Text = "";
                lstFootNoteSuspectsFiltered = null;
            }
            else
            {
                objRecentlyAppliedStyle = null;
                if (txtSuspectFilter.Text.ToString().Trim() == "")
                    return;
                btnSuspectFilter.Content = "Clear";
                lstFootNoteSuspectsFiltered = lstFootNoteSuspects.Where(i => i.strText == txtSuspectFilter.Text.ToString().Trim()).ToList();
                currentIndex = lstFootNoteSuspectsFiltered.IndexOf((FootNoteHunter)lstBoxFootNoteSuspects.SelectedItem);
                itemToBeFocused = currentIndex == -1 ? null : (FootNoteHunter)lstBoxFootNoteSuspects.SelectedItem;
            }
            isSuspectListFiltered = !isSuspectListFiltered;
            SortAllLists();
            RefreshListBoxes();
            
            if (itemToBeFocused != null)
            {
                lstBoxFootNoteSuspects.ScrollIntoView(itemToBeFocused);
                lstBoxFootNoteSuspects.SelectedItem = itemToBeFocused;
                ((ListBoxItem)lstBoxFootNoteSuspects.ItemContainerGenerator.ContainerFromIndex(lstBoxFootNoteSuspects.SelectedIndex)).Focus();
            }
        }

        /// <summary>
        /// Code to filter the contents in the Footnote Listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFootNoteFilter_Click(object sender, RoutedEventArgs e)
        {
            FootNoteHunter itemToBeFocused = null;
            int currentIndex = 0;
            if (isFootNoteListFiltered)
            {
                if (objRecentlyClearedStyle == null)
                    itemToBeFocused = (FootNoteHunter)lstBoxFootNotes.SelectedItem;
                else
                {
                    List<FootNoteHunter> lstCopy = lstFootNotes.ToList();
                    lstCopy.Add(objRecentlyClearedStyle);
                    lstCopy.Sort(new FootNoteSortByRangeAscending());
                    currentIndex = lstCopy.FindIndex(a => a.rngLocation.Start == objRecentlyClearedStyle.rngLocation.Start);

                    if (currentIndex < lstCopy.Count - 1)
                        itemToBeFocused = (FootNoteHunter)lstCopy.ElementAt(currentIndex + 1);
                    else if (currentIndex > 0)
                        itemToBeFocused = (FootNoteHunter)lstCopy.ElementAt(currentIndex - 1);
                }
                btnFootNoteFilter.Content = "Filter";
                txtFootNoteFilter.Text = "";
                lstFootNotesFiltered = null;
            }
            else
            {
                objRecentlyClearedStyle = null;
                if (txtFootNoteFilter.Text.ToString().Trim() == "")
                    return;
                btnFootNoteFilter.Content = "Clear";
                lstFootNotesFiltered = lstFootNotes.Where(i => i.strText == txtFootNoteFilter.Text.ToString().Trim()).ToList();
                currentIndex = lstFootNotesFiltered.IndexOf((FootNoteHunter)lstBoxFootNotes.SelectedItem);
                itemToBeFocused = currentIndex == -1 ? null : (FootNoteHunter)lstBoxFootNotes.SelectedItem;
            }

            isFootNoteListFiltered = !isFootNoteListFiltered;
            SortAllLists();
            RefreshListBoxes();

            if (itemToBeFocused != null)
            {
                lstBoxFootNotes.ScrollIntoView(itemToBeFocused);
                lstBoxFootNotes.SelectedItem = itemToBeFocused;
                ((ListBoxItem)lstBoxFootNotes.ItemContainerGenerator.ContainerFromIndex(lstBoxFootNotes.SelectedIndex)).Focus();
            }
        }

        /// <summary>
        /// Code for checkbox responsible for sorting lists in ascending order by text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSortByText_Checked(object sender, RoutedEventArgs e)
        {
            FootNoteHunter itemToBeFocused = (FootNoteHunter)lstBoxFootNoteSuspects.SelectedItem;
            isSortByRange = false;
            SortAllLists();
            RefreshListBoxes();
            if (itemToBeFocused != null)
            {
                lstBoxFootNoteSuspects.ScrollIntoView(itemToBeFocused);
                lstBoxFootNoteSuspects.SelectedItem = itemToBeFocused;
                ((ListBoxItem)lstBoxFootNoteSuspects.ItemContainerGenerator.ContainerFromItem(itemToBeFocused)).Focus();
            }
        }
        
        /// <summary>
        /// Code for checkbox responsible for sorting lists in ascending order by text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSortByText_Unchecked(object sender, RoutedEventArgs e)
        {
            FootNoteHunter itemToBeFocused = (FootNoteHunter)lstBoxFootNoteSuspects.SelectedItem;
            isSortByRange = true;
            SortAllLists();
            RefreshListBoxes();
            if (itemToBeFocused != null)
            {
                lstBoxFootNoteSuspects.ScrollIntoView(itemToBeFocused);
                lstBoxFootNoteSuspects.SelectedItem = itemToBeFocused;
                ((ListBoxItem)lstBoxFootNoteSuspects.ItemContainerGenerator.ContainerFromItem(itemToBeFocused)).Focus();
            }
        }

        /// <summary>
        /// Refresh to find new suspects and footnotes from the document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            txtSuspectFilter.Text = "";
            txtFootNoteFilter.Text = "";
            RefreshFootNoteSuspects();
            RefreshFootNotes();
        }

        /// <summary>
        /// Code for global shortcuts in the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            // Close the UI
            if (e.Key == Key.Escape)
                FootnoteHunterForm.ActiveForm.Close();
            // When enter key is pressed in the Suspect Filter textbox
            else if (txtSuspectFilter.IsFocused && e.Key == Key.Enter && !isSuspectListFiltered)
                btnSuspectFilter_Click(this, new RoutedEventArgs());
            // When enter key is pressed in the Footnote Filter textbox
            else if (txtFootNoteFilter.IsFocused && e.Key == Key.Enter && !isFootNoteListFiltered)
                btnFootNoteFilter_Click(this, new RoutedEventArgs());
            // Global shortcuts for Applying and Clearing Style. Only triggered when focus is not in one of the textboxes
            else if (!(txtSuspectFilter.IsFocused || txtFootNoteFilter.IsFocused))
            {
                if (e.Key == Key.Enter || e.Key == Key.A)
                    btnApplyStyle_Click(sender, e);
                else if (e.Key == Key.Back || e.Key == Key.R)
                    btnClearStyle_Click(sender, e);
            }
        }

        /// <summary>
        /// Code to prevent Arrow key navigation to change focus to a different UI element, when at the start/end of list box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBoxFootNotes_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((lstBoxFootNotes.SelectedIndex >= lstBoxFootNotes.Items.Count - 1 && e.Key == Key.Down) 
                || (lstBoxFootNotes.SelectedIndex <= 0 && e.Key == Key.Up) 
                || e.Key == Key.Left
                || e.Key == Key.Right)
                
                e.Handled = true;
        }
        
        /// <summary>
        /// Code to prevent Arrow key navigation to change focus to a different UI element, when at the start/end of list box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBoxFootNoteSuspects_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((lstBoxFootNoteSuspects.SelectedIndex >= lstBoxFootNoteSuspects.Items.Count - 1 && e.Key == Key.Down) 
                || (lstBoxFootNoteSuspects.SelectedIndex <= 0 && e.Key == Key.Up)
                || e.Key == Key.Left
                || e.Key == Key.Right)

                e.Handled = true;
        }

        /// <summary>
        /// Advanced Setting option for user to specify number of digits for a qualifying suspect.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDigitsLimit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDigitsLimit.IsLoaded)
            {
                ComboBoxItem selectedItem = (ComboBoxItem)(cmbDigitsLimit.SelectedValue);
                string selectedDigit = (string)selectedItem.Content;
                intDigitLimit = int.Parse(selectedDigit);
                btnRefresh_Click(sender, e);
            }
        }

        /// <summary>
        /// Dynamic UI size, based on the expander being collapsed/expanded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreferencesExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            FootnoteHunterForm.ActiveForm.Height = 420;
        }

        /// <summary>
        /// Dynamic UI size, based on the expander being collapsed/expanded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvancedExpander_Expanded(object sender, RoutedEventArgs e)
        {
            FootnoteHunterForm.ActiveForm.Height = 450;
        }

        /// <summary>
        /// Checkbox to toggle highlight suspect functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkHighlightSuspect_Checked(object sender, RoutedEventArgs e)
        {
            isSuspectSuggestionEnabled = true;
            if (lstFootNotes != null)
            {
                if (lstFootNotes.Count != 0)
                    FindAndHighlightNextSuspect(lstFootNotes[lstFootNotes.Count - 1].strText, lstFootNotes[lstFootNotes.Count - 1].rngLocation.Start);
                else
                    lstFootNoteSuspects.ForEach(a => a.strColor = "Transparent");
            }
            RefreshListBoxes();
        }

        /// <summary>
        /// Checkbox to toggle highlight suspect functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkHighlightSuspect_Unchecked(object sender, RoutedEventArgs e)
        {
            isSuspectSuggestionEnabled = false;
            lstFootNoteSuspects.ForEach(a => a.strColor = "Transparent");
            RefreshListBoxes();
        }
    }
}
