﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{

    /// <summary>
    /// Interaction logic for CharacterAccuracyCheckerControl.xaml
    /// </summary>
    public partial class CharacterAccuracyCheckerControl : UserControl
    {
        HashSet<string> skipWords;
        bool boolAdvancedSetting;
        public bool boolEnabled { get; set; }
        Word.Range rangeToSearch;
        List<CharacterAccuracySuspects> lstSuspects;

        public CharacterAccuracyCheckerControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            boolAdvancedSetting = true;
            skipWords = new HashSet<string>();
            rangeToSearch = Globals.ThisAddIn.Application.Selection.Range;
            boolEnabled = false;
            GetSuspects();
            UpdateUI();
        }

        private void GetSuspects()
        {
            lstSuspects = CharacterAccuracySuspects.GetListOfAllSuspects(boolAdvancedSetting);
            lstSuspects = lstSuspects.Where(i => !skipWords.Contains(i.strText.Trim())).ToList();

            RefreshCharacterAccuracySuspectList();

            //Resetting the radio buttons
            foreach (RadioButton rb in mainGrid.Children.OfType<RadioButton>())
                rb.IsChecked = false;

            //Resetting the custom textbox
            txtCustomReplaceWord.Text = "";
        }

        public void RefreshCharacterAccuracySuspectList()
        {
            lstBoxCharacterAccuracySuspects.ItemsSource = null;
            lstBoxCharacterAccuracySuspects.ItemsSource = lstSuspects;
            //UIElementsEnabled(lstSuspects.Count == 0 ? false : true, lstSuspects.Count == 0 ? false : true);
        }

        public void UpdateUI()
        {
            if (lstSuspects.Count == 0)
            {
                UIElementsEnabled(isRadioButtonEnabled: false, resetRadioButtonSelection: true, isButtonsEnabled: false);
                return;
            }

            if (lstBoxCharacterAccuracySuspects.SelectedItems.Count == 0)
            {
                UIElementsEnabled(isRadioButtonEnabled: false, resetRadioButtonSelection: true, isButtonsEnabled: false);
                return;
            }

            if (lstBoxCharacterAccuracySuspects.SelectedItems.Count == 1)
            {
                CharacterAccuracySuspects item = (CharacterAccuracySuspects)lstBoxCharacterAccuracySuspects.SelectedItems[0];
                item.rngLocation.Select();
                optAllNumbers.Content = ConvertAllSuspectsToCharactersOrNumbers(item.strText, false);
                optAllCharactersUpperCase.Content = ConvertAllSuspectsToCharactersOrNumbers(item.strText, true).ToUpper();
                optAllCharactersTitleCase.Content = optAllCharactersUpperCase.Content.ToString().Substring(0, 1) + optAllCharactersUpperCase.Content.ToString().Substring(1).ToLower();
                optAllCharactersLowerCase.Content = optAllCharactersUpperCase.Content.ToString().ToLower();
                UIElementsEnabled(isRadioButtonEnabled: true, resetRadioButtonSelection: false, isButtonsEnabled: true);
                optAllNumbers.IsChecked = true;
                return;
            }

            if (lstBoxCharacterAccuracySuspects.SelectedItems.Count > 1)
            {
                UIElementsEnabled(isRadioButtonEnabled: false, resetRadioButtonSelection: true, isButtonsEnabled: true, radioButtonContent: "N/A");
                btnReplaceOnce.IsEnabled = false;
                btnReplaceAll.IsEnabled = false;
                return;
            }
        }

        public void UIElementsEnabled(bool isRadioButtonEnabled, bool resetRadioButtonSelection, bool isButtonsEnabled, string radioButtonContent = "No Item Selected")
        {

            foreach (RadioButton rb in gridActionItems.Children.OfType<RadioButton>())
            {
                if (isRadioButtonEnabled || resetRadioButtonSelection)
                    rb.IsChecked = false;
                rb.IsEnabled = isRadioButtonEnabled;
                if (!isRadioButtonEnabled)
                    if (rb.Name != "optCustom")
                        rb.Content = radioButtonContent;
            }
            txtCustomReplaceWord.Text = "";
            txtCustomReplaceWord.IsEnabled = false;

            foreach (Button btn in stackPanel.Children.OfType<Button>().Union(gridActionItems.Children.OfType<Button>()))
                btn.IsEnabled = isButtonsEnabled;
        }

        public int GetRemainingSuspectsCount()
        {
            return CharacterAccuracySuspects.GetListOfAllSuspects(true).Count();
        }

        /// <summary>
        /// This function helps in providing suggestions to the suspects found in the CharacterAccuracyChecker.
        /// </summary>
        /// <param name="word"></param>
        /// <param name="convertToCharacters"></param>
        /// <returns></returns>
        private string ConvertAllSuspectsToCharactersOrNumbers(string word, bool convertToCharacters)
        {
            System.Text.RegularExpressions.Regex rgxYear = new System.Text.RegularExpressions.Regex("[0-9]{1,4}s");

            string newWord = word;

            //List of characters and their suspected number equivalent.
            Dictionary<string, string> suspectCharacters = new Dictionary<string, string>
            {
                {"I", "1"},
                {"L", "1"},
                {"O", "0"},
                {"S", "5"},
                {"Z", "2"},
            };

            foreach (var item in suspectCharacters)
                if (convertToCharacters)
                {
                    newWord = newWord.Replace(item.Value.ToString(), item.Key.ToString().ToUpper());
                    newWord = newWord.Replace(item.Value.ToString(), item.Key.ToString().ToLower());
                }
                else
                {
                    if (!(item.Key.ToString().ToLower() == "s" && rgxYear.IsMatch(newWord.ToLower())))
                    {
                        newWord = newWord.Replace(item.Key.ToString().ToUpper(), item.Value.ToString());
                        newWord = newWord.Replace(item.Key.ToString().ToLower(), item.Value.ToString());
                    }
                }
            return newWord;
        }

        private void optCustom_Checked(object sender, RoutedEventArgs e)
        {
            txtCustomReplaceWord.IsEnabled = true;
            txtCustomReplaceWord.Focus();
        }

        private void optCustom_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCustomReplaceWord.IsEnabled = false;
        }

        private void btnReplaceOnce_Click(object sender, RoutedEventArgs e)
        {
            if (lstBoxCharacterAccuracySuspects.SelectedItems.Count != 1)
                return;

            CharacterAccuracySuspects itemToReplace = (CharacterAccuracySuspects)lstBoxCharacterAccuracySuspects.SelectedItems[0];
            CharacterAccuracySuspects itemToFocus = null;

            int intItemIndex = lstSuspects.FindIndex(a => a == lstBoxCharacterAccuracySuspects.SelectedItems[0]);

            intItemIndex = intItemIndex == lstSuspects.Count - 1 ? lstSuspects.Count - 2 : intItemIndex + 1;

            List<CharacterAccuracySuspects> lstTempCopy = lstSuspects.ToList();
            lstTempCopy.Remove(itemToReplace);

            if (lstTempCopy.Count > 0)
                if (intItemIndex >= lstTempCopy.Count)
                    if (lstTempCopy.Count > 0)
                        itemToFocus = lstTempCopy.ElementAt(lstTempCopy.Count - 1);
                    else
                        itemToFocus = null;
                else
                    itemToFocus = lstSuspects.ElementAt(intItemIndex);

            RadioButton checkedButton = gridActionItems.Children.OfType<RadioButton>().FirstOrDefault(r => r.IsChecked == true);

            if (checkedButton == null)
                return;

            if (checkedButton.Name == "optCustom")
            {
                if (txtCustomReplaceWord.Text.Length == 0)
                {
                    System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Are you sure you want to replace " + itemToReplace.strText + " with nothing?", "Replace", System.Windows.Forms.MessageBoxButtons.YesNoCancel);
                    if (result == System.Windows.Forms.DialogResult.Yes)
                        itemToReplace.rngLocation.Text = txtCustomReplaceWord.Text;
                    else
                        return;
                }
                else
                    itemToReplace.rngLocation.Text = txtCustomReplaceWord.Text;
            }
            else
                itemToReplace.rngLocation.Text = checkedButton.Content.ToString();


            lstSuspects = lstTempCopy;

            RefreshCharacterAccuracySuspectList();

            if (intItemIndex >= lstSuspects.Count)
                intItemIndex = lstSuspects.Count - 1;

            try
            {
                if (intItemIndex != -1 && itemToFocus != null)
                {
                    lstBoxCharacterAccuracySuspects.ScrollIntoView(itemToFocus);
                    lstBoxCharacterAccuracySuspects.SelectedItem = itemToFocus;
                    ((ListBoxItem)lstBoxCharacterAccuracySuspects.ItemContainerGenerator.ContainerFromIndex(lstBoxCharacterAccuracySuspects.SelectedIndex)).Focus();
                }
            }
            catch (NullReferenceException ne)
            {
                itemToFocus = null;
            }

            UpdateUI();
        }

        private void btnReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            if (lstBoxCharacterAccuracySuspects.SelectedItems.Count != 1)
                return;

            RadioButton checkedButton = gridActionItems.Children.OfType<RadioButton>().FirstOrDefault(r => r.IsChecked == true);
            if (checkedButton == null)
                return;

            CharacterAccuracySuspects itemToReplace = (CharacterAccuracySuspects)lstBoxCharacterAccuracySuspects.SelectedItems[0];
            CharacterAccuracySuspects itemToFocus = null;
            bool foundItemToFocus = false;

            int intItemIndex = lstSuspects.FindIndex(a => a == lstBoxCharacterAccuracySuspects.SelectedItems[0]);

            List<CharacterAccuracySuspects> lstTempCopy = lstSuspects.ToList();
            lstTempCopy.RemoveAll(i => i.strText.Equals(itemToReplace.strText));

            if (lstTempCopy.Count > 0)
            {
                while (intItemIndex < lstSuspects.Count - 1)
                    if (!lstSuspects.ElementAt(++intItemIndex).strText.Equals(itemToReplace.strText))
                    {
                        foundItemToFocus = true;
                        break;
                    }

                if (foundItemToFocus)
                    itemToFocus = lstSuspects.ElementAt(intItemIndex);
                else if (lstTempCopy.Count > 0)
                    itemToFocus = lstTempCopy.ElementAt(lstTempCopy.Count - 1);
                else
                    itemToFocus = null;
            }

            int replaceCount = Globals.ThisAddIn.Find(Globals.ThisAddIn.GetActiveDocument().Content, itemToReplace.strText).Count();
            string replaceText = "";

            if (checkedButton.Name == "optCustom")
            {
                if (txtCustomReplaceWord.Text.Length == 0)
                    replaceText = "";
                else
                    replaceText = txtCustomReplaceWord.Text;
            }
            else
                replaceText = checkedButton.Content.ToString();

            System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Are you sure you want to replace all occurences of " +
                itemToReplace.strText.ToString() + " with " + (replaceText == "" ? "nothing" : replaceText) + "?" +
                " (" + replaceCount + " occurrences)", "Replace All", System.Windows.Forms.MessageBoxButtons.YesNoCancel);

            if (result == System.Windows.Forms.DialogResult.Yes)
                Globals.ThisAddIn.ReplaceAll(Globals.ThisAddIn.GetActiveDocument().Content, itemToReplace.strText, replaceText);
            else
                return;

            lstSuspects = lstTempCopy;

            RefreshCharacterAccuracySuspectList();
            try
            {
                if (intItemIndex >= lstSuspects.Count)
                    intItemIndex = lstSuspects.Count - 1;

                if (intItemIndex != -1 && itemToFocus != null)
                {
                    lstBoxCharacterAccuracySuspects.ScrollIntoView(itemToFocus);
                    lstBoxCharacterAccuracySuspects.SelectedItem = itemToFocus;
                    ((ListBoxItem)lstBoxCharacterAccuracySuspects.ItemContainerGenerator.ContainerFromIndex(lstBoxCharacterAccuracySuspects.SelectedIndex)).Focus();
                }
            }
            catch (NullReferenceException ne)
            {
                itemToFocus = null;
            }
            UpdateUI();
        }

        private void chkAdvancedSearch_Checked(object sender, RoutedEventArgs e)
        {
            if (chkAdvancedSearch.IsLoaded)
            {
                boolAdvancedSetting = true;
                GetSuspects();
            }
        }

        private void chkAdvancedSearch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chkAdvancedSearch.IsLoaded)
            {
                boolAdvancedSetting = false;
                GetSuspects();
            }
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtCustomReplaceWord.IsFocused)
                return;
            else if (e.Key == Key.H)
                btnHideAll_Click(sender, e);
            else if (e.Key == Key.R)
                btnReplaceOnce_Click(sender, e);
            else if (e.Key == Key.A)
                btnReplaceAll_Click(sender, e);
        }

        private void lstBoxCharacterAccuracySuspects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateUI();
        }

        private void lstBoxCharacterAccuracySuspects_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((lstBoxCharacterAccuracySuspects.SelectedIndex >= lstBoxCharacterAccuracySuspects.Items.Count - 1 && e.Key == Key.Down)
                || (lstBoxCharacterAccuracySuspects.SelectedIndex <= 0 && e.Key == Key.Up)
                || e.Key == Key.Left
                || e.Key == Key.Right)

                e.Handled = true;
        }

        private void btnHideAll_Click(object sender, RoutedEventArgs e)
        {
            int lastItemIndex = Int32.MaxValue;
            List<String> skippedItems = new List<string>();
            CharacterAccuracySuspects itemToFocus = null;

            foreach (CharacterAccuracySuspects item in lstBoxCharacterAccuracySuspects.SelectedItems)
            {
                skippedItems.Add(item.strText);
                int indexFound = lstSuspects.FindIndex(a => a == item);
                if (lastItemIndex > indexFound)
                    lastItemIndex = indexFound;
                skipWords.Add(item.strText);
            }
            List<CharacterAccuracySuspects> lstTempCopy = lstSuspects.Where(i => !skipWords.Contains(i.strText.Trim())).ToList();

            while (lastItemIndex > 0 && skippedItems.Contains(lstSuspects.ElementAt(lastItemIndex).strText))
            {
                itemToFocus = lstSuspects.ElementAt(--lastItemIndex);
                if (!lstTempCopy.Contains(itemToFocus))
                    itemToFocus = null;
            }

            if (lstTempCopy.Count > 0 && itemToFocus == null)
                itemToFocus = lstTempCopy.ElementAt(0);

            lstSuspects = lstTempCopy;

            RefreshCharacterAccuracySuspectList();
            try
            {
                if (lastItemIndex >= lstSuspects.Count)
                    lastItemIndex = lstSuspects.Count - 1;

                if (lastItemIndex != -1 && itemToFocus != null)
                {
                    lstBoxCharacterAccuracySuspects.ScrollIntoView(itemToFocus);
                    lstBoxCharacterAccuracySuspects.SelectedItem = itemToFocus;
                    ((ListBoxItem)lstBoxCharacterAccuracySuspects.ItemContainerGenerator.ContainerFromIndex(lstBoxCharacterAccuracySuspects.SelectedIndex)).Focus();
                }
            }
            catch (NullReferenceException ne)
            {
                itemToFocus = null;
            }
            UpdateUI();
        }
    }
}
