﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for ParagraphFixerControl.xaml
    /// </summary>
    public partial class ParagraphFixerControl : UserControl
    {

        public delegate void RequestCloseDelegate();
        public event RequestCloseDelegate OnRequestClose;

        public ParagraphFixerControl()
        {
            InitializeComponent();
        }

        public SuiteFunction SearchForNextCommand
        {
            get
            {
                return new SuiteFunction(SearchForNext);
            }
        }
        public void SearchForNext(SuiteFunctionUpdateArgs args)
        {
            // Move the selection back a paragraph so that I don't completely
            // overstep
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            range.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            range.Move(Word.WdUnits.wdParagraph, 1);
            range.Select();

            GetNextParagraphPair();
        }

        public SuiteFunction CombineCommand
        {
            get
            {
                return new SuiteFunction(Combine);
            }
        }
        public void Combine(SuiteFunctionUpdateArgs args)
        {
            General.RepairParagraph(Globals.ThisAddIn.Application.Selection.Range);
        }

        public SuiteFunction CancelCommand
        {
            get
            {
                return new SuiteFunction(Cancel);
            }
        }
        public void Cancel(SuiteFunctionUpdateArgs args)
        {
            OnRequestClose();
        }

        private void GetNextParagraphPair()
        {
            if (!General.SelectBrokenParagraphPair())
            {
                MessageBox.Show("Didn't find any more broken paragraphs.\n\nSince it isn't perfect, still check the document in case there are more.");
                OnRequestClose();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;
        }
    }
}
