﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Commands;
using System.Text.RegularExpressions;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for CheckerControl.xaml
    /// </summary>
    public partial class CheckerControl : UserControl
    {
        static private CheckerItem selectedCheckerItem = null;
        // Show fixer event
        public delegate void ShowFixerFormDelegate(System.Windows.Forms.Form f);
        public event ShowFixerFormDelegate OnShowFixerForm;
        public delegate void ShowFixerUserControlDelegate(System.Windows.Forms.UserControl u);
        public event ShowFixerUserControlDelegate OnShowFixerUserControl;

        public CheckerControl()
        {
            InitializeComponent();
        }

        public List<CheckerItem> CheckerItems { get; set; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;
        }

        public void RefreshItems()
        {
            selectedCheckerItem = null;
            foreach (CheckerItem item in CheckerItems)
                item.RefreshResult = true;

            txtBlockActionArea.Text = "";

            this.DataContext = null;
            this.DataContext = this;
            refreshButton.Content = "Re-Inspect All";
            menuActionItems.Visibility = System.Windows.Visibility.Hidden;

            foreach (CheckerItem item in CheckerItems)
                if (!item.CheckOnceButtonName.Substring(0, 3).Equals("Re-"))
                    item.CheckOnceButtonName = "Re-" + item.CheckOnceButtonName.Substring(0, 1).ToLower() + item.CheckOnceButtonName.Substring(1);
        }

        public void RefreshSingleItem(string label)
        {
            foreach (CheckerItem item in CheckerItems)
                if (item.Label == label)
                    item.RefreshResult = true;

            this.DataContext = null;
            this.DataContext = this;
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshItems();
        }

        private void lstBoxCheckerItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckerItem item = (CheckerItem)lstBoxCheckerItems.SelectedItem as CheckerItem;

            if (item == null || item == selectedCheckerItem)
                return;
            else
                selectedCheckerItem = item;
            
            if(item.IsActionsAvailable)
                menuActionItems.Visibility = System.Windows.Visibility.Visible;
            else
                menuActionItems.Visibility = System.Windows.Visibility.Hidden;

            setCheckerDescription(item);
            AddMenuItems(item);
        }

        /// <summary>
        /// Check and update the result of the selected checker item.
        /// </summary>
        private void CheckResultSingleItem()
        {
            CheckerItem item = lstBoxCheckerItems.SelectedItem as CheckerItem;
            RefreshSingleItem(item.Label);

            if (!item.CheckOnceButtonName.Substring(0, 3).Equals("Re-"))
                item.CheckOnceButtonName = "Re-" + item.CheckOnceButtonName.Substring(0, 1).ToLower() + item.CheckOnceButtonName.Substring(1);

            lstBoxCheckerItems.SelectedItem = item;
            setCheckerDescription(item);
            AddMenuItems(item);
        }

        /// <summary>
        /// Set context specific description for each of the CheckerItems.
        /// Currently supports new lines (\n), bold <b></b>, italics <i></i>, underline <u></u> and URL links <a></a>.
        /// Does NOT support multiple stylings, such as <b><i>This text is bold and italics</i></b>
        /// Format for URL links: <a> URL; text_to_appear </a>.
        /// E.g.: This is normal text.\n\n<b>This is all bold</b>.<i>This is italics</i>This is a normal text.Click on this <a>https://www.google.com/;link</a> here.
        /// </summary>
        /// <param name="item"></param>
        public void setCheckerDescription(CheckerItem item)
        {
            String strDescription = "No information found";

            switch (item.Result)
            {
                case CheckerResult.Passed:
                    strDescription = item.DescriptionPassed;
                    break;
                case CheckerResult.Failed:
                    strDescription = item.DescriptionFailed;
                    break;
                case CheckerResult.Empty:
                    strDescription = item.DescriptionEmpty;
                    break;
                case CheckerResult.NotInspected:
                    strDescription = item.DescriptionNotInspected;
                    break;
            }

            Span objSpan = new Span();

            txtBlockActionArea.Text = "";

            while (strDescription.Length != 0)
            {
                Match startTag = new Regex("<.>").Match(strDescription);
                if (startTag.Success)
                {
                    if (startTag.Index > 0)
                    {
                        objSpan.Inlines.Add(strDescription.Substring(0, startTag.Index));
                        strDescription = strDescription.Substring(startTag.Index);
                    }

                    Match endTag = new Regex(startTag.Value.Insert(1, "/")).Match(strDescription);

                    if (startTag.Value == "<b>")
                        objSpan.Inlines.Add(new Bold(new Run(strDescription.Substring(3, endTag.Index - 3))));
                    else if (startTag.Value == "<i>")
                        objSpan.Inlines.Add(new Italic(new Run(strDescription.Substring(3, endTag.Index - 3))));
                    else if (startTag.Value == "<u>")
                        objSpan.Inlines.Add(new Underline(new Run(strDescription.Substring(3, endTag.Index - 3))));
                    else if (startTag.Value == "<a>")
                    {
                        try
                        {
                            // String format: <a>web_address;text_to_appear</a>
                            // E.g.: Click on this <a>https://www.google.com;link</a> here to access Google.
                            string strWebLink = strDescription.Substring(3, endTag.Index - 3);
                            string[] webLinkArray = strWebLink.Split(';');
                            Hyperlink link = new Hyperlink();
                            link.NavigateUri = new System.Uri(webLinkArray[0].Trim());
                            link.Inlines.Add(webLinkArray[1].Trim());
                            link.RequestNavigate += (sender1, args) => System.Diagnostics.Process.Start(args.Uri.ToString());
                            objSpan.Inlines.Add(link);
                        }
                        catch (System.UriFormatException e)
                        {
                            System.Diagnostics.Debug.Print("Invalid URL format");
                            objSpan.Inlines.Add(strDescription.Substring(0, endTag.Index + 4));
                        }
                    }
                    else
                        objSpan.Inlines.Add(strDescription.Substring(3, endTag.Index));

                    strDescription = strDescription.Substring(endTag.Index + 4);
                }
                else
                {
                    objSpan.Inlines.Add(new Run(strDescription));
                    strDescription = "";
                }
            }
            txtBlockActionArea.Inlines.Add(objSpan);
            txtBlockActionArea.TextWrapping = TextWrapping.Wrap;
        }

        /// <summary>
        /// Helper function to dynamically add new buttons for any checker item.
        /// </summary>
        /// <param name="item"></param>
        private void AddExtraItems(CheckerItem item)
        {
            switch (item.Label)
            {
                case "Images have alt text":
                    break;
                case "Page numbers correct":
                    break;
                case "Footnote references":
                    menuItemAvailableActions.Items.Add(new MenuItem() { Header = "Footnote Hunter", Command = new DelegateCommand(() => FootNoteHunter.ShowFootNoteHunter()) });
                    break;
                case "Tactile graphic references in order":
                    break;
                case "Ran spell check":
                    break;
                case "Correct formatting":
                    break;
                case "Free of Text Boxes/WordArt":
                    break;
                case "Free of charts and SmartArt":
                    break;
                case "Character Accuracy Checker":
                    break;
            }
        }

        /// <summary>
        /// Helper function to dynamically populate the menu of available actions for the selected checker item.
        /// </summary>
        /// <param name="item"></param>
        private void AddMenuItems(CheckerItem item)
        {
            if (item == null)
                return;

            bool boolCheckOnceItem = item.CheckOnceButtonName != null && !item.CheckOnceButtonName.ToString().Equals("");
            bool boolRunFunctionItem = item.RunFunctionButtonName != null && !item.RunFunctionButtonName.ToString().Equals("");

            menuItemAvailableActions.Items.Clear();

            if (boolCheckOnceItem)
                menuItemAvailableActions.Items.Add(new MenuItem() { Header = item.CheckOnceButtonName, Command = new DelegateCommand(() => CheckResultSingleItem()) });

            if (boolCheckOnceItem && boolRunFunctionItem)
                menuItemAvailableActions.Items.Add(new Separator());

            if (boolRunFunctionItem)
                menuItemAvailableActions.Items.Add(new MenuItem() { Header = item.RunFunctionButtonName, Command = new DelegateCommand(() => RunCheckerFunction()) });

            AddExtraItems(item);
        }
        /// <summary>
        /// Runs the appropriate fixer for the selected checker item. 
        /// Handles both Forms and User Controls
        /// </summary>
        private void RunCheckerFunction()
        {
            CheckerItem item = (CheckerItem)lstBoxCheckerItems.SelectedItem;
            bool isForm = false;
            bool isUserControl = false;
            System.Windows.Forms.Form f = null;
            System.Windows.Forms.UserControl u = null;
            try
            {
                f = item.FixerForm();
                isForm = true;
            }
            catch (NullReferenceException e1)
            {
                try
                {
                    u = item.FixerUserControl();
                    isUserControl = true;
                }
                catch (NullReferenceException e2)
                { }
            }

            if (f != null && isForm)
                OnShowFixerForm(f);
        }
    }
}