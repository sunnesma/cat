﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for ChartConverterCompleteControl.xaml
    /// </summary>
    public partial class ChartConverterCompleteControl : UserControl
    {
        public static readonly RoutedEvent OKEvent = EventManager.RegisterRoutedEvent("OK", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ChartConverterCompleteControl));

        public event RoutedEventHandler OK
        {
            add { AddHandler(OKEvent, value); }
            remove { RemoveHandler(OKEvent, value); }
        }

        public ChartConverterCompleteControl()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventArgs args = new RoutedEventArgs(ChartConverterCompleteControl.OKEvent);
            RaiseEvent(args);
        }
    }
}
