﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for TextBoxFinderControl.xaml
    /// </summary>
    public partial class TextBoxFinderControl : UserControl
    {
        public TextBoxFinderControl()
        {
            InitializeComponent();
        }

        private void ListView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // Get the textbox object it clicked on
                DependencyObject dep = (DependencyObject)e.OriginalSource;
                while ((dep != null) && !(dep is ListBoxItem))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
                if (dep == null)
                    return;
                TextBoxShape item = (TextBoxShape)TextboxList.ItemContainerGenerator.ItemFromContainer(dep);

                // Select it to navigate to it
                item.Shape.Anchor.Select();
                item.Shape.Select();
            }
            catch (COMException ex)
            {
                Debug.Print("Textbox probably doesn't exist. Refreshing list...");
                RefreshList();
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshList();
        }

        private void RefreshList()
        {
            object thing = TextboxList.DataContext;
            TextboxList.DataContext = null;
            TextboxList.DataContext = thing;
        }

        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            // Get the current item in list and copy its text content to
            // clipboard
            try
            {
                TextBoxShape shape = (TextBoxShape)TextboxList.SelectedItem;
                if (shape != null)
                {
                    Clipboard.SetText(shape.Text, TextDataFormat.Text);
                }
            }
            catch (COMException ex)
            {
                Debug.Print("Textbox probably doesn't exist. Refreshing list...");
                RefreshList();
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            // Deletes the text box that was created
            try
            {
                TextBoxShape shape = (TextBoxShape)TextboxList.SelectedItem;
                if (shape != null)
                {
                    shape.Shape.Delete();
                    RefreshList();
                }
            }
            catch (COMException ex)
            {
                Debug.Print("Textbox probably doesn't exist. Refreshing list...");
                RefreshList();
            }
        }
        private void AutoComplete_Click(object sender, RoutedEventArgs e)
        {
            // Get the current item in list and copy its text content to
            // clipboard, delete the text box, paste the text at the anchor
            try
            {
                TextBoxShape shape = (TextBoxShape)TextboxList.SelectedItem;
                
                Word.Range r = shape.Shape.Anchor;
                
                if(shape != null)
                {
                    
                    Clipboard.SetText(shape.Text, TextDataFormat.Text);
                    shape.Shape.Delete();
                    r.InsertAfter(Clipboard.GetText());
                    // General.FormatParagraphStyle(r, TextBoxBoarder.ParagraphStyle);
                    RefreshList();
                }
            }
            catch(COMException ex)
            {
                Debug.Print("Textbox probably doesn't exist. Refreshing list...");
                RefreshList();
            }
        }

        private void TextboxList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
