﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Windows.Documents;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for TemplateableWebBrowser.xaml
    /// </summary>
    public partial class TemplateableHtmlRenderer : UserControl
    {

        #region Dependency Properties
        public static readonly DependencyProperty HtmlStringProperty = DependencyProperty.Register(
            "HtmlString", typeof(string), typeof(TemplateableHtmlRenderer), new FrameworkPropertyMetadata(OnHtmlChanged));

        private static void OnHtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TemplateableHtmlRenderer mine = d as TemplateableHtmlRenderer;
            if (mine != null)
            {
                string myHtml = (string)e.NewValue;
                myHtml = "<html><body>" + myHtml + "</body></html>";

                XDocument doc = XDocument.Load(XmlReader.Create(new StringReader(myHtml)));

                var xamlElems = from elem in doc.Descendants("body").Elements()
                                select CreateXamlElement(elem, new XamlConversionArgs());

                mine.htmlElementSink.Children.Clear();
                foreach (UIElement elem in xamlElems)
                {
                    if (elem != null)
                        mine.htmlElementSink.Children.Add(elem);
                }
            }
        }

        public string HtmlString
        {
            get { return (string)GetValue(HtmlStringProperty); }
            set { SetValue(HtmlStringProperty, value); }
        }
        #endregion

        #region HTML to XAML conversion

        private class XamlConversionArgs
        {
            public XamlConversionArgs()
            {
                IndentLevel = 0;
                LineSpacing = 5;
                IsBulletPoint = false;
            }

            public int IndentLevel { get; set; }
            public double LineSpacing { get; set; }
            public bool IsBulletPoint { get; set; }
        }

        private static UIElement CreateXamlElement(XElement elem, XamlConversionArgs args)
        {
            string elemName = elem.Name.ToString().ToLower();

            switch (elemName)
            {
                case "ul":
                    return CreateBulletedList(elem, args);
                default:
                    return CreateText(elem, args);
            }
        }

        private static UIElement CreateText(XElement elem, XamlConversionArgs args)
        {
            TextBlock para = new TextBlock();
            para.TextWrapping = TextWrapping.Wrap;

            Thickness lineSpacing = new Thickness(0, 0, 0, args.LineSpacing);
            para.Margin = lineSpacing;

            var runs = from node in elem.Nodes()
                       select CreateRun(node, args);

            foreach (Inline run in runs)
            {
                para.Inlines.Add(run);
            }

            // Insert bullet point, if able
            if (args.IsBulletPoint)
            {
                Grid grid = new Grid();

                ColumnDefinition col1 = new ColumnDefinition();
                col1.Width = new GridLength(1, GridUnitType.Auto);

                ColumnDefinition col2 = new ColumnDefinition();
                col2.Width = new GridLength(1, GridUnitType.Star);

                grid.ColumnDefinitions.Add(col1);
                grid.ColumnDefinitions.Add(col2);

                TextBlock bullet = new TextBlock();
                Thickness margin = new Thickness(0, 0, 10, 0);
                bullet.Margin = margin;
                bullet.Text = "\u2022";

                Grid.SetColumn(bullet, 0);
                Grid.SetColumn(para, 1);

                grid.Children.Add(bullet);
                grid.Children.Add(para);

                return grid;
            }
            return para;
        }

        private static Inline CreateRun(XNode run, XamlConversionArgs args)
        {
            try
            {
                XElement elem = XElement.Parse(run.ToString());

                switch (elem.Name.ToString().ToLower())
                {
                    case "u":
                        Underline u = new Underline();
                        u.Inlines.Add(new Run(elem.Value));
                        return u;
                    case "i":
                        Italic i = new Italic();
                        i.Inlines.Add(new Run(elem.Value));
                        return i;
                    case "b":
                        Bold b = new Bold();
                        b.Inlines.Add(new Run(elem.Value));
                        return b;
                    default:
                        return new Run(elem.Value);
                }
            }
            catch (XmlException ex)
            {
                return new Run(run.ToString());
            }
        }

        private static UIElement CreateBulletedList(XElement elem, XamlConversionArgs args)
        {
            args.IndentLevel += 1;
            args.IsBulletPoint = true;

            StackPanel list = new StackPanel();
            list.HorizontalAlignment = HorizontalAlignment.Stretch;
            Thickness margin = new Thickness(args.IndentLevel * 10, 0, 0, args.LineSpacing);
            list.Margin = margin;

            var bullets = from li in elem.Elements()
                          select CreateXamlElement(li, args);

            foreach (UIElement item in bullets)
            {
                list.Children.Add(item);
            }

            args.IndentLevel -= 1;
            args.IsBulletPoint = false;

            return list;
        }

        #endregion

        public TemplateableHtmlRenderer()
        {
            InitializeComponent();
        }
    }
}
