﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for ChartConverterWizardControl.xaml
    /// </summary>
    public partial class ChartConverterWizardControl : UserControl
    {
        public delegate void BasicDelegate();
        public event BasicDelegate OnRequestClose;

        public delegate void WindowResizeDelegate(int width, int height);
        public event WindowResizeDelegate OnRequestResize;

        private BackgroundWorker chartConversionWorker;

        #region Stages
        private int stage;
        public bool Stage1Visible
        {
            get
            {
                return stage == 1;
            }
        }
        public bool Stage2Visible
        {
            get
            {
                return stage == 2;
            }
        }
        public bool Stage3Visible
        {
            get
            {
                return stage == 3;
            }
        }
        public bool Stage4Visible
        {
            get
            {
                return stage == 4;
            }
        }
        #endregion

        public ChartConverterWizardControl()
        {
            InitializeComponent();
            SetStage(1);
        }

        private void SetStage(int newStage)
        {
            stage = newStage;
            this.DataContext = null;
            this.DataContext = this;
        }

        private void ChartConverterQuestionControl_Start(object sender, RoutedEventArgs e)
        {

            if (ChartConverter.CheckForAssessibleCharts() == CheckerResult.Failed)
            {

                // Have user save document to different place
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Get original path (or make one up if not saved)
                string originalPath = doc.Path;
                if (originalPath.Length == 0)
                {
                    // Make up something on the desktop
                    originalPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                // Show the Save As dialog
                Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
                dialog.FileName = System.IO.Path.GetFileNameWithoutExtension(doc.Name) + "_Accessible";
                dialog.DefaultExt = "docx";
                dialog.Filter = "Word documents (.docx)|*.docx";
                dialog.InitialDirectory = originalPath;

                Nullable<bool> result = dialog.ShowDialog();

                if (result == true)
                {
                    doc.SaveAs2(dialog.FileName);

                    SetStage(2);

                    // Run background worker to convert all of the things
                    chartConversionWorker = new BackgroundWorker();
                    chartConversionWorker.DoWork += chartConversionWorker_DoWork;
                    chartConversionWorker.RunWorkerCompleted += chartConversionWorker_RunWorkerCompleted;

                    chartConversionWorker.RunWorkerAsync();
                }
                else
                {
                    OnRequestClose();
                }
            }

            else
            {
                MessageBox.Show("No charts or SmartArt to convert!");
                OnRequestClose();
            }
        }

        private void ChartConverterQuestionControl_Cancel(object sender, RoutedEventArgs e)
        {
            OnRequestClose();
        }

        void chartConversionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ChartConverter.ConvertAll();
        }

        void chartConversionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetStage(3);
        }

        private void ChartConverterCompleteControl_OK(object sender, RoutedEventArgs e)
        {
            // Close the current window
            foreach (var entry in ThisAddIn.MyTaskPanes)
                if (entry.Value.Title == "Chart Converter")
                {
                    entry.Value.Visible = false;
                    break;
                }
        }

        private void ImageTagger_Finish(object sender, RoutedEventArgs e)
        {
            OnRequestClose();
        }
    }
}
