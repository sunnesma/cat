﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for TranscribersCheckerControl.xaml
    /// </summary>
    public partial class TranscribersCheckerControl : UserControl
    {

        public TranscribersCheckerControl()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IList items = e.AddedItems;
            if (items.Count > 0)
                SelectItem((TranscribersNoteItem)items[0]);
        }

        private void SelectItem(TranscribersNoteItem item)
        {
            descriptionBox.Text = item.DescriptionText;
            pageNumberBox.Text = item.PageNumberText;

            item.WholeRange.Select();
        }

        private void RefreshItems()
        {
            object context = this.DataContext;
            this.DataContext = null;
            this.DataContext = context;

            descriptionBox.Text = "";
            pageNumberBox.Text = "";
        }

        private void ApplyChanges()
        {
            if (itemList.SelectedIndex >= 0)
            {
                TranscribersNoteItem item = (TranscribersNoteItem)itemList.SelectedItem;

                if (item.Description != null)
                    item.Description.Text = descriptionBox.Text;

                if (item.PageNumber != null)
                    item.PageNumber.Text = pageNumberBox.Text;

                RefreshItems();
            }
        }

        private void descriptionBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;

            ApplyChanges();
        }

        private void pageNumberBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;

            ApplyChanges();
        }

        private void descriptionBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ApplyChanges();
        }

        private void pageNumberBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ApplyChanges();
        }
    }
}
