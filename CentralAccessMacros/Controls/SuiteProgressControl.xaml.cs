﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for SuiteProgressControl.xaml
    /// </summary>
    public partial class SuiteProgressControl : UserControl
    {
        public SuiteProgressControl()
        {
            InitializeComponent();
        }

        private List<SuiteFunction> functions;

        private delegate void SuiteFunctionWorker(string description, int itemIndex, int suiteProgress);
        private event SuiteFunctionWorker OnSuiteFunctionRunning;

        public delegate void BasicWindowDelegate();
        public event BasicWindowDelegate OnRequestClose;

        public int CurrentProgress { get; set; }
        public int CurrentSuiteProgress { get; set; }
        public string CurrentFunctionDescription { get; set; }

        private bool IsCancel { get; set; }

        public void LoadFunctions(List<SuiteFunction> functions)
        {
            this.functions = functions;
        }

        public void RunFunctions()
        {
            OnSuiteFunctionRunning += SuiteProgressForm_OnSuiteFunctionRunning;

            // Start my worker to run da jobs
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;

            worker.DoWork += Run;
            worker.RunWorkerCompleted += (sender, args) =>
            {
                OnRequestClose();
                if (!IsCancel)
                    MessageBox.Show("Suite finished!");
                else
                    MessageBox.Show("Suite canceled.");
            };

            worker.RunWorkerAsync();
        }

        void SuiteProgressForm_OnSuiteFunctionRunning(string description, int itemIndex, int suiteProgress)
        {
            CurrentProgress = (int)(((float)itemIndex / (float)functions.Count) * 100.0f);
            CurrentSuiteProgress = suiteProgress;
            CurrentFunctionDescription = description;

            this.Dispatcher.Invoke(new Action(() => {
                this.DataContext = null;
                this.DataContext = this;
            }));
        }

        /// <summary>
        /// Runs the suite, updating progress as it goes.
        /// </summary>
        private void Run(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;

            for (int i = 0; i < functions.Count; i++)
            {
                // Get the description of the function from the attribute
                IList<object> attributes = functions[i].Method.GetCustomAttributes(typeof(SuiteDescription), false);
                string myDescription = "";
                if (attributes.Count > 0)
                {
                    SuiteDescription d = (SuiteDescription)attributes[0];
                    myDescription = d.Description;
                }
                else
                {
                    Debug.Print("WARNING: Suite function " + functions[i].Method.GetType().Name + " not given attribute " + typeof(SuiteDescription).Name);
                }

                // Set the progress indicator
                OnSuiteFunctionRunning(myDescription, i, 0);
                worker.ReportProgress(0);

                SuiteFunctionUpdateArgs myArgs = new SuiteFunctionUpdateArgs
                {
                    UpdateProgress = (progress) => { OnSuiteFunctionRunning(myDescription, i, progress); },
                    IsCanceled = () => { return IsCancel; },
                    OrigControl = (UserControl)this,
                };

                if (!IsCancel)
                {
                    try
                    {
                        // Run the suite function
                        functions[i](myArgs);
                    }
                    catch (Exception ex)
                    {
                        Debug.Print("Exception in suite function: {0}, {1}", ex.GetType().Name, ex.ToString());
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            IsCancel = true;
        }
    }
}
