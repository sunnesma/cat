﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for PageCounterControl.xaml
    /// </summary>
    public partial class PageCounterControl : UserControl
    {

        private Word.Range CurrentPageNumber { get; set; }

        public string TotalPages
        {
            get
            {
                if (Reports != null)
                {
                    if (Reports.Count == 1)
                    {
                        return "1 page";
                    }
                    else
                    {
                        return Reports.Count.ToString() + " pages";
                    }
                }
                return "0 pages";
            }
        }

        // Members that are a part of the DataContext
        public List<PageNumberReport> Reports { get; set; }
        public bool IsGood { get; set; }

        public PageCounterControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshNumberList();
        }

        private void RefreshNumberList()
        {
            Reports = PageNumbers.GetPageNumberList();

            PageNumberTextBox.Text = "";
            CurrentPageNumber = null;

            // Give a report on how well I did
            IsGood = true;
            foreach (PageNumberReport r in Reports)
            {
                if (!r.IsGood)
                {
                    IsGood = false;
                    break;
                }
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        private void ApplyPageNumber()
        {
            if (CurrentPageNumber != null)
            {
                Word.Range myRange = Globals.ThisAddIn.CopyRange(CurrentPageNumber);
                myRange.MoveEnd(Word.WdUnits.wdCharacter, -1);
                myRange.Text = PageNumberTextBox.Text.Trim();
                CurrentPageNumber = myRange;
                RefreshNumberList();
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshNumberList();
        }

        private void PageNumberTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ApplyPageNumber();
        }

        private void PageNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;
            
            ApplyPageNumber();
        }

        private void PageNumberList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IList items = e.AddedItems;

            if (items.Count > 0)
            {
                PageNumberReport item = (PageNumberReport)items[0];

                PageNumberTextBox.Text = item.PageNumber;
                CurrentPageNumber = item.Range;
                CurrentPageNumber.Select();
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentPageNumber.Select();
            Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
            RefreshNumberList();
        }
    }
}
