﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using System.Threading;
using System.Globalization;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for EditDocumentControl.xaml
    /// </summary>
    public partial class EditDocumentControl : UserControl
    {
        public EditDocumentControl()
        {
            InitializeComponent();
        }

        private void heading1Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Core.IRibbonControl DummyObject = null;
            new MainRibbon().FormatHeading1(DummyObject);
        }

        private void heading2Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Core.IRibbonControl DummyObject = null;
            new MainRibbon().FormatHeading2(DummyObject);
        }

        private void heading3Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Core.IRibbonControl DummyObject = null;
            new MainRibbon().FormatHeading3(DummyObject);
        }

        private void heading4Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Core.IRibbonControl DummyObject = null;
            new MainRibbon().FormatHeading4(DummyObject);
        }

        private void pageNumberButton_Click(object sender, RoutedEventArgs e)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            General.FormatCharacterStyleLikeParagraph(selection, Word.WdBuiltinStyle.wdStylePageNumber, true);
        }

        private void transcribersNoteButton_Click(object sender, RoutedEventArgs e)
        {
            TranscribersNote.ShowTranscribersNoteDialog();
        }

        private void repairParagraphButton_Click(object sender, RoutedEventArgs e)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            General.RepairParagraph(selection);
        }
        private void highlightEquationButton_Click(object sender, RoutedEventArgs e)
        {
            MainRibbon.RunHighlightSingleEquation();
        }
        private void titleConvertCaseButton_Click(object sender, RoutedEventArgs e)
        {
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            MainRibbon.RunConvertTitleTextHelper(selection);
        }
        private void MyClearFormatting_Click(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.ClearFormatting();
        }
    }
}
