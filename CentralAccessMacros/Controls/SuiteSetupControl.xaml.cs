﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{

    /// <summary>
    /// A generic checkable item that allows me to make selections from a list
    /// of items. A collection of CheckableItem's would get bound to a
    /// ItemControl in WPF.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CheckableItem<T> : INotifyPropertyChanged
    {
        public CheckableItem(T value)
        {
            _value = value;
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    /// Converter for converting a delegate for a suite function into a string
    /// describing what it would do using the SuiteDescription attribute.
    /// </summary>
    public class SuiteFunctionToStringConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SuiteFunction func = (SuiteFunction)value;

            // Get the description of the function from the attribute
            IList<object> attributes = func.Method.GetCustomAttributes(typeof(SuiteDescription), false);
            string myDescription = "";
            if (attributes.Count > 0)
            {
                SuiteDescription d = (SuiteDescription)attributes[0];
                myDescription = d.Description;
            }

            return myDescription;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Interaction logic for SuiteSetupControl.xaml
    /// </summary>
    public partial class SuiteSetupControl : UserControl
    {
        List<SuiteFunction> functionsList;
        public List<CheckableItem<SuiteFunction>> Functions { get; set; }

        public SuiteSetupControl()
        {
            InitializeComponent();
        }

        public void LoadFunctions(List<SuiteFunction> functions,string ocrSourceType="OmniPage")
        {
            Functions = new List<CheckableItem<SuiteFunction>>();
            this.functionsList = functions;
            foreach (SuiteFunction d in functions)
            {
                Functions.Add(new CheckableItem<SuiteFunction>(d));
            }

            foreach (CheckableItem<SuiteFunction> i in Functions)
            {
                // Check it if it is not optional to run it
                IList<object> attributes = i.Value.Method.GetCustomAttributes(typeof(SuiteDescription), false);
                bool optional = false;
                if (attributes.Count > 0)
                {
                    SuiteDescription d = (SuiteDescription)attributes[0];
                    //Considering ocrSourceType_default = "OmniPage";

                    if (ocrSourceType.Equals("OmniPage"))
                    {
                        optional = d.Optional1;
                    }
                    else if (ocrSourceType.Equals("INFTY Reader XML"))
                    {
                        optional = d.Optional2;
                    }
                    else if (ocrSourceType.Equals("ABBYY Fine Reader"))
                    {
                        optional = d.Optional3;
                    }
                    //else if (ocrSourceType.Equals("Bookshare Word Doc"))
                    //{
                    //    optional = d.Optional4;
                    //}
                }

                i.IsChecked = !optional;
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        public List<SuiteFunction> GetAcceptedFunctions()
        {
            List<SuiteFunction> myFunctions = new List<SuiteFunction>();

            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                if (item.IsChecked)
                    myFunctions.Add(item.Value);
            }

            return myFunctions;
        }

        public string SuiteName { get; set; }

        private void selectAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                item.IsChecked = true;
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        private void deselectAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                item.IsChecked = false;
            }

            this.DataContext = null;
            this.DataContext = this;
        }
         private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            // ... A List.
            List<string> data = new List<string>();
            data.Add("ABBYY Fine Reader");
            data.Add("OmniPage");
            data.Add("INFTY Reader XML");
            //data.Add("Bookshare Word Doc");
            
            
            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;

            // ... Use SelectedItem to suggest functions.
           string comboBoxValue = comboBox.SelectedItem as string;
           LoadFunctions(this.functionsList, comboBoxValue);

        }
        
    }
}

 