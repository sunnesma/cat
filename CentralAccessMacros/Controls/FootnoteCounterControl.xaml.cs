﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for FootnoteCounterControl.xaml
    /// </summary>
    public partial class FootnoteCounterControl : UserControl
    {

        private Word.Range CurrentFootnoteNumber { get; set; }

        public string TotalFootnotes
        {
            get
            {
                if (Reports != null)
                {
                    if (Reports.Count == 1)
                    {
                        return "1 Footnote";
                    }
                    else
                    {
                        return Reports.Count.ToString() + " Footnotes";
                    }
                }
                return "0 Footnotes";
            }
        }

        // Members that are a part of the DataContext
        public List<FootnoteNumberReport> Reports { get; set; }
        public bool IsGood { get; set; }

        public FootnoteCounterControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshNumberList();
        }

        private void RefreshNumberList()
        {
            Reports = FootnoteNumbers.GetFootnoteNumberList();

            FootnoteNumberTextBox.Text = "";
            CurrentFootnoteNumber = null;

            // Give a report on how well I did
            IsGood = true;
            foreach (FootnoteNumberReport r in Reports)
            {
                if (!r.IsGood)
                {
                    IsGood = false;
                    break;
                }
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        private void ApplyFootnoteNumber()
        {
            if (CurrentFootnoteNumber != null)
            {
                Word.Range myRange = Globals.ThisAddIn.CopyRange(CurrentFootnoteNumber);
                //myRange.MoveEnd(Word.WdUnits.wdCharacter, -1);
                myRange.Text = FootnoteNumberTextBox.Text.Trim();
                CurrentFootnoteNumber = myRange;
                RefreshNumberList();
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshNumberList();
        }

        private void FootnoteNumberTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ApplyFootnoteNumber();
        }

        private void FootnoteNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;

            ApplyFootnoteNumber();
        }

        private void FootnoteNumberList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IList items = e.AddedItems;

            if (items.Count > 0)
            {
                FootnoteNumberReport item = (FootnoteNumberReport)items[0];

                FootnoteNumberTextBox.Text = item.FootnoteNumber;
                CurrentFootnoteNumber = item.Range;
                CurrentFootnoteNumber.Select();
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentFootnoteNumber.Select();
            Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
            RefreshNumberList();
        }
    }
}
