﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{

    /// <summary>
    /// Interaction logic for FormattingFixerControl.xaml
    /// </summary>
    public partial class FormattingFixerControl : UserControl, INotifyPropertyChanged
    {

        public FormattingFixerControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.SetSelectionToBeginning();
            GetNextBadFormatting();
        }

        #region Properties

        private bool _replaceAllConfirmationVisible = false;
        public bool ReplaceAllConfirmationVisible
        {
            get
            {
                return _replaceAllConfirmationVisible;
            }
            set
            {
                _replaceAllConfirmationVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ReplaceAllConfirmationVisible"));
            }
        }

        private string _replaceWithCharacter = " ";
        public string ReplaceWithCharacter
        {
            get
            {
                return _replaceWithCharacter;
            }
            set
            {
                _replaceWithCharacter = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ReplaceWithCharacter"));
            }
        }

        private static string BadSpacingRegex
        {
            get
            {
                return "[ ^t]{2,}";
            }
        }

        private string _warningLabel;
        public string WarningLabel
        {
            get
            {
                return _warningLabel;
            }

            set
            {
                _warningLabel = value;
                this.DataContext = null;
                this.DataContext = this;
            }
        }

        private Word.Range _foundRange;
        public Word.Range FoundRange
        {
            get
            {
                return _foundRange;
            }
            set
            {
                _foundRange = value;

                // Broadcast out that my range has changed
                PropertyChanged(this, new PropertyChangedEventArgs("RangeContents"));
                PropertyChanged(this, new PropertyChangedEventArgs("FoundRange"));
            }
        }

        public string RangeContents
        {
            get
            {
                if (FoundRange != null)
                {
                    string myString = FoundRange.Text;
                    myString = myString.Replace(" ", "SPACE ").Replace("\t", "TAB ");
                    return myString;
                }
                return "<no formatting issues found>";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        void completelyRemoveButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            if (rng.Start != rng.End)
            {
                rng.Text = "";
                rng.Select();
            }
            GetNextBadFormatting();
        }

        void replaceWithTabButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            if (rng.Start != rng.End)
            {
                rng.Text = "\t";
                rng.Select();
            }
            GetNextBadFormatting();
        }

        void replaceWithSpaceButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            if (rng.Start != rng.End)
            {
                rng.Text = " ";
                rng.Select();
            }
            GetNextBadFormatting();
        }

        //private void GetPreviousBadSpacing()
        //{
        //    WarningLabel = "";
        //    if (FormattingFixer.CheckBeforeSelection())
        //    {
        //        Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
        //        rng.SetRange(1, rng.Start);
        //        if (Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).Any())
        //        {
        //            rng = Globals.ThisAddIn.Find(rng, BadSpacingRegex, forwardDirection: false, useWildcards: true).First();
        //            rng.Select();
        //        }
        //    }
        //    else
        //    {
        //        WarningLabel = "No more previous items.";
        //    }
        //}

        private void GetNextBadFormatting()
        {
            WarningLabel = "";
            if (FormattingFixer.CheckAfterSelection())
            {
                Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
                rng.SetRange(rng.End, Globals.ThisAddIn.GetActiveDocument().Content.End);
                if (Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).Any())
                {
                    rng = Globals.ThisAddIn.Find(rng, BadSpacingRegex, useWildcards: true).First();
                    rng.Select();
                    FoundRange = rng;
                }
            }
            else
            {
                FoundRange = null;
                WarningLabel = "No more next items.";
            }
        }

        private void FindNext_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !ReplaceAllConfirmationVisible;
        }

        private void FindNext_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            GetNextBadFormatting();
        }

        private void Replace_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !ReplaceAllConfirmationVisible;
        }

        private void Replace_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (FoundRange.Start != FoundRange.End)
            {
                FoundRange.Text = ReplaceWithCharacter;
                FoundRange.Select();
            }
            GetNextBadFormatting();
        }

        private void ReplaceAll_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !ReplaceAllConfirmationVisible;
        }

        private void ReplaceAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FoundRange.Select();
            ReplaceAllConfirmationVisible = true;
        }

        private void ConfirmReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            string contents = FoundRange.Text;
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, contents, ReplaceWithCharacter);
            ReplaceAllConfirmationVisible = false;

            // Set selection back to start, and search for new things
            range = Globals.ThisAddIn.GetActiveDocument().Content;
            range.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            range.Select();

            GetNextBadFormatting();
        }

        private void CancelReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            ReplaceAllConfirmationVisible = false;
        }
    }

    public class ReplaceCharacterToCheckedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string myParam = parameter as string;
            string myValue = value as string;
            if (myParam.Equals(myValue))
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Debug.Print("Conversion back: {0}", parameter);
            return (string)parameter;
        }
    }

    public class InvertBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool myValue = (bool)value;
            return !myValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
