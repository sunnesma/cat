﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{

    /// <summary>
    /// Interaction logic for SpellCheckTextBox.xaml
    /// </summary>
    public partial class ImageAltTextEditorControl : UserControl
    {
        // Commands
        public static RoutedCommand NextImageCommand = new RoutedCommand();
        public static RoutedCommand PreviousImageCommand = new RoutedCommand();
        public static RoutedCommand DeleteImageCommand = new RoutedCommand();
        // Custom event
        public delegate void RequestCloseHandler();
        public event RequestCloseHandler OnRequestClose;

        public static readonly RoutedEvent FinishEvent = EventManager.RegisterRoutedEvent("Finish", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ImageAltTextEditorControl));

        public event RoutedEventHandler Finish
        {
            add { AddHandler(FinishEvent, value); }
            remove { RemoveHandler(FinishEvent, value); }
        }

        private List<Word.InlineShape> imageList;
        private List<Word.InlineShape> filteredList;
        private int currentImage;
        private int lastFilteredCount = int.MaxValue;
        private System.Drawing.Image PreClipBoardImage;
        private string PreClipBoardText;
        public ImageAltTextEditorControl()
        {
            InitializeComponent();
        }

        public string GetAltText()
        {
            return textBox.Text;
        }

        public void SetImage(Word.InlineShape shape, int currentIndex, int imageCount)
        {
            imageHolder.Source = ToBitmapImage(shape);
            textBox.Text = shape.AlternativeText;

            // Set the label for the image count/progress thing
            currentPageBox.Text = currentIndex.ToString();
            totalPagesLabel.Text = imageCount.ToString();

            // Hide the empty placeholder
            emptyPlaceholder.Visibility = Visibility.Hidden;
        }

        public void ClearImage()
        {
            imageHolder.Source = null;
            textBox.Text = "";

            // Set the label for the image count/progress thing
            currentPageBox.Text = "0";
            totalPagesLabel.Text = "0";

            // Show the empty placeholder
            emptyPlaceholder.Visibility = Visibility.Visible;
        }
        public void DeleteImage()
        {
            
            imageHolder.Source = null;
            textBox.Text = "";
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            int count = 0;
            foreach (Word.InlineShape shape in doc.InlineShapes)
            {
                if (shape.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    if (count == currentImage)
                    {
                        shape.Delete();
                    }

                }
                count++;
            }
            
            // Show the empty placeholder
            emptyPlaceholder.Visibility = Visibility.Visible;
            
            RefreshImageList();
        }
        public void SetEmptiesOnly(bool isChecked)
        {
            this.emptiesOnly.IsChecked = isChecked;
            RefreshImageList();
            UpdateImage();
        }

        private BitmapImage ToBitmapImage(Word.InlineShape shape)
        {
            // Get the image first from the inline shape
            shape.Range.Select();
            bool containsImage = false;
            bool containsText = false;
            if (System.Windows.Forms.Clipboard.ContainsImage()) { containsImage = true; }

            if (containsImage)
            {
                PreClipBoardImage = System.Windows.Forms.Clipboard.GetImage();
            }
            if (System.Windows.Forms.Clipboard.ContainsText()) { containsText = true; }
            if (containsText)
            {
                PreClipBoardText = System.Windows.Forms.Clipboard.GetText();
            }             
            Globals.ThisAddIn.Application.ActiveWindow.Selection.Copy();
            Bitmap bitmap = new Bitmap(System.Windows.Forms.Clipboard.GetImage());
            if (containsImage) { System.Windows.Forms.Clipboard.SetImage(PreClipBoardImage); }
            else if (containsText) { System.Windows.Forms.Clipboard.SetText(PreClipBoardText); }           
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                return bitmapImage;
            }
        }

        /// <summary>
        /// Returns true if this image should be edited, false otherwise. The
        /// result of this function depends on the settings.
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        private bool IsImageEditable(Word.InlineShape shape)
        {
            bool shouldEdit = true;

            if (shape != null)
            {
                if (shape.AlternativeText == null)
                    return true;
            }
            if ((bool)emptiesOnly.IsChecked)
            {
                shouldEdit &= shape.AlternativeText.Trim().Length == 0;
            }

            if ((bool)urlsEmpty.IsChecked)
            {
                // Check if URI
                Uri uri;
                shouldEdit |= Uri.TryCreate(shape.AlternativeText.Trim(), UriKind.Absolute, out uri);

                // Check if file system path
                try
                {
                    shouldEdit |= System.IO.Path.IsPathRooted(shape.AlternativeText.Trim());
                }
                catch (Exception e)
                {
                    // Do nothing
                }
            }

            return shouldEdit;
        }

        public static CheckerResult CheckImagesHaveAltText()
        {
            // Get active document
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Get list of all shapes, filtered down to just pictures
            List<Word.InlineShape> imageList = new List<Word.InlineShape>();

            foreach (Word.InlineShape shape in doc.InlineShapes)
            {
                if (shape.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    imageList.Add(shape);
                }
            }

            // Check each one for correct alt text
            if (imageList.Count > 0)
            {
                foreach (Word.InlineShape shape in imageList)
                {

                    // Check if any text at all
                    if (shape.AlternativeText == null)
                        return CheckerResult.Failed;

                    if (shape.AlternativeText.Trim().Length == 0) return CheckerResult.Failed;

                    // Check if URI
                    Uri uri;
                    if (Uri.TryCreate(shape.AlternativeText.Trim(), UriKind.Absolute, out uri)) return CheckerResult.Failed;

                    // Check if file system path
                    try
                    {
                        if (System.IO.Path.IsPathRooted(shape.AlternativeText.Trim())) return CheckerResult.Failed;
                    }
                    catch (ArgumentException e)
                    {
                        // Do nothing
                    }
                }
            }
            else
            {
                return CheckerResult.Empty;
            }

            return CheckerResult.Passed;
        }

        // Updates the form for the new current image
        private void UpdateImage()
        {
            // Check to see if I have anything at all. Otherwise, say that no image exists
            if (filteredList.Count > 0)
            {

                // If the filtered list count last time is larger than what
                // it is now, adjust the current image accordingly
                if ((lastFilteredCount - filteredList.Count) > 0)
                {
                    currentImage -= lastFilteredCount - filteredList.Count;
                }

                currentImage = Globals.ThisAddIn.Clamp(currentImage, filteredList.Count - 1, 0);
                SetImage(filteredList[currentImage], currentImage + 1, filteredList.Count);
                LoadTranscribersNote();
            }
            else
            {
                ClearImage();
            }

            lastFilteredCount = filteredList.Count;
        }

        /// <summary>
        /// Updates the alt text for a given shape with the new alt text in my
        /// control.
        /// </summary>
        private void SaveAltText()
        {
            if (currentImage >= 0 && currentImage < filteredList.Count)
            {
                try 
                {
                    filteredList[currentImage].AlternativeText = GetAltText();
                }
                catch { RefreshImageList(); }
            }
        }
        
        /// <summary>
        /// Gets a list of images from the active document
        /// </summary>
        private void RefreshImageList()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.InlineShapes shapes = doc.InlineShapes;

            imageList = new List<Word.InlineShape>();
            int count = 0;
            foreach (Word.InlineShape shape in shapes)
            {
                if (shape.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    //shape.Title = count.ToString();
                    imageList.Add(shape);
                }
            }

            // Refresh the filtered list
            RefreshFilteredList();
        }

        /// <summary>
        /// Refreshed the filtered list, which is a subset of the original
        /// list. The list is filtered by the settings.
        /// </summary>
        private void RefreshFilteredList()
        {
            filteredList = new List<Word.InlineShape>();

            foreach (Word.InlineShape s in imageList)
            {
                if (IsImageEditable(s))
                    filteredList.Add(s);
            }

            if (filteredList.Count > 0)
            {
                currentImage = Globals.ThisAddIn.Clamp(currentImage, filteredList.Count - 1, 0);
            }
            else
            {
                // Some sentinel value
                currentImage = -1;
            }
        }

        /// <summary>
        /// Loads the transcriber note information from the current image.
        /// </summary>
        private void LoadTranscribersNote()
        {
            // Clear what I had before
            descriptionBox.Text = "";
            pageNumberBox.Text = "";

            // Load in the new information
            Word.Range currentRange = filteredList[currentImage].Range;

            currentRange = currentRange.Paragraphs[1].Range;
            currentRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);

            int previous = currentRange.Start;

            currentRange.Move(Word.WdUnits.wdParagraph, -1);

            // Load information if I actually succeeded in arriving at the
            // previous paragraph
            if (currentRange.End != previous)
            {
                currentRange = currentRange.Paragraphs[1].Range;
                IList<Word.Range> descriptions = Globals.ThisAddIn.Find(currentRange, "", style: TranscribersNote.TactileDescriptionStyle).ToList();
                IList<Word.Range> pageNumbers = Globals.ThisAddIn.Find(currentRange, "", style: TranscribersNote.TactilePageNumberStyle).ToList();

                if (descriptions.Count > 0)
                {
                    descriptionBox.Text = descriptions[0].Text;
                }

                if (pageNumbers.Count > 0)
                {
                    pageNumberBox.Text = pageNumbers[0].Text;
                }

            }
        }

        /// <summary>
        /// Updates the transcriber's note for this image. It will completely
        /// overwrite what's there and replace it with my own.
        /// </summary>
        private void UpdateTranscribersNote()
        {
            if ((descriptionBox.Text.Length == 0) && (pageNumberBox.Text.Length == 0))
            {
                DeleteTranscribersNote();
            }
            else
            {
                Word.Range currentRange = filteredList[currentImage].Range;
                currentRange.set_Style(TranscribersNote.ParagraphStyle);

                DeletePreviousParagraphTranscribersNote();

                // Insert my own transcriber's note
                currentRange = currentRange.Paragraphs[1].Range;
                currentRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                currentRange.InsertParagraphBefore();
                currentRange.Move(Word.WdUnits.wdParagraph, -1);

                TranscribersNote.InsertTranscribersNote(currentRange, descriptionBox.Text, pageNumberBox.Text);
            }
        }

        private void DeleteTranscribersNote()
        {
            Word.Range currentRange = filteredList[currentImage].Range;
            Word.Style myStyle = (Word.Style)currentRange.ParagraphStyle;
            
            currentRange.Select();
            Globals.ThisAddIn.Application.Selection.ClearFormatting();

            DeletePreviousParagraphTranscribersNote();
        }

        private void DeletePreviousParagraphTranscribersNote()
        {
            Word.Range currentRange = filteredList[currentImage].Range;

            // Check the previous paragraph to see if a transcriber note is
            // there. If so, delete it.
            Word.Range pr = Globals.ThisAddIn.CopyRange(currentRange);

            pr.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            pr.Move(Word.WdUnits.wdParagraph, -1);

            // If I actually moved somewhere, then try and delete it
            if (pr.End != currentRange.Paragraphs[1].Range.Start)
            {
                Word.Style paraStyle = (Word.Style)pr.Paragraphs[1].Range.ParagraphStyle;

                if (paraStyle.NameLocal == TranscribersNote.ParagraphStyle.NameLocal)
                {
                    // Make sure I'm not deleting an image in there.
                    if (pr.Paragraphs[1].Range.InlineShapes.Count == 0)
                        pr.Paragraphs[1].Range.Delete();
                }
            }
        }

        #region Callbacks

        private void NextImageBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveAltText();
            RefreshFilteredList();
            currentImage += 1;
            UpdateImage();
        }
        private void DeleteImageBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveAltText();
            System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are you sure you want to 'Delete the Image'", "Action Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            if (dialogResult.ToString().Equals("Yes"))
            {
                DeleteImage();
                RefreshFilteredList();
                currentImage += 1;
                UpdateImage();
            }
          
        }
        private void PreviousImageBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveAltText();
            //RefreshFilteredList();
            RefreshImageList();
            currentImage -= 1;
            UpdateImage();
        }

        private void NextImageBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (filteredList != null)
            {
                if (currentImage >= (filteredList.Count - 1))
                {
                    e.CanExecute = false;
                }
                else
                {
                    e.CanExecute = true;
                }
            }
        }
        private void DeleteImageBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (filteredList != null)
            {
                if (filteredList.Count > 0)
                {
                    e.CanExecute = true;
                }                
            }
            else
            {
                e.CanExecute = false;
            }
            
            }
        private void PreviousImageBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (currentImage <= 0)
            {
                e.CanExecute = false;
            }
            else
            {
                e.CanExecute = true;
            }
        }

        private void emptiesOnly_Click(object sender, RoutedEventArgs e)
        {
            SaveAltText();
            RefreshFilteredList();
            UpdateImage();
        }

        private void urlsEmpty_Click(object sender, RoutedEventArgs e)
        {
            SaveAltText();
            RefreshFilteredList();
            UpdateImage();
        }

        private void currentPageBox_LostFocus(object sender, RoutedEventArgs e)
        {
            int myValue = -1;
            if (int.TryParse(currentPageBox.Text, out myValue))
            {
                SaveAltText();
                myValue -= 1;
                currentImage = Globals.ThisAddIn.Clamp(myValue, filteredList.Count - 1, 0);
            }
            UpdateImage();
        }

        private void currentPageBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;

            int myValue = -1;
            if (int.TryParse(currentPageBox.Text, out myValue))
            {
                SaveAltText();
                myValue -= 1;
                currentImage = Globals.ThisAddIn.Clamp(myValue, filteredList.Count - 1, 0);
            }
            UpdateImage();
        }

        private void currentPageBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int myValue = -1;
            if (int.TryParse(currentPageBox.Text, out myValue))
            {
                RefreshImageList();
                RefreshFilteredList();
                SaveAltText();
                myValue -= 1;
                currentImage = Globals.ThisAddIn.Clamp(myValue, filteredList.Count - 1, 0);
            }
            UpdateImage();
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
           
            SaveAltText();
            RefreshImageList();
            UpdateImage();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SaveAltText();
        }

        private void finishButton_Click(object sender, RoutedEventArgs e)
        {
            SaveAltText();
            if (OnRequestClose != null)
            {
                OnRequestClose();
            }

            RoutedEventArgs args = new RoutedEventArgs(ImageAltTextEditorControl.FinishEvent);
            RaiseEvent(args);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshImageList();
            UpdateImage();
        }

        private void updateTranscribersNoteButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateTranscribersNote();
        }

        private void clearTranscribersNoteButton_Click(object sender, RoutedEventArgs e)
        {
            descriptionBox.Text = "";
            pageNumberBox.Text = "";
            UpdateTranscribersNote();
        }

        private void descriptionBox_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateTranscribersNote();
        }

        private void pageNumberBox_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateTranscribersNote();
        }

        #endregion
        
    }
}
