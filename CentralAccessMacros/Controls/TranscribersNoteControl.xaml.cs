﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for TranscribersNoteControl.xaml
    /// </summary>
    public partial class TranscribersNoteControl : UserControl
    {

        public delegate void BasicWindowDelegate();
        public event BasicWindowDelegate OnRequestClose;

        public TranscribersNoteControl()
        {
            InitializeComponent();
        }

        private void InsertButton_Click(object sender, RoutedEventArgs e)
        {
            InsertIntoDocument();
            OnRequestClose();
        }

        private void InsertIntoDocument()
        {
            Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
            TranscribersNote.InsertTranscribersNote(range, descriptionBox.Text, pageNumberBox.Text, altTextBox.Text);
        }

        private void pageNumberBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            e.Handled = true;

            InsertIntoDocument();
            OnRequestClose();
        }
    }
}
