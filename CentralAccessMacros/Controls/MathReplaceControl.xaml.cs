﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for MathReplaceControl.xaml
    /// </summary>
    public partial class MathReplaceControl : UserControl, INotifyPropertyChanged
    {
        public MathReplaceControl()
        {
            InitializeComponent();
        }        

        #region Properties

        private string _findTerms = "";
        public string FindTerms
        {
            get
            {
                return _findTerms;
            }
            set
            {
                _findTerms = value;
                PropertyChanged(this, new PropertyChangedEventArgs("FindTerms"));
            }
        }

        private string _replaceTerms = "";
        public string ReplaceTerms
        {
            get
            {
                return _replaceTerms;
            }
            set
            {
                _replaceTerms = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ReplaceTerms"));
            }
        }

        private bool _matchCase = false;
        public bool MatchCase
        {
            get
            {
                return _matchCase;
            }
            set
            {
                _matchCase = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MatchCase"));
            }
        }

        private bool _wholeWords = true;
        public bool WholeWords
        {
            get
            {
                return _wholeWords;
            }
            set
            {
                _wholeWords = value;
                PropertyChanged(this, new PropertyChangedEventArgs("WholeWords"));
            }
        }

        private bool _wildcards = false;
        public bool Wildcards
        {
            get
            {
                return _wildcards;
            }
            set
            {
                _wildcards = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Wildcards"));
            }
        }

        public class InsertMathSymbolCommand
        {
            public string Symbol { get; set; }
            public InsertSymbolDelegate Function { get; set; }

            DelegateCommand _functionCommand { get; set; }
            public ICommand FunctionCommand
            {
                get
                {
                    if (_functionCommand == null)
                    {
                        _functionCommand = new DelegateCommand(() => Function(Symbol));
                    }
                    return _functionCommand;
                }
            }
        }
        public delegate void InsertSymbolDelegate(string symbol);

        public List<InsertMathSymbolCommand> MathSymbols
        {
            get
            {
                return new List<InsertMathSymbolCommand>
                {
                    new InsertMathSymbolCommand
                    {
                        Symbol = "√",
                        Function = InsertMathSymbol
                    },
                    new InsertMathSymbolCommand
                    {
                        Symbol = "×",
                        Function = InsertMathSymbol
                    },
                    new InsertMathSymbolCommand
                    {
                        Symbol = "±",
                        Function = InsertMathSymbol
                    },
                    new InsertMathSymbolCommand
                    {
                        Symbol = "÷",
                        Function = InsertMathSymbol
                    }, 
                    //new InsertMathSymbolCommand
                    //{
                    //    Symbol = "∑",
                    //    Function = InsertMathSymbol
                    //},                    
                    new InsertMathSymbolCommand
                    {
                        Symbol = "Superscript",
                        Function = InsertMathSymbol
                    },                    
                    new InsertMathSymbolCommand
                    {
                        Symbol = "Subscript",
                        Function = InsertMathSymbol
                    },                    
                };
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private IEnumerable<Word.Range> DoFind(Word.Range searchRange)
        {
            return Globals.ThisAddIn.Find(searchRange, FindTerms, useWildcards: Wildcards, matchCase: MatchCase, wholeWords: WholeWords,
                wrapSearch: Word.WdFindWrap.wdFindContinue);
        }

        private Word.Range DoReplace(Word.Range searchRange)
        {
            return Globals.ThisAddIn.Replace(searchRange, FindTerms, ReplaceTerms,
                useWildcards: Wildcards, matchCase: MatchCase, wholeWords: WholeWords,
                wrapSearch: Word.WdFindWrap.wdFindContinue);
        }

        private void InsertMathSymbol(string symbol)
        {
            switch (symbol)
            {
                case "Superscript": symbol = "^^";
                    break;
                case "Subscript": symbol = "_";
                    break;
            }

            ReplaceTerms = txtBoxReplace.Text.Insert(txtBoxReplace.SelectionStart, symbol);
        }

        private bool MyFindNext()
        {
            Word.Range mySearchRange = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            mySearchRange.Start = selection.End;

            if (DoFind(Globals.ThisAddIn.CopyRange(mySearchRange)).Any())
            {
                mySearchRange = DoFind(mySearchRange).First();
                mySearchRange.Select();
                return true;
            }
            //else
            //{
            //    // Try looping back around
            //    mySearchRange = Globals.ThisAddIn.GetActiveDocument().Content;
            //    if (DoFind(Globals.ThisAddIn.CopyRange(mySearchRange)).Any())
            //    {
            //        mySearchRange = DoFind(mySearchRange).First();
            //        mySearchRange.Select();
            //        return true;
            //    }
            //}

            return false;
        }

        private bool MyReplace()
        {            
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            selection.End = Globals.ThisAddIn.GetActiveDocument().Content.End;

            selection = DoReplace(selection);
            if (selection != null)
                Math.ConvertRangeToMath(selection);

            return MyFindNext();
        }

        #region Command Callbacks

        private void FindNext_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void FindNext_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!MyFindNext())
            {
                MessageBox.Show("Found no more occurrences.");
            }
        }

        private void Replace_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Replace_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MyReplace();
        }

        private void ReplaceAll_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ReplaceAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            bool foundOne = false;

            while (MyReplace())
                foundOne = true;

            if (foundOne)
                MessageBox.Show("Replaced all occurrences.");
            else
                MessageBox.Show("Found no more occurrences.");
        }

        #endregion
    }

    public static class MathReplaceCommands
    {
        public static readonly RoutedUICommand FindNext = new RoutedUICommand("Find Next", "FindNext", typeof(MathReplaceCommands));
        public static readonly RoutedUICommand Replace = new RoutedUICommand("Replace", "Replace", typeof(MathReplaceCommands));
        public static readonly RoutedUICommand ReplaceAll = new RoutedUICommand("Replace All", "ReplaceAll", typeof(MathReplaceCommands));
    }
}
