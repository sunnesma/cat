﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for GuideMeCheckBox.xaml
    /// </summary>
    public partial class GuideMeCheckBox : UserControl
    {

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool), typeof(GuideMeCheckBox), new PropertyMetadata(false));
        public bool IsChecked
        {
            get
            {
                return (bool)GetValue(IsCheckedProperty);
            }
            set
            {
                SetValue(IsCheckedProperty, value);
            }
        }

        private static readonly DependencyProperty ShowUncheckedProperty =
            DependencyProperty.Register("ShowUnchecked", typeof(bool), typeof(GuideMeCheckBox), new PropertyMetadata(false));
        private bool ShowUnchecked
        {
            get
            {
                return (bool)GetValue(ShowUncheckedProperty);
            }
            set
            {
                SetValue(ShowUncheckedProperty, value);
            }
        }

        public static readonly DependencyProperty DogEarWidthProperty =
            DependencyProperty.Register("DogEarWidth", typeof(int), typeof(GuideMeCheckBox), new PropertyMetadata(24));
        public int DogEarWidth
        {
            get
            {
                return (int)GetValue(DogEarWidthProperty);
            }
            set
            {
                SetValue(DogEarWidthProperty, value);
            }
        }

        public static readonly DependencyProperty DogEarHeightProperty =
            DependencyProperty.Register("DogEarHeight", typeof(int), typeof(GuideMeCheckBox), new PropertyMetadata(24));
        public int DogEarHeight
        {
            get
            {
                return (int)GetValue(DogEarHeightProperty);
            }
            set
            {
                SetValue(DogEarHeightProperty, value);
            }
        }

        public GuideMeCheckBox()
        {
            InitializeComponent();
        }

        private void checkButton_Click(object sender, RoutedEventArgs e)
        {
            IsChecked = !IsChecked;
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            ShowUnchecked = true;
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            ShowUnchecked = false;
        }
    }
}
