﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for ChartConverterQuestionControl.xaml
    /// </summary>
    public partial class ChartConverterQuestionControl : UserControl
    {

        public static readonly RoutedEvent StartEvent = EventManager.RegisterRoutedEvent("Start", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ChartConverterQuestionControl));

        public event RoutedEventHandler Start
        {
            add { AddHandler(StartEvent, value); }
            remove { RemoveHandler(StartEvent, value); }
        }

        public ChartConverterQuestionControl()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventArgs args = new RoutedEventArgs(ChartConverterQuestionControl.StartEvent);
            RaiseEvent(args);
        }
    }
}
