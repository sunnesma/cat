﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for LargePrintControl.xaml
    /// </summary>
    public partial class LargePrintControl : UserControl
    {

        public delegate void BasicWindowDelegate();
        public event BasicWindowDelegate OnRequestClose;

        public LargePrintControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            MyPageFormat = PageFormats.First();
            MyBaseSize = BaseSizes.First();
            CustomWidth = 8.5f;
            CustomHeight = 11.0f;
            this.DataContext = this;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (MyPageFormat.IsCustom)
            {
                MyPageFormat.Width = CustomWidth;
                MyPageFormat.Height = CustomHeight;
            }

            LargePrint.FormatAll(MyBaseSize, (bool)doubleImageSizeCheckBox.IsChecked,(bool)tripleImageSizeCheckBox.IsChecked, MyPageFormat, (bool)underlineHeadingsCheckBox.IsChecked, (bool)SetLineSpacingCheckBox.IsChecked, (bool)SetParagraphSpacingCheckBox.IsChecked, (bool)FormatTablesCheckbox.IsChecked, (bool)AddPageBreakBeforePageNumbersCheckbox.IsChecked, (bool)PutChaptersOnOddPageCheckbox.IsChecked,(bool)RemoveEquationHighlightsCheckbox.IsChecked,(bool)ConvertListtoTextCheckbox.IsChecked, this);
            MessageBox.Show("Done!");
            OnRequestClose();

            //if (int.TryParse(myNumber, out num))
            //{
            //    LargePrint.FormatAll(num, (bool)doubleImageSizeCheckBox.IsChecked, (PageFormat)pageFormatsBox.SelectedItem);
            //    MessageBox.Show("Done!");
            //    OnRequestClose();
            //}
            //else
            //{
            //    MessageBox.Show(myNumber + " is not a valid number.");
            //}
        }

        public float CustomWidth { get; set; }
        public float CustomHeight { get; set; }

        public PageFormat MyPageFormat { get; set; }
        public List<PageFormat> PageFormats
        {
            get
            {
                List<PageFormat> formats = new List<PageFormat>
                {
                    new PageFormat("US Braille Paper", 11.5f, 11.0f),
                    new PageFormat("US Letter", 8.5f, 11.0f),
                    new PageFormat("Tabloid", 11.0f, 17.0f),
                    new PageFormat("Legal", 8.5f, 14.0f),
                    new PageFormat("Executive", 7.25f, 10.5f),
                    new PageFormat("A3", 11.69f, 16.54f),
                    new PageFormat("A4", 8.27f, 11.69f),
                    new PageFormat("B4 (JIS)", 10.12f, 14.33f),
                    new PageFormat("B5 (JIS)", 7.17f, 10.12f),
                    new PageFormat("", 0.0f, 0.0f, isCustom: true)
                };

                return formats;
            }
        }

        public int MyBaseSize { get; set; }
        public List<int> BaseSizes
        {
            get
            {
                List<int> sizes = new List<int>();
                for (int i = 18; i <= 100; i++)
                {
                    sizes.Add(i);
                }
                return sizes;
            }
        }

        private void baseSizeCombo_LostFocus(object sender, RoutedEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            if (box.SelectedIndex < 0)
            {
                box.SelectedIndex = 0;
            }
        }
    }
}
