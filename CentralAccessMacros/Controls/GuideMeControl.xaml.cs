﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{
    /// <summary>
    /// Interaction logic for GuideMeControl.xaml
    /// </summary>
    public partial class GuideMeControl : UserControl
    {
        public GuideMeControl()
        {
            InitializeComponent();
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink mySender = (Hyperlink)sender;
            Process.Start(mySender.NavigateUri.ToString());
        }
    }
    
    [ValueConversion(typeof(int), typeof(int))]
    public class Base1IndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int myNumber = (int)value;
            return myNumber + 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(int), typeof(Brush))]
    public class AlternationToColorConverter : IValueConverter
    {

        private static Color[] Colors = new Color[]
        {
            Color.FromRgb(40, 80, 173), // Blue
            Color.FromRgb(227, 96, 34), // Orange
            Color.FromRgb(185, 51, 173), // Purple
            Color.FromRgb(0, 147, 60), // Green
            Color.FromRgb(153, 102, 51), // Brown
        };

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int myNumber = (int)value;
            int myColor = myNumber % Colors.Length;
            return new SolidColorBrush(Colors[myColor]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
