﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentralAccessMacros
{

    /// <summary>
    /// Interaction logic for SuiteSetupControl.xaml
    /// </summary>
    public partial class AudioSuiteSetupControl : UserControl
    {
        List<SuiteFunction> functionsList;
        public List<CheckableItem<SuiteFunction>> Functions { get; set; }

        public AudioSuiteSetupControl()
        {
            InitializeComponent();
        }

        public void LoadFunctions(List<SuiteFunction> functions,string ocrSourceType="OmniPage")
        {
            Functions = new List<CheckableItem<SuiteFunction>>();
            this.functionsList = functions;
            foreach (SuiteFunction d in functions)
            {
                Functions.Add(new CheckableItem<SuiteFunction>(d));
            }

            foreach (CheckableItem<SuiteFunction> i in Functions)
            {
                // Check it if it is not optional to run it
                IList<object> attributes = i.Value.Method.GetCustomAttributes(typeof(SuiteDescription), false);
                bool optional = false;
                if (attributes.Count > 0)
                {
                    SuiteDescription d = (SuiteDescription)attributes[0];
                    //Considering ocrSourceType_default = "OmniPage";

                    if (ocrSourceType.Equals("OmniPage"))
                    {
                        optional = d.Optional1;
                    }
                    else if (ocrSourceType.Equals("INFTY Reader XML"))
                    {
                        optional = d.Optional2;
                    }
                    else if (ocrSourceType.Equals("ABBYY Fine Reader"))
                    {
                        optional = d.Optional3;
                    }
                    //else if (ocrSourceType.Equals("Bookshare Word Doc"))
                    //{
                    //    optional = d.Optional4;
                    //}
                }

                i.IsChecked = !optional;
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        public List<SuiteFunction> GetAcceptedFunctions()
        {
            List<SuiteFunction> myFunctions = new List<SuiteFunction>();

            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                if (item.IsChecked)
                    myFunctions.Add(item.Value);
            }

            return myFunctions;
        }

        public string SuiteName { get; set; }

        private void selectAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                item.IsChecked = true;
            }

            this.DataContext = null;
            this.DataContext = this;
        }

        private void deselectAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckableItem<SuiteFunction> item in Functions)
            {
                item.IsChecked = false;
            }

            this.DataContext = null;
            this.DataContext = this;
        }
         private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            // ... A List.
            List<string> data = new List<string>();
            data.Add("OmniPage");
            data.Add("INFTY Reader XML");
            data.Add("ABBYY Fine Reader");
            //data.Add("Bookshare Word Doc");
            
            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;

            // ... Use SelectedItem to suggest functions.
           string comboBoxValue = comboBox.SelectedItem as string;
           LoadFunctions(this.functionsList, comboBoxValue);

        }
        
    }
}

 