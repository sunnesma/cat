﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    class TranscribersNote
    {
        public static void ShowTranscribersNoteDialog()
        {
            TranscribersNoteForm f = new TranscribersNoteForm();
            Globals.ThisAddIn.ShowForm(f);
        }

        public static void InsertTranscribersNote(Word.Range range,
            string description = "",
            string pageNumber = "",
            string altText = "",
            bool haveCloseTag = false)
        {
            range.Collapse(Word.WdCollapseDirection.wdCollapseStart);

            // See if I'm starting on an empty paragraph. If not, create
            // another paragraph to fit the note.
            if (range.Paragraphs[1].Range.Text.Trim().Length != 0)
            {
                range.InsertParagraph();
                range.Move(Word.WdUnits.wdParagraph, 1);
            }

            // Record some ranges so that I can style them later
            int noteStart = 0;
            int noteEnd = 0;
            int descriptionStart = 0;
            int descriptionEnd = 0;
            int pageStart = 0;
            int pageEnd = 0;

            // Record start
            noteStart = range.Start;

            range.Text = "Transcriber's note: ";

            range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

            // Insert tactile reference if those fields are filled
            if (description.Length > 0 || pageNumber.Length > 0)
            {
                // Tactile description (add space if empty)
                if (description.Length > 0)
                    range.Text = description;
                else
                    range.Text = " ";
                descriptionStart = range.Start;
                descriptionEnd = range.End;

                range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                range.Text = " is found as a tactile graphic on page ";

                range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                // Page number (add space if empty)
                if (pageNumber.Length > 0)
                    range.Text = pageNumber;
                else
                    range.Text = " ";
                pageStart = range.Start;
                pageEnd = range.End;

                range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                // Alt text, if any
                if (altText.Trim().Length > 0)
                {
                    range.Text = ". " + altText.Trim();

                }
                else
                {
                    range.Text = ".";
                }
            }
            else
            {
                // Alt text, if any
                if (altText.Trim().Length > 0)
                {
                    range.Text = altText.Trim();
                }
                else
                {
                    range.Text = ".";
                }
            }

            if (haveCloseTag)
            {
                range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                range.Text = " End transcriber's note.";
            }

            range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

            // Record end
            noteEnd = range.End;

            // Set paragraph style
            range.SetRange(noteStart, noteEnd);
            range.Paragraphs[1].set_Style(TranscribersNote.ParagraphStyle);
            range.Paragraphs[1].Range.set_Style(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);

            // Set character styles
            range.SetRange(descriptionStart, descriptionEnd);
            range.set_Style(TranscribersNote.TactileDescriptionStyle);

            range.SetRange(pageStart, pageEnd);
            range.set_Style(TranscribersNote.TactilePageNumberStyle);
        }

        [CheckerDescription("Tactile references in order")]
        public static CheckerResult CheckGoodTranscriberNotes()
        {
            List<TranscribersNoteItem> items = TranscribersNote.Items;

            if (items.Count == 0)
                return CheckerResult.Empty;

            foreach (TranscribersNoteItem item in items)
            {
                if (!item.IsGood)
                    return CheckerResult.Failed;
            }
            return CheckerResult.Passed;
        }

        public static UserControl ShowTranscriberNoteEditor()
        {
            TranscribersCheckerForm f = new TranscribersCheckerForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Transcriber's Checker", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
            //return Globals.ThisAddIn.ShowForm(f);
        }

        #region Styles

        /// <summary>
        /// Gets the transcriber paragraph style. If it doesn't exist, then
        /// it creates one.
        /// </summary>
        public static Word.Style ParagraphStyle
        {
            get
            {
                string styleName = "Transcriber's Note";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraphOnly);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleNormal);

                newStyle.ParagraphFormat.Shading.BackgroundPatternColor = Word.WdColor.wdColorGray05;
                newStyle.ParagraphFormat.KeepWithNext = (int)VBABoolean.True;

                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);

                List<Word.WdBorderType> borders = new List<Word.WdBorderType>
                {
                    Word.WdBorderType.wdBorderLeft,
                    Word.WdBorderType.wdBorderRight,
                    Word.WdBorderType.wdBorderTop,
                    Word.WdBorderType.wdBorderBottom
                };

                foreach (Word.WdBorderType b in borders)
                {
                    newStyle.Borders[b].LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    newStyle.Borders[b].LineWidth = Word.WdLineWidth.wdLineWidth050pt;
                    newStyle.Borders[b].Color = Word.WdColor.wdColorGray05;
                }

                newStyle.Borders.DistanceFromTop = 10;
                newStyle.Borders.DistanceFromBottom = 10;
                newStyle.Borders.DistanceFromLeft = 10;
                newStyle.Borders.DistanceFromRight = 10;

                return newStyle;
            }
        }

        /// <summary>
        /// Gets the style for the description of the graphic in the
        /// transcriber's note. Creates a new one if it doesn't exist in this
        /// document.
        /// </summary>
        public static Word.Style TactileDescriptionStyle
        {
            get
            {
                string styleName = "Transcriber's Description";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);

                newStyle.Font.Color = Word.WdColor.wdColorRed;
                // Turning off bold because it shows up in Braille
                //newStyle.Font.Bold = 1;

                return newStyle;
            }
        }

        /// <summary>
        /// Gets the style for the page number of the tactile in the
        /// transcriber's note. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style TactilePageNumberStyle
        {
            get
            {
                string styleName = "Transcriber's Page Number";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.set_BaseStyle(Word.WdBuiltinStyle.wdStyleDefaultParagraphFont);

                newStyle.Font.Color = Word.WdColor.wdColorGreen;
                // Turning off bold because it shows up in Braille
                //newStyle.Font.Bold = 1;

                return newStyle;
            }
        }

        #endregion

        public static List<TranscribersNoteItem> Items
        {
            get
            {
                List<TranscribersNoteItem> items = new List<TranscribersNoteItem>();
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

                // Get the ranges of all items I need first. Batching searches
                // like this is faster.
                List<Word.Range> noteRanges = Globals.ThisAddIn.Find(range, "", style: TranscribersNote.ParagraphStyle).ToList();
                List<Word.Range> descriptionRanges = Globals.ThisAddIn.Find(range, "", style: TranscribersNote.TactileDescriptionStyle).ToList();
                List<Word.Range> pageNumberRanges = Globals.ThisAddIn.Find(range, "", style: TranscribersNote.TactilePageNumberStyle).ToList();

                // Used for checking prefixes and if numbers are in chronological order
                int lastMajorNumber = -1;
                int lastMinorNumber = -1;
                bool isLastMinorNumberValid = false;
                bool isPrefixExpected = false;
                int count = 0;
                String lastPrefix = null;
                HashSet<String> setPageNumberPrefix = new HashSet<string>();

                foreach (Word.Range r in noteRanges)
                {
                    TranscribersNoteItem myItem = new TranscribersNoteItem();

                    // Check for tactile description
                    foreach (Word.Range myRange in descriptionRanges)
                    {
                        if ((myRange.Start >= r.Start) && (myRange.End <= r.End))
                        {
                            myItem.Description = myRange;
                            descriptionRanges.Remove(myRange);
                            break;
                        }
                    }

                    // Check for page number
                    foreach (Word.Range myRange in pageNumberRanges)
                    {
                        if ((myRange.Start >= r.Start) && (myRange.End <= r.End))
                        {
                            myItem.PageNumber = myRange;
                            pageNumberRanges.Remove(myRange);
                            break;
                        }
                    }

                    // Add it if it contains a description or page number. If
                    // it doesn't have either, it is ignored.
                    if ((myItem.Description != null) || (myItem.PageNumber != null))
                    {
                        myItem.WholeRange = r;

                        // Assess the item to see if it good or not
                        if (myItem.Description == null)
                        {
                            myItem.BadEvaluation = "No description.";
                        }
                        else if (myItem.PageNumber == null)
                        {
                            myItem.BadEvaluation = "No page number.";
                        }
                        else if (myItem.PageNumberText.Contains(" "))
                            myItem.BadEvaluation = "Invalid Page Number.";
                        else
                        {
                            // Check page number for chronological order
                            Match match = Regex.Match(myItem.PageNumberText, @"([A-z]*)(\d+)([.]\d+){0,1}");

                            #region Prefix check
                            bool isPrefixGood = true;
                            String prefix = match.Groups[1].Value;

                            // First run. Checking if prefix is expected or not, based on the first page number.
                            if (count++ == 0 && prefix.Trim().Length != 0)
                                isPrefixExpected = true;

                            if (isPrefixExpected)
                            {
                                if (prefix.Trim().Length == 0)
                                    isPrefixGood = false;

                                //first run when prefix is expected
                                else if (lastPrefix == null)
                                {
                                    lastPrefix = prefix;
                                    setPageNumberPrefix.Add(prefix);
                                }
                                else
                                {
                                    if (lastPrefix.Equals(prefix))
                                        lastPrefix = prefix;
                                    else if (setPageNumberPrefix.Contains(prefix))
                                        isPrefixGood = false;
                                    else
                                    {
                                        setPageNumberPrefix.Add(prefix);
                                        lastPrefix = prefix;
                                    }
                                }
                            }
                            else
                                if (prefix.Trim().Length != 0)
                                    isPrefixGood = false;
                            #endregion

                            #region Page Number check
                            bool isPageNumberGood = match.Success;
                            if (match.Success)
                            {
                                int currMajorNumber, currMinorNumber;
                                currMajorNumber = int.Parse(match.Groups[2].Value);
                                bool isCurrMinorNumberValid;

                                // Check if minor number is provided or not. 
                                try
                                {
                                    currMinorNumber = int.Parse(match.Groups[3].Value.Substring(1));
                                    isCurrMinorNumberValid = true;
                                }
                                catch (System.ArgumentOutOfRangeException e)
                                {
                                    currMinorNumber = 0;
                                    isCurrMinorNumberValid = false;
                                }

                                // Check that this wasn't the first one.
                                if ((lastMajorNumber >= 0) && (lastMinorNumber >= 0))
                                {
                                    // Check if major numbers are different. If
                                    // they are, check those
                                    if (lastMajorNumber == currMajorNumber)
                                    {
                                        // Check the minor ones
                                        if (isLastMinorNumberValid && isCurrMinorNumberValid)
                                            isPageNumberGood = isPageNumberGood && (currMinorNumber == (lastMinorNumber + 1));
                                        else
                                            isPageNumberGood = false;
                                    }
                                    else
                                    {
                                        isPageNumberGood = isPageNumberGood && (currMajorNumber == (lastMajorNumber + 1));
                                    }
                                }

                                lastMajorNumber = currMajorNumber;
                                lastMinorNumber = currMinorNumber;
                                isLastMinorNumberValid = isCurrMinorNumberValid;
                            }
                            #endregion

                            if (!isPageNumberGood && !isPrefixGood)
                                myItem.BadEvaluation = "Bad prefix and page number.";
                            else if (!isPageNumberGood)
                                myItem.BadEvaluation = "Bad page number.";
                            else if (!isPrefixGood)
                                myItem.BadEvaluation = "Bad prefix.";
                        }

                        items.Add(myItem);
                    }
                }

                return items;
            }
        }

        public static void StyleAsTactileDescription(Word.Range range)
        {
            General.FormatCharacterStyle(range, TranscribersNote.TactileDescriptionStyle);
        }

        public static void StyleAsTactilePageNumber(Word.Range range)
        {
            General.FormatCharacterStyle(range, TranscribersNote.TactilePageNumberStyle);
        }
    }

    public class TranscribersNoteItem
    {
        public Word.Range WholeRange { get; set; }

        public string BadEvaluation { get; set; }
        public bool IsGood
        {
            get
            {
                if (BadEvaluation != null)
                    return BadEvaluation.Length == 0;
                else
                    return true;
            }
        }

        public Word.Range Description { get; set; }
        public string DescriptionText
        {
            get
            {
                if (Description != null)
                    return Description.Text;
                return "";
            }
        }

        public Word.Range PageNumber { get; set; }
        public string PageNumberText
        {
            get
            {
                if (PageNumber != null)
                    return PageNumber.Text;
                return "";
            }
        }
    }
}
