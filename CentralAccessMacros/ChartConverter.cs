﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
            
namespace CentralAccessMacros
{
    class ChartConverter
    {
        [CheckerDescription("Free of charts and SmartArt")]
        public static CheckerResult CheckForAssessibleCharts()
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Shape s in range.ShapeRange)
            {
                if (s.Type == Microsoft.Office.Core.MsoShapeType.msoChart)
                    return CheckerResult.Failed;

                if (s.Type == Microsoft.Office.Core.MsoShapeType.msoSmartArt)
                    return CheckerResult.Failed;
            }

            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (s.Type == Word.WdInlineShapeType.wdInlineShapeChart)
                    return CheckerResult.Failed;
                
                if (s.Type == Word.WdInlineShapeType.wdInlineShapeSmartArt)
                    return CheckerResult.Failed;
            }

            return CheckerResult.Passed;
        }

        public static UserControl ShowChartConverter()
        {
            ChartConverterWizardForm f = new ChartConverterWizardForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Chart Converter", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
//            return Globals.ThisAddIn.ShowForm(f);
        }

        public static void ConvertAll()
        {
            ConvertCharts();
            ConvertSmartArt();
        }

        private static void ConvertCharts()
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Shape s in range.ShapeRange)
            {
                if (s.Type == Microsoft.Office.Core.MsoShapeType.msoChart)
                {
                    // Extract table of data out of chart
                    Word.Chart chart = s.Chart;
                    Word.ChartData data = chart.ChartData;
                    data.Activate();
                    Excel.Workbook wb = (Excel.Workbook)data.Workbook;
                    Excel.Worksheet ws = wb.Worksheets[1];
                    ws.Rows.Copy();
                    wb.Close();

                    // Paste as new table below shape
                    s.Select();
                    Word.Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                    selectionRange.Move(Word.WdUnits.wdParagraph, 1);
                    selectionRange.InsertParagraph();
                    selectionRange.Select();
                    selectionRange.Paste();

                    // Convert the original shape to an image
                    s.Select();
                    Word.Range myRange = Globals.ThisAddIn.Application.Selection.Range;
                    myRange.CopyAsPicture();
                    myRange.Paste();
                }
            }

            range = Globals.ThisAddIn.GetActiveDocument().Content;
            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (s.Type == Word.WdInlineShapeType.wdInlineShapeChart)
                {
                    // Extract table of data out of chart
                    Word.Chart chart = s.Chart;
                    Word.ChartData data = chart.ChartData;
                    data.Activate();
                    Excel.Workbook wb = (Excel.Workbook)data.Workbook;
                    Excel.Worksheet ws = wb.Worksheets[1];
                    ws.Rows.Copy();
                    wb.Close();

                    // Paste as new table below shape
                    s.Select();
                    Word.Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                    selectionRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    selectionRange.InsertParagraph();
                    selectionRange.Move(Word.WdUnits.wdParagraph, 1);
                    selectionRange.Text = "Chart Data:";
                    selectionRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    selectionRange.Move(Word.WdUnits.wdParagraph, 1);
                    selectionRange.InsertParagraph();
                    selectionRange.Select();
                    selectionRange.Paste();

                    // Modify the table to how I see fit
                    if (selectionRange.Tables.Count > 0)
                    {
                        Word.Table table = selectionRange.Tables[1];

                        // Make the borders plain and black
                        Word.WdBorderType[] borders = new Word.WdBorderType[]
                        {
                            Word.WdBorderType.wdBorderTop,
                            Word.WdBorderType.wdBorderBottom,
                            Word.WdBorderType.wdBorderLeft,
                            Word.WdBorderType.wdBorderRight,
                            Word.WdBorderType.wdBorderHorizontal,
                            Word.WdBorderType.wdBorderVertical
                        };
                        foreach (Word.WdBorderType t in borders)
                        {
                            Word.Border b = table.Borders[t];
                            b.LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                            b.LineWidth = Word.WdLineWidth.wdLineWidth100pt;
                            b.Color = Word.WdColor.wdColorBlack;
                        }

                        // Remove unnecessary rows. Basically, any row besides
                        // the first that doesn't have text in the first cell
                        // is an unnecessary row.
                        bool gotFirst = false;
                        foreach (Word.Row r in table.Rows)
                        {
                            if (gotFirst)
                            {
                                string myText = r.Cells[1].Range.Text.Trim();

                                // Remove character 0x07 too. It's "BELL"
                                // apparently
                                myText = myText.Replace("\x07", "");

                                if (myText.Length == 0)
                                {
                                    r.Delete();
                                }
                            }
                            else
                            {
                                gotFirst = true;
                            }
                        }
                    }
                    else
                    {
                        Debug.Print("ERROR: Didn't detect table in data pasted from chart!");
                    }

                    try
                    {
                        s.Select();
                        Globals.ThisAddIn.Application.Selection.Copy();
                        s.Range.PasteSpecial(DataType: Word.WdPasteDataType.wdPasteBitmap | Word.WdPasteDataType.wdPasteText | Word.WdPasteDataType.wdPasteShape);
                    }
                    catch (COMException e)
                    {
                        s.Select();
                        Word.Range chartRange = Globals.ThisAddIn.Application.Selection.Range;
                        chartRange.CopyAsPicture();
                        chartRange.PasteSpecial(DataType: Word.WdPasteDataType.wdPasteBitmap);
                    }

                    // Delete the original image
                    s.Delete();
                }
            }
        }

        private class SmartArtText
        {
            public SmartArtText(int level, string text)
            {
                Level = level;
                Text = text;
            }
            public int Level { get; set; }
            public string Text { get; set; }
        }

        private static void ConvertSmartArt()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            foreach (Word.InlineShape s in doc.InlineShapes)
            {
                if (s.Type == Word.WdInlineShapeType.wdInlineShapeSmartArt)
                {
                    // Extract list data out of SmartArt
                    Microsoft.Office.Core.SmartArt art = s.SmartArt;

                    List<SmartArtText> texts = new List<SmartArtText>();
                    foreach (Microsoft.Office.Core.SmartArtNode n in art.AllNodes)
                    {
                        texts.Add(new SmartArtText(n.Level, n.TextFrame2.TextRange.Text));
                    }

                    // Insert as bulleted list below the SmartArt
                    Word.Range myRange = s.Range;
                    myRange.Move(Word.WdUnits.wdCharacter, 1);
                    myRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    myRange.InsertParagraph();
                    myRange.Move(Word.WdUnits.wdParagraph, 1);
                    myRange.Text = "SmartArt Data:";
                    myRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                    myRange.InsertParagraphAfter();
                    myRange.Move(Word.WdUnits.wdParagraph, 1);

                    CreateList(myRange, texts);

                    // Convert the original shape to an image
                    try
                    {
                        s.Select();
                        Globals.ThisAddIn.Application.Selection.Copy();
                        s.Range.PasteSpecial(DataType: Word.WdPasteDataType.wdPasteBitmap | Word.WdPasteDataType.wdPasteText | Word.WdPasteDataType.wdPasteShape);
                    }
                    catch (COMException e)
                    {
                        s.Select();
                        Word.Range smartArtRange = Globals.ThisAddIn.Application.Selection.Range;
                        smartArtRange.CopyAsPicture();
                        smartArtRange.PasteSpecial(DataType: Word.WdPasteDataType.wdPasteBitmap);
                    }

                    // Delete the original image
                    s.Delete();
                }
            }
        }

        /// <summary>
        /// Generates an unordered list from a list of items. It assumes that
        /// the range is set at a new paragraph 
        /// </summary>
        private static void CreateList(Word.Range range, List<SmartArtText> textList)
        {
            Word.ListGallery bulletGallery = Globals.ThisAddIn.Application.ListGalleries[Word.WdListGalleryType.wdBulletGallery];
            range.ListFormat.ApplyListTemplateWithLevel(bulletGallery.ListTemplates[1],
                false,
                Word.WdListApplyTo.wdListApplyToWholeList,
                Word.WdDefaultListBehavior.wdWord10ListBehavior);

            foreach (SmartArtText text in textList)
            {
                Word.ListFormat listThing = range.ListFormat;

                // Indent to right place
                int myListIndent = listThing.ListLevelNumber;
                while (myListIndent != text.Level)
                {
                    if (text.Level < myListIndent)
                    {
                        listThing.ListOutdent();
                        myListIndent -= 1;
                    }
                    else
                    {
                        listThing.ListIndent();
                        myListIndent += 1;
                    }
                }

                // Put text in
                range.Text = text.Text;

                // Add another paragraph
                range.InsertParagraphAfter();
                range.Move(Word.WdUnits.wdParagraph, 1);
            }

            range.ListFormat.RemoveNumbers(Word.WdNumberType.wdNumberParagraph);

        }
    }
}
