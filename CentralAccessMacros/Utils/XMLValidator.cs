﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.IO;

namespace CentralAccessMacros
{
    class XMLValidator
    {
        private bool _isValid;

        public XMLValidator(string xmlData, string schemaData)
        {
            _isValid = true;

            XmlReaderSettings config = new XmlReaderSettings();
            config.ValidationType = ValidationType.Schema;
            config.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            config.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            config.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
            config.ValidationEventHandler += config_ValidationEventHandler;
            config.Schemas.Add("", XmlReader.Create(new StringReader(schemaData)));

            XmlReader reader = XmlReader.Create(new StringReader(xmlData), config);
            while (reader.Read()) ;
        }

        void config_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
                Debug.Print("Warning from Guide Me: {0}", e.Message);
            else
            {
                Debug.Print("ERROR: Guide Me data did not validate! {0}", e.Message);
                _isValid = false;
            }
        }

        public bool IsValid
        {
            get
            {
                return _isValid;
            }
        }
    }
}
