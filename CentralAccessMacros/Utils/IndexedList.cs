﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace CentralAccessMacros
{
    public struct Indexed<T>
    {
        public int Index { get; private set; }
        public T Value { get; private set; }
        public Indexed(int index, T value)
            : this()
        {
            Index = index;
            Value = value;
        }

        public override string ToString()
        {
            return "(Indexed: " + Index + ", " + Value.ToString() + " )";
        }
    }

    public class Indexed
    {
        public static Indexed<T> Create<T>(int indexed, T value)
        {
            return new Indexed<T>(indexed, value);
        }
    }

    public class IndexedConverter : IValueConverter
    {
        public object Convert
            (object value
            , Type targetType
            , object parameter
            , CultureInfo culture
            )
        {
            IEnumerable t = value as IEnumerable;
            if (t == null)
            {
                return null;
            }

            IEnumerable<object> e = t.Cast<object>();

            return CreateIndexedCollection(e);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }

        private List<Indexed<T>> CreateIndexedCollection<T>(IEnumerable<T> collection)
        {
            T[] myCollection = collection.ToArray();
            List<Indexed<T>> newCollection = new List<Indexed<T>>();

            for (int i = 0; i < myCollection.Length; i++)
            {
                newCollection.Add(Indexed.Create(i, myCollection[i]));
            }

            return newCollection;
        }
    }

}
