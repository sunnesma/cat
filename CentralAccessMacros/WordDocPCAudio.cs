﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    /// <summary>
    /// Set of functions specifically for Word Doc and PC Audio production.
    /// </summary>
    class WordDocPCAudio
    {

        /// <summary>
        /// Runs a suite of functions for fixing and modifying the document.
        /// </summary>
        public static void RunSuite()
        {
            List<SuiteFunction> functions = new List<SuiteFunction>
            {
                General.SetDefaultFormatting,
                General.ClearNonDefaultFormatting,
                General.RemoveHyperlinks,
                General.CleanUpApostrophesQuotesDashes,
                General.RemoveEmptyParagraphs,
                General.ReplaceAutoNumbering,
                General.ConvertParenLetterToLetterDot,
                General.ConvertParenNumberToNumberDot,
                WordDocPCAudio.RemoveAllLineBreaks,
                WordDocPCAudio.RemoveWeirdEmdashes,
                WordDocPCAudio.FixBullets,
                WordDocPCAudio.FixSquareBullets,
                General.FixCommonOCRErrors,
                General.ClearFormattingInTables,
                //Math.StyleMathInDefaultFont, This was way too slow.
                Math.LinearizeOMMLEquations,
                Math.ReplaceDotWithTimes,
                Math.FixUnicodeCharacters,
                Math.ReplaceEmptyParensWithZero,
                Math.RemoveSpacesInMath,
                Math.UnlinearizeOMMLEquations,
                Math.ConvertOMMLToMathType,
                Math.HighlightEquations,
                WordDocPCAudio.ReplaceTabsWithSpaces,
                WordDocPCAudio.RemoveRedundantSpaces,
                Braille.RemoveRedundantPunctuation,
                General.TrimWhitespaceFromParagraphs,
                PageNumbers.StylePageNumbers,
                General.ConvertDAISYNumbersToPageNumbers,
                General.RepaginateDocument,
                General.ConvertImagesToDefaultFormat,
                General.AutoRepairParagraph,
                Math.DeleteDashSpace,
                Math.DeleteSpaceDash,
                General.RemoveAllImageAltText,
                WordDocPCAudio.FixEllipsesPrepare,
            };

            General.RunSuite(functions, "Prepare Document");
        }

        /// <summary>
        /// Runs a checker to assess the document on all of its standards.
        /// </summary>
        public static void RunChecker()
        {
            List<CheckerItem> checkItems = new List<CheckerItem>
            {
                new CheckerItem
                {
                    Checker = General.CheckImagesHaveAltText,
                    FixerForm = General.ShowImageAltTextEditor,
                    DescriptionPassed = "All images have alt-text. Images still need to be checked for quality. Select “Image Alt Text Editor” in Available Actions to review images.",
                    DescriptionFailed = "One or more images is missing alt-text. Select “Image Alt Text Editor” in Available Actions to review images.",
                    DescriptionEmpty = "CAT could not find any images in this document.",
                    DescriptionNotInspected = "Images have not been inspected. Select “Inspect Images” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Images",
                    RunFunctionButtonName = "Image Alt Text Editor",
                },
                new CheckerItem
                {
                    Checker = PageNumbers.CheckPageNumbers,
                    FixerUserControl = PageNumbers.ShowPageCounter,
                    DescriptionPassed = "All page numbers styled with CAT page number style are in order. Select “Page Counter” in Available Actions to review page numbers.\n\n<b>Note: Page numbers at the beginning or end of the text may be missing.</b>",
                    DescriptionFailed = "One or more page numbers may be missing or out of order. Select “Page Counter” in Available Actions to review page numbers.\n\n<b>Note: Some content skips page numbers or uses an unconventional numbering scheme that is not recognized by CAT.</b>",
                    DescriptionEmpty = "CAT could not find any numbers styled with the Page Number style. You can mark content as a page number in the “Edit” group on the ribbon.",
                    DescriptionNotInspected = "Page numbers have not been inspected. Select “Inspect Page Numbers” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Page Numbers",
                    RunFunctionButtonName = "Page Counter",
                },
                new CheckerItem
                {
                    Checker = FootnoteNumbers.CheckFootnoteNumbers,
                    FixerUserControl = FootnoteNumbers.ShowFootnoteCounter,
                    DescriptionPassed = "All footnote references are styled with CAT Footnote Ref style and are in order.\n\n<b>Note: Footnote References at the beginning or end of the text may be missing.</b>",
                    DescriptionFailed = "One or more footnote references may be missing or out of order.\n\n<b>Review footnote references:</b>\n Select “Footnote Counter” in Available Actions to review footnote references.\n\n<b>Find footnotes:</b>\n Select “Footnote Hunter” in Available Actions to help find missing footnotes.\n\n<b>Note: Some content repeats footnote numbering or uses an unconventional footnote scheme.</b>",
                    DescriptionEmpty = "CAT could not find any text styled with the Footnote Ref style. You can mark content as a footnote reference in the “Advanced” group on the ribbon.",
                    DescriptionNotInspected = "Footnote references have not been inspected. Select “Inspect Footnote Refs” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Footnotes Refs",
                    RunFunctionButtonName = "Footnote Counter",
                },

                new CheckerItem
                {
                    Checker = TranscribersNote.CheckGoodTranscriberNotes,
                    FixerUserControl = TranscribersNote.ShowTranscriberNoteEditor,
                    DescriptionPassed = "All tactile references styled with CAT are in order. Select “Tactile Ref Counter” in Available Actions to review.",
                    DescriptionFailed = "One or more tactile references may be missing or out of order. Select “Tactile Ref Counter” in Available Actions to review.",
                    DescriptionEmpty = "CAT could not find any tactile references. You can insert tactile references two ways.\n\n1.CAT > Braille Edit > TN, or \n\n2.CAT > Check [dropdown] > Image Alt Text Editor.",
                    DescriptionNotInspected = "Tactile references have not been inspected. Select “Inspect Tactile Refs” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Tactile Refs",
                    RunFunctionButtonName = "Tactile Ref Counter",
                },

                new CheckerItem
                {
                    Checker = General.CheckSpellCheckWasRan,
                    FixerForm = General.RunSpellCheck,
                    DescriptionPassed = "Microsoft Word Spell Check reports no misspelled words. Select “Open Spell Check” in Available Actions to review.\n\n<b>Note: No automatic spell checker is perfect.</b>",
                    DescriptionFailed = "Microsoft Word Spell Check reports that one more words may be misspelled. Select “Inspect Spelling” and fully complete the Spell Check for all suspected words.",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "Spelling has not been inspected. Select “Inspect Spelling” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Spelling",
                    RunFunctionButtonName = "Open Spell Check",
                },

                new CheckerItem
                {
                    Checker = FormattingFixer.CheckGoodFormatting,
                    FixerUserControl = FormattingFixer.ShowFormattingFixer,
                    DescriptionPassed = "CAT did not identify any excessive spaces or tab characters.",
                    DescriptionFailed = "CAT has discovered excessive spaces and/or tab characters. Select “Formatting Fixer” in Available Actions to review possible misuses of space or tab characters.\n\n<b>Note: Excessive space or tab character may be a sign that the document is using spaces and/or tabs to visually place text or object within the document (eg. to look a like a table or columns, or visual cue). Screen readers and document translated (i.e. Braille conversion software) cannot convey placement of text or objects using spaces or tab.</b>",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "Formatting has not been inspected. Select “Inspect Formatting” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Formatting",
                    RunFunctionButtonName = "Formatting Fixer",
                },

                new CheckerItem
                {
                    Checker = TextBoxFinder.CheckForNoTextBoxes,
                    FixerUserControl = TextBoxFinder.ShowTextboxFinder,
                    DescriptionPassed = "CAT did not identify any text boxes or WordArt.",
                    DescriptionFailed = "CAT has identified text boxes and/or WordArt. Select “TextBox Finder” in Available Actions to review.\n\n<b>Note: Text Boxes and WordArt are often inaccessible to Screen Readers, and typically are not supported by conversion software (ex. Duxbury Braille translator).</b>",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "CAT has not checked for text boxes and WordArt. Select “Inspect Text Boxes/WordArt” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Text Boxes/WordArt",
                    RunFunctionButtonName = "TextBox Finder",
                },

                new CheckerItem
                {
                    Checker = ChartConverter.CheckForAssessibleCharts,
                    FixerUserControl = ChartConverter.ShowChartConverter,
                    DescriptionPassed = "CAT did not identify any Charts or SmartArt.",
                    DescriptionFailed = "This document contains Charts and/or SmartArt. Select “Charts and SmartArt Fixer” in Available Actions to locate and resolve.\n\n<b>Note: Charts and SmartArt are inaccessible to Screen Readers, and typically are not supported by conversion software (i.e. Braille conversion).</b>",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "Charts and SmartArt have not been inspected. Select “Inspect Charts and SmartArt” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Charts and SmartArt",
                    RunFunctionButtonName = "Charts and SmartArt Fixer",
                },

                new CheckerItem
                {
                    Checker = CharacterAccuracyChecker.CheckGoodFormatting,
                    FixerUserControl = CharacterAccuracyChecker.ShowCharacterAccuracyChecker,
                    DescriptionPassed = "No suspect characters found.\n\n<b>Note: Character Accuracy Checker looks for misrecognized characters like “fa11” (should be “fall”). However, it cannot find every possible misrecognition. Carefully review the document for any additional recognition errors.</b>",
                    DescriptionFailed = "CAT has identified some characters that may have been misrecognized. Select “Inspect Character Accuracy” in Available Actions in Available Actions to locate suspect characters.\n\n<b>Note: Character Accuracy Checker looks for misrecognized characters like “a11” (should be “all”). However, it cannot find every possible misrecognition. Carefully review the document for any additional recognition errors.</b>",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "CAT hasn't checked for misrecognized characters. Select “Inspect Character Accuracy” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Character Accuracy",
                    RunFunctionButtonName = "Character Accuracy Tool",
                },

                new CheckerItem
                {
                    Checker = General.CheckFinalizeDocument,
                    FixerForm = General.TitleAuthorConfirmBox,
                    DescriptionPassed = "Select “Inspect Document Title and Author” in Available Actions to verify title and author for this document.",
                    DescriptionFailed = "Either the Document Title or Author is missing. Select “Input Title and Author” in Available Actions to input the Document Title and Author fields.",
                    DescriptionEmpty = "",
                    DescriptionNotInspected = "CAT hasn't checked the document title and author. Select “Inspect Document Title and Author” in Available Actions or “Inspect All”.",
                    CheckOnceButtonName = "Inspect Document Title and Author",
                    RunFunctionButtonName = "Input Title and Author",
                },
                new CheckerItem
                {
                    Checker = General.InbuiltWordAccessibilityChecker,
                    FixerForm = null,
                    DescriptionPassed = "Pass",
                    DescriptionFailed = "CAT cannot verify the Word Accessibility Checker results. “Needs Review” means you should open up the Word Accessibility Checker and verify that all relevant issues are resolved.\n\n<a>https://support.office.com/en-gb/article/use-the-accessibility-checker-to-find-accessibility-issues-a16f6de0-2f39-4a2b-8bd8-5ad801426c7f;Use the Accessibility Checker to find accessibility issues</a>",
                    DescriptionEmpty = "Empty",
                    DescriptionNotInspected = "CAT cannot verify the Word Accessibility Checker results. Open up the Word Accessibility Checker and verify that all relevant issues are resolved.\n\n<a>https://support.office.com/en-gb/article/use-the-accessibility-checker-to-find-accessibility-issues-a16f6de0-2f39-4a2b-8bd8-5ad801426c7f;Use the Accessibility Checker to find accessibility issues</a>",
                    CheckOnceButtonName = "Inspect Word Accessibility",
                    RunFunctionButtonName = "",
                    CurrentResult = CheckerResult.Failed,
                    IsActionsAvailable = false,
                }
            };
            General.RunChecker(checkItems);
        }

        /// <summary>
        /// Removes all tabs and converts them to spaces. If multiple tabs are
        /// consecutive, they all get converted to single space.
        /// </summary>
        [SuiteDescription("Replace all tabs with spaces")]
        public static void ReplaceTabsWithSpaces(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, @"[^t]{1,}", " ", useWildcards: true);
        }

        /// <summary>
        /// Removes redundant spacing, combining each instance into a single
        /// space.
        /// </summary>
        [SuiteDescription("Remove redundant spacing")]
        public static void RemoveRedundantSpaces(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, " {2,}", " ", useWildcards: true);
        }

        /// <summary>
        /// Removes all line breaks.
        /// </summary>
        [SuiteDescription("Remove all line breaks")]
        public static void RemoveAllLineBreaks(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, "^l", " ", useWildcards: true);
        }

        /// <summary>
        /// Removes emdashes that use the "not" character. They pop up
        /// everywhere when using OmniPage.
        /// </summary>
        [SuiteDescription("Remove emdashes")]
        public static void RemoveWeirdEmdashes(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            args.UpdateProgress(50);
            Globals.ThisAddIn.ReplaceAll(range, "^-", "");
            args.UpdateProgress(100);
            Globals.ThisAddIn.ReplaceAll(range, "^u8208 ", "");
        }

        /// <summary>
        /// Fixes bullet points by detecting the character at the beginning and
        /// then formatting that paragraph as a bulleted list.
        /// </summary>
        [SuiteDescription("Fix bullet points")]
        public static void FixBullets(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            args.UpdateProgress(0);
            foreach (Word.Paragraph firstItem in doc.Content.ListParagraphs.OfType<Word.Paragraph>().Reverse())
            {
                if (firstItem.Range.ListFormat.List != null)
                {
                    foreach (Word.Paragraph item in firstItem.Range.ListFormat.List.ListParagraphs)
                    {
                        if (firstItem.Range.ListFormat.ListType != Word.WdListType.wdListBullet
                            && firstItem.Range.ListFormat.ListType != Word.WdListType.wdListListNumOnly
                            && firstItem.Range.ListFormat.ListType != Word.WdListType.wdListMixedNumbering
                            && firstItem.Range.ListFormat.ListType != Word.WdListType.wdListSimpleNumbering
                            && firstItem.Range.ListFormat.ListType != Word.WdListType.wdListNoNumbering)
                        {
                            item.Range.ListFormat.ConvertNumbersToText();
                        }
                    }
                }
            }
            
            args.UpdateProgress(25);
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "\u61623"))
            {
                r.MoveEnd(Word.WdUnits.wdParagraph, 0);
                General.CreateBullets(r);

            }

            args.UpdateProgress(50);
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, ""))
            {
                r.MoveEnd(Word.WdUnits.wdParagraph, 0);
                General.CreateBullets(r);
            }
            args.UpdateProgress(75);
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "*"))
            {
                Word.Range tempSelection = r;
                if (tempSelection.Start > 0)
                {
                    tempSelection.SetRange(tempSelection.Start - 1, tempSelection.Start);
                    tempSelection.Select();
                    if (tempSelection.Text.ToString() == "\r" || tempSelection.Text.ToString() == "\a")
                    {
                        tempSelection.SetRange(r.Start + 1, r.End + 1);
                        r.MoveEnd(Word.WdUnits.wdParagraph, 0);
                        General.CreateBullets(r);
                    }
                }
                tempSelection.SetRange(0, 0);
                tempSelection.Select();
            }
        }

        [SuiteDescription("Normalize square bullets")]
        public static void FixSquareBullets(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            args.UpdateProgress(50);
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, ""))
            {
                General.FixSquareBullets(r);
            }

            args.UpdateProgress(75);
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, ""))
            {
                General.FixSquareBullets(r);
            }
            Globals.ThisAddIn.ReplaceAll(range, "", "");
            Globals.ThisAddIn.ReplaceAll(range, "", "");
        }

        /// <summary>
        /// Transforms paragraphs that are given an outline level (essentially,
        /// show up in the navigation pane), into an actual recognizable
        /// heading. This occurs often with ABBYY output.
        /// </summary>
        [SuiteDescription("Convert outline level to heading", optional1: true,optional2:true,optional3:true)]
        public static void ConvertOutlineLevelToHeading(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Dictionary for mapping outline levels to heading styles
            Dictionary<Word.WdOutlineLevel, Word.WdBuiltinStyle> outlineMap = new Dictionary<Word.WdOutlineLevel, Word.WdBuiltinStyle>
            {
                {Word.WdOutlineLevel.wdOutlineLevel1, Word.WdBuiltinStyle.wdStyleHeading1},
                {Word.WdOutlineLevel.wdOutlineLevel2, Word.WdBuiltinStyle.wdStyleHeading2},
                {Word.WdOutlineLevel.wdOutlineLevel3, Word.WdBuiltinStyle.wdStyleHeading3},
                {Word.WdOutlineLevel.wdOutlineLevel4, Word.WdBuiltinStyle.wdStyleHeading4},
                {Word.WdOutlineLevel.wdOutlineLevel5, Word.WdBuiltinStyle.wdStyleHeading5},
                {Word.WdOutlineLevel.wdOutlineLevel6, Word.WdBuiltinStyle.wdStyleHeading6},
                {Word.WdOutlineLevel.wdOutlineLevel7, Word.WdBuiltinStyle.wdStyleHeading7},
                {Word.WdOutlineLevel.wdOutlineLevel8, Word.WdBuiltinStyle.wdStyleHeading8},
                {Word.WdOutlineLevel.wdOutlineLevel9, Word.WdBuiltinStyle.wdStyleHeading9},
                {Word.WdOutlineLevel.wdOutlineLevelBodyText, Word.WdBuiltinStyle.wdStyleBodyText}
            };

            // Do it for all of the paragraphs
            foreach (Word.Paragraph p in range.Paragraphs)
            {
                p.set_Style(doc.Styles[outlineMap[p.OutlineLevel]]);

                // Report progress and check cancel
                args.UpdateRangePercentage(p.Range, range);
                if (args.IsCanceled())
                    break;
            }
        }


        [SuiteDescription("Remove single space between ellipses dots (period)", optional1: false, optional2: true, optional3: true, optional4: true)]
        public static void FixEllipsesPrepare(SuiteFunctionUpdateArgs args)
        {
            General.FixEllipses(args);
        }
    }
}
