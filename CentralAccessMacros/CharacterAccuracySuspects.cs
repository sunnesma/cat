﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    class CharacterAccuracySuspectsSortByRangeAscending : IComparer<CharacterAccuracySuspects>
    {
        public int Compare(CharacterAccuracySuspects a, CharacterAccuracySuspects b)
        {
            return a.intStartRange - b.intStartRange;
        }
    }

    public class CharacterAccuracySuspects
    {
        public Word.Range rngLocation { get; set; }
        public string strText { get; set; }
        public int intStartRange { get; set; }


        public CharacterAccuracySuspects(Word.Range rngLocation, string strText, int intStartRange)
        {
            this.rngLocation = rngLocation;
            this.strText = strText;
            this.intStartRange = intStartRange;
        }

        public static List<CharacterAccuracySuspects> GetListOfAllSuspects(bool boolAdvancedSetting)
        {
            Word.Range rangeToSearch = Globals.ThisAddIn.GetActiveDocument().Content;
            List<CharacterAccuracySuspects> lstSuspects = new List<CharacterAccuracySuspects>();

            System.Text.RegularExpressions.Regex rgxYear = new System.Text.RegularExpressions.Regex("^[0-9]{1,4}(s|st|nd|rd|th)$");
            List<string> exceptionToRgxYear = new List<string>();
            exceptionToRgxYear.Add("1s");
            exceptionToRgxYear.Add("0s");

            foreach (String strWildCard in GetListOfWildCards(boolAdvancedSetting))
            {
                List<Word.Range> lstSuspectRange = Globals.ThisAddIn.FindWildCard(rangeToSearch, strWildCard, forwardDirection: true, useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop)
                    //.Where(i => !skipWords.Contains(i.Text.Trim()))
                    .Where(i => i.Text.Length <= 50)
                    .Where(i => !MainRibbon.CheckStylePerCharacter(i.Start, i.End, "FootnoteReference"))
                    .Where(i => !rgxYear.IsMatch(i.Text) || (rgxYear.IsMatch(i.Text) && (exceptionToRgxYear.Contains(i.Text))))
                    .Where(i => !System.Text.RegularExpressions.Regex.IsMatch(i.Text.ToString(), @"^\d+$")) //Remove suspects which are just numbers.
                    .ToList();

                foreach (Word.Range r in lstSuspectRange)
                    if(!lstSuspects.Exists(i => i.rngLocation.Start == r.Start))
                        lstSuspects.Add(new CharacterAccuracySuspects(r, r.Text.Trim(), r.Start));
            }
            lstSuspects.Sort(new CharacterAccuracySuspectsSortByRangeAscending()); //Sort all the elements by their range.
            return lstSuspects;
        }

        /// <summary>
        /// Returns lists of wildcards to be used for finding the suspects.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListOfWildCards(bool boolAdvancedSetting)
        {
            List<string> wildCards = new List<string>();

            //Choosing wildcards to use, dependent on the advanced setting checkbox.
            if (boolAdvancedSetting)
            {
                wildCards.Add("<([A-z]@[0-9]@){1,}>"); //Number-character
                wildCards.Add("<([0-9]@[A-z]@){1,}>"); //Character-Number
                wildCards.Add("<([0-9]@[A-z0-9]@[0-9]@){1,}>"); //Number-anything-number
                wildCards.Add("<([0-9]@[A-z0-9]@[A-z]@){1,}>"); //Number-anything-Character
                wildCards.Add("<([0-9]@[iloszILOSZ]@){1,}>"); //Number-character
                wildCards.Add("<([iloszILOSZ]@[0-9]@){1,}>"); //Character-number
                wildCards.Add("<([0-9]@[iloszILOSZ0-9]@[0-9]@){1,}>"); //Number-anything-number
                wildCards.Add("<([0-9]@[iloszILOSZ0-9]@[iloszILOSZ]@){1,}>"); //Number-anything-Character

                // Following wildcards result in all words in a document to become a suspect. Because of the [A-z0-9]@. Need more work.

                //wildCards.Add("<([A-z]@[A-z0-9]@[A-z]@){1,}>"); //Character-anything-Character
                //wildCards.Add("<([A-z]@[A-z0-9]@[0-9]@){1,}>"); //Character-anything-Number
            }
            else
            {
                wildCards.Add("<([0-9]@[iloszILOSZ]@){1,}>"); //Number-character
                wildCards.Add("<([iloszILOSZ]@[0-9]@){1,}>"); //Character-number
                wildCards.Add("<([0-9]@[iloszILOSZ0-9]@[0-9]@){1,}>"); //Number-anything-number
                wildCards.Add("<([0-9]@[iloszILOSZ0-9]@[iloszILOSZ]@){1,}>"); //Number-anything-Character

                // Following wildcards result in all words in a document to become a suspect. Because of the [A-z0-9]@. Need more work.

                //wildCards.Add("<([iloszILOSZ]@[A-z0-9]@[iloszILOSZ]@){1,}>"); //Character-anything-Character
                //wildCards.Add("<([iloszILOSZ]@[A-z0-9]@[0-9]@){1,}>"); //Character-anything-Number
            }
            return wildCards;
        }
    }
}
