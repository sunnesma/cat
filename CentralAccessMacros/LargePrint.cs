﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Controls;
using System.Diagnostics;

namespace CentralAccessMacros
{
    class LargePrint
    {
        /// <summary>
        /// Runs the interface for formatting this document as large print.
        /// </summary>
        public static void ShowForm()
        {
            LargePrintForm f = new LargePrintForm();
            Globals.ThisAddIn.ShowForm(f);
        }

        public static void FormatAll(int basePointSize, bool doubleImageSizes, bool tripleImageSizes, PageFormat pageFormat, bool underlineHeadings, bool SetLineSpacing, bool SetParagraphSpacing, bool FormatTables, bool AddPageBreakBeforePageNumbers, bool PutChaptersOnOddPage, bool RemoveEquationHighlights, bool ConvertListtoText, UserControl orig)
        {
            General.SaveAsNewCopy(new SuiteFunctionUpdateArgs { OrigControl = orig });
            if (ConvertListtoText)
                LargePrint.ConvertListtoText();
            LargePrint.SetNormalSize(basePointSize);
            LargePrint.SetPageSetup(pageFormat);
            LargePrint.SetFont();
            General.ConvertDAISYNumbersToPageNumbers(new SuiteFunctionUpdateArgs());
            if (AddPageBreakBeforePageNumbers)
                Braille.AddPageBreakBeforePageNumbers(new SuiteFunctionUpdateArgs());
            if (SetLineSpacing)
                LargePrint.SetLineSpacing(basePointSize);
            if (SetParagraphSpacing)
                LargePrint.SetParagraphSpacing(basePointSize);
            LargePrint.TurnOffOrphanControl();
            if (FormatTables)
                LargePrint.FormatTables();
            LargePrint.FormatHeadings(basePointSize);
            LargePrint.FormatPageNumbers(basePointSize);
            General.RemoveEmptyParagraphs(new SuiteFunctionUpdateArgs());
            if (doubleImageSizes)
                LargePrint.DoubleImageSizes();
            if (tripleImageSizes)
                LargePrint.TripleImageSizes();
            if (underlineHeadings)
                LargePrint.UnderlineHeadings();
            if (PutChaptersOnOddPage)
                LargePrint.PutChaptersOnOddPage();
            LargePrint.DeleteEmptyPageNumbers();
            if (RemoveEquationHighlights)
                Math.RemoveEquationHighlights(new SuiteFunctionUpdateArgs());

        }

        private static void UnderlineHeadings()
        {
            Word.WdBuiltinStyle[] headings = new Word.WdBuiltinStyle[]
            {
                Word.WdBuiltinStyle.wdStyleHeading1,
                Word.WdBuiltinStyle.wdStyleHeading2,
                Word.WdBuiltinStyle.wdStyleHeading3,
                Word.WdBuiltinStyle.wdStyleHeading4,
                Word.WdBuiltinStyle.wdStyleHeading5,
                Word.WdBuiltinStyle.wdStyleHeading6,
            };

            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            foreach (Word.WdBuiltinStyle heading in headings)
            {
                Word.Style style = doc.Styles[heading];
                style.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
            }
        }

        private static void TurnOffOrphanControl()
        {
            Word.Style style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal];
            style.ParagraphFormat.WidowControl = (int)VBABoolean.True;
        }

        private static void DoubleImageSizes()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;

            // Figure out the maximum size that any image can be
            float maxWidth = doc.PageSetup.PageWidth - doc.PageSetup.LeftMargin - doc.PageSetup.RightMargin;
            float maxHeight = doc.PageSetup.PageHeight - doc.PageSetup.TopMargin - doc.PageSetup.BottomMargin;
            
            foreach (Word.InlineShape s in range.InlineShapes)
            {
                float currWidth = s.Width;
                float currHeight = s.Height;

                currWidth *= 2.0f;
                currHeight *= 2.0f;

                if (currWidth > maxWidth)
                {
                    float scaleBack = maxWidth / currWidth;
                    currWidth *= scaleBack;
                    currHeight *= scaleBack;
                }

                if (currHeight > maxHeight)
                {
                    float scaleBack = maxHeight / currHeight;
                    currWidth *= scaleBack;
                    currHeight *= scaleBack;
                }

                s.Width = currWidth;
                s.Height = currHeight;
            }
        }

        private static void TripleImageSizes()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;

            // Figure out the maximum size that any image can be
            float maxWidth = doc.PageSetup.PageWidth - doc.PageSetup.LeftMargin - doc.PageSetup.RightMargin;
            float maxHeight = doc.PageSetup.PageHeight - doc.PageSetup.TopMargin - doc.PageSetup.BottomMargin;

            foreach (Word.InlineShape s in range.InlineShapes)
            {
                float currWidth = s.Width;
                float currHeight = s.Height;

                currWidth *= 3.0f;
                currHeight *= 3.0f;

                if (currWidth > maxWidth)
                {
                    float scaleBack = maxWidth / currWidth;
                    currWidth *= scaleBack;
                    currHeight *= scaleBack;
                }

                if (currHeight > maxHeight)
                {
                    float scaleBack = maxHeight / currHeight;
                    currWidth *= scaleBack;
                    currHeight *= scaleBack;
                }

                s.Width = currWidth;
                s.Height = currHeight;
            }
        }

        private static void SetPageSetup(PageFormat pageFormat)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            // Page dimensions
            doc.PageSetup.PageWidth = Globals.ThisAddIn.Application.InchesToPoints(pageFormat.Width);
            doc.PageSetup.PageHeight = Globals.ThisAddIn.Application.InchesToPoints(pageFormat.Height);

            // Margins
            doc.PageSetup.LeftMargin = Globals.ThisAddIn.Application.InchesToPoints(0.75f);
            doc.PageSetup.RightMargin = Globals.ThisAddIn.Application.InchesToPoints(0.5f);
            doc.PageSetup.TopMargin = Globals.ThisAddIn.Application.InchesToPoints(0.5f);
            doc.PageSetup.BottomMargin = Globals.ThisAddIn.Application.InchesToPoints(0.5f);
        }

        private static void FormatPageNumbers(int basePointSize)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            object wdStylePageNumber = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: wdStylePageNumber).ToList())
            {
                r.Select();
                Globals.ThisAddIn.Application.Selection.ClearCharacterStyle();
                r.set_Style(Globals.ThisAddIn.GetActiveDocument().Styles[Braille.LargePrintPageNumber]);
                r.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                r.Font.Size = basePointSize;
            }
        }

        private static void SetNormalSize(int basePointSize)
        {
            // Normal font
            Word.Style style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal];
            style.Font.Size = basePointSize;

            // Default paragraph font
            style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleDefaultParagraphFont];
            style.Font.Size = basePointSize;

            // Lists
            Word.WdListGalleryType[] galleries = new Word.WdListGalleryType[] {
                Word.WdListGalleryType.wdBulletGallery,
                Word.WdListGalleryType.wdNumberGallery
            };
            foreach (Word.WdListGalleryType t in galleries)
            {
                Word.ListGallery gallery = Globals.ThisAddIn.Application.ListGalleries[t];
                gallery.ListTemplates[1].ListLevels[1].Font.Size = basePointSize;
            }

            // Transcriber's note styles
            style = TranscribersNote.TactileDescriptionStyle;
            style.Font.Size = basePointSize;
            style = TranscribersNote.TactilePageNumberStyle;
            style.Font.Size = basePointSize;
        }

        private static void SetFont()
        {
            Word.Style style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal];
            style.Font.Name = "Arial";

            style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleDefaultParagraphFont];
            style.Font.Name = "Arial";
        }

        private static void SetLineSpacing(int basePointSize)
        {
            Word.Style style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal];
            style.ParagraphFormat.LineSpacing = (float)basePointSize;
        }

        private static void SetParagraphSpacing(int basePointSize)
        {
            Word.Style style = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal];
            style.ParagraphFormat.SpaceBefore = (float)basePointSize;
            style.ParagraphFormat.SpaceAfter = (float)basePointSize;
            style.ParagraphFormat.WidowControl = (int)VBABoolean.False;
        }

        private static void FormatTables()
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Style myStyle = Globals.ThisAddIn.GetActiveDocument().Styles["Table Grid"];

            // Add striping on alternate rows
            myStyle.Table.RowStripe = 1;
            Word.ConditionalStyle bandStyle = myStyle.Table.Condition(Word.WdConditionCode.wdOddRowBanding);
            bandStyle.Shading.BackgroundPatternColor = (Word.WdColor) ColorTranslator.ToOle(LargePrint.RegulationYellow);

            Word.WdBorderType[] borders = new Word.WdBorderType[] {
                Word.WdBorderType.wdBorderBottom,
                Word.WdBorderType.wdBorderLeft,
                Word.WdBorderType.wdBorderRight,
                Word.WdBorderType.wdBorderTop,
                Word.WdBorderType.wdBorderHorizontal,
                Word.WdBorderType.wdBorderVertical
            };

            foreach (Word.WdBorderType b in borders)
            {
                Word.Border myB = bandStyle.Borders[b];
                myB.LineStyle = Word.WdLineStyle.wdLineStyleSingle;
                myB.LineWidth = Word.WdLineWidth.wdLineWidth050pt;
                myB.ColorIndex = Word.WdColorIndex.wdBlack;
            }

            // Set the style on all of the tables
            Word.PageSetup pageSetup = Globals.ThisAddIn.GetActiveDocument().PageSetup;

            foreach (Word.Table t in range.Tables)
            {
                t.set_Style(myStyle);
                t.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitContent);
                t.AllowAutoFit = true;
                t.PreferredWidth = pageSetup.PageWidth - pageSetup.LeftMargin - pageSetup.RightMargin;
                t.Rows.AllowBreakAcrossPages = 0;
            }
        }

        private static void FormatHeadings(int basePointSize)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            Dictionary<Word.WdBuiltinStyle, Color> headingColors = new Dictionary<Word.WdBuiltinStyle, Color>
            {
                {Word.WdBuiltinStyle.wdStyleHeading1, LargePrint.FederalBlue},
                {Word.WdBuiltinStyle.wdStyleHeading2, LargePrint.Crimson},
                {Word.WdBuiltinStyle.wdStyleHeading3, LargePrint.RegulationGreen},
                {Word.WdBuiltinStyle.wdStyleHeading4, LargePrint.RegulationPurple},
                {Word.WdBuiltinStyle.wdStyleHeading5, LargePrint.RegulationBrown},
                {Word.WdBuiltinStyle.wdStyleHeading6, LargePrint.RegulationRed}
            };

            float headingSize = (float)basePointSize * 1.5f;

            foreach (Word.WdBuiltinStyle style in headingColors.Keys)
            {
                Word.Style myStyle = doc.Styles[style];
                myStyle.Font.Color = (Word.WdColor)ColorTranslator.ToOle(headingColors[style]);
                myStyle.Font.Size = (int)headingSize;
                headingSize *= 0.9f;
            }
        }

        public static void DeleteEmptyPageNumbers()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range docRange = doc.Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(docRange, "", style: doc.Styles[Word.WdBuiltinStyle.wdStylePageNumber]))
            {
                if (r.Text == "\r")
                {
                    r.Delete();
                }
            }
        }

        /// <summary>
        /// Adds a specific kind of page break before Heading 1's that cause it
        /// to break to the next odd page.
        /// </summary>
        public static void PutChaptersOnOddPage()
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Style heading1 = doc.Styles[Word.WdBuiltinStyle.wdStyleHeading1];

            // I prefer to do this all in one loop, but for some reason I can't
            // do the operation within the Find loop without breaking
            // something.
            List<Word.Range> headings = new List<Word.Range>();
            foreach (Word.Range r in Globals.ThisAddIn.Find(doc.Content, "", style: heading1))
            {
                Debug.Print("Found a heading! {0}", r.Text);
                headings.Add(r);
            }
            foreach (Word.Range r in headings)
            {
                r.InsertBreak(Word.WdBreakType.wdSectionBreakOddPage);
            }
        }

        public static void ConvertListtoText()
        {
            Globals.ThisAddIn.GetActiveDocument().Content.ListFormat.ConvertNumbersToText();
        }

        #region Colors
        private static Color RegulationYellow
        {
            get
            {
                return Color.FromArgb(255, 255, 153);
            }
        }

        private static Color RegulationBrown
        {
            get
            {
                return Color.FromArgb(71, 15, 0);
            }
        }

        private static Color RegulationGreen
        {
            get
            {
                return Color.FromArgb(0, 240, 22);
            }
        }

        private static Color RegulationPurple
        {
            get
            {
                return Color.FromArgb(30, 0, 232);
            }
        }

        private static Color RegulationRed
        {
            get
            {
                return Color.FromArgb(224, 20, 79);
            }
        }

        private static Color FederalGold
        {
            get
            {
                return Color.FromArgb(240, 211, 14);
            }
        }

        private static Color FederalBlue
        {
            get
            {
                return Color.FromArgb(0, 96, 240);
            }
        }

        private static Color Crimson
        {
            get
            {
                return Color.FromArgb(167, 4, 50);
            }
        }
        #endregion
    }

    public class PageFormat
    {
        public PageFormat(string name, float widthInches, float heightInches, bool isCustom = false)
        {
            Name = name;
            Width = widthInches;
            Height = heightInches;
            IsCustom = isCustom;
        }

        public float Width { get; set; }
        public float Height { get; set; }
        public string Name { get; set; }
        public bool IsCustom { get; set; }

        public override string ToString()
        {
            if (!IsCustom)
                return Name + ": " + Width.ToString() + "\" x " + Height.ToString() + "\"";
            else
                return "[Custom]";
        }
    }
}
