﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;

namespace CentralAccessMacros
{
    partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();
            this.Text = String.Format("About {0}", AssemblyTitle);
            this.lblAboutCATHeading.Text = String.Format("Version {0}", AssemblyVersion);
            
            this.txtContributors.Text = "Contributors: \nMarshall Sunnes ->";
            LinkLabel lnkMarshall = new LinkLabel();
            lnkMarshall.Text = "LinkedIn";
            lnkMarshall.AutoSize = true;
            lnkMarshall.Location = this.txtContributors.GetPositionFromCharIndex(this.txtContributors.TextLength);
            lnkMarshall.LinkClicked +=lnkMarshall_LinkClicked;
            this.txtContributors.Controls.Add(lnkMarshall);
            
            this.txtContributors.Text = this.txtContributors.Text + "\nAnuj Bhatia -> ";
            LinkLabel lnkAnuj = new LinkLabel();
            lnkAnuj.Text = "LinkedIn";
            lnkAnuj.AutoSize = true;
            lnkAnuj.Location = this.txtContributors.GetPositionFromCharIndex(this.txtContributors.TextLength);
            lnkAnuj.LinkClicked += lnkAnuj_LinkClicked;
            this.txtContributors.Controls.Add(lnkAnuj);

            this.txtContributors.Text = this.txtContributors.Text + "\nAnuj Bhatia ->\n\n\n\nasd asd asd ";
            //Central Access Toolbar License
            //Updated December 22, 2017

            //This license governs use of the accompanying software. If you use the software, you
            //accept this license. If you do not accept the license, do not use the software.

            //1. Definitions
            //The terms "reproduce," "reproduction," "derivative works," and "distribution" have the
            //same meaning here as under U.S. copyright law.
            //A "contribution" is the original software, or any additions or changes to the software.
            //A "contributor" is any person that distributes its contribution under this license.
            //"Licensed patents" are a contributor's patent claims that read directly on its contribution.

            //2. Grant of Rights
            //(A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free copyright 

            //license to reproduce its contribution, prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
            //(B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, each contributor grants you a non-exclusive, worldwide, royalty-free license under 

            //its licensed patents to make, have made, use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the contribution in the software.

            //3. Conditions and Limitations
            //(A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
            //(B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, your patent license from such contributor to the software ends automatically.
            //(C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, and attribution notices that are present in the software.
            //(D) If you distribute any portion of the software in source code form, you may do so only under this license by including a complete copy of this license with your distribution. If you distribute any 

            //portion of the software in compiled or object code form, you may only do so under a license that complies with this license.
            //(E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. You may have additional consumer rights under your local laws 

            //which this license cannot change. To the extent permitted under your local laws, the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-

            //infringement.

            //This license was based on the Open Source Initiative's Microsoft Public License (MS-PL) language: https://opensource.org/licenses/MS-PL
        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion


        private void btnLicence_Click(object sender, EventArgs e)
        {
            Process.Start(Properties.Resources.LicenceURL);
        }
        private void lnkAlternateFormatTools_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.altformattools.com");
        }
        private void lnkAnuj_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www.linkedin.com/in/anujb93");
        }
        private void lnkMarshall_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(e.Link.ToString());
        }
    }
}