﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Drawing;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    /// <summary>
    /// Provides functions for obtaining and checking Footnote numbers.
    /// </summary>
    class FootnoteNumbers
    {
        /// <summary>
        /// Gets the style for the Poetry 4 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>

      
        [CheckerDescription("Footnote references")]
        public static CheckerResult CheckFootnoteNumbers()
        {
            List<FootnoteNumberReport> reports = FootnoteNumbers.GetFootnoteNumberList();

            if (reports.Count == 0)
                return CheckerResult.Empty;

            foreach (FootnoteNumberReport r in reports)
            {
                if (!r.IsGood)
                    return CheckerResult.Failed;
            }
            return CheckerResult.Passed;
        }

        public static List<FootnoteNumberReport> GetFootnoteNumberList()
        {
            /*
             * Rules for correct Footnote numbers:
             * 1. The Footnote numbers are roman numerals or normal digits.
             * 2. Lowercase Roman characters go before arabic numerals.
             * 3. If same style, later must be sequentially higher than
             *    previous.
             */

            // Precedence determines what kinds of numbers go before what
            int lastPrecedence = -1;
            int currPrecedence = 0;

            // Value is the numeric value of whatever the number was. This
            // makes sure that I can treat all kinds of numbers similarly.
            int lastValue = -1;
            int currValue = 0;

            List<FootnoteNumberReport> reports = new List<FootnoteNumberReport>();

            // Run the search
            Word.Range content = Globals.ThisAddIn.GetActiveDocument().Content;
            object FootnoteNumberStyle = MainRibbon.FootnoteReference;
            object characterunit = Word.WdUnits.wdCharacter;
            int lastEnd = -1;
            foreach (Word.Range r in Globals.ThisAddIn.Find(content, "", style: FootnoteNumberStyle))
            {
                char[] rangeCharArray = r.Text.ToCharArray();
                char charLast = rangeCharArray[rangeCharArray.Length - 1];
                if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                {
                    r.MoveEnd(ref characterunit, 0);
                }
                if (r.Start != lastEnd)
                {
                    string FootnoteNumber = r.Text.Trim();

                    // Parse the Footnote number
                    if (IsRumanNumerals(FootnoteNumber.ToUpper()))
                    {
                        currPrecedence = 0;
                        currValue = ConvertRomanNumeralToInt(FootnoteNumber.ToUpper());
                    }
                    else if (int.TryParse(FootnoteNumber, out currValue))
                    {
                        currPrecedence = 1;
                    }
                    else
                    {
                        // Flag with -2 that it didn't parse correctly
                        currPrecedence = -2;
                        currValue = -2;
                    }

                    // Check sequential order
                    bool isBad = currPrecedence == -2;
                    isBad = isBad || (currPrecedence < lastPrecedence);
                    if ((lastValue > -1) && (lastPrecedence > -1))
                    {
                        isBad = isBad || ((currPrecedence == lastPrecedence) && (currValue != (lastValue + 1)));
                    }

                    reports.Add(new FootnoteNumberReport(r, !isBad));

                    lastPrecedence = currPrecedence;
                    lastValue = currValue;
                }

                lastEnd = r.End;
            }

            return reports;
        }

        public static UserControl ShowFootnoteCounter()
        {
            FootnoteCounterForm f = new FootnoteCounterForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Footnote Counter", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
            //return Globals.ThisAddIn.ShowForm(form);
        }

        #region Roman Numerals

        public class RomanNumber
        {
            public string Numeral { get; set; }
            public int Value { get; set; }
            public int Hierarchy { get; set; }
        }

        private static List<RomanNumber> RomanNumbers = new List<RomanNumber>
        {
            new RomanNumber {Numeral = "M", Value = 1000, Hierarchy = 4},
            //{"CM", 900},
            new RomanNumber {Numeral = "D", Value = 500, Hierarchy = 4},
            //{"CD", 400},
            new RomanNumber {Numeral = "C", Value = 100, Hierarchy = 3},
            //{"XC", 90},
            new RomanNumber {Numeral = "L", Value = 50, Hierarchy = 3},
            //{"XL", 40},
            new RomanNumber {Numeral = "X", Value = 10, Hierarchy = 2},
            //{"IX", 9},
            new RomanNumber {Numeral = "V", Value = 5, Hierarchy = 2},
            //{"IV", 4},
            new RomanNumber {Numeral = "I", Value = 1, Hierarchy = 1}
        };

        private static bool IsRumanNumerals(string romanNumeralString)
        {
            foreach (char c in romanNumeralString)
            {
                bool found = false;

                foreach (RomanNumber r in RomanNumbers)
                {
                    if (c == r.Numeral[0])
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    return false;
            }

            return true;
        }

        private static int ConvertRomanNumeralToInt(string romanNumeralString)
        {
            if (romanNumeralString == null) return int.MinValue;

            var total = 0;
            for (var i = 0; i < romanNumeralString.Length; i++)
            {
                // get current value
                var current = romanNumeralString[i].ToString();
                var curRomanNum = RomanNumbers.First(rn => rn.Numeral.ToUpper() == current.ToUpper());

                // last number just add the value and exit
                if (i + 1 == romanNumeralString.Length)
                {
                    total += curRomanNum.Value;
                    break;
                }

                // check for exceptions IV, IX, XL, XC etc
                var next = romanNumeralString[i + 1].ToString();
                var nextRomanNum = RomanNumbers.First(rn => rn.Numeral.ToUpper() == next.ToUpper());

                // exception found
                if (curRomanNum.Hierarchy == (nextRomanNum.Hierarchy - 1))
                {
                    total += nextRomanNum.Value - curRomanNum.Value;
                    i++;
                }
                else
                {
                    total += curRomanNum.Value;
                }
            }

            return total;
        }

        #endregion
    }

    /// <summary>
    /// Class for reporting whether a Footnote number is good. It will also
    /// give the range of where the Footnote number can be found.
    /// </summary>
    public class FootnoteNumberReport
    {
        private bool isGood;
        public bool IsGood
        {
            get
            {
                return isGood;
            }
        }
        private Word.Range range;
        public Word.Range Range
        {
            get
            {
                return range;
            }
        }
        private string footnoteNumber;
        public string FootnoteNumber
        {
            get
            {
                return footnoteNumber;
            }
        }

        public FootnoteNumberReport(Word.Range range, bool isGood)
        {
            this.isGood = isGood;
            this.range = range;
            if (range.Text != null)
                this.footnoteNumber = range.Text.Trim();
            else
                this.footnoteNumber = "";
        }
      
    }
}
