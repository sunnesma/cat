﻿using Microsoft.Office.Tools.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Drawing;
namespace CentralAccessMacros
{
    /// <summary>
    /// This class has functions for processing Braille projects.
    /// </summary>
    class Braille
    {
        /// <summary>
        /// Runs the initial set of functions for preparing the document before
        /// human intervention.
        /// </summary>
        public static void RunSuite()
        {
            List<SuiteFunction> functions = new List<SuiteFunction>
            {
                Braille.RunSaveAsNewCopyBraille,
                Braille.ReplaceSymbolsWithProse,
                Braille.ConvertAllNumbersToOMMLBraille,
                Braille.ConvertOMMLToMathTypeBraille,
                Braille.HighlightEquationsBraille,
                Braille.ReplaceParagraphsWithManualBreaksInComputerCode,
                Braille.ExtractAltTextFromImagesforDuxbury,
                Braille.RemoveImagesBraille,
                Braille.AddAmpersandsToPageNumbers,
                Braille.Insert2PageBreaksAfterTranscribersNote,
                Braille.EquationCommentarytoExactBrailleTranslation,
                Braille.ConvertPageNumberstoDuxburyPageStyle,
                Braille.ConvertPageNumberstoDuxburyPageNemethStyle,
                Braille.RemovePageNumberStyleKeepText,
                Braille.ConvertTNtoDuxburyTN,
                Braille.ConvertTNtoDuxburyTNNemeth,
                Braille.ConvertBoxIndicatorstoDuxbury,
                Braille.AddNoteReferenceIndicators,
                WordDocPCAudio.FixBullets,
                Braille.AddListedStyleAllTables,
                Braille.ConvertListsToTextAndApplyIndents,
            };

            General.RunSuite(functions, "Duxbury Ready");
        }

        /// <summary>
        /// Calls the General Save as New Copy function.
        /// Customized for Duxbury Ready drop-down list.
        /// </summary>
        [SuiteDescription("Save copy of document", optional1: false, optional2: false)]
        public static void RunSaveAsNewCopyBraille(SuiteFunctionUpdateArgs args)
        {

            General.SaveAsNewCopy(args);
        }
        
        /// <summary>
        /// Replaces some symbols in the document with prose, e.g., the
        /// copyright symbol would be replaced with "copyright"
        /// </summary>
        [SuiteDescription("Replace some symbols with prose", optional1: true, optional2: true)]
        public static void ReplaceSymbolsWithProse(SuiteFunctionUpdateArgs args)
        {
            Dictionary<string, string> symbolsToProseDict = new Dictionary<string, string>
            {
                // Unicodes are represented as decimal
                {"^u169", " copyright "},
                {"^u8482", " trademark "},
                {"^u174", " registered trademark"},
                {"^u176", " degrees "},
                {"^u189", " one half "},
                {"^u188", " one fourth "},
                {"^u190", " three fourths "}
            };

            foreach (string k in symbolsToProseDict.Keys)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Globals.ThisAddIn.ReplaceAll(range, k, symbolsToProseDict[k]);
            }
        }

        /// <summary>
        /// Calls the Math Convert numbers to OMML function.
        /// Customized for Duxbury Ready drop-down list.
        /// </summary>
        [SuiteDescription("Convert numbers to OMML", optional1: true, optional2: true)]
        public static void ConvertAllNumbersToOMMLBraille(SuiteFunctionUpdateArgs args)
        {

            Math.ConvertAllNumbersToOMML(args);
        }

        /// <summary>
        /// Calls the Math Convert all OMML to MathType.
        /// Customized for Duxbury Ready drop-down list.
        /// </summary>
        [SuiteDescription("Convert all OMML to MathType", optional1: true, optional2: true)]
        public static void ConvertOMMLToMathTypeBraille(SuiteFunctionUpdateArgs args)
        {

            Math.ConvertOMMLToMathType(args);
        }

        /// <summary>
        /// Calls the Math Highlight All Equations.
        /// Customized for Duxbury Ready drop-down list.
        /// </summary>
        [SuiteDescription("Highlight all equations", optional1: true, optional2: true)]
        public static void HighlightEquationsBraille(SuiteFunctionUpdateArgs args)
        {

            Math.HighlightEquations(args);
        }


        [SuiteDescription("Add page breaks before page numbers")]
        public static void AddPageBreakBeforePageNumbers(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;

            bool gotFirst = false;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: doc.Styles[Word.WdBuiltinStyle.wdStylePageNumber]))
            {
                int end = r.End;

                r.Collapse(Word.WdCollapseDirection.wdCollapseStart);

                if (gotFirst)
                {
                    r.InsertBreak(Word.WdBreakType.wdPageBreak);
                }
                else
                {
                    gotFirst = true;
                }
                    
                r.SetRange(end, end);
            }

            // Clear the formatting on the page breaks. Also push them back
            // into the previous paragraph so that they don't create empty
            // pages
            range = doc.Content;
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "^m"))
            {
                r.Select();
                Globals.ThisAddIn.Application.Selection.ClearFormatting();
                r.Move(Word.WdUnits.wdCharacter, -2);
                r.Delete(Word.WdUnits.wdCharacter, 1);
                r.Move(Word.WdUnits.wdCharacter, 1);
            }            
        }

        [SuiteDescription("Extract alt text from images")]
        public static void ExtractAltTextFromImagesforDuxbury(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            int i = 0;
            int count = range.InlineShapes.Count;
            foreach (Word.InlineShape s in range.InlineShapes)
            {
                if (args.IsCanceled())
                    break;

                if (s.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                {
                    string altText = "";
                    if (s.AlternativeText != null)
                        altText = s.AlternativeText;

                    if (altText.Length > 0)
                    {
                        Word.Range r = s.Range;
                        r.Move(Word.WdUnits.wdParagraph, -1);
                        r.InsertParagraphBefore();
                        TranscribersNote.InsertTranscribersNote(r, altText: altText, haveCloseTag: true);
                    }
                }

                i += 1;
                args.UpdatePercentage(i, count);
            }
        }
        
        /// <summary>
        /// Calls the DAISY Remove Images function for Braille.
        /// Customized for Duxbury Ready drop-down list.
        /// </summary>
        [SuiteDescription("Remove images", optional1: true, optional2: true)]
        public static void RemoveImagesBraille(SuiteFunctionUpdateArgs args)
        {

            DAISY.RemoveImages(args);
        }
        [SuiteDescription("Insert &&& before page numbers", optional1: true, optional2:true)]
        public static void AddAmpersandsToPageNumbersAsk(SuiteFunctionUpdateArgs args)
        {
            DialogResult result = MessageBox.Show(Properties.Resources.AddAmpersandsQuestion_Body,
                Properties.Resources.AddAmpersandsQuestion_Title,
                MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                Braille.AddAmpersandsToPageNumbers(args);
            }
        }

        [SuiteDescription("Insert &&& before page numbers", optional1: true, optional2: true)]
        public static void AddAmpersandsToPageNumbers(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: Word.WdBuiltinStyle.wdStylePageNumber))
            {
                r.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                r.Text = "&&&";
            }
        }

        [SuiteDescription("Remove redundant punctuation")]
        public static void RemoveRedundantPunctuation(SuiteFunctionUpdateArgs args)
        {
            Dictionary<string, string> redundantPunctuation = new Dictionary<string, string>
            {
                //{"...", "."},
                //{"..", "."},
                {".,", ","},
                {".-", "-"},
                {"^-", "-"},
                {"^+", "-"},
                {"^=", "-"},
                {".:", ":"},
                {".?", "?"},
                {".!", "!"},
                {"._", "_"},
                {". .", ".."},
                {":.", "."},
                {".\"\".", ".\"\""},
                {".].", ".]"},
                {".}.", ".}"},
            };

            foreach (string k in redundantPunctuation.Keys)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                Globals.ThisAddIn.ReplaceAll(range, k, redundantPunctuation[k]);
            }
        }

        [SuiteDescription("Replace images with its alt text")]
        public static void ReplaceImagesWithAltText(SuiteFunctionUpdateArgs args)
        {

            DialogResult result = MessageBox.Show("This will replace all images with its alt text. Make a copy before running this function.\n\nDo you still want to run it?", "Run Alt Text Extractor", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;

                foreach (Word.InlineShape shape in range.InlineShapes)
                {
                    if (shape.Type == Word.WdInlineShapeType.wdInlineShapePicture)
                    {
                        string myAltText = ",’ Transcriber's note: ";
                        myAltText += shape.AlternativeText;
                        myAltText += " End transcriber's note. ,’";

                        Word.Range r = shape.Range;
                        r.Text = myAltText;
                    }
                }

                MessageBox.Show("Done!");
            }
        }

        [SuiteDescription("Replace paragraphs with line breaks in computer code", optional1: true, optional2: true)]
        public static void ReplaceParagraphsWithManualBreaksInComputerCode(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Globals.ThisAddIn.ReplaceAll(range, "^p", "^l", findStyle: ComputerCode.ParagraphStyle);
        }

        [SuiteDescription("Insert 2 page breaks after transcriber's note", optional1: true, optional2: true)]
        public static void Insert2PageBreaksAfterTranscribersNote(SuiteFunctionUpdateArgs args)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;           

            List<Word.Range> notes = new List<Word.Range>();

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: TranscribersNote.ParagraphStyle))
            {
                notes.Add(r);
            }

            // Loop through the paragraphs. If two paragraphs are not next to
            // each other, then put in the page breaks. Otherwise, ignore it,
            // since the find function above only gets single paragraphs, and
            // we use transcriber's notes with images often.
            Word.Range prevRange = notes.First();
            foreach (Word.Range r in notes)
            {
                // Check that my range is not next to my previous range
                if (r.Start > (prevRange.End + 1))
                {
                    // Put in the page breaks after the previous range
                    prevRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    prevRange.InsertBreak(Word.WdBreakType.wdPageBreak);
                    prevRange.InsertBreak(Word.WdBreakType.wdPageBreak);
                }

                prevRange = r;
            }

            // Put page breaks on the last one
            prevRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            prevRange.InsertBreak(Word.WdBreakType.wdPageBreak);
            prevRange.InsertBreak(Word.WdBreakType.wdPageBreak);
        }


        [SuiteDescription("Add Braille indicators around equation commentary", optional1: false, optional2: false)]
        public static void EquationCommentarytoExactBrailleTranslation(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc =Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
           // foreach (Word.Style style in doc.Styles)
            object equationCommentaryStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.EquationCommentaryStyle];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: equationCommentaryStyle).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    try
                    {
                        myStyle = doc.Styles["ExactTranslation"];
                    }
                    catch (COMException e)
                    {
                        myStyle = ExactTranslation;
                    }
                    string rangeText = r.Text;
                    object characterunit = Word.WdUnits.wdCharacter;
                    rangeText = r.Text;
                    start = r.Start;
                    end = r.End;
                    intialRange = end - start;
                    char[] rangeCharArray = r.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length -1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        r.MoveEnd(ref characterunit, -1);
                    }
                    r.InsertBefore(" _: ");
                    r.InsertAfter(" _%");

                    r.set_Style(myStyle);
                    r.MoveStart(ref characterunit, 4);
                    r.MoveEnd(ref characterunit, -3);
                    r.set_Style(MainRibbon.EquationCommentaryStyle);
                    //rangeText = r.Text;
                    //movecount = intialRange + 3;
                    //start = r.Start;
                    //int move = r.MoveStart(ref characterunit, -movecount);
                    //rangeText = r.Text;
                    //start = r.Start;
                                        
                }
                
           }

        }
        [SuiteDescription("Add note reference indicators", optional1: true, optional2: true)]
        public static void AddNoteReferenceIndicators(SuiteFunctionUpdateArgs args){
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            object characterunit = Word.WdUnits.wdCharacter;
            object FootnoteReference = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.FootnoteReference];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: FootnoteReference).ToList()){
                if (r.Characters.Count > 0){
                    char[] r_Array= r.Text.ToCharArray();
                    Char lastChar = r_Array[r_Array.Length -1];
                    if (lastChar.Equals((char)0x2029) || lastChar.Equals('\r') || lastChar.Equals('\n')){
                        r.MoveEnd(ref characterunit, -1);
                    }
                    char[] r_reverse = (char[])r.Text.ToCharArray().Clone();
                    Array.Reverse(r_reverse);
                    foreach (char character_i in r_Array){
                        if (Char.IsWhiteSpace(character_i)){
                            r.MoveStart(ref characterunit, 1);
                        }
                        else{
                            break;
                        }
                    }
                    foreach (char character_j in r_reverse){
                        if (Char.IsWhiteSpace(character_j)){
                            r.MoveEnd(ref characterunit, -1);
                        }
                        else{
                            break;
                        }
                    }
                    //The fresh range after processing for paragraph end and starting and trailing whitespaces
                    char[] r_characterarray = r.Text.ToCharArray();
                    if (Char.IsNumber(r_characterarray[0])){
                        r.InsertBefore(";9#");
                    }
                    else if(Char.IsLetter(r_characterarray[0])){
                        r.InsertBefore(";9");
                    }
                    else{
                        r.InsertBefore(";9");
                    }
                    Word.Style myStyle;

                    try{
                        myStyle = doc.Styles["ExactTranslation"];
                    }
                    catch (COMException e){
                        myStyle = ExactTranslation;
                    }
                    r.set_Style(myStyle);
                    if (Char.IsNumber(r_characterarray[0])){
                        r.MoveStart(ref characterunit,3);
                    }
                    else if (Char.IsLetter(r_characterarray[0])){
                        r.MoveStart(ref characterunit,2);
                    }
                    else{
                        r.MoveStart(ref characterunit,2);
                    }
                    r.set_Style(MainRibbon.FootnoteReference);
                }
            }
        }
        /// <summary>
        /// Converts Page Number Style to RePageNumber, for use in Duxbury Braille translator
        /// </summary>
        [SuiteDescription("Convert page numbers to Duxbury page style", optional1: false, optional2: true)]
        public static void ConvertPageNumberstoDuxburyPageStyle(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            // foreach (Word.Style style in doc.Styles)
            object wdStylePageNumber = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: wdStylePageNumber).ToList())
            {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    try
                    {
                        myStyle = doc.Styles["RefPageNumber"];
                    }
                    catch (COMException e)
                    {
                        myStyle = RefPageNumber;
                    }
                    
                    string rangeText = r.Text;

                    r.set_Style(myStyle);
            }
        }

        /// <summary>
        /// Converts Page Number Style to RePageNumber, for use in Duxbury Braille translator
        /// </summary>
        [SuiteDescription("Convert page numbers to Duxbury Nemeth page style", optional1: true, optional2: false)]
        public static void ConvertPageNumberstoDuxburyPageNemethStyle(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            // foreach (Word.Style style in doc.Styles)
            object wdStylePageNumber = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: wdStylePageNumber).ToList())
            {
                int end = new int();
                int start = new int();
                //int movecount = new int();
                int intialRange = new int();
                Word.Style myStyle;

                try
                {
                    myStyle = doc.Styles["RefPageNemeth"];
                }
                catch (COMException e)
                {
                    myStyle = RefPageNemeth;
                }

                string rangeText = r.Text;

                r.set_Style(myStyle);
            }
        }

        [SuiteDescription("Remove Page Number style (keep text)", optional1: true, optional2: true)]
        public static void RemovePageNumberStyleKeepText(SuiteFunctionUpdateArgs args)
        {
            General.RemovePageNumberKeepText();
        }

        /// <summary>
        /// Converts Box Indicators to Duxbury friendly format
        /// </summary>
        [SuiteDescription("Convert box indicators to duxbury friendly format", optional1: false, optional2: false)]
        public static void ConvertBoxIndicatorstoDuxbury(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            ConvertBeginBoxIndicatorstoDuxbury(doc,range);

        }
        public static void ConvertBeginBoxIndicatorstoDuxbury(Word.Document doc,Word.Range range)
        {
            string strUniqueString = "{MWr)-Er8>t`Dp%";
            // Adding a paragraph with a single whitespace before a table starts. This whitespace is later removed when the process completes.
            // This (weird) solution helps in identifying all the [Begin Box] styled markers in the document.
            foreach (Word.Table tbl in Globals.ThisAddIn.Application.ActiveDocument.Tables)
                new MainRibbon().AddAndStyleTextBeforeSelection(tbl.Range.Start, tbl.Range.End, doc.Styles["Normal"], strUniqueString);
            
            // foreach (Word.Style style in doc.Styles)
            object BoxBegin = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxBegin];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: BoxBegin).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    myStyle = doc.Styles["BoxBegin"];
                    myStyle.Font.Name = "SimBraille";
                    //string rangeText = r.Text;
                    object characterunit = Word.WdUnits.wdCharacter;
                    char[] rangeCharArray = r.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        r.MoveEnd(ref characterunit, -1);
                    }

                    r.Text = "7777777777777777777777777777777777777777";
                    r.set_Style(myStyle);
                }
            }
            object BoxEnd = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxEnd];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: BoxEnd).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    myStyle = doc.Styles["BoxEnd"];
                    myStyle.Font.Name = "SimBraille";
                    //string rangeText = r.Text;
                    object characterunit = Word.WdUnits.wdCharacter;
                    char[] rangeCharArray = r.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        r.MoveEnd(ref characterunit, -1);
                    }

                    r.Text = "gggggggggggggggggggggggggggggggggggggggg";
                    r.set_Style(myStyle);
                }
            }
            object BoxDouble = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.BoxDouble];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: BoxDouble).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    myStyle = doc.Styles["BoxDouble"];
                    myStyle.Font.Name = "SimBraille";
                    //string rangeText = r.Text;
                    object characterunit = Word.WdUnits.wdCharacter;
                    char[] rangeCharArray = r.Text.ToCharArray();
                    char charLast = rangeCharArray[rangeCharArray.Length - 1];
                    if (charLast.Equals((char)0x2029) || charLast.Equals('\r') || charLast.Equals('\n'))
                    {
                        r.MoveEnd(ref characterunit, -1);
                    }

                    r.Text = "========================================";
                    r.set_Style(myStyle);
                }
            }
            //Removing the unique string and extra paragraphs introduced in the process.
            Globals.ThisAddIn.ReplaceAll(Globals.ThisAddIn.GetActiveDocument().Content, strUniqueString, "");
            ProTools.RemoveEmptyParagraphsProTools(new SuiteFunctionUpdateArgs());
        }
        /// <summary>
        /// Converts Transcriber's Note to Duxbury BANA UEB template, for use in Duxbury Braille translator
        /// </summary>
        [SuiteDescription("Convert Transcriber's note to BANA template style", optional1: false, optional2: true)]
        public static void ConvertTNtoDuxburyTN(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            // foreach (Word.Style style in doc.Styles)
            object TN = Globals.ThisAddIn.GetActiveDocument().Styles["Transcriber's Note"];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: TN).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    try
                    {
                        myStyle = doc.Styles["TranscriberNote"];
                    }
                    catch (COMException e)
                    {
                        myStyle = TranscriberNote;
                    }

                    string rangeText = r.Text;

                    r.set_Style(myStyle);
                }
            }
        }
        /// <summary>
        /// Converts Transcriber's Note to Duxbury Nemeth BANA UEB with Nemeth template, for use in Duxbury Braille translator
        /// </summary>
        [SuiteDescription("Convert Transcriber's note to BANA with Nemeth template style", optional1: true, optional2: false)]
        public static void ConvertTNtoDuxburyTNNemeth(SuiteFunctionUpdateArgs args)
        {
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            // foreach (Word.Style style in doc.Styles)
            object TN = Globals.ThisAddIn.GetActiveDocument().Styles["Transcriber's Note"];
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: TN).ToList())
            {
                if (r.Characters.Count > 1)
                {
                    int end = new int();
                    int start = new int();
                    //int movecount = new int();
                    int intialRange = new int();
                    Word.Style myStyle;

                    try
                    {
                        myStyle = doc.Styles["TN-Nemeth"];
                    }
                    catch (COMException e)
                    {
                        myStyle = TranscriberNote;
                    }

                    string rangeText = r.Text;

                    r.set_Style(myStyle);
                }
            }
        }

        /// <summary>
        /// Converts all the lists in the document into their text equivalent and then applies 
        /// proper indentation style to each item in the list, depending on the item level.
        /// </summary>
        [SuiteDescription("Convert auto-lists to Duxbury friendly format", optional1: true, optional2: true)]
        public static void ConvertListsToTextAndApplyIndents(SuiteFunctionUpdateArgs args)
        {
            #region Field Declarations
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            Word.Range temp = Globals.ThisAddIn.Application.Selection.Range;
            string levelText = "";
            int maxLevel;
            int currentLevel;
            bool bulletListFlag = false;
            int currentListCount = 0;
            int totalListCount = Globals.ThisAddIn.Application.ActiveDocument.Lists.Count;
            #endregion

            //Looping through all the lists in the document
            foreach (Word.List list in Globals.ThisAddIn.Application.ActiveDocument.Lists)
            {

                if (args.IsCanceled())
                    break;

                //Check if the list is purely a numbered list.
                bulletListFlag = list.Range.ListFormat.ListType != Word.WdListType.wdListListNumOnly ? true : false;
                maxLevel = 0;
                currentLevel = 0;

                //find the maximum item level in the current list.
                foreach (Word.Paragraph listPara in list.ListParagraphs)
                {
                    listPara.Range.Select();
                    if (maxLevel < Globals.ThisAddIn.Application.Selection.Range.ListFormat.ListLevelNumber)
                        maxLevel = Globals.ThisAddIn.Application.Selection.Range.ListFormat.ListLevelNumber;
                }

                maxLevel = 2 * maxLevel + 1;

                list.Range.Select();

                //Looping through all the list paragraphs in the current list.
                foreach (Word.Paragraph listPara in list.ListParagraphs)
                {
                    listPara.Range.Select();
                    int listParaStart = listPara.Range.Start;
                    int listParaEnd = listPara.Range.End;
                    temp.SetRange(Globals.ThisAddIn.Application.Selection.Range.Start, Globals.ThisAddIn.Application.Selection.Range.End);
                    temp.Copy();
                    
                    //Known issue: Even when the background color is set for all the styles, the text don't seem to have any background color after the style is applied. 
                    //Hence, we select the list item and manually apply a background color. 
                    //This is done to give a visual confirmation to the user that the list was successfully converted to its text format.
                    Globals.ThisAddIn.Application.Selection.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 214, 227, 188));

                    levelText = Globals.ThisAddIn.Application.Selection.Range.ListFormat.ListString;
                    currentLevel = 2 * Globals.ThisAddIn.Application.Selection.Range.ListFormat.ListLevelNumber - 1;

                    //Exception code for level text when its a bullet list.
                    if (bulletListFlag)
                    {
                        char[] array = levelText.ToCharArray();
                        if (array[0] == 61623) //Round bullet point
                            levelText = "\u2022";
                        else if (array[0] == 61607) //Small square bullet point
                            levelText = "\u25AA";
                    }

                    listPara.Range.Delete();
                    temp.Paste();
                    temp.SetRange(listParaStart, listParaStart);
                    temp.Text = levelText + " ";

                    new MainRibbon().DynamicTextSelection(listParaStart, listParaEnd);
                    Globals.ThisAddIn.Application.Selection.Range.set_Style(Word.WdBuiltinStyle.wdStyleNormal);

                    //Apply appropriate indentation style.
                    Globals.ThisAddIn.Application.Selection.Range.set_Style(MainRibbon.ApplyIndentationStyle(currentLevel + "-" + maxLevel));
                }
                args.UpdatePercentage(++currentListCount, totalListCount);
            }
        }

        /// [Begin Listed Suite]
        /// <summary>
        /// First function (AddListedStyleAllTables) loops through all the tables in the document, and calls the second helper function (ListedTableCode),
        /// which adds the listed table text and the end text.
        /// </summary>
        /// <param name="args"></param>
        [SuiteDescription("Add listed style to each table", optional1: true, optional2: true)]
        public static void AddListedStyleAllTables(SuiteFunctionUpdateArgs args)
        {
            int currentTableCount = 0;
            foreach (Word.Table table in Globals.ThisAddIn.GetActiveDocument().Tables)
            {
                currentTableCount++;
                table.Select();
                ListedTableCode();
                args.UpdatePercentage(currentTableCount, Globals.ThisAddIn.GetActiveDocument().Tables.Count);
            }
        
        }

        public static void ListedTableCode()
        {
            #region Field Declarations
                Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
                string strListedText = "[[*htbs;r:0:0:b:n:l*]]";
                string strEndText = "[[*htbe*]][[*sk1*]]";
                object ListedStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.DBTCode];
                object EndStyle = Globals.ThisAddIn.GetActiveDocument().Styles[MainRibbon.DBTCode]; ; 
            #endregion

            //Exiting the code if nothing is selected in the document.
            if (selection.Start == selection.End)
                return;

            new MainRibbon().AddAndStyleTextBeforeSelection(selection.Start, selection.End, ListedStyle, strListedText);
            new MainRibbon().AddAndStyleTextAfterSelection(selection.Start, selection.End, EndStyle, strEndText);
        }
        /// [End Listed Suite]

        public static Word.Style ExactTranslation
        {
            get
            {
                string styleName = "ExactTranslation";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    myStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeCharacter);
                newStyle.Font.Name = "SimBraille";
                newStyle.Font.Size = 14;
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 215, 228, 248));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_NextParagraphStyle(Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the RefPageNumber in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style RefPageNumber
        {
            get
            {
                string styleName = "RefPageNumber";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = MainRibbon.BrailleBase;
                }
                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Size = 14;
                newStyle.Font.ColorIndex = Word.WdColorIndex.wdViolet;
                newStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdWhite;
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                newStyle.set_BaseStyle(mybaseStyle);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the RefPageNemeth in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style RefPageNemeth
        {
            get
            {
                string styleName = "RefPageNemeth";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = MainRibbon.BrailleBase;
                }
                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Size = 14;
                newStyle.Font.ColorIndex = Word.WdColorIndex.wdViolet;
                newStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdWhite;
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                newStyle.set_BaseStyle(mybaseStyle);
                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the TranscriberNote in the
        /// Duxbury Ready suite. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style TranscriberNote
        {
            get
            {
                string styleName = "TranscriberNote";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = MainRibbon.BrailleBase;
                }
                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Size = 14;
                newStyle.Font.ColorIndex = Word.WdColorIndex.wdDarkBlue;
                newStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdWhite;
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_BaseStyle(mybaseStyle);
                //newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;

                return newStyle;
            }
        }
        /// <summary>
        /// Gets the style for the TN-Nemeth for Nemeth in the
        /// Duxbury Ready suite. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style TNNemeth
        {
            get
            {
                string styleName = "TN-Nemeth";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = MainRibbon.BrailleBase;
                }
                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Size = 14;
                newStyle.Font.ColorIndex = Word.WdColorIndex.wdViolet;
                newStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdWhite;
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_BaseStyle(mybaseStyle);
                //newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;

                return newStyle;
            }
        }

        public static Word.Style LargePrintPageNumber
        {
            get
            {
                string styleName = "LargePrintPageNumber";

                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }
                Word.Style mybaseStyle;
                try
                {
                    string baseStyleName = "Normal";
                    mybaseStyle = doc.Styles[baseStyleName];
                }
                catch (COMException e)
                {
                    mybaseStyle = MainRibbon.BrailleBase;
                }
                // If I didn't get one, create a new one and modify it
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Size = 14;
                newStyle.Font.ColorIndex = Word.WdColorIndex.wdWhite;
                newStyle.Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdBlack;
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                newStyle.set_BaseStyle(mybaseStyle);
                //newStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;

                return newStyle;
            }
        }
        #region Content Control Creation

        private static int CURRENT_CONTROL_ID = 0;

        public static Word.Range CreateTextControl(Word.Range range, string placeholderText)
        {
            CURRENT_CONTROL_ID += 1;

            Document doc = Globals.Factory.GetVstoObject(Globals.ThisAddIn.GetActiveDocument());

            PlainTextContentControl control = doc.Controls.AddPlainTextContentControl(range, "control" + CURRENT_CONTROL_ID.ToString());
            control.PlaceholderText = placeholderText;

            return control.Range;
        }

        #endregion
    }
}
