﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class ImageAltTextEditor : Form
    {
        public ImageAltTextEditor()
        {
            InitializeComponent();
            Control.OnRequestClose += Control_OnRequestClose;
        }

        void Control_OnRequestClose()
        {
            this.Close();
        }

        public ImageAltTextEditorControl Control
        {
            get
            {
                return (ImageAltTextEditorControl)elementHost1.Child;
            }
        }
    }
}
