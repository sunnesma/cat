﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class TranscribersNoteForm : Form
    {
        public TranscribersNoteForm()
        {
            InitializeComponent();
        }

        private void TranscribersNoteForm_Load(object sender, EventArgs e)
        {
            transcribersNoteControl1.OnRequestClose += transcribersNoteControl1_OnRequestClose;
        }

        void transcribersNoteControl1_OnRequestClose()
        {
            this.Close();
        }        
    }
}
