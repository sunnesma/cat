﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class CheckerForm : UserControl
    {
        public CheckerForm()
        {
            InitializeComponent();
        }

        public List<CheckerItem> CheckerItems
        {
            get
            {
                return Control.CheckerItems;
            }
            set
            {
                Control.CheckerItems = value;
            }
        }

        private CheckerControl Control
        {
            get
            {
                return (CheckerControl)elementHost1.Child;
            }
        }

        private void CheckerForm_Load(object sender, EventArgs e)
        {
            Control.OnShowFixerForm += Control_OnShowFixer;
        }

        void Control_OnShowFixer(Form f)
        {
            // Show the fixer and hide this window
            //f.FormClosed += Fixer_FormClosed;
            //this.WindowState = FormWindowState.Minimized;
            Globals.ThisAddIn.ShowForm(f);
        }

        void Fixer_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Make this pop back up when the fixer form is closed
            //Globals.ThisAddIn.ShowForm(this);
            Control.RefreshItems();
        }
    }
}
