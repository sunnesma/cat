﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    class CharacterAccuracyChecker
    {

        public static UserControl ShowCharacterAccuracyChecker()
        {
            CharacterAccuracyCheckerForm f = new CharacterAccuracyCheckerForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Character Accuracy Checker", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
        }

        [CheckerDescription("Character Accuracy Checker")]        
        public static CheckerResult CheckGoodFormatting()
        {
            if (new CharacterAccuracyCheckerControl().GetRemainingSuspectsCount() > 0)
                return CheckerResult.Failed;
            else
                return CheckerResult.Passed;
        }
    }
}
