﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class ChartConverterWizardForm : UserControl
    {
        public ChartConverterWizardForm()
        {
            InitializeComponent();
        }

        private ChartConverterWizardControl Control
        {
            get
            {
                return (ChartConverterWizardControl)elementHost1.Child;
            }
        }

        private void ChartConverterWizardForm_Load(object sender, EventArgs e)
        {
            Control.OnRequestClose += Control_OnRequestClose;
            Control.OnRequestResize += Control_OnRequestResize;
        }

        void Control_OnRequestResize(int width, int height)
        {
            this.Width = width;
            this.Height = height;

            // Reposition it correctly
            //Globals.ThisAddIn.ShowForm(this);
        }

        void Control_OnRequestClose()
        {
            //this.Close();
        }
    }
}
