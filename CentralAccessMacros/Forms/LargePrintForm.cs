﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class LargePrintForm : Form
    {
        public LargePrintForm()
        {
            InitializeComponent();
        }

        private LargePrintControl Control
        {
            get
            {
                return largePrintControl1;
            }
        }

        private void LargePrintForm_Load(object sender, EventArgs e)
        {
            Control.OnRequestClose += Control_OnRequestClose;
        }

        void Control_OnRequestClose()
        {
            this.Close();
        }
    }
}
