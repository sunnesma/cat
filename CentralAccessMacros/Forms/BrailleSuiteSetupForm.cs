﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class BrailleSuiteSetupForm : Form
    {

        public BrailleSuiteSetupForm()
        {
            InitializeComponent();

            Control.RunButton.Click += RunButton_Click;
        }

        public string SuiteName
        {
            get
            {
                //return Control.SuiteName;
                return this.Text;
            }

            set
            {
                //Control.SuiteName = value;
                this.Text = value;
            }
        }

        void RunButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Start the progress form and close this one
            SuiteProgressForm form = new SuiteProgressForm(Control.GetAcceptedFunctions());
            Globals.ThisAddIn.ShowForm(form);

            this.Close();
        }

        public void LoadFunctions(List<SuiteFunction> functions)
        {
            Control.LoadFunctions(functions);
        }

        public List<SuiteFunction> GetAcceptedFunctions()
        {
            return Control.GetAcceptedFunctions();
        }

        private BrailleSuiteSetupControl Control
        {
            get
            {
                return (BrailleSuiteSetupControl)elementHost1.Child;
            }
        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }
    }
}
