﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace CentralAccessMacros
{
    public partial class ParagraphFixerForm : Form
    {
        public ParagraphFixerForm()
        {
            InitializeComponent();
            Control.OnRequestClose += Control_OnRequestClose;
        }

        void Control_OnRequestClose()
        {
            this.Close();
        }

        private ParagraphFixerControl Control
        {
            get
            {
                return (ParagraphFixerControl)elementHost1.Child;
            }
        }

        private void ParagraphFixerForm_Load(object sender, EventArgs e)
        {
            // Set selection to beginning
            Word.Range r = Globals.ThisAddIn.GetActiveDocument().Content;
            r.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            r.Select();

            Control.SearchForNext(null);
        }
    }
}
