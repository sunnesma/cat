﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class SuiteProgressForm : Form
    {
        public SuiteProgressForm(List<SuiteFunction> f)
        {
            InitializeComponent();

            Control.OnRequestClose += Control_OnRequestClose;

            Control.LoadFunctions(f);
            Control.RunFunctions();
        }

        void Control_OnRequestClose()
        {
            this.Close();
        }

        private SuiteProgressControl Control
        {
            get
            {
                return (SuiteProgressControl)elementHost1.Child;
            }
        }
    }
}
