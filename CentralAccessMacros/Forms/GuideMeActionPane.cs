﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    public partial class GuideMeActionPane : UserControl
    {
        public GuideMeActionPane()
        {
            InitializeComponent();
        }        

        public GuideMeControl HostedControl
        {
            get
            {
                return (GuideMeControl)elementHost1.Child;
            }
        }
    }
}
