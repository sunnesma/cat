﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Forms;

namespace CentralAccessMacros
{
    /// <summary>
    /// Custom sorting for the list of footnotes, based on their text value.
    /// </summary>
    class FootNoteSortByTextAscending : IComparer<FootNoteHunter>
    {
        public int Compare(FootNoteHunter a, FootNoteHunter b)
        {
            int n;
            if (int.TryParse(a.strText, out n) && int.TryParse(b.strText, out n))
                return (Int32.Parse(a.strText) - Int32.Parse(b.strText)) != 0 ? Int32.Parse(a.strText) - Int32.Parse(b.strText) : a.intStartRange - b.intStartRange;
            else if (int.TryParse(a.strText, out n))
                return 1;
            else if (int.TryParse(b.strText, out n))
                return -1;
            else
                return a.strText.CompareTo(b.strText);
        }
    }

    /// <summary>
    /// Custom sorting for the list of footnotes, based on their location in the document.
    /// </summary>
    class FootNoteSortByRangeAscending : IComparer<FootNoteHunter>
    {
        public int Compare(FootNoteHunter a, FootNoteHunter b)
        {
            return a.intStartRange - b.intStartRange;
        }
    }

    class FootNoteHunter
    {
        public Word.Range rngLocation { get; set; }
        public string strText { get; set; }
        public int intStartRange { get; set; }
        public string strColor { get; set; }
        public static string strFootNoteSymbols = "†‡§*";

        public static Form ShowFootNoteHunter()
        {
            FootnoteHunterForm form = new FootnoteHunterForm();
            return Globals.ThisAddIn.ShowForm(form);
        }

        public FootNoteHunter(Word.Range rngLocation, string strFootNoteText, int intStartRange)
        {
            this.rngLocation = rngLocation;
            this.strText = strFootNoteText;
            this.intStartRange = intStartRange;
            this.strColor = "Transparent";
        }


        public static List<FootNoteHunter> GetAllFootNoteSuspects(int intDigitLimit = 2)
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            int intStart = 0;
            int intEnd = 0;

            List<FootNoteHunter> lstSuspects = new List<FootNoteHunter>();
            string strRegexFootnoteSymbol = "[0-9" + strFootNoteSymbols + "]{1,}";

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, strRegexFootnoteSymbol, useWildcards: true).ToList())
            {
                if (r.Start >= intStart && r.End <= intEnd)
                    continue;

                intStart = r.Start;
                intEnd = r.End;

                selection = r;

                //Exception code to handle suspects with period in between.
                if (selection.Text.Contains("."))
                {
                    while (selection.Start != intEnd)
                    {
                        string text = selection.Text;
                        int indexOfPeriod = text.IndexOf('.');
                        int tempStart = selection.Start;

                        if (selection.Text.Contains("."))
                            selection.SetRange(tempStart, tempStart + indexOfPeriod);

                        if (selection.Text != null && selection.Text.Trim() != "" && selection.Text != "\r")
                        {
                            lstSuspects.Add(new FootNoteHunter(selection.Duplicate, selection.Text.ToString().Trim(), selection.Start));
                        }

                        selection.SetRange(selection.End + 1, intEnd);
                    }
                    continue;
                }
                lstSuspects.Add(new FootNoteHunter(selection, selection.Text.ToString().Trim(), selection.Start));
            }

            foreach (FootNoteHunter item in lstSuspects.ToList())
            {
                // Remove numbers greater than 999. These usually represent years.
                if (item.strText.Length > intDigitLimit)
                {
                    lstSuspects.Remove(item);
                    continue;
                }
                // Remove if the suspect is one of the known false flags
                if (GetListOfFalseFlags().Contains(item.strText))
                {
                    lstSuspects.Remove(item);
                    continue;
                }
                // Remove if the suspect is a Page Number or an already marked FootnoteReference
                if (MainRibbon.CheckStylePerCharacter(item.rngLocation.Start, item.rngLocation.End, "Page Number", "FootnoteReference"))
                {
                    lstSuspects.Remove(item);
                    continue;
                }
                // Remove if the number is a start of a paragraph. These are usually lists in text format, instead of Word's List format.
                if (item.rngLocation.Start != 0 && Globals.ThisAddIn.Application.ActiveDocument.Range(item.rngLocation.Start - 1, item.rngLocation.Start).Text == "\r")
                {
                    lstSuspects.Remove(item);
                    continue;
                }
            }
            return lstSuspects;
        }

        /// <summary>
        /// List of all items to be excluded while searching for a footnote.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListOfFalseFlags()
        {
            List<string> falseFlags = new List<string>();
            falseFlags.Add("");
            falseFlags.Add("\r");
            falseFlags.Add(".");
            falseFlags.Add("..");
            falseFlags.Add("...");
            falseFlags.Add("....");
            falseFlags.Add("0");
            falseFlags.Add("00");
            falseFlags.Add("000");

            return falseFlags;
        }

        /// <summary>
        /// Returns the list of already styled footnotes, found in the document.
        /// </summary>
        /// <returns></returns>
        public static List<FootNoteHunter> GetAllFootNotes()
        {
            Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
            Word.Range selection = Globals.ThisAddIn.Application.Selection.Range;
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

            List<FootNoteHunter> lstFootnotes = new List<FootNoteHunter>();

            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: MainRibbon.FootnoteReference))
                lstFootnotes.Add(new FootNoteHunter(r, r.Text.ToString().Trim(), r.Start));

            return lstFootnotes;
        }
    }
}
