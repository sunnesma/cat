﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Drawing;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    /// <summary>
    /// Provides functions for obtaining and checking page numbers.
    /// </summary>
    class PageNumbers
    {
        /// <summary>
        /// Gets the style for the Poetry 4 style with indentation in the
        /// Advance Edit part. Creates a new one if it doesn't exist in the
        /// document.
        /// </summary>
        public static Word.Style StartOfDocument
        {
            get
            {
                string styleName = "StartOfDocument";
                Word.Document doc = Globals.ThisAddIn.GetActiveDocument();

                // Grab it if already in there
                try
                {
                    Word.Style myStyle = doc.Styles[styleName];
                    return myStyle;
                }
                catch (COMException e)
                {
                    // Couldn't get the style, which means I must make one
                }

                // If I didn't get one, create a new one and modify it
                // Grab it if already in there
                Word.Style newStyle = doc.Styles.Add(styleName, Word.WdStyleType.wdStyleTypeParagraph);
                newStyle.Font.Shading.BackgroundPatternColor = (Microsoft.Office.Interop.Word.WdColor)ColorTranslator.ToOle(Color.FromArgb(0, 229, 223, 236));
                newStyle.Font.Shading.Texture = Word.WdTextureIndex.wdTextureNone;
                return newStyle;
            }
        }
        /// <summary>
        /// Goes through the document and finds numbers that are in their own
        /// line. It will then format it into a page number.
        /// </summary>
        /// 


        [SuiteDescription("Style all page numbers")]
        public static void StylePageNumbers(SuiteFunctionUpdateArgs args)
        {
            General.SetPageStyleToLookLikeDAISY(args);
            object characterwd = Word.WdUnits.wdCharacter;
            Word.Document doc = Globals.ThisAddIn.GetActiveDocument();
            Word.Range range = doc.Content;
            range.InsertBefore("\r\nStartOfDocument\r\n");
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"^013StartOfDocument^013", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {
                r.set_Style(StartOfDocument);
            }

            // Find roman numeral page numbers and apply the page number style
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"^013[a-zA-Z]{1,}^013", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {
                System.Text.RegularExpressions.Regex rgxRomanNumerals = new System.Text.RegularExpressions.Regex("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                //Debug.Print("Range : {0}",r.Text);
                if (args.IsCanceled())
                    break;

                if (rgxRomanNumerals.IsMatch(r.Text.Replace("\r","")))
                {
                    args.UpdateRangePercentage(r, range);
                    
                    r.MoveStart(ref characterwd, 1);
                    // Check that the range of the selection matches with the range
                    // of the paragraph it is in.
                    Debug.Print("My Range: {0}\nParagraph Range: {1}", r.Text, r.Paragraphs.First.Range.Text);
                    if (r.IsEqual(r.Paragraphs.First.Range))
                    {
                        General.FormatCharacterStyleLikeParagraph(r, Word.WdBuiltinStyle.wdStylePageNumber, selectRange: false);
                    }
                }
            }
            
            // Find numeric page numbers and apply the page number style
            foreach (Word.Range r in Globals.ThisAddIn.FindWildCard(range, @"^013[0-9]{1,}^013", useWildcards: true, wrapSearch: Word.WdFindWrap.wdFindStop).ToList())
            {
                //Debug.Print("Range : {0}",r.Text);
                if (args.IsCanceled())
                    break;

                args.UpdateRangePercentage(r, range);
                
                r.MoveStart(ref characterwd, 1);
                // Check that the range of the selection matches with the range
                // of the paragraph it is in.
                Debug.Print("My Range: {0}\nParagraph Range: {1}", r.Text, r.Paragraphs.First.Range.Text);
                if (r.IsEqual(r.Paragraphs.First.Range))
                {
                    General.FormatCharacterStyleLikeParagraph(r, Word.WdBuiltinStyle.wdStylePageNumber, selectRange: false);
                }
            }
            foreach (Word.Range r in Globals.ThisAddIn.Find(range, "", style: StartOfDocument))
            {
                r.Delete();                
            }
            doc.Styles[StartOfDocument].Delete();
        }

        [CheckerDescription("Page numbers correct")]
        public static CheckerResult CheckPageNumbers()
        {
            List<PageNumberReport> reports = PageNumbers.GetPageNumberList();

            if (reports.Count == 0)
                return CheckerResult.Empty;

            foreach (PageNumberReport r in reports)
            {
                if (!r.IsGood)
                    return CheckerResult.Failed;
            }
            return CheckerResult.Passed;
        }

        public static List<PageNumberReport> GetPageNumberList()
        {
            /*
             * Rules for correct page numbers:
             * 1. The page numbers are roman numerals or normal digits.
             * 2. Lowercase Roman characters go before arabic numerals.
             * 3. If same style, later must be sequentially higher than
             *    previous.
             */

            // Precedence determines what kinds of numbers go before what
            int lastPrecedence = -1;
            int currPrecedence = 0;

            // Value is the numeric value of whatever the number was. This
            // makes sure that I can treat all kinds of numbers similarly.
            int lastValue = -1;
            int currValue = 0;

            List<PageNumberReport> reports = new List<PageNumberReport>();

            // Run the search
            Word.Range content = Globals.ThisAddIn.GetActiveDocument().Content;
            object pageNumberStyle = Globals.ThisAddIn.GetActiveDocument().Styles[Word.WdBuiltinStyle.wdStylePageNumber];
            int lastEnd = -1;
            foreach (Word.Range r in Globals.ThisAddIn.Find(content, "", style: pageNumberStyle))
            {
                if (r.Start != lastEnd)
                {
                    string pageNumber = r.Text.Trim();

                    // Parse the page number
                    if (IsRumanNumerals(pageNumber.ToUpper()))
                    {
                        currPrecedence = 0;
                        currValue = ConvertRomanNumeralToInt(pageNumber.ToUpper());
                    }
                    else if (int.TryParse(pageNumber, out currValue))
                    {
                        currPrecedence = 1;
                    }
                    else
                    {
                        // Flag with -2 that it didn't parse correctly
                        currPrecedence = -2;
                        currValue = -2;
                    }

                    // Check sequential order
                    bool isBad = currPrecedence == -2;
                    isBad = isBad || (currPrecedence < lastPrecedence);
                    if ((lastValue > -1) && (lastPrecedence > -1))
                    {
                        isBad = isBad || ((currPrecedence == lastPrecedence) && (currValue != (lastValue + 1)));
                    }

                    reports.Add(new PageNumberReport(r, !isBad));

                    lastPrecedence = currPrecedence;
                    lastValue = currValue;
                }

                lastEnd = r.End;
            }

            return reports;
        }

        public static UserControl ShowPageCounter()
        {
            PageCounterForm f = new PageCounterForm();
            Globals.ThisAddIn.ShowTaskPane(f, "Page Counter", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
        }

        #region Roman Numerals

        public class RomanNumber
        {
            public string Numeral { get; set; }
            public int Value { get; set; }
            public int Hierarchy { get; set; }
        }

        private static List<RomanNumber> RomanNumbers = new List<RomanNumber>
        {
            new RomanNumber {Numeral = "M", Value = 1000, Hierarchy = 4},
            //{"CM", 900},
            new RomanNumber {Numeral = "D", Value = 500, Hierarchy = 4},
            //{"CD", 400},
            new RomanNumber {Numeral = "C", Value = 100, Hierarchy = 3},
            //{"XC", 90},
            new RomanNumber {Numeral = "L", Value = 50, Hierarchy = 3},
            //{"XL", 40},
            new RomanNumber {Numeral = "X", Value = 10, Hierarchy = 2},
            //{"IX", 9},
            new RomanNumber {Numeral = "V", Value = 5, Hierarchy = 2},
            //{"IV", 4},
            new RomanNumber {Numeral = "I", Value = 1, Hierarchy = 1}
        };

        private static bool IsRumanNumerals(string romanNumeralString)
        {
            foreach (char c in romanNumeralString)
            {
                bool found = false;

                foreach (RomanNumber r in RomanNumbers)
                {
                    if (c == r.Numeral[0])
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    return false;
            }

            return true;
        }

        private static int ConvertRomanNumeralToInt(string romanNumeralString)
        {
            if (romanNumeralString == null) return int.MinValue;

            var total = 0;
            for (var i = 0; i < romanNumeralString.Length; i++)
            {
                // get current value
                var current = romanNumeralString[i].ToString();
                var curRomanNum = RomanNumbers.First(rn => rn.Numeral.ToUpper() == current.ToUpper());

                // last number just add the value and exit
                if (i + 1 == romanNumeralString.Length)
                {
                    total += curRomanNum.Value;
                    break;
                }

                // check for exceptions IV, IX, XL, XC etc
                var next = romanNumeralString[i + 1].ToString();
                var nextRomanNum = RomanNumbers.First(rn => rn.Numeral.ToUpper() == next.ToUpper());

                // exception found
                if (curRomanNum.Hierarchy == (nextRomanNum.Hierarchy - 1))
                {
                    total += nextRomanNum.Value - curRomanNum.Value;
                    i++;
                }
                else
                {
                    total += curRomanNum.Value;
                }
            }

            return total;
        }

        #endregion
    }

    /// <summary>
    /// Class for reporting whether a page number is good. It will also
    /// give the range of where the page number can be found.
    /// </summary>
    public class PageNumberReport
    {
        private bool isGood;
        public bool IsGood
        {
            get
            {
                return isGood;
            }
        }
        private Word.Range range;
        public Word.Range Range
        {
            get
            {
                return range;
            }
        }
        private string pageNumber;
        public string PageNumber
        {
            get
            {
                return pageNumber;
            }
        }

        public PageNumberReport(Word.Range range, bool isGood)
        {
            this.isGood = isGood;
            this.range = range;
            if (range.Text != null)
                this.pageNumber = range.Text.Trim();
            else
                this.pageNumber = "";
        }
      
    }
}
