﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

namespace CentralAccessMacros
{
    class TextBoxShape
    {
        private Word.Shape shape;
        public Word.Shape Shape
        {
            get
            {
                return shape;
            }
        }

        public TextBoxShape(Word.Shape s)
        {
            shape = s;
        }

        public string Text
        {
            get
            {
                return shape.TextFrame.TextRange.Text;
            }
        }
    }


    /// <summary>
    /// Set of functions for checking and finding textboxes.
    /// </summary>
    class TextBoxFinder
    {

        [CheckerDescription("Free of Text Boxes/WordArt")]
        public static CheckerResult CheckForNoTextBoxes()
        {
            if (TextBoxFinder.TextBoxes.Count == 0)
                return CheckerResult.Passed;
            else
                return CheckerResult.Failed;
        }

        public static UserControl ShowTextboxFinder()
        {
            TextBoxFinderForm f = new TextBoxFinderForm();
            Globals.ThisAddIn.ShowTaskPane(f, "TextBox Finder", customWidth: 250, restrictPosition: MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal);
            return null;
            //return Globals.ThisAddIn.ShowForm(f);
        }

        /// <summary>
        /// Property for all textboxes in the document.
        /// </summary>
        public static List<TextBoxShape> TextBoxes
        {
            get
            {
                List<TextBoxShape> items = new List<TextBoxShape>();

                Word.Range range = Globals.ThisAddIn.GetActiveDocument().Content;
                foreach (Word.Shape s in range.ShapeRange)
                {
                    //Debug.Print("Shape type: " + s.Type.ToString());

                    if (s.Type == Microsoft.Office.Core.MsoShapeType.msoTextBox)
                    {
                        items.Add(new TextBoxShape(s));
                    }

                    else if (s.Type == Microsoft.Office.Core.MsoShapeType.msoAutoShape)
                    {
                        items.Add(new TextBoxShape(s));
                    }
                }

                foreach (Word.InlineShape s in range.InlineShapes)
                {
                    //Debug.Print("Shape type: " + s.Type.ToString());
                }

                return items;
            }
        }
    }
}
